# Finds the GNU Scientific Library (GSL).

# This module defines
#  GSL_INCLUDE_DIR, where to find gsl/*.h.
#  GSL_LIBRARIES, libraries to link against to use GSL.
#  GSL_FOUND, If false, do not try to use GSL.
#
#  Also defined, but not for general use are
#  GSL_LIBRARY, where to find the GSL library.
#  GSL_CBLAS_LIBRARY, where to find the CBLAS library.

FIND_PATH(GSL_INCLUDE_DIR gsl/gsl_multifit.h
  /usr/local/include
  /usr/include
  /usr/include/gsl
)

SET(GSL_NAMES ${GSL_NAMES} gsl)
FIND_LIBRARY(GSL_LIBRARY
  NAMES ${GSL_NAMES}
  PATHS /usr/lib /usr/local/lib
)

SET(GSL_CBLAS_NAMES ${GSL_CBLAS_NAMES} gslcblas)
FIND_LIBRARY(GSL_CBLAS_LIBRARY
  NAMES ${GSL_CBLAS_NAMES}
  PATHS /usr/lib /usr/local/lib
)

IF(GSL_INCLUDE_DIR)
  IF(GSL_LIBRARY AND GSL_CBLAS_LIBRARY)
    SET(GSL_FOUND "YES")
    SET(GSL_LIBRARIES ${GSL_LIBRARY} ${GSL_CBLAS_LIBRARY})
  ENDIF(GSL_LIBRARY AND GSL_CBLAS_LIBRARY)
ENDIF(GSL_INCLUDE_DIR)
