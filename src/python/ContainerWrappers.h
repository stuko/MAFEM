///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __PYTHON_CONTAINER_WRAPPERS_H
#define __PYTHON_CONTAINER_WRAPPERS_H

#include "mafem2python.h"
 
namespace Scripting {

using namespace boost::python;

/**
 * \brief Helper class that supports in exporting a read-only Qt QVector container to Python.
 * 
 * This class is the facade class for
 * the management of QVector containers intended to be integrated
 * to Python. The objective is make a QVector container look and
 * feel and behave exactly as we'd expect a Python sequence type.
 */
template<class Container>
class QVector_readonly_indexing_suite : public def_visitor<
	QVector_readonly_indexing_suite<Container> >
{
public:
    typedef typename Container::value_type data_type;
    typedef typename Container::value_type key_type;
    typedef typename Container::size_type index_type;
    typedef typename Container::size_type size_type;
    typedef typename Container::difference_type difference_type;
        
    typedef typename mpl::if_<
    			is_integral<data_type>
              , return_value_policy<return_by_value>
              , return_value_policy<return_by_value> >::type
              return_policy_sub1;

    typedef typename mpl::if_<
    			is_reference<data_type>
              , return_value_policy<copy_const_reference>
              , return_policy_sub1 >::type
              return_policy_sub2;
    
    typedef typename mpl::if_<
    			is_pointer<data_type>
              , return_internal_reference<>
              , return_policy_sub2 >::type
              return_policy;    
              
    typedef boost::python::iterator<Container, return_policy> def_iterator;
    
    template<class Class>
    void visit(Class& cl) const {   
        cl
            .def("__len__", &get_size)
            .def("__setitem__", &set_item)
            .def("__delitem__", &delete_item)
            .def("__getitem__", &get_item_wrapper)
            .def("__contains__", &contains)
            .def("__iter__", def_iterator())
        ;
    }
    
    static size_t get_size(Container& container) {
        return container.size();
    }    
    
    static 
    typename mpl::if_<
        is_class<data_type>
      , data_type&
      , data_type
    >::type
    get_item(Container& container, index_type i) { 
        return container[i];
    }

    template <class DataType> 
    static object get_item_helper(DataType const& p, mpl::true_) { 
        return object(ptr(p));
    }

    template <class DataType> 
    static object get_item_helper(DataType const& x, mpl::false_) { 
        return object(x);
    }
    
    static object get_item_wrapper(back_reference<Container&> container, PyObject* i) { 
        if(PySlice_Check(i)) {
        	PyErr_SetString(PyExc_NotImplementedError, "This sequence type does not support slicing.");
        	throw_error_already_set();
        }
        return get_item_helper(get_item(container.get(), convert_index(container.get(), i)),
        		 is_pointer<data_type>());
    }

    static void set_item(Container& container, PyObject* i, PyObject* v) {
		PyErr_SetString(PyExc_NotImplementedError, "This sequence type is read-only.");
		throw_error_already_set();         
    }
    
	static void delete_item(Container& container, PyObject* i) {         
		PyErr_SetString(PyExc_NotImplementedError, "This sequence type is read-only.");
		throw_error_already_set();         
    }   
    
	template<bool b>
    static bool contains_impl(Container& container, key_type const& key, const integral_constant<bool, b>&) {
		PyErr_SetString(PyExc_NotImplementedError, "This sequence type does not support the contains() method.");
		throw_error_already_set();
		return false;         
	}

    static bool contains_impl(Container& container, key_type const& key, const false_type&) {
		return container.contains(key);
	}

    static bool contains(Container& container, key_type const& key) {
    	return contains_impl(container, key, is_class<key_type>());        
    }
    
    static index_type convert_index(Container& container, PyObject* i_) { 
        extract<long> i(i_);
        if(i.check()) {
            long index = i();
            if (index < 0)
                index += get_size(container);
            if (index >= long(container.size()) || index < 0) {
                PyErr_SetString(PyExc_IndexError, "Index out of range");
                throw_error_already_set();
            }
            return index;
        }
        
        PyErr_SetString(PyExc_TypeError, "Invalid index type");
        throw_error_already_set();
        return index_type();
    }    
};


/**
 * \brief Helper class that supports in exporting a boost::array<> class to Python.
 * 
 * This class is the facade class for
 * the management of boost::array intended to be integrated
 * to Python. The objective is make a boost::array look and
 * feel and behave exactly as we'd expect a Python sequence type.
 */
template<class Container>
class boost_array_indexing_suite : public def_visitor<
	boost_array_indexing_suite<Container> >
{
public:
    typedef typename Container::value_type data_type;
    typedef typename Container::value_type key_type;
    typedef typename Container::size_type index_type;
    typedef typename Container::size_type size_type;
    typedef typename Container::difference_type difference_type;

    typedef typename mpl::if_<
    			is_integral<data_type>
              , return_value_policy<return_by_value>
              , default_call_policies >::type
              return_policy_sub1;
    
    typedef typename mpl::if_<
    			is_pointer<data_type>
              , return_internal_reference<>
              , return_policy_sub1 >::type
              return_policy;
    
    typedef boost::python::iterator<Container, return_policy> def_iterator;
    
    template<class Class>
    void visit(Class& cl) const {   
        cl
            .def("__len__", &get_size)
            .def("__setitem__", &set_item)
            .def("__delitem__", &delete_item)
            .def("__getitem__", &get_item_wrapper)
            .def("__contains__", &contains)
            .def("__iter__", def_iterator())
        ;
    }
    
    static size_t get_size(Container& container) {
        return container.size();
    }    
    
    static 
    typename mpl::if_<
        is_class<data_type>
      , data_type&
      , data_type
    >::type
    get_item(Container& container, index_type i) { 
        return container[i];
    }

    template <class DataType> 
    static object get_item_helper(DataType const& p, mpl::true_) { 
        return object(ptr(p));
    }

    template <class DataType> 
    static object get_item_helper(DataType const& x, mpl::false_) { 
        return object(x);
    }
    
    static object get_item_wrapper(back_reference<Container&> container, PyObject* i) { 
        if(PySlice_Check(i)) {
        	PyErr_SetString(PyExc_NotImplementedError, "This sequence type does not support slicing.");
        	throw_error_already_set();
        }
        return get_item_helper(get_item(container.get(), convert_index(container.get(), i)),
        		 is_pointer<data_type>());
    }

    static void set_item(Container& container, PyObject* i, PyObject* v) {
    	extract<data_type> value(v);
        if(value.check()) {
            container[convert_index(container, i)] = value;
            return;
        }
        PyErr_SetString(PyExc_TypeError, "Invalid value type");
        throw_error_already_set();
    }
    
	static void delete_item(Container& container, PyObject* i) {         
		PyErr_SetString(PyExc_NotImplementedError, "This sequence type is not resizable.");
		throw_error_already_set();         
    }   
    
    static bool contains(const Container& container, key_type const& key) {
    	return std::find(container.begin(), container.end(), key) != container.end();        
    }
    
    static index_type convert_index(Container& container, PyObject* i_) { 
        extract<long> i(i_);
        if(i.check()) {
            long index = i();
            if (index < 0)
                index += get_size(container);
            if (index >= long(container.size()) || index < 0) {
                PyErr_SetString(PyExc_IndexError, "Index out of range");
                throw_error_already_set();
            }
            return index;
        }
        
        PyErr_SetString(PyExc_TypeError, "Invalid index type");
        throw_error_already_set();
        return index_type();
    }    
};

};

#endif // __PYTHON_CONTAINER_WRAPPERS_H
