///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __SCRIPTING_INTERFACE_H
#define __SCRIPTING_INTERFACE_H

/// Intel compiler compatibility work-around.
#ifdef __ICC
	// Replacement of unknown atomic function.
	#define __sync_fetch_and_add(ptr,addend) _InterlockedExchangeAdd(const_cast<void*>(reinterpret_cast<volatile void*>(ptr)), addend);
#endif

// Microsoft VC 8 specifc
#ifndef _CRT_SECURE_NO_DEPRECATE
	#define _CRT_SECURE_NO_DEPRECATE 1	// Tell the compiler to stop complaining about using localtime etc...
#endif
#ifndef _SCL_SECURE_NO_DEPRECATE
	#define _SCL_SECURE_NO_DEPRECATE 1	// Stop complaining about std::copy etc...
#endif
#ifndef _CRT_NONSTDC_NO_DEPRECATE
	#define _CRT_NONSTDC_NO_DEPRECATE 1	// To stop complaining about old POSIX names.
#endif

// Include the Boost.Python library.
#include <boost/python.hpp>
#include <boost/python/to_python_converter.hpp>
#include <boost/python/object.hpp>
#include <boost/python/handle.hpp>
#include <boost/python/iterator.hpp>
#include <boost/python/init.hpp> 
#include <boost/python/suite/indexing/indexing_suite.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>

// Include the Base module. 
#include <Base.h>
// Include the Kernel module. 
#include <MAFEM.h>

#endif // __SCRIPTING_INTERFACE_H
