///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_EAM_POTENTIAL_H
#define __MAFEM_EAM_POTENTIAL_H

#include <MAFEM.h>

#define DOP 3 /// The degree of the polynomial used for interpolation.

namespace MAFEM {

/**
 * \brief A function f(x) that is described by discrete sampling points on a grid. 
 * 
 * When the function is evaluated at a position x then its value is interpolated
 * from the nearby sampling points.
 */
class InterpolatingFunction 
{
public:
	
	/// Default constructor.
	InterpolatingFunction() : _numSamples(0), _stepSize(0) {} 

	/// Sets the sampling grid values and allocates memory for the samples.
	void setSamplingGrid(int numberOfSamples, double stepSize);

	/// Returns the number of grid samples stored in this object.
	int numSamples() const { return _numSamples; }

	/// Returns the grid spacing (step size) of this interpolation function.
	double stepSize() const { return _stepSize; }

	/// Sets the value of the function at the given sample position.
	void setSampleValue(int samplePosition, double value) {
		MAFEM_ASSERT(samplePosition < numSamples());
		samples[samplePosition] = value;
	}

	/// Returns the value of the function at the given sample position.
	double sampleValue(int samplePosition) const { 
		MAFEM_ASSERT(samplePosition < numSamples());
		return samples[samplePosition];
	}

	/// Pre-computes the interpolation data for the grid values.
	/// This must be called before Eval() may be called.
	void computeInterpolation();

	/// Evaluates the function at the position r and computes first and second derivative.
	/// Precondition:  0 <= r <= stepSize*numSamples
	double eval(double r, double* dfdx, double* d2fdx2) const
	{
		MAFEM_ASSERT(numSamples() >= 5);
		if (r >= stepSize() * numSamples())
		{
			*dfdx = *d2fdx2 = 0.0;
			return samples.back();
		}
		MAFEM_ASSERT(r >= 0 && r <= stepSize() * numSamples());
		double x = r / stepSize();
		int k = (int)x;
		k = std::min(k, numSamples()-1);
		x = std::min(x - (double)k, 1.0);
		MAFEM_ASSERT(k >= 0 && k < numSamples()-1);
		
		#if DOP == 3
			double xExp[] = {pow(x,3), x*x, x, 1, 0};
			
			/// Function value
			double fx;
			double prodSum[4];
			for (int i = 0; i < 4; ++i)
				prodSum[i] = xExp[i] * intpl[k][i];
			for (int i = 0; i < 4; ++i)
				fx += prodSum[i];
			cout << "fx fast = " << fx << endl;
			fx = intpl[k][0] * pow(x,3) + intpl[k][1] * x*x + intpl[k][2] * x;
			cout << "fx slow = " << fx << endl;
			
			/// First derivative
			double dfdxCoefs[] = {3,2,1,0};
			for (int i = 0; i < 4; ++i)
				prodSum[i] = xExp[i+1] * intpl[k][i];
			for (int i = 0; i < 4; ++i)
				prodSum[i] *= dfdxCoefs[i];
			for (int i = 0; i < 4; ++i)
				*dfdx += prodSum[i];
			cout << "dfdx fast = " << *dfdx << endl;
			*dfdx = 3 * intpl[k][0] * x*x + 2 * intpl[k][1] * x + intpl[k][2];
			cout << "dfdx slow = " << *dfdx << endl;
	
			*dfdx /= stepSize();

/*			
			/// Second derivative
			double d2fdx2Coefs[] = {6,2};
			for (int i = 0; i < 2; ++i)
				prodSum[i] = xExp[i+2] * intpl[k][i];
			for (int i = 0; i < 2; ++i)
				prodSum[i] *= d2fdx2Coefs[i];
			for (int i = 0; i < 2; ++i)
				*d2fdx2 += prodSum[i];
			*d2fdx2 /= (_stepSize * _stepSize);
*/			
			/// Second derivative
			*d2fdx2 = intpl[4][k] + (intpl[4][k+1] - intpl[4][k]) * x;
			
			return fx + samples[k];
		#elif DOP == 5
			*d1 = ((((5.0 * intpl[4][k] * p + 4.0 * intpl[3][k]) * p + 3.0 * intpl[2][k]) * p + 2.0 * intpl[1][k]) * p + intpl[0][k]) / stepSize();
			*d2 = (((20.0 * intpl[4][k] * p + 12.0 * intpl[3][k]) * p + 6.0 * intpl[2][k]) * p + 2.0 * intpl[1][k]) / (stepSize() * stepSize());
			return ((((intpl[4][k] * p + intpl[3][k]) * p + intpl[2][k]) * p + intpl[1][k]) * p + intpl[0][k]) * p + samples[k];
		#endif
	}

	/// Evaluates the function at the position r and computes first derivative.
	/// Precondition:  0 <= r <= stepSize*numSamples
	double eval(double r, double* dfdx) const
	{
		MAFEM_ASSERT(numSamples() >= 5);
		if(r >= stepSize() * numSamples())
		{
			*dfdx = 0.0;
			return samples.back();
		}
		MAFEM_ASSERT_MSG(r >= 0 && r <= stepSize() * numSamples(), "InterpolatingFunction::eval()", QString("Function argument r=%1 is out of range.").arg(r).toLatin1().constData());
		double x = r/stepSize();
		int k = (int)x;
		k = std::min(k, numSamples()-1);
		x = std::min(x - (double)k, 1.0);
		MAFEM_ASSERT(k >= 0 && k < numSamples()-1);
		#if DOP == 3
			double xExp[] = {pow(x,3), x*x, x, 1, 0};
			
			/// Function value
			double fx;
			double prodSum[4];
			for (int i = 0; i < 4; ++i)
				prodSum[i] = xExp[i] * intpl[k][i];
			for (int i = 0; i < 4; ++i)
				fx += prodSum[i];
			
			/// First derivative
			double dfdxCoefs[] = {3,2,1,0};
			for (int i = 0; i < 4; ++i)
				prodSum[i] = xExp[i+1] * intpl[k][i];
			for (int i = 0; i < 4; ++i)
				prodSum[i] *= dfdxCoefs[i];
			for (int i = 0; i < 4; ++i)
				*dfdx += prodSum[i];
			*dfdx /= stepSize();
		
			MAFEM_ASSERT_MSG(isfinite(*dfdx), "InterpolatingFunction::eval()", QString("The derivative is not-a-number. Function argument was r=%1").arg(r).toLatin1().constData());
			return fx + samples[k];
		#elif DOP == 5
			*d1 = ((((5.0 * intpl[4][k] * p + 4.0 * intpl[3][k]) * p + 3.0 * intpl[2][k]) * p + 2.0 * intpl[1][k]) * p + intpl[0][k]) / stepSize();
			return ((((intpl[4][k] * p + intpl[3][k]) * p + intpl[2][k]) * p + intpl[1][k]) * p + intpl[0][k]) * p + samples[k];
		#endif
	}
	
	/// Evaluates the function at the position r.
	/// Precondition:  0 <= r <= stepSize*numSamples
	double eval(double r) const
	{ 
		MAFEM_ASSERT(numSamples() >= 5);
		if(r >= stepSize() * numSamples())
			return samples.back();
		MAFEM_ASSERT(r >= 0 && r <= stepSize() * numSamples());
		double x = r/stepSize();
		int k = (int)x;
		k = std::min(k, numSamples()-1);
		x = std::min(x - (double)k, 1.0);
		MAFEM_ASSERT(k >= 0 && k < numSamples()-1);
		#if DOP == 3
			double xExp[] = {pow(x,3), x*x, x, 1, 0};
			
			/// Function value
			double fx;
			double prodSum[4];
			for (int i = 0; i < 4; ++i)
				prodSum[i] = xExp[i] * intpl[k][i];
			for (int i = 0; i < 4; ++i)
				fx += prodSum[i];
			return fx + samples[k];
		#elif DOP == 5
			return ((((intpl[4][k] * p + intpl[3][k]) * p + intpl[2][k]) * p + intpl[1][k]) * p + intpl[0][k]) * p + samples[k];
		#endif
	}

private:
	
	/// Number of sample points.
	int _numSamples;

	/// The constant interval between two sample points.
	double _stepSize;

	/// The values of the function at the sample points.
	vector<double> samples;
	
	/// The interpolation data.
	vector< vector<double> > intpl;
/*	
	#if DOP == 3
		vector<double> intpl[DOP + 1];
	#elif DOP == 5
		vector<double> intpl[DOP];
	#endif
*/

	friend QTextStream& operator>>(QTextStream& is, InterpolatingFunction& f);
};

/// Reads the sample values of the inpolating function from an input stream.
inline QTextStream& operator>>(QTextStream& is, InterpolatingFunction& f) {
	for(int i = 0; i < f.numSamples(); i++)
		is >> f.samples[i];
	return is;
}

/**
 * \brief An inter-atomic potential that controls the interaction between
 * 		  atoms.
 * Embedded Atom Method:
 * 
 * Implements a potential that describes bonding in transition metal systems. 
 * This potential has an attractive interaction which models "embedding" a 
 * positively charged pseudo-atom core in the electron density due to the 
 * free valance "sea" of electrons created by the surrounding atoms in the system. 
 * A pairwise part of the potential (which is primarily repulsive) describes 
 * the interaction of the positively charged metal core ions with one another.
 */
class EAMPotential
{
public:

	/// Constructor.
	EAMPotential(const QString& eamfile, bool _finisSinclairFormat) : 
		filename(eamfile), finisSinclairFormat(_finisSinclairFormat) {
		load();
	}

	InterpolatingFunction F_of_Rho;
	InterpolatingFunction Rho_of_r;
	InterpolatingFunction U_of_r;

	/// The cutoff radius for this potential.
	double cutoffRadius;

	/// The maximum electron density that can be handled by this potential.
	double electronDensityRange;
	
	/// The filename of the EAM input file.
	QString filename;
	
	// Specifies the format of the potential file.
	bool finisSinclairFormat;

private:

	/// Deserialization constructor.
	EAMPotential() {}

	/// Loads the potential file.
	void load();
	
	/// Prepares the potential for simulation.
	void preparePotential();
	
	template<class Archive>
    void save(Archive &ar, const unsigned int version) const {
    	ar & filename;
    	ar & finisSinclairFormat;
    }
	template<class Archive>
    void load(Archive &ar, const unsigned int version) {
    	ar & filename;
    	ar & finisSinclairFormat;
    	load();
    }
    
	BOOST_SERIALIZATION_SPLIT_MEMBER()
    
	friend class boost::serialization::access;	
};

}; // End of namespace MAFEM

#endif // __MAFEM_EAM_POTENTIAL_H
