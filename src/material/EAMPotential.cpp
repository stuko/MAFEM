///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "EAMPotential.h"

namespace MAFEM {

/******************************************************************************
* Loads a DYNAMO EAM potential file.
******************************************************************************/
void EAMPotential::load()
{
	using namespace std;
	MsgLogger() << logdate << "Loading EAM file:" << filename << endl;

	// Reads all the inter atomic potentials from specified file.

    // Open potential file
	QFile infile(filename);
	if(!infile.open(QIODevice::ReadOnly | QIODevice::Text))
		throw Exception(QString("Failed to open EAM file %1: %2").arg(filename).arg(infile.errorString()));
	QTextStream instream(&infile);

    // Skip comment lines
	instream.readLine();
	if(finisSinclairFormat) {
		instream.readLine();
		instream.readLine();
	}

	// Read number of atomic types
	int nTypes;
	if(finisSinclairFormat)
		instream >> nTypes;
	else
		nTypes = 1;
	
	int nRho, nR;		// Number of grid points
	FloatType dRho, dR;	// Grid spacing
	FloatType amass; // Atomic mass
    FloatType blat;
    QString elementName;
    QString latticeType;
	
	if(finisSinclairFormat) {
		instream >> nRho;
		instream >> dRho;
		instream >> nR;
		instream >> dR;
		instream >> cutoffRadius;
	    instream >> elementName;
		instream >> amass;
		instream >> blat;
		// Skip to end of line
		instream.readLine();
	}
	else {
	    instream >> elementName;
		instream >> amass;
		instream >> blat;
		instream >> latticeType;
		instream >> nRho;
		instream >> dRho;
		instream >> nR;
		instream >> dR;
		instream >> cutoffRadius;
		// Skip to end of line
		instream.readLine();
	}
	MsgLogger() << "  Element name: " << elementName << endl;
	MsgLogger() << "  Lattice constant: " << blat << endl;
	MsgLogger() << "  Lattice type: " << latticeType << endl;
	MsgLogger() << "  Cutoff radius: " << cutoffRadius << endl;

	if(nTypes <= 0 || dRho <= 0.0 || nR < 5 || nRho < 5)
		throw Exception(QString("Invalid EAM file format: %1").arg(filename));
	if(nTypes != 1)
		throw Exception("Can only handle EAM files with one atomic species.");
	
	// Parse Rho(r) grid values
	F_of_Rho.setSamplingGrid(nRho, dRho);
	electronDensityRange = (FloatType)nRho * dRho;
	instream >> F_of_Rho;

	if(finisSinclairFormat) {
		// Parse R grid values
		Rho_of_r.setSamplingGrid(nR, dR);
		instream >> Rho_of_r;
	
		// Parse z2r grid values.
		U_of_r.setSamplingGrid(nR, dR);
		instream >> U_of_r;
	}
	else {
		// Parse z2r grid values.
		U_of_r.setSamplingGrid(nR, dR);
		instream >> U_of_r;
		// Convert to U function.
		for(int i=0; i<nR; i++) {
			U_of_r.setSampleValue(i, square(U_of_r.sampleValue(i)) * (27.2*0.529));
		}
		
		// Parse R grid values
		Rho_of_r.setSamplingGrid(nR, dR);
		instream >> Rho_of_r;
	}
	
	// Override cutoff radius.
	cutoffRadius = U_of_r.stepSize() * (FloatType)(U_of_r.numSamples() - 1);
	
	// Compute interpolation.
	preparePotential();
}


/***********************************************************************
* Sets the sampling grid values and allocates memory for the samples.
************************************************************************/
void InterpolatingFunction::setSamplingGrid(int numberOfSamples, FloatType stepSize)
{
	MAFEM_ASSERT(numberOfSamples >= 5);
	MAFEM_ASSERT(stepSize > 0.0);
	this->_numSamples = numberOfSamples;
	this->_stepSize = stepSize;

	// Allocate sample data.
	samples.resize(numSamples());
	std::fill(samples.begin(), samples.end(), 0);

	// Release old interpolation.
	#if DOP == 3
		for(int i = 0; i < DOP + 1; ++i)
			intpl[i].clear();
	#elif DOP == 5
		for(int i = 0; i < DOP; ++i)
			intpl[i].clear();
	#endif
}

/***********************************************************************
* Pre-computes the interpolation data for the grid values.
*
* Compute the value and slope at the computational grid points.
* The methodology is as follows:  
* At each point, the slope is estimated from a 5-point Lagrange 
* interpolation of the data. Between each pair of points, a cubic 
* polynomial is used which is fit to the value and slope at the two 
* points. This yields an interpolation which gives continuous value 
* and first derivatives without introducing the long-range effects of 
* glitches in the data that results from using splines.  (i.e. the 
* procedure is local)
************************************************************************/
void InterpolatingFunction::computeInterpolation() 
{
	MAFEM_ASSERT(numSamples() >= 5);
	
#if DOP == 3
	for(int i = 0; i < DOP + 1; ++i)
		intpl[i].resize(numSamples());

	// First derivative at the beginning and end of data range
	intpl[0][0] = samples[1] - samples[0];
	intpl[0][1] = 0.5 * (samples[2] - samples[0]);
	intpl[0][numSamples()-2] = 0.5 * (samples[numSamples()-1] - samples[numSamples()-3]);
	intpl[0][numSamples()-1] = samples[numSamples()-1] - samples[numSamples()-2];

	intpl[3][1] = intpl[3][0] = (samples[2] + samples[0] - 2.0 * samples[1]) / (stepSize() * stepSize());
	intpl[3][numSamples()-1] = intpl[3][numSamples()-2] = (samples[numSamples()-3] + samples[numSamples()-1] - 2.0 * samples[numSamples()-2]) / (stepSize() * stepSize());
    
	for(size_t i = 2; i <= numSamples()-3; ++i)
	{
		intpl[0][i] = ((samples[i-2] - samples[i+2]) + 8.0 * (samples[i+1] - samples[i-1])) / 12.0; // First derivative of the Lagrange Polynomial at i for unit steps
		intpl[3][i] = (16.0 * (samples[i-1] + samples[i+1]) - (samples[i+2] + samples[i-2]) - 30.0 * samples[i]) / (12.0 * stepSize() * stepSize()); // Second derivative of the Lagrange Polynomial at i
	}

	// Compute cubic polynomial: a_3*x^3 + a_2*x^2 + a_1*x + a_0
	// a_3 = intpl[2][i]
	// a_2 = intpl[1][i]
	// a_1 = intpl[0][i]
	// a_0 = 0
	for(size_t i = 0; i <= numSamples() - 2; ++i)
	{
		intpl[1][i] = 3.0 * (samples[i+1] - samples[i]) - 2.0 * intpl[0][i] - intpl[0][i+1];
		intpl[2][i] = intpl[0][i] + intpl[0][i+1] - 2.0 * (samples[i+1] - samples[i]);
	}
	intpl[1][numSamples()-1] = 0.0;
	intpl[2][numSamples()-1] = 0.0;

#elif DOP == 5
	for(int i = 0; i < DOP; ++i)
		intpl[i].resize(numSamples());

	// First derivative at the beginning and end of data range
	intpl[0][0] = samples[1] - samples[0];
	intpl[0][1] = 0.5 * (samples[2] - samples[0]);
	intpl[0][numSamples()-2] = 0.5 * (samples[numSamples()-1] - samples[numSamples()-3]);
	intpl[0][numSamples()-1] = samples[numSamples()-1] - samples[numSamples()-2];
	
	// Second derivative / 2 at the beginning and end of data range
	intpl[1][1] = intpl[1][0] = (samples[2] + samples[0] - 2.0 * samples[1]) / 2;
	intpl[1][numSamples()-1] = intpl[1][numSamples()-2] = (samples[numSamples()-3] + samples[numSamples()-1] - 2.0 * samples[numSamples()-2]) / 2;
    
	for(size_t i = 2; i <= numSamples()-3; ++i)
	{
		intpl[0][i] = ((samples[i-2] - samples[i+2]) + 8.0 * (samples[i+1] - samples[i-1])) / 12.0; // First derivative of the Lagrange Polynomial at i for unit steps
		intpl[1][i] = (16.0 * (samples[i-1] + samples[i+1]) - (samples[i+2] + samples[i-2]) - 30.0 * samples[i]) / (2 * 12.0); // Second derivative / 2 of the Lagrange Polynomial at i
	}
	
	// Compute 5th order polynomial: a*x^5 + b*x^4 + c*x^3 + d*x^2 + e*x + f
	// a = intpl[4][i]
	// b = intpl[3][i]
	// c = intpl[2][i]
	// d = intpl[1][i]
	// e = intpl[0][i]
	// f = 0
	for(size_t i = 0; i <= numSamples() - 2; ++i)
	{
		intpl[2][i] = 10.0 * (samples[i+1] - samples[i]) - 6.0 * intpl[0][i] - 4.0 * intpl[0][i+1] - 3.0 * intpl[1][i] + intpl[1][i+1];
		intpl[3][i] = -15.0 * (samples[i+1] - samples[i]) + 8.0 * intpl[0][i] + 7.0 * intpl[0][i+1] + 3.0 * intpl[1][i] - 2.0 * intpl[1][i+1];
		intpl[4][i] = 6.0 * (samples[i+1] - samples[i]) - 3.0 * intpl[0][i] - 3.0 * intpl[0][i+1] - intpl[1][i] + intpl[1][i+1];
	}
	intpl[1][numSamples()-1] = 0.0;
	intpl[2][numSamples()-1] = 0.0;
	intpl[3][numSamples()-1] = 0.0;
#endif

#ifdef _DEBUG
	#if DOP == 3
		for(size_t j = 0; j < numSamples(); ++j)
		{
			MAFEM_ASSERT(isfinite(samples[j]));
			for(size_t i = 0; i < DOP + 1; ++i)
				MAFEM_ASSERT(isfinite(intpl[i][j]));
		}
	#elif DOP == 5
		for(size_t j = 0; j < numSamples(); ++j)
		{
			MAFEM_ASSERT(isfinite(samples[j]));
			for(size_t i = 0; i < DOP; ++i)
				MAFEM_ASSERT(isfinite(intpl[i][j]));
		}
	#endif
#endif
}

/***********************************************************************
* Prepares the potential for simulation.
************************************************************************/
void EAMPotential::preparePotential()
{
    F_of_Rho.computeInterpolation();
    Rho_of_r.computeInterpolation();
    U_of_r.computeInterpolation();
}

}; // End of namespace MAFEM

