///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_SIMULATION_RESOURCE_H
#define __MAFEM_SIMULATION_RESOURCE_H

#include <MAFEM.h>

namespace MAFEM {

class Simulation;	// defined in Simulation.h

/**
 * \brief Base class for all objects that are part of a Simulation instance.
 * 
 * This mix-in class stores a pointer to the owning Simulation.
 * \author Alexander Stukowski
 */
class SimulationResource
{
public:

	/// \brief Default constructor that creates the object with a parent Simulation instance.		
	SimulationResource() : _sim(NULL) {}
	
	/// \brief Constructor.
	/// \param sim Pointer to the Simulation instance that owns this object.
	SimulationResource(Simulation* sim) : _sim(sim) {
		MAFEM_ASSERT_MSG(sim != NULL, "SimulationResource constructor", "This constructor requires an instance of the Simulation class.");
	}

	/// \brief Returns the simulation that owns this object.
	/// \return Pointer to the global Simulation object.
	///
	/// Calling this method is only allowed when hasSimulation() returns true.
	Simulation* simulation() const {
		MAFEM_ASSERT_MSG(_sim != NULL, "SimulationResource::simulation()", "This object derived from SimulationResource has not been assigned to a Simulation instance.");
		return _sim; 
	}
	
	/// \brief Returns whether this object belong to a Simulation instance.
	bool hasSimulation() const { 
		return _sim != NULL;
	}

	/// Sets the internal pointer to the owning Simulation instance.
	void setSimulation(Simulation* sim) {
		MAFEM_ASSERT(_sim == NULL || _sim == sim);
		_sim = sim;
	}	

private:

	/// The internal pointer.
	Simulation* _sim;
	
	friend class Simulation;
	friend class boost::serialization::access;

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version);
};

}; // End of namespace MAFEM

#endif // __MAFEM_SIMULATION_RESOURCE_H
