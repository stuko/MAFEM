///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "Simulation.h"
#include <atoms/RepAtom.h>

BOOST_CLASS_EXPORT(MAFEM::Simulation)
BOOST_CLASS_EXPORT(MAFEM::SimulationSettings)

namespace MAFEM {

/// Pointer to the Simulation object that is currently being deserialized. 
Simulation* Simulation::_serializationInstance = NULL;

#ifdef _DEBUG	
QtMsgHandler defaultQtMessageHandler = NULL;

/******************************************************************************
* Handler method for Qt error messages.
* This can be used to set a debugger breakpoint for the MAFEM_ASSERT macros.
******************************************************************************/
static void qtMessageOutput(QtMsgType type, const char *msg)
{
	if(type == QtFatalMsg) {
		// This code line can be used to set a breakpoint to debug OVITO_ASSERT() macro exceptions. 
		int a = 1;	
	}
	// Pass message on to default handler.
	if(defaultQtMessageHandler) defaultQtMessageHandler(type, msg);
	else std::cerr << msg << std::endl;
}
#endif

/******************************************************************************
* Initializes the object.
******************************************************************************/
Simulation::Simulation() 
{
#ifdef _DEBUG	
	// Install custom Qt error message handler to catch fatal errors in debug mode.
	defaultQtMessageHandler = qInstallMsgHandler(qtMessageOutput);
#endif	
	
	_mesh.setSimulation(this);
	
	// Set settings to default values.
	_settings.periodicBC.assign(false);
	_settings.clusterRadius = 0;
	_settings.clusterSkinThickness = 0;
	_settings.neighborSkinThickness = 0;
	_settings.keepFullyAtomisticElements = true;
	_settings.latticeStaticsMode = false;
	_settings.QCForceMode = false;
	_settings.octreeMesh = false;
	_settings.qhullMesh =  false;
	_settings.tetgenMesh =  false;
	_settings.quadratureMethod = false;
	_settings.octreeElementSearch = false;
	_settings.segmentTreeElementSearch = false;
	_settings.qrVolBasedWeightFactor = false;
	_settings.openMPThread = 1;
	_settings.indentLocationX = 0;
	_settings.indentLocationY = 0;
	_settings.indentLocationZ = 0;

	// Start with an empty simulation box.
	_simCell = NULL_MATRIX;
	_inverseSimCell = NULL_MATRIX;
	_deformedSimCell = NULL_MATRIX;
	_inverseDeformedSimCell = NULL_MATRIX;
	
	// Define the default ALL group.
	memset(_groups, 0, sizeof(_groups));
	_groups[0] = new Group(this, shared_ptr<Region>(), 0);
}

/******************************************************************************
* Deletes the object.
******************************************************************************/
Simulation::~Simulation() 
{
	for(int i=0; i<MAX_GROUP_COUNT; i++) 
		delete _groups[i];
}

/******************************************************************************
* Adds a grain to the simulation.
******************************************************************************/
void Simulation::addGrain(const shared_ptr<Grain>& grain)
{
	CHECK_POINTER(grain.get());
	
	if(grain->hasSimulation())
		throw Exception("Error in Simulation::addGrain(): This grain is aleady part of a simulation.");

/**////
	if(_settings.qhullMesh or _settings.tetgenMesh ){
		grain->setSimulation(this);
		_grains.push_back(grain);
	}

/**////
	if(_settings.octreeMesh){
		grain->setSimulation(this);
		grain->Boundary().setGrainIndex(_grains.size()-1);
		_grains.push_back(grain);
	}


	MsgLogger() << logdate << "Adding grain to simulation:" << endl;
	MsgLogger() << "  Atom type: " << grain->atomType() << endl;
	MsgLogger() << "  Lattice type: " << grain->lattice().latticeType() << endl;
	MsgLogger() << "  Bravais vector 1: " << grain->lattice().bravaisVector(0) << endl;	
	MsgLogger() << "  Bravais vector 2: " << grain->lattice().bravaisVector(1) << endl;
	MsgLogger() << "  Bravais vector 3: " << grain->lattice().bravaisVector(2) << endl;
	MsgLogger() << "  Lattice vector 1: " << grain->lattice().latticeVector(0) << endl;
	MsgLogger() << "  Lattice vector 2: " << grain->lattice().latticeVector(1) << endl;
	MsgLogger() << "  Lattice vector 3: " << grain->lattice().latticeVector(2) << endl;
	MsgLogger() << "  Lattice origin: " << grain->lattice().translation() << endl;
	MsgLogger() << "  Unit cell volume: " << grain->lattice().unitCellVolume() << endl;
	MsgLogger() << "  Lattice minimum eigenvalue: " << grain->lattice().minEigenvalue() << endl;
	MsgLogger() << "  Lattice # nearest neighbors: " << grain->lattice().numNearestNeighbors() << endl;
}

/******************************************************************************
* Sets the potential used for interaction computations.
******************************************************************************/
void Simulation::setPotential(const shared_ptr<EAMPotential>& potential) 
{ 
	_potential = potential; 
}
	
/******************************************************************************
* Sets initial the simulation cell geometry. 
******************************************************************************/
void Simulation::setSimulationCell(const AffineTransformation& cell) 
{
	if(abs(cell.determinant()) < FLOATTYPE_EPSILON)
		throw Exception("Simulation cell matrix is a singular matrix.");	

	MsgLogger() << "Initial simulation cell matrix:" << endl << cell;

	_simCell = cell;  
	_inverseSimCell = cell.inverse();
	_deformedSimCell = cell;
	_inverseDeformedSimCell = _inverseSimCell;
}

/******************************************************************************
* Sets the current simulation cell geometry. 
* All repatom coordinates are rescaled to the new cell shape based on their current reduced coordinates.
******************************************************************************/
void Simulation::setDeformedSimulationCell(const AffineTransformation& cell) 
{
	if(abs(cell.determinant()) < FLOATTYPE_EPSILON)
		throw Exception("Simulation cell matrix is a singular matrix.");	
	MAFEM_ASSERT(_deformedSimCell != NULL_MATRIX);
	MAFEM_ASSERT(_inverseDeformedSimCell != NULL_MATRIX);
	
	AffineTransformation deltaTM = cell * _inverseDeformedSimCell;
	
	MsgLogger() << logdate << "Deforming simulation cell. Rescaling all repatom coordinates to new cell shape." << endl;
	MsgLogger() << "  New simulation cell matrix:" << endl << cell; 
	
	// Recale atomic positions to new cell shape.
	Q_FOREACH(RepAtom* repatom, mesh().repatoms()) {
		repatom->setDeformedPos(deltaTM * repatom->deformedPos());
	}

	_deformedSimCell = cell;  
	_inverseDeformedSimCell = cell.inverse(); 
}


/******************************************************************************
* Adds a new group of repatoms to the simulation.
******************************************************************************/
Group* Simulation::createGroup(const shared_ptr<Region>& region)
{
	for(int i=0; i<MAX_GROUP_COUNT; i++) {
		if(_groups[i] == NULL) {
			_groups[i] = new Group(this, region, i);
			
			// Add all repatoms and sampling atoms inside the region to the group.
			int repatomMemberCount = 0, samplingatomMemberCount = 0;
			if(region) {
				Q_FOREACH(RepAtom* node, mesh().nodes()) {
					if(region->contains(node->pos())) {
						node->addToGroup(_groups[i]);
						repatomMemberCount++;
					}
				}
				Q_FOREACH(SamplingAtom* atom, mesh().samplingAtoms()) {
					if(atom->isActive() && region->contains(atom->pos())) {
						atom->addToGroup(_groups[i]);
						samplingatomMemberCount++;
					}
				}
			}
			MsgLogger() << logdate << "Defining new group with" << repatomMemberCount << "nodes and" << samplingatomMemberCount << "sampling atoms belonging to it initially." << endl;
			if(region)
				MsgLogger() << "  Geometric region is" << region->toString() << endl;
			
			return _groups[i];
		}
	}
	throw Exception("The maximum number of user-defined groups has been reached.");
}

Group* Simulation::breakGroup(const Group* group)
{
	for (int i = 0; i < mesh().samplingAtoms().size(); ++i)
		mesh().nodes()[i]->removeFromGroup(group);
	
	for (int i = 0; i < mesh().nodes().size(); ++i)
		mesh().samplingAtoms()[i]->removeFromGroup(group);
		
	for (int i = 0; i < MAX_GROUP_COUNT; ++i)
		if (_groups[i] == group)
		{
			delete _groups[i];
			_groups[i] = NULL;
		}
}

/******************************************************************************
* Save the complete state of the simulation to a file.
******************************************************************************/
void saveSimulation(const Simulation& sim, const QString& filename)
{
	MsgLogger() << logdate << "Saving simulation state to restart file" << filename << endl;
    ofstream ofs(filename.toLatin1().constData(), ios_base::out|ios_base::binary);
    if(ofs.fail())
    	throw Exception("Failed to open restart file for writing.");
    try {
	    boost::archive::binary_oarchive oa(ofs);
	    oa << sim;
    }
    catch(const boost::archive::archive_exception& ex) {
    	throw Exception(QString("Archive exception: %1").arg(ex.what()));
    }
}

/******************************************************************************
* Loads the state of a simulation from a file.
******************************************************************************/
shared_ptr<Simulation> loadSimulation(const QString& filename)
{
	MsgLogger() << logdate << "Loading simulation state from restart file" << filename << endl;
    ifstream ifs(filename.toLatin1().constData(), ios_base::in|ios_base::binary);
    if(ifs.fail())
    	throw Exception("Failed to open restart file for reading.");
    try {
    	boost::archive::binary_iarchive ia(ifs);
    	shared_ptr<Simulation> sim(new Simulation());
    	ia >> *sim;
    	return sim;
    }
    catch(const boost::archive::archive_exception& ex) {
    	throw Exception(QString("Archive exception: %1").arg(ex.what()));
    }
}

template<class Archive>
void SimulationResource::serialize(Archive &ar, const unsigned int version) 
{ 
	ar & _sim; 
}	
template void SimulationResource::serialize<boost::archive::binary_iarchive>(boost::archive::binary_iarchive & ar, const unsigned int file_version);
template void SimulationResource::serialize<boost::archive::binary_oarchive>(boost::archive::binary_oarchive & ar, const unsigned int file_version);

}; // End of namespace MAFEM
