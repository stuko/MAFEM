///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "Calculator.h"
#include "voro/voro++.hh"
#include "gsl/gsl_linalg.h"
#include "FEAST/matrix_operations.h"

#if buildLocationCluster
#include "FEAST/feast_solver.h"
#endif

using namespace voro;

namespace MAFEM {

/******************************************************************************
 * This performs the full calculation of the forces.
 *****************************************************************************/
void Calculator::calculateEnergyAndForces(CalculationResult& output)
{
	// Initialize force boundary condition objects.
	const ForceBoundaryConditionList::const_iterator forceBCbegin = simulation()->forceBC().constBegin();
	const ForceBoundaryConditionList::const_iterator forceBCend = simulation()->forceBC().constEnd();
	for(ForceBoundaryConditionList::const_iterator forceBC = forceBCbegin; forceBC != forceBCend; ++forceBC)
		(*forceBC)->prepare(mesh());
	
	// Initialize displacement boundary condition objects.
/*	
	const DisplacementBoundaryConditionList::const_iterator displBCbegin = simulation()->displacementBC().begin();
	const DisplacementBoundaryConditionList::const_iterator displBCend = simulation()->displacementBC().end();
	for(DisplacementBoundaryConditionList::const_iterator displBC = displBCbegin; displBC != displBCend; ++displBC)
		(*displBC)->prepare(mesh());
*/
	Q_FOREACH(const shared_ptr<DisplacementBoundaryCondition>& displBC, simulation()->displacementBC())
		displBC->prepare(mesh());

	// Associate the output container with the simulation.
	output.setSimulation(this->simulation());

	// Reset output fields.
	output.forces().resize(mesh().repatoms().size());
	output.reforces().resize(mesh().repatoms().size());
	output.internalEnergy().resize(mesh().repatoms().size());
	output.externalEnergy().resize(mesh().repatoms().size());

	memset(output.forces().data(), 0, output.forces().size() * sizeof(Vector3));
	memset(output.reforces().data(), 0, output.reforces().size() * sizeof(Vector3));
	memset(output.internalEnergy().data(),0,output.internalEnergy().size()*sizeof(FloatType));
	memset(output.externalEnergy().data(),0,output.externalEnergy().size()*sizeof(FloatType));

	output.setTotalEnergy(0);
	
	// Let the sub-class do the actual work.
	this->calculateEnergyAndForcesImpl(output);

	// Let the displacement boundary conditions reset selected entries in the force vector.
	Q_FOREACH(const shared_ptr<DisplacementBoundaryCondition>& displBC, simulation()->displacementBC())
		displBC->resetForces(mesh(), output);
}

/******************************************************************************
 * This performs only the calculation of the forces.
 *****************************************************************************/
void Calculator::calculateForces(CalculationResult& output)
{
	// Initialize force boundary condition objects.
	const ForceBoundaryConditionList::const_iterator forceBCbegin = simulation()->forceBC().constBegin();
	const ForceBoundaryConditionList::const_iterator forceBCend = simulation()->forceBC().constEnd();
	for(ForceBoundaryConditionList::const_iterator forceBC = forceBCbegin; forceBC != forceBCend; ++forceBC)
		(*forceBC)->prepare(mesh());
/*	
	Q_FOREACH(const shared_ptr<DisplacementBoundaryCondition>& displBC, simulation()->displacementBC())
		displBC->prepare(mesh());
*/
	// Associate the output container with the simulation.
	output.setSimulation(this->simulation());

	// Reset output fields.
	output.forces().resize(mesh().repatoms().size());
	output.reforces().resize(mesh().repatoms().size());
	output.internalEnergy().resize(mesh().repatoms().size());
	output.externalEnergy().resize(mesh().repatoms().size());

	memset(output.forces().data(), 0, output.forces().size() * sizeof(Vector3));
	memset(output.reforces().data(), 0, output.reforces().size() * sizeof(Vector3));
	memset(output.internalEnergy().data(),0,output.internalEnergy().size()*sizeof(FloatType));
	memset(output.externalEnergy().data(),0,output.externalEnergy().size()*sizeof(FloatType));

	// Let the sub-class do the actual work.
	this->calculateForcesImpl(output);

	// Let the displacement boundary conditions reset selected entries in the force vector.
	Q_FOREACH(const shared_ptr<DisplacementBoundaryCondition>& displBC, simulation()->displacementBC())
		displBC->resetForces(mesh(), output);
}

/******************************************************************************
 * This performs only the calculation of the energy.
 *****************************************************************************/
void Calculator::calculateEnergy(CalculationResult& output)
{
	// Initialize force boundary condition objects.
	const ForceBoundaryConditionList::const_iterator forceBCbegin = simulation()->forceBC().constBegin();
	const ForceBoundaryConditionList::const_iterator forceBCend = simulation()->forceBC().constEnd();
	for(ForceBoundaryConditionList::const_iterator forceBC = forceBCbegin; forceBC != forceBCend; ++forceBC)
		(*forceBC)->prepare(mesh());
/*	
	Q_FOREACH(const shared_ptr<DisplacementBoundaryCondition>& displBC, simulation()->displacementBC())
		displBC->prepare(mesh());
*/
	// Associate the output container with the simulation.
	output.setSimulation(this->simulation());

	// Reset output field.
	output.setTotalEnergy(0);

	// Let the sub-class do the actual work.
	this->calculateEnergyImpl(output);
}

/*
void Calculator::calculateCentroSymmetry(const Box3& box, CalculationResult& output,FloatType cutoff, FloatType maxVol, FloatType tol)
{
	output.simulation()->settings().QCForceMode = true;
	output.simulation()->mesh().samplingAtoms().buildNeighborLists(simulation());
	output.setCentroSymBox(box);
	progress_display progress(output.simulation()->mesh().samplingAtoms().size());
	for(int i=0; i<output.simulation()->mesh().samplingAtoms().size(); ++i)
	{
		SamplingAtom* atom1 = output.simulation()->mesh().samplingAtoms()[i];
		++progress;
		if(!box.contains(atom1->pos(),FLOATTYPE_EPSILON)) continue;
		
		const CrystalLattice* lattice = atom1->site().lattice();
		Vector3 centrosymNearestNeighbors[12];
		FloatType centrosymMinDistance[12];
		for(size_t i=0; i<lattice->numNearestNeighbors(); i++)
				centrosymMinDistance[i] = FLOATTYPE_MAX;

		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter)
		{
			const SamplingAtom* neigh = *neighborIter;
			Vector3 r = atom1->deformedPos() - neigh->deformedPos();
			for(size_t i=0; i<lattice->numNearestNeighbors(); ++i)
			{
				FloatType diff = LengthSquared(r - lattice->nearestNeighbor(i));
				if(diff < centrosymMinDistance[i])
				{
					centrosymMinDistance[i] = diff;
					centrosymNearestNeighbors[i] = r;
				}
			}
		}

		FloatType value = 0.0;
		for(size_t i=0; i<lattice->numNearestNeighbors(); i+=2)
			value += LengthSquared(centrosymNearestNeighbors[i] + centrosymNearestNeighbors[i+1]);
		if(value >= cutoff)
		{
			atom1->setCentroSymmetry(value);
			output.centroatoms.push_back(atom1);
			output.countCentroSymmetryAtoms();
		}
	}
	calculateEnergyAndForces(output);
	calculateSelectedAtomStresses(output,maxVol,tol);
}
*/

/******************************************************************************
 * This calculates the centrosymmetry of all RepAtoms.
 *****************************************************************************/
void Calculator::calculateCentrosymmetry(CalculationResult& output)
{
	MsgLogger() << "Calculating centrosymmetry parameter." << endl;
	
	output.centrosymmetry().resize(mesh().repatoms().size());
	memset(output.centrosymmetry().data(),0,output.centrosymmetry().size()*sizeof(FloatType));
	
//	output.simulation()->settings().QCForceMode = true;
	output.simulation()->mesh().samplingAtoms().buildNeighborLists(simulation());
//	progress_display progress(output.simulation()->mesh().repatoms().size());
	for(int i = 0; i < output.simulation()->mesh().samplingAtoms().size(); ++i)
	{
		SamplingAtom* atom1 = output.simulation()->mesh().samplingAtoms()[i];
		if(!atom1->samplingNode()) continue;

//		++progress;

		const CrystalLattice* lattice = atom1->site().lattice();
		Vector3 centrosymNearestNeighbors[12];
		FloatType centrosymMinDistance[12];
		for(size_t i = 0; i < lattice->numNearestNeighbors(); ++i)
				centrosymMinDistance[i] = FLOATTYPE_MAX;

		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter)
		{
			const SamplingAtom* neigh = *neighborIter;
			Vector3 r = atom1->deformedPos() - neigh->deformedPos();
			for(size_t i = 0; i < lattice->numNearestNeighbors(); ++i)
			{
				FloatType diff = LengthSquared(r - lattice->nearestNeighbor(i));
				if(diff < centrosymMinDistance[i])
				{
					centrosymMinDistance[i] = diff;
					centrosymNearestNeighbors[i] = r;
				}
			}
		}

		FloatType value = 0.0;
		for(size_t i = 0; i < lattice->numNearestNeighbors(); i += 2)
			value += LengthSquared(centrosymNearestNeighbors[i] + centrosymNearestNeighbors[i + 1]);
		
		output.centrosymmetry().data()[atom1->samplingNode()->realRepatom()->index()] = value;
	}
}

void Calculator::calculateSelectedAtomStresses(CalculationResult& output,FloatType maxVol, FloatType tol)
{
	MsgLogger() << "Calculating stress/atom using atomic volume by voronoi tessellation." << endl;
	int numThread = simulation()->settings().openMPThread;
	if(numThread > omp_get_max_threads()) numThread =  omp_get_max_threads();
	const EAMPotential* potential = simulation()->potential().get();
	const FloatType potentialCutoffRadiusSquared = square(potential->cutoffRadius);
	Box3 simCell = simulation()->simulationCell() * Box3(Point3(0,0,0),Point3(1,1,1));
	// Set up constants for the container geometry
	FloatType x_min= 0.0,x_max=0.0;
	FloatType y_min=0.0,y_max=0.0;
	FloatType z_min=0.0,z_max=0.0;
	
	// Set up the number of blocks that the container is divided into
	const int n_x=1,n_y=1,n_z=1;

	QVector<const SamplingAtom*> storage(0);

//#pragma omp parallel for private(x_min,y_min,z_min,x_max,y_max,z_max,storage) num_threads(numThread)
	for(int i=0; i< output.getCentroSymmetryAtoms(); ++i)
	{
		SamplingAtom* atom1 = output.centroatoms[i];

		x_min = atom1->deformedPos().X; x_max = atom1->deformedPos().X; y_min = atom1->deformedPos().Y; y_max = atom1->deformedPos().Y; z_min = atom1->deformedPos().Z; z_max = atom1->deformedPos().Z;
		storage.append(atom1);

		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter)
		{
			const SamplingAtom* neigh = *neighborIter;
			// Compute distance vector to neighbor.
			const Vector3 r = atom1->deformedPos() - neigh->deformedPos();
			const FloatType distSquared = LengthSquared(r);
			if(distSquared >= potentialCutoffRadiusSquared) continue;

			storage.append(neigh);

			if(x_min>neigh->deformedPos().X) x_min = neigh->deformedPos().X;
			if(y_min>neigh->deformedPos().Y) y_min = neigh->deformedPos().Y;
			if(z_min>neigh->deformedPos().Z) z_min = neigh->deformedPos().Z;

			if(x_max<neigh->deformedPos().X) x_max = neigh->deformedPos().X;
			if(y_max<neigh->deformedPos().Y) y_max = neigh->deformedPos().Y;
			if(z_max<neigh->deformedPos().Z) z_max = neigh->deformedPos().Z;		
		}
		
		container con(x_min-tol,x_max+tol,y_min-tol,y_max+tol,z_min-tol,z_max+tol,n_x,n_y,n_z,false,false,false,8);

		for(int s=0; s < storage.size(); ++s)
		{
			const SamplingAtom* atom = storage.at(s);
			con.put(s,atom->deformedPos().X,atom->deformedPos().Y,atom->deformedPos().Z);
		}
		
		FloatType volume = (FloatType) con.cellVolume(0);	

		FloatType suum = 0.0;
		if(volume > maxVol)
		{
			for(int s=0; s < storage.size(); ++s)
				if(con.cellVolume(s) < maxVol)
					suum += con.cellVolume(s);
		    volume = (FloatType) suum/storage.size();
		}
		
		atom1->setAtomicVolume(volume);
		storage.clear();
		con.clear();
	}

// Calculate electron density and the embedding energy derivative
// for each sampling atom.
//#pragma omp parallel for num_threads(numThread)
	for(int i=0; i<mesh().samplingAtoms().size(); ++i)
	{

		SamplingAtom* atom1 = mesh().samplingAtoms()[i];
		// The electron density at the location of atom 1.
		FloatType rho = 0;		
		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter)
		{
			const SamplingAtom* atom2 = *neighborIter;
		
			// Compute distance vector to neighbor.
			const FloatType distSquared = DistanceSquared(atom1->deformedPos(), atom2->deformedPos());
			if(distSquared >= potentialCutoffRadiusSquared) continue;
			MAFEM_ASSERT_MSG(distSquared > FLOATTYPE_EPSILON, "ForceQCCalculator::calculateForces()", "Atoms too close together.");
			const FloatType dist = sqrt(distSquared);
		
			// Compute the contribution of atom 2 to the electron density at the location of atom 1.
			rho += potential->Rho_of_r.eval(dist);
		}
	
		// Compute embedding energy derivative.
		FloatType derivativeF;
		potential->F_of_Rho.eval(rho, &derivativeF);
		atom1->setEmbeddingEnergyDerivative(derivativeF);
	}

//#pragma omp parallel for num_threads(numThread)
	for(int i=0; i< output.getCentroSymmetryAtoms(); ++i)
	{
		SamplingAtom* atom1 = output.centroatoms[i];	
		
	  	FloatType vol = atom1->getAtomicVolume();
		for(int r=0; r<6; ++r)
			atom1->setStressComponent(r,0.0);
		// Loop over all neighbors.
		
		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter)
		{
			const SamplingAtom* atom2 = *neighborIter;
			
			// Compute distance vector to neighbor.
			const Vector3 r = atom1->deformedPos() - atom2->deformedPos();
			const FloatType distSquared = LengthSquared(r);
			if(distSquared >= potentialCutoffRadiusSquared) continue;
			MAFEM_ASSERT_MSG(distSquared > FLOATTYPE_EPSILON, "ForceQCCalculator::calculateForces()", "Atoms too close together.");
			const FloatType dist = sqrt(distSquared);
			
			Vector3 phiGradient = NULL_VECTOR;
			Vector3 rhoGradient = NULL_VECTOR;
			
			// Calculate pair energy term.
			FloatType derivative1;
			const FloatType Z = potential->U_of_r.eval(dist, &derivative1);
			const FloatType pairEnergy = Z / dist;
			
			// Compute gradient of pair energy.
			derivative1 = (derivative1 - pairEnergy)/distSquared;
			phiGradient = (derivative1) * r;

			MAFEM_ASSERT_MSG(isfinite(derivative1), "EnergyQCCalculator::calculateEnergyAndForces()", QString("The pair functional derivative is Not-a-Number. Pair distance was %1").arg(dist).toLatin1().constData());
			
			
			
			FloatType unit = 1.602*1e2;  //1.602*1e-10*1e-9;
			                                                    
			atom1->setStressComponent(0, atom1->getStressComponent(0) + ((0.5*r[0]*phiGradient[0]*unit)/vol));
			atom1->setStressComponent(1, atom1->getStressComponent(1) + ((0.5*r[1]*phiGradient[1]*unit)/vol));
			atom1->setStressComponent(2, atom1->getStressComponent(2) + ((0.5*r[2]*phiGradient[2]*unit)/vol));
			atom1->setStressComponent(3, atom1->getStressComponent(3) + ((0.5*r[0]*phiGradient[1]*unit)/vol));
			atom1->setStressComponent(4, atom1->getStressComponent(4) + ((0.5*r[0]*phiGradient[2]*unit)/vol));
			atom1->setStressComponent(5, atom1->getStressComponent(5) + ((0.5*r[1]*phiGradient[2]*unit)/vol));
		    
			// Compute the contribution of atom 2 to the electron density at the location of atom 1.
			FloatType derivativeRho;
			potential->Rho_of_r.eval(dist, &derivativeRho);
			MAFEM_ASSERT(isfinite(derivativeRho));
			rhoGradient = (derivativeRho / dist) * r;

			atom1->setStressComponent(0, atom1->getStressComponent(0) + ((0.5*r[0]*rhoGradient[0]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
			atom1->setStressComponent(1, atom1->getStressComponent(1) + ((0.5*r[1]*rhoGradient[1]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
			atom1->setStressComponent(2, atom1->getStressComponent(2) + ((0.5*r[2]*rhoGradient[2]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
			atom1->setStressComponent(3, atom1->getStressComponent(3) + ((0.5*r[0]*rhoGradient[1]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
			atom1->setStressComponent(4, atom1->getStressComponent(4) + ((0.5*r[0]*rhoGradient[2]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
			atom1->setStressComponent(5, atom1->getStressComponent(5) + ((0.5*r[1]*rhoGradient[2]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
		}
	}
}

void Calculator::calculateStresses(CalculationResult& output,FloatType maxVol, FloatType tol)
{
	MsgLogger() << "Calculating stress/atom using atomic volume by voronoi tessellation." << endl;
	int numThread = simulation()->settings().openMPThread;
	if(numThread > omp_get_max_threads()) numThread =  omp_get_max_threads();
	const EAMPotential* potential = simulation()->potential().get();
    const FloatType potentialCutoffRadiusSquared = square(potential->cutoffRadius);
	Box3 simCell = simulation()->simulationCell() * Box3(Point3(0,0,0),Point3(1,1,1));
	// Set up constants for the container geometry
	FloatType x_min = 0.0, x_max = 0.0;
	FloatType y_min = 0.0, y_max = 0.0;
	FloatType z_min = 0.0, z_max = 0.0;
	
	// Set up the number of blocks that the container is divided into
	const int n_x = 1, n_y = 1, n_z = 1;

	QVector<const SamplingAtom*> storage(0);

//#pragma omp parallel for private(x_min,y_min,z_min,x_max,y_max,z_max,storage) num_threads(numThread)
	for(int i=0; i<output.simulation()->mesh().samplingAtoms().size(); ++i)
	{
		SamplingAtom* atom1 = output.simulation()->mesh().samplingAtoms()[i];

		if(!atom1->samplingNode()) continue;

		x_min = atom1->deformedPos().X;
		x_max = atom1->deformedPos().X;
		y_min = atom1->deformedPos().Y;
		y_max = atom1->deformedPos().Y;
		z_min = atom1->deformedPos().Z;
		z_max = atom1->deformedPos().Z;
		storage.append(atom1);

		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter)
		{
			const SamplingAtom* neigh = *neighborIter;
			// Compute distance vector to neighbor.
			const Vector3 r = atom1->deformedPos() - neigh->deformedPos();
			const FloatType distSquared = LengthSquared(r);
			if(distSquared >= potentialCutoffRadiusSquared) continue;

			storage.append(neigh);

			if(x_min>neigh->deformedPos().X) x_min = neigh->deformedPos().X;
			if(y_min>neigh->deformedPos().Y) y_min = neigh->deformedPos().Y;
			if(z_min>neigh->deformedPos().Z) z_min = neigh->deformedPos().Z;

			if(x_max<neigh->deformedPos().X) x_max = neigh->deformedPos().X;
			if(y_max<neigh->deformedPos().Y) y_max = neigh->deformedPos().Y;
			if(z_max<neigh->deformedPos().Z) z_max = neigh->deformedPos().Z;		
		}
		
		container con(x_min-tol,x_max+tol,y_min-tol,y_max+tol,z_min-tol,z_max+tol,n_x,n_y,n_z,false,false,false,8);

		for(int s = 0; s < storage.size(); ++s)
		{
			const SamplingAtom* atom = storage.at(s);
			con.put(s,atom->deformedPos().X,atom->deformedPos().Y,atom->deformedPos().Z);
		}
		
		FloatType volume = (FloatType) con.cellVolume(0);	

		FloatType suum = 0.0;
		if(volume > maxVol)
		{
			for(int s=0; s < storage.size(); ++s)
				if(con.cellVolume(s) < maxVol)
					suum+= con.cellVolume(s);
		    volume = (FloatType) suum/storage.size();
		}
		
		atom1->samplingNode()->setAtomicVolume(volume);
		atom1->setAtomicVolume(volume);
		storage.clear();
		con.clear();
	}

// Calculate electron density and the embedding energy derivative
// for each sampling atom.
//#pragma omp parallel for num_threads(numThread)
	for(int i=0; i<mesh().samplingAtoms().size(); ++i)
	{
		SamplingAtom* atom1 = mesh().samplingAtoms()[i];
		// The electron density at the location of atom 1.
		FloatType rho = 0;		
		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter)
		{
			const SamplingAtom* atom2 = *neighborIter;
		
			// Compute distance vector to neighbor.
			const FloatType distSquared = DistanceSquared(atom1->deformedPos(), atom2->deformedPos());
			if(distSquared >= potentialCutoffRadiusSquared) continue;
			MAFEM_ASSERT_MSG(distSquared > FLOATTYPE_EPSILON, "ForceQCCalculator::calculateForces()", "Atoms too close together.");
			const FloatType dist = sqrt(distSquared);
		
			// Compute the contribution of atom 2 to the electron density at the location of atom 1.
			rho += potential->Rho_of_r.eval(dist);
		}
	
		// Compute embedding energy derivative.
		FloatType derivativeF;
		potential->F_of_Rho.eval(rho, &derivativeF);
		atom1->setEmbeddingEnergyDerivative(derivativeF);
	}

//#pragma omp parallel for num_threads(numThread)
	for(int i=0; i<mesh().samplingAtoms().size(); ++i)
	{
		SamplingAtom* atom1 = mesh().samplingAtoms()[i];	
		
		if(!atom1->samplingNode()) continue;  
	  	FloatType vol = atom1->samplingNode()->getAtomicVolume();
		for(int r=0; r<6; ++r)
			atom1->samplingNode()->setStressComponent(r,0.0);
		
		// Loop over all neighbors.
		
		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter)
		{
			const SamplingAtom* atom2 = *neighborIter;
			
			// Compute distance vector to neighbor.
			const Vector3 r = atom1->deformedPos() - atom2->deformedPos();
			const FloatType distSquared = LengthSquared(r);
			if(distSquared >= potentialCutoffRadiusSquared) continue;
			MAFEM_ASSERT_MSG(distSquared > FLOATTYPE_EPSILON, "ForceQCCalculator::calculateForces()", "Atoms too close together.");
			const FloatType dist = sqrt(distSquared);
			
			Vector3 phiGradient = NULL_VECTOR;
			Vector3 rhoGradient = NULL_VECTOR;
			
			// Calculate pair energy term.
			FloatType derivative1;
			const FloatType Z = potential->U_of_r.eval(dist, &derivative1);
			const FloatType pairEnergy = Z / dist;
			
			// Compute gradient of pair energy.
			derivative1 = (derivative1 - pairEnergy)/distSquared;
			phiGradient = (derivative1) * r;

			MAFEM_ASSERT_MSG(isfinite(derivative1), "EnergyQCCalculator::calculateEnergyAndForces()", QString("The pair functional derivative is Not-a-Number. Pair distance was %1").arg(dist).toLatin1().constData());
			
			FloatType unit = 1.602*1e2;  //1.602*1e-10*1e-9;
			RepAtom* rep = atom1->samplingNode();                                                         
			rep->setStressComponent(0, rep->getStressComponent(0) + ((0.5*r[0]*phiGradient[0]*unit)/vol));
			rep->setStressComponent(1, rep->getStressComponent(1) + ((0.5*r[1]*phiGradient[1]*unit)/vol));
			rep->setStressComponent(2, rep->getStressComponent(2) + ((0.5*r[2]*phiGradient[2]*unit)/vol));
			rep->setStressComponent(3, rep->getStressComponent(3) + ((0.5*r[0]*phiGradient[1]*unit)/vol));
			rep->setStressComponent(4, rep->getStressComponent(4) + ((0.5*r[0]*phiGradient[2]*unit)/vol));
			rep->setStressComponent(5, rep->getStressComponent(5) + ((0.5*r[1]*phiGradient[2]*unit)/vol));
		    
			// Compute the contribution of atom 2 to the electron density at the location of atom 1.
			FloatType derivativeRho;
			potential->Rho_of_r.eval(dist, &derivativeRho);
			MAFEM_ASSERT(isfinite(derivativeRho));
			rhoGradient = (derivativeRho / dist) * r;

			rep->setStressComponent(0, rep->getStressComponent(0) + ((0.5*r[0]*rhoGradient[0]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
			rep->setStressComponent(1, rep->getStressComponent(1) + ((0.5*r[1]*rhoGradient[1]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
			rep->setStressComponent(2, rep->getStressComponent(2) + ((0.5*r[2]*rhoGradient[2]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
			rep->setStressComponent(3, rep->getStressComponent(3) + ((0.5*r[0]*rhoGradient[1]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
			rep->setStressComponent(4, rep->getStressComponent(4) + ((0.5*r[0]*rhoGradient[2]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));
			rep->setStressComponent(5, rep->getStressComponent(5) + ((0.5*r[1]*rhoGradient[2]*(atom1->embeddingEnergyDerivative()+atom2->embeddingEnergyDerivative())*unit)/vol));	
		}
	}

	for(int i=0; i<mesh().samplingAtoms().size(); ++i)
	{
		SamplingAtom* atom1 = mesh().samplingAtoms()[i];	
		
		if(!atom1->samplingNode()) continue;  
		for(int j= 0; j < 6; ++j)
			atom1->setStressComponent(j,atom1->samplingNode()->getStressComponent(j));
	}
}

/******************************************************************************
 * This calculates the stiffness matrix.
 *****************************************************************************/
void Calculator::calculateStiffness(CalculationResult& output)
{
	MsgLogger() << logdate << "Calculating stiffness matrix." << endl;
	
	bool useEmbedding = true;
	
	int numNodes = mesh().repatoms().size();
	
	/// Allocate Stiffness Matrix
	if (output.analyticalCRSStiffnessMatrix.N > 0)
		output.analyticalCRSStiffnessMatrix.deallocate_elements();
	output.analyticalCRSStiffnessMatrix.allocate_elements(numNodes * NDOF, 1);

	// Reset output fields
//	output.analyticalStiffnessMatrix().resize(pow(mesh().repatoms().size(), 2));
//	memset(output.analyticalStiffnessMatrix().data(), 0, output.analyticalStiffnessMatrix().size() * sizeof(Matrix3));
	
	progress_display progress(mesh().samplingAtoms().size());
	
	const EAMPotential* potential = simulation()->potential().get();
	const double potentialCutoffRadiusSquared = square(potential->cutoffRadius);
	
	struct LoopStruct
	{
		const SamplingAtom* neighbor;
		Vector3 r_kl;
		double dist;
		double derivative1Rho;
		double derivative2Rho;
	};
	LoopStruct loopStruct[MAFEM_MAXIMUM_NEIGHBOR_COUNT];

	// Loop over all sampling atoms k.
	for (int i = 0; i < mesh().samplingAtoms().size(); ++i)
	{
		const SamplingAtom* atom_k = mesh().samplingAtoms()[i];
		
		// Skip passive atoms.
		if (atom_k->isPassive()) continue;
						
		// The electron density and its derivative at the location of atom 1.
		double sumRho = 0;
		
		// First loop over all neighbors l.
		LoopStruct* ls = loopStruct;
		for (SamplingAtom** neighborIter = atom_k->neighborsBegin(); neighborIter != atom_k->neighborsEnd(); ++neighborIter)
		{
			//MsgLogger() << "write: " << ls << endl;
			
			const SamplingAtom* atom_l = *neighborIter;
			ls->neighbor = atom_l;
			
			// Compute distance vector to neighbor and save to struct.
			ls->r_kl = atom_k->deformedPos() - atom_l->deformedPos();
				
			const double distSquared = LengthSquared(ls->r_kl);
			if (distSquared >= potentialCutoffRadiusSquared) continue;			
			ls->dist = sqrt(distSquared);
	
			// Compute the contribution of atom 2 to the electron density at the location of atom 1 and its first and second derivative.
			sumRho += potential->Rho_of_r.eval(ls->dist, &(ls->derivative1Rho), &(ls->derivative2Rho));
			
			++ls;
		}
		
		// The first and second derivative of the F functional (embeddig energy).
		double derivative1F, derivative2F;
		potential->F_of_Rho.eval(sumRho, &derivative1F, &derivative2F);
		
		
		
		// Second loop over all neighbors.
		const LoopStruct* lsend = ls;
		for (ls = loopStruct; ls != lsend; ++ls)
		{
			//MsgLogger() << "read: " << ls << endl;
			const SamplingAtom* atom_l = ls->neighbor;
			
			// Compute the pair potential and its first and second derivative.
			double derivative1U, derivative2U;
			const double U = potential->U_of_r.eval(ls->dist, &derivative1U, &derivative2U);
			
			// Compute the pair energy and its first and second derivative.
			double derivative1V = derivative1U / ls->dist - U / (ls->dist * ls->dist);
			double derivative2V = (derivative2U - 2 * derivative1V) / ls->dist;
			
			// Compute the first and second derivative of the atomic energy.
			double derivative1E = derivative1V / 2;
			double derivative2E = derivative2V / 2;
			if (useEmbedding)
			{
				derivative1E += derivative1F * ls->derivative1Rho;
				derivative2E += derivative1F * ls->derivative2Rho;
			}
			
			// Compute the second-order partial derivative of the atomic energy.
			Matrix3 hessE = (derivative1E / ls->dist) * Matrix3(IDENTITY) + ((derivative2E / (ls->dist * ls->dist)) - (derivative1E / pow(ls->dist, 3))) * Dyadic(ls->r_kl, ls->r_kl);
			
			int affectedNodes[2 * NEN];
			double phi_k[2 * NEN];
			double phi_l[2 * NEN];
			int numAffectedNodes = getAffectedNodes(atom_k, atom_l, &affectedNodes[0], &phi_k[0], &phi_l[0]);
			double barycentricFactor;
			Matrix3 localStiffness;
			
			for (int a = 0; a < numAffectedNodes; ++a)
			{
				for (int b = 0; b < numAffectedNodes; ++b)
				{
					barycentricFactor = (phi_k[a] - phi_l[a]) * (phi_k[b] - phi_l[b]);
//					if (barycentricFactor)
					localStiffness = hessE * barycentricFactor * atom_k->weight();
					output.analyticalCRSStiffnessMatrix.add_matrix((double*)(&localStiffness), NDOF, NDOF * affectedNodes[a], NDOF * affectedNodes[b]); // CRS

				}
			}
			
			if (useEmbedding)
			{
				LoopStruct* ls2;
				for (ls2 = loopStruct; ls2 != lsend; ++ls2)
				{
					const SamplingAtom* atom_l2 = ls2->neighbor;
					
					Matrix3 hessE2 = atom_k->weight() * derivative2F * ls->derivative1Rho * ls2->derivative1Rho / (ls->dist * ls2->dist) * Dyadic(ls->r_kl, ls2->r_kl);
					
					int affectedNodes2[2 * NEN];
					double phi_k2[2 * NEN];
					double phi_l2[2 * NEN];
					int numAffectedNodes2 = getAffectedNodes(atom_k, atom_l2, &affectedNodes2[0], &phi_k2[0], &phi_l2[0]);
					
					int sharedNodes[numAffectedNodes + numAffectedNodes2][2];
					int numSharedNodes = 0;
					for (int j = 0; j < numAffectedNodes; ++j)
						for (int k = 0; k < numAffectedNodes2; ++k)
							if (affectedNodes[j] == affectedNodes2[k])
							{
								sharedNodes[numSharedNodes][0] = j;
								sharedNodes[numSharedNodes][1] = k;
								++numSharedNodes;
							}
					int numAffectedNodesUnion = numAffectedNodes + numAffectedNodes2 - numSharedNodes;
					
					int affectedNodesUnion[numAffectedNodesUnion];
					double phi_kUnion[numAffectedNodesUnion];
					double phi_l1Union[numAffectedNodesUnion];
					double phi_l2Union[numAffectedNodesUnion];
					int n = 0;
					for (int j = 0; j < numAffectedNodes; ++j)
					{
						bool sharedNode = false;
						for (int k = 0; k < numSharedNodes; ++k)
							if (j == sharedNodes[k][0])
								sharedNode = true;
						if (sharedNode == false)
						{
							affectedNodesUnion[n] = affectedNodes[j];
							phi_kUnion[n] = phi_k[j];
							phi_l1Union[n] = phi_l[j];
							phi_l2Union[n] = 0;
							++n;
						}
					}
							
					for (int j = 0; j < numSharedNodes; ++j)
					{
						affectedNodesUnion[n] = affectedNodes[sharedNodes[j][0]];
						phi_kUnion[n] = phi_k[sharedNodes[j][0]];
						phi_l1Union[n] = phi_l[sharedNodes[j][0]];
						phi_l2Union[n] = phi_l2[sharedNodes[j][1]];
						++n;
					}
					
					for (int j = 0; j < numAffectedNodes2; ++j)
					{
						bool sharedNode = false;
						for (int k = 0; k < numSharedNodes; ++k)
							if (j == sharedNodes[k][1])
								sharedNode = true;
						if (sharedNode == false)
						{
							affectedNodesUnion[n] = affectedNodes2[j];
							phi_kUnion[n] = phi_k2[j];
							phi_l1Union[n] = 0;
							phi_l2Union[n] = phi_l2[j];
							++n;
						}
					}
					
					double barycentricFactor2;
					for (int a = 0; a < numAffectedNodesUnion; ++a)
					{
						for (int b = 0; b < numAffectedNodesUnion; ++b)
						{
							barycentricFactor2 = (phi_kUnion[a] - phi_l2Union[a]) * (phi_kUnion[b] - phi_l1Union[b]);
							localStiffness = hessE2 * barycentricFactor2;
							output.analyticalCRSStiffnessMatrix.add_matrix((double*)(&localStiffness), NDOF, NDOF * affectedNodesUnion[a], NDOF * affectedNodesUnion[b]); // CRS
						}
					}
				}
			}
		}
		++progress;
	}
	
	/// Apply displacement boundary conditions.
	std::vector<int> dirichletNodes;
	for (size_t i = 0; i < numNodes; ++i)
		for (DisplacementBoundaryConditionList::const_iterator displBC = simulation()->displacementBC().constBegin(); displBC != simulation()->displacementBC().constEnd(); ++displBC)
			if (mesh().repatoms()[i]->belongsToGroup((*displBC)->group()))
			{
				dirichletNodes.push_back(i);
				break;
			}
	int numDirichletNodes = dirichletNodes.size();
	cout << "Apply displacement boundary conditions for " << numDirichletNodes << " nodes." << endl;

	if (numDirichletNodes)
	{
		index_matrix dirichlet;
		dirichlet.allocate_and_clear(numDirichletNodes, 1 + NDOF);
		for (int i = 0; i < numDirichletNodes; ++i)
		{
			dirichlet.element[i][0] = dirichletNodes[i];
			for (DisplacementBoundaryConditionList::const_iterator displBC = simulation()->displacementBC().constBegin(); displBC != simulation()->displacementBC().constEnd(); ++displBC)
				if (mesh().repatoms()[dirichletNodes[i]]->belongsToGroup((*displBC)->group()))
					for (int dof = 0; dof < NDOF; ++dof)
						dirichlet.element[i][dof + 1] |= (int)(*displBC)->isFixed(dof);
//							if((*displBC)->isFixed(dof))
//								dirichlet.element[i][dof + 1] = 1;
		}
		
		output.analyticalCRSStiffnessMatrix.dirichlet_boundary(dirichlet); // CRS
		dirichlet.deallocate();
	}
	output.analyticalCRSStiffnessMatrix.PrintInfo();
/*
	matrix fullMatrix = output.analyticalCRSStiffnessMatrix.MakeFullMatrix();
	char *file_name;
	file_name = "/home/gk684/MAFEM_2013_09_01/InputFiles/Test/NewStiffnessTest/analyticalStima_new_new.dat";
	matrix_write_to_file(fullMatrix, file_name);

	matrix fullnumericalMatrix = output.numericalCRSStiffnessMatrix.MakeFullMatrix();
	
	matrix_mul_skalar(fullnumericalMatrix.element[0],-1,fullnumericalMatrix.M,fullnumericalMatrix.N);
	matrix_add(fullMatrix.element[0], fullnumericalMatrix.element[0], fullnumericalMatrix.M,fullnumericalMatrix.N);
	
	file_name = "/home/gk684/MAFEM_2013_09_01/InputFiles/Test/NewStiffnessTest/analytical-numerical_new_new.dat";
	matrix_write_to_file(fullMatrix, file_name);
*/
}

int Calculator::getAffectedNodes(const SamplingAtom* atom_k, const SamplingAtom* atom_l, int* affectedNodes, double* phi_k, double* phi_l)
{
	int numAffectedNodes;
	if (atom_k->samplingNode()) // The sampling atom k is a mesh node itself.
	{
		const RepAtom* repatom_k = atom_k->samplingNode()->realRepatom();
		if (atom_l->samplingNode()) // The neighbor atom l is a mesh node itself.
		{
			const RepAtom* repatom_l = atom_l->samplingNode()->realRepatom();
			numAffectedNodes = 2;
			
			*affectedNodes++ = repatom_k->index();
			*phi_k++ = 1;
			*phi_l++ = 0;
			
			*affectedNodes++ = repatom_l->index();
			*phi_k++ = 0;
			*phi_l++ = 1;
		}
		else // The neighbor atom l is inside of an element.
		{
			numAffectedNodes = NEN + 1;
			for (int v = 0; v < NEN; ++v)
				if (repatom_k == atom_l->element()->vertex(v)->realRepatom())
					--numAffectedNodes;
			
			for (int v = 0; v < NEN; ++v)
			{
				*affectedNodes++ = atom_l->element()->vertex(v)->realRepatom()->index();
				if (atom_l->element()->vertex(v)->realRepatom()->index() == repatom_k->index())
					*phi_k++ = 1;
				else
					*phi_k++ = 0;
				*phi_l++ = atom_l->barycentric()[v];
			}
			
			if (numAffectedNodes == NEN + 1)
			{
				*affectedNodes++ = repatom_k->index();
				*phi_k++ = 1;
				*phi_l++ = 0;
			}
		}
	}
	else // The samplig atom k is inside of an element.
	{
		if (atom_l->samplingNode()) // The neighbor atom l is a mesh node itself.
		{
			const RepAtom* repatom_l = atom_l->samplingNode()->realRepatom();
			numAffectedNodes = NEN + 1;
			for (int v = 0; v < NEN; ++v)
				if (repatom_l == atom_k->element()->vertex(v)->realRepatom())
					--numAffectedNodes;
			
			for (int v = 0; v < NEN; ++v)
			{
				*affectedNodes++ = atom_k->element()->vertex(v)->realRepatom()->index();
				*phi_k++ = atom_k->barycentric()[v];
				if (atom_k->element()->vertex(v)->realRepatom()->index() == repatom_l->index())
					*phi_l++ = 1;
				else
					*phi_l++ = 0;
			}
			
			if (numAffectedNodes == NEN + 1)
			{
				*affectedNodes++ = repatom_l->index();
				*phi_k++ = 0;
				*phi_l++ = 1;
			}
		}
		else // The neighbor atom l is inside of an element.
		{
			int sharedNodes[NEN][2];
			int numSharedNodes = 0;
			for (int v1 = 0; v1 < NEN; ++v1)
				for (int v2 = 0; v2 < NEN; ++v2)
					if (atom_k->element()->vertex(v1) == atom_l->element()->vertex(v2))
					{
						sharedNodes[numSharedNodes][0] = v1;
						sharedNodes[numSharedNodes][1] = v2;
						++numSharedNodes;
					}
			numAffectedNodes = 2 * NEN - numSharedNodes;
			
			for (int v = 0; v < NEN; ++v)
			{
				bool sharedVertex = false;
				for (int i = 0; i < numSharedNodes; ++i)
					if (v == sharedNodes[i][0])
						sharedVertex = true;
				if (sharedVertex == false)
				{
					*affectedNodes++ = atom_k->element()->vertex(v)->realRepatom()->index();
					*phi_k++ = atom_k->barycentric()[v];
					*phi_l++ = 0;
				}
			}
					
			for (int i = 0; i < numSharedNodes; ++i)
			{
				*affectedNodes++ = atom_k->element()->vertex(sharedNodes[i][0])->realRepatom()->index();
				*phi_k++ = atom_k->barycentric()[sharedNodes[i][0]];
				*phi_l++ = atom_l->barycentric()[sharedNodes[i][1]];
			}
			
			for (int v = 0; v < NEN; ++v)
			{
				bool sharedVertex = false;
				for (int i = 0; i < numSharedNodes; ++i)
					if (v == sharedNodes[i][1])
						sharedVertex = true;
				if (sharedVertex == false)
				{
					*affectedNodes++ = atom_l->element()->vertex(v)->realRepatom()->index();
					*phi_k++ = 0;
					*phi_l++ = atom_l->barycentric()[v];
				}
			}
		}
	}
	return numAffectedNodes;
}

int Calculator::calculateEigenvalues(CalculationResult& output, FloatType min, FloatType max, int n, char uplo = 'U')
{
#if buildLocationCluster
	time_t now = time(0);
	char* dt = ctime(&now);
	cout << dt << endl;
	MsgLogger() << logdate << "Calculating eigenvalues for ";
	if (uplo == 'U')
		cout << "upper";
	else if (uplo == 'L')
		cout << "lower";
	else if (uplo == 'A')
		cout << "arithmetic averaged upper";
	else if (uplo == 'G')
		cout << "geometric averaged upper";
	cout << " triangular matrix." << endl;
	
	// Buffer for the solver results
	matrix Eigenvalues;
	matrix Eigenvectors;

	// Run the Feast solver
	bool SilentMode = true;

	int NumberOfEigenvaluesFound = feast_solve_system(output.analyticalCRSStiffnessMatrix, Eigenvalues, Eigenvectors, min, max, uplo, SilentMode);
	
	// Reduce the Number of Eigenvalues found
	NumberOfEigenvaluesFound = std::min(NumberOfEigenvaluesFound, n);
	
	// Allocate memory for the copy target destination
	output.setNumEvals(NumberOfEigenvaluesFound);
	
	// Copy Eigenvalues
	FloatType* evalsArray = output.evals().data();
	memcpy(evalsArray, Eigenvalues.element[0], sizeof(FloatType) * NumberOfEigenvaluesFound);
	
	// Copy Eigenvectors
	Vector3* evecsArray = output.evecs().data();
	memcpy(evecsArray, Eigenvectors.element[0], sizeof(FloatType) * Eigenvectors.N * NumberOfEigenvaluesFound);
	
	// Eigenvalues
	cout << "Eigenvalues" << endl;
	for(int i = 0; i < NumberOfEigenvaluesFound; ++i)
		cout << output.evals()[i] << endl;
	cout << endl;
/*	
	// Eigenvectors
	cout << "Eigenvectors" << endl;
	for(int i = 0; i < numNodes * NumberOfEigenvaluesFound; ++i)
		cout << output.evecs()[i] << endl;
	cout << endl;
*/
	return NumberOfEigenvaluesFound;
#else
	MsgLogger() << "Eigenvalue calculation not possible." << endl << "Please compile on HorUS Cluster." << endl;
	return 0;
#endif
}

bool Calculator::checkPositiveDefiniteForm(CalculationResult& output)
{
/*
	MsgLogger() << "Testing for definite quadratic form." << endl;
	
	int numNodes = mesh().repatoms().size();
	
	gsl_matrix *stiffnessMatrix = gsl_matrix_alloc (numNodes * NDOF, numNodes * NDOF);
	for(int i = 0; i < numNodes; ++i)
	{
		for(int j = 0; j < numNodes; ++j)
		{
			for(int iDof = 0; iDof < NDOF; ++iDof)
			{
				for(int jDof = 0; jDof < NDOF; ++jDof)
				{
					//gsl_complex z;// = gsl_complex_rect(output.numericalStiffnessMatrix()[j * numNodes + i](iDof, jDof), 0);
					//GSL_SET_REAL(&z, output.numericalStiffnessMatrix()[j * numNodes + i](iDof, jDof));
					double z = output.numericalStiffnessMatrix()[j * numNodes + i](iDof, jDof);
					gsl_matrix_set(stiffnessMatrix, (i * NDOF + iDof), (j * NDOF + jDof), z);
				}
			}
		}
	}
	
	gsl_set_error_handler_off();
	int gslerr = gsl_linalg_cholesky_decomp(stiffnessMatrix);
	if(gslerr == 0)
	{
		MsgLogger() << "Stiffness matrix is positive definite." << endl;
		return true;
	}
	else
	{
		MsgLogger() << "Stiffness matrix is not positive definite. Error code" << gslerr << endl;
		return false;
	}
*/
	return false;
}

int Calculator::kroneckerDelta(int i, int j)
{
  if(i==j) return 1;
  return 0;
}

}; // End of namespace MAFEM
