///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_CALCULATION_RESULT_H
#define __MAFEM_CALCULATION_RESULT_H

#include <MAFEM.h>
#include <simulation/SimulationResource.h>
#include <simulation/Simulation.h>
#include <FEAST/matrix_sparse.h>

namespace MAFEM {

/**
 * \brief This class stores the results of the energy/force calculation. 
 * 
 * \author Alexander Stukowski
 */
class CalculationResult : public SimulationResource
{
public:
	
	/// \brief Default constructor.
	CalculationResult() : _totalEnergy(0),numCentroAtoms(0) {}

	/// Returns the computed total energy of the system.
	FloatType totalEnergy() const { return _totalEnergy; }
	
	/// Sets the total energy value.
	void setTotalEnergy(FloatType e) { _totalEnergy = e; }
	
	/// Returns the computed internal repatom forces
	const QVector<Vector3>& forces() const { return _forces; }
	
	/// Returns the internal force vector
	QVector<Vector3>& forces() { return _forces; }
	
	/// Returns the computed external repatom reforces
	const QVector<Vector3>& reforces() const { return _reforces; }

	/// Returns the external force vector
	QVector<Vector3>& reforces() { return _reforces; }
	
	/// Returns the computed inernal energy
	const QVector<FloatType>& internalEnergy() const { return _internalEnergy; }

	/// Returns the internal internal energy
	QVector<FloatType>& internalEnergy() { return _internalEnergy; }

	/// Returns the computed external energy
	const QVector<FloatType>& externalEnergy() const { return _externalEnergy; }

	/// Returns the internal external energy
	QVector<FloatType>& externalEnergy() { return _externalEnergy; }
	
	/// Returns the centrosymmetry parameter
	const QVector<FloatType>& centrosymmetry() const { return _centrosymmetry; }
	
	/// Returns the centrosymmetry parameter
	QVector<FloatType>& centrosymmetry() { return _centrosymmetry; }

	void countCentroSymmetryAtoms() {numCentroAtoms+=1;}

	/// Returns the number of centroSymmetry atoms
	int getCentroSymmetryAtoms() { return numCentroAtoms; }

	void setCentroSymBox(Box3 box) { centroSymBox = box;}

	Box3 getCentroSymBox() { return centroSymBox;}
	
	/// Returns the analytical stiffness matrix
	sparse_CRS_matrix analyticalCRSStiffnessMatrix;
	sparse_CRS_matrix numericalCRSStiffnessMatrix;
	
	/// Changes the eigenvectors
	void setEvec(int n, size_t index, Vector3 evec) { _evecs[_forces.size() * n + index] = evec; }
	
	void setEval(int index, FloatType eval) { _evals[index] = eval; }
	
	/// Resizes the eigenvectors
	void setNumEvals(int numEvals)
	{
		_evals.resize(numEvals);
		memset(_evals.data(), 0, _evals.size() * sizeof(FloatType));
		
		_evecs.resize(_forces.size() * numEvals);
		memset(_evecs.data(), 0, _evecs.size() * sizeof(Vector3));
	}
	
	/// Returns the eigenvectors
	QVector<Vector3>& evecs() { return _evecs; }
	
	/// Returns the eigencalues
	QVector<FloatType>& evals() { return _evals; }

private:
	
	/// The total energy of the system.
	FloatType _totalEnergy;
	
	/// This stores the internal forces on the repatoms.
	QVector<Vector3> _forces;
	
	/// This stores the external forces on the repatoms.
	QVector<Vector3> _reforces;

	/// This stores the internal energy
	QVector<FloatType> _internalEnergy;

	/// This stores the internal energy
	QVector<FloatType> _externalEnergy;
	
	/// This stores the centrosymmetry parameter
	QVector<FloatType> _centrosymmetry;
	
	/// This stores number of atoms for centroSymmetry parameter
	int numCentroAtoms;

	/// This stores the centroSymmetry box
	Box3 centroSymBox;
	
	/// This stores the eigenvalues.
	QVector<FloatType> _evals;
	
	/// This stores the eigenvectors.
	QVector<Vector3> _evecs;
	
public:
	QVector<SamplingAtom*> centroatoms;
};

}; // End of namespace MAFEM

#endif // __MAFEM_CALCULATION_RESULT_H
