///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "LatticeStaticsCalculator.h"
#include <atoms/SamplingAtom.h>
#include <material/EAMPotential.h>

namespace MAFEM {

/******************************************************************************
 * This performs the full calculation of the forces and total energy.
 *****************************************************************************/
void LatticeStaticsCalculator::calculateEnergyAndForcesImpl(CalculationResult& output)
{
	// For quick access to the potential.
	const EAMPotential* potential = simulation()->potential().get();
	const FloatType potentialCutoffRadiusSquared = square(potential->cutoffRadius);
	
	// For fast looping over the boundary conditions.	
	const ForceBoundaryConditionList::const_iterator forceBCbegin = simulation()->forceBC().constBegin();
	const ForceBoundaryConditionList::const_iterator forceBCend = simulation()->forceBC().constEnd();
	
	// Check if lattice statics mode is enabled.
	if(!simulation()->settings().latticeStaticsMode)
		throw Exception("The LatticeStaticsCalculator can only be used for simulations with 'LatticeStaticsMode' enabled.");

	FloatType totalEnergy = 0;
	Vector3* forceArray = output.forces().data();
	
	// This stores the electron density for each site.
	vector<FloatType> rhoSites(mesh().repatoms().size());
	// This stores the derivate of the embedding energy for each site.
	vector<FloatType> Fderiv(mesh().repatoms().size());

	Q_FOREACH(const SamplingAtom* atom1, mesh().samplingAtoms()) {
		if(atom1->isPassive()) continue;
		
		// The electron density and its derivative at the location of atom 1.
		FloatType rho = 0;
		Vector3 rhoGrad = NULL_VECTOR;
		
		// The force acting on atom 1 and its energy. 
		Vector3 localInternalForce = NULL_VECTOR;
		FloatType localInternalEnergy = 0;
		
		// Loop over all neighbors.
		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter) {
			const SamplingAtom* atom2 = *neighborIter;
			
			// Compute distance vector to neighbor.
			const Vector3 r = atom1->deformedPos() - atom2->deformedPos();			
			const FloatType distSquared = LengthSquared(r);
			if(distSquared >= potentialCutoffRadiusSquared) continue;			
			const FloatType dist = sqrt(distSquared);
			
			// Calculate pair energy term.
			FloatType derivative1;
			const FloatType Z = potential->U_of_r.eval(dist, &derivative1);
			const FloatType pairEnergy = Z / dist;
			localInternalEnergy += 0.5 * pairEnergy;

			// Compute gradient of pair energy.
			derivative1 = (derivative1 - pairEnergy)/distSquared;
			localInternalForce -= derivative1 * r;
			
			// Compute the contribution of atom 2 to the electron density at the location of atom 1.
			FloatType rhoDerivative;
			rho += potential->Rho_of_r.eval(dist, &rhoDerivative);
			rhoGrad += (rhoDerivative / dist) * r;
		} 

		// Compute embedding energy term.
		FloatType derivativeF;
		const RepAtom* repatom1 = atom1->clusterRepatom();
		localInternalEnergy += potential->F_of_Rho.eval(rho, &derivativeF);
		localInternalForce -= derivativeF * rhoGrad;
		MAFEM_ASSERT(repatom1->index() >= 0 && repatom1->index() < rhoSites.size());
		rhoSites[repatom1->index()] = rho;
		Fderiv[repatom1->index()] = derivativeF;

		// Calculate forces imposed on this atom by the boundary conditions.
		for(ForceBoundaryConditionList::const_iterator forceBC = forceBCbegin; forceBC != forceBCend; ++forceBC) {
			if(atom1->belongsToGroup((*forceBC)->group()))
				localInternalEnergy += (*forceBC)->calculateEnergyAndForces(atom1, localInternalForce);
		}

		MAFEM_ASSERT(atom1->samplingNode() != NULL);
		MAFEM_ASSERT(repatom1 == atom1->samplingNode()->realRepatom());
		MAFEM_ASSERT(repatom1->index() >= 0 && repatom1->index() < output.forces().size());
		MAFEM_ASSERT(repatom1->clusterWeight() == 1.0);
		forceArray[repatom1->index()] += localInternalForce;

		totalEnergy += localInternalEnergy;
	}

	Q_FOREACH(const SamplingAtom* atom1, mesh().samplingAtoms()) {
		if(atom1->isPassive()) continue;
		// Loop over all neighbors.
		const RepAtom* repatom1 = atom1->clusterRepatom();
		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter) {
			const SamplingAtom* atom2 = *neighborIter;
			MAFEM_ASSERT(atom2->samplingNode() != NULL);
			const RepAtom* repatom2 = atom2->samplingNode()->realRepatom();
						
			// Compute distance vector to neighbor.
			const Vector3 r = atom1->deformedPos() - atom2->deformedPos();			
			const FloatType distSquared = LengthSquared(r);
			if(distSquared >= potentialCutoffRadiusSquared) continue;			
			const FloatType dist = sqrt(distSquared);		
			
			FloatType rhoDerivative;
			potential->Rho_of_r.eval(dist, &rhoDerivative);
			
			MAFEM_ASSERT(repatom2->index() >= 0 && repatom2->index() < Fderiv.size());
			forceArray[repatom1->index()] -= (rhoDerivative / dist * Fderiv[repatom2->index()]) * r;
		}
	}
	
	output.setTotalEnergy(totalEnergy);	
}

/******************************************************************************
 * This performs only the calculation of the energy.
 *****************************************************************************/
void LatticeStaticsCalculator::calculateEnergyImpl(CalculationResult& output)
{
	// For quick access to the potential.
	const EAMPotential* potential = simulation()->potential().get();
	const FloatType potentialCutoffRadiusSquared = square(potential->cutoffRadius);

	// For fast looping over the boundary conditions.	
	const ForceBoundaryConditionList::const_iterator forceBCbegin = simulation()->forceBC().constBegin();
	const ForceBoundaryConditionList::const_iterator forceBCend = simulation()->forceBC().constEnd();

	// Check if lattice statics mode is enabled.
	if(!simulation()->settings().latticeStaticsMode)
		throw Exception("The LatticeStaticsCalculator can only be used for simulations with 'LatticeStaticsMode' enabled.");
	
	FloatType totalEnergy = 0;
	
	Q_FOREACH(const SamplingAtom* atom1, mesh().samplingAtoms()) {
		
		// Skip passive atoms.
		if(atom1->isPassive()) continue;
		
		// The electron density at the location of atom 1.
		FloatType rho = 0;
		
		FloatType localInternalEnergy = 0;
		
		// Loop over all neighbors.
		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter) {
			const SamplingAtom* atom2 = *neighborIter;
			
			// Compute distance vector to neighbor.
			const Vector3 r = atom1->deformedPos() - atom2->deformedPos();			
			const FloatType distSquared = LengthSquared(r);
			if(distSquared >= potentialCutoffRadiusSquared) continue;			
			const FloatType dist = sqrt(distSquared);
			
			// Calculate pair energy term.
			const FloatType pairEnergy = 0.5 * potential->U_of_r.eval(dist) / dist;
			localInternalEnergy += pairEnergy;
			
			// Compute the contribution of atom 2 to the electron density at the location of atom 1.
			rho += potential->Rho_of_r.eval(dist);
		} 

		// Compute embedding energy term.
		localInternalEnergy += potential->F_of_Rho.eval(rho);

		// Calculate energy contribution for this atom from the boundary conditions.
		for(ForceBoundaryConditionList::const_iterator forceBC = forceBCbegin; forceBC != forceBCend; ++forceBC) {
			if(atom1->belongsToGroup((*forceBC)->group()))
				localInternalEnergy += (*forceBC)->calculateEnergy(atom1);
		}

		// Get the central cluster atom.
		totalEnergy += localInternalEnergy;
	}
	
	output.setTotalEnergy(totalEnergy);
}


}; // End of namespace MAFEM
