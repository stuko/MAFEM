///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_ENERGY_QC_CALCULATOR_H
#define __MAFEM_ENERGY_QC_CALCULATOR_H

#include <MAFEM.h>
#include "Calculator.h"

namespace MAFEM {

/**
 * \brief This class calculates the forces acting on the repatoms using the 
 *        energy based fully nonlocal QC scheme. 
 * 
 * \author Alexander Stukowski
 */
class EnergyQCCalculator : public Calculator
{
public:
	
	/// \brief Constructor.
	/// \params sim The global simulation object.  
	EnergyQCCalculator(Simulation* sim) : Calculator(sim) {}
	
protected: 
	
	/// \brief This performs the full calculation of the forces.
	/// \param output The method will store the calculated forces in this object.
	virtual void calculateEnergyAndForcesImpl(CalculationResult& output);

	/// \brief This performs only the calculation of the total energy.
	/// \param output The method will store the calculated energy in this object.
	virtual void calculateEnergyImpl(CalculationResult& output);
	
	/// \brief This performs the calculation of the forces only.
	/// \param output The method will store the calculated forces in this object.
	virtual void calculateForcesImpl(CalculationResult& output) {
		calculateEnergyAndForcesImpl(output);
	}
};

}; // End of namespace MAFEM

#endif // __MAFEM_ENERGY_QC_CALCULATOR_H
