///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_FORCE_QC_CALCULATOR_H
#define __MAFEM_FORCE_QC_CALCULATOR_H

#include <MAFEM.h>
#include "Calculator.h"

namespace MAFEM {

/**
 * \brief This class calculates the forces acting on the repatoms using the 
 *        force based fully nonlocal QC scheme. 
 * 
 * \author Alexander Stukowski
 */
class ForceQCCalculator : public Calculator
{
public:
	
	/// \brief Constructor.
	/// \params sim The global simulation object.  
	ForceQCCalculator(Simulation* sim) : Calculator(sim) {}
	
protected: 
	
	/// \brief This performs the full calculation of the forces.
	/// \param output The method will store the calculated forces in this object.
	virtual void calculateEnergyAndForcesImpl(CalculationResult& output) {
		throw Exception("The force based QC method can not be used to calculate the total energy of the system.");
	}

	/// \brief This performs only the calculation of the total energy.
	/// \param output The method will store the calculated energy in this object.
	virtual void calculateEnergyImpl(CalculationResult& output) {
		throw Exception("The force based QC method can not be used to calculate the total energy of the system.");
	}
	
	/// \brief This performs the calculation of the forces only.
	/// \param output The method will store the calculated forces in this object.
	virtual void calculateForcesImpl(CalculationResult& output);
};

}; // End of namespace MAFEM

#endif // __MAFEM_FORCE_QC_CALCULATOR_H
