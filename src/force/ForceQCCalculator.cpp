///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "ForceQCCalculator.h"
#include <atoms/SamplingAtom.h>
#include <material/EAMPotential.h>

namespace MAFEM {

/******************************************************************************
 * This performs the calculation of the nodal forces.
 *****************************************************************************/
void ForceQCCalculator::calculateForcesImpl(CalculationResult& output)
{
	// For quick access to the potential.
	const EAMPotential* potential = simulation()->potential().get();
	const FloatType potentialCutoffRadiusSquared = square(potential->cutoffRadius);
	
	// For fast looping over the boundary conditions.	
	const ForceBoundaryConditionList::const_iterator forceBCbegin = simulation()->forceBC().constBegin();
	const ForceBoundaryConditionList::const_iterator forceBCend = simulation()->forceBC().constEnd();

	if(!simulation()->settings().QCForceMode)
		throw Exception("The force-based QC scheme has to be enabled using the appropriate simulation setting.");

	// Reset output array.
	Vector3* forceArray = output.forces().data();
	
	// Calculate electron density and the embedding energy derivative
	// for each sampling atom.
	Q_FOREACH(SamplingAtom* atom1, mesh().samplingAtoms()) {
		// The electron density at the location of atom 1.
		FloatType rho = 0;		
		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter) {
			const SamplingAtom* atom2 = *neighborIter;
			
			// Compute distance vector to neighbor.
			const FloatType distSquared = DistanceSquared(atom1->deformedPos(), atom2->deformedPos());
			if(distSquared >= potentialCutoffRadiusSquared) continue;
			MAFEM_ASSERT_MSG(distSquared > FLOATTYPE_EPSILON, "ForceQCCalculator::calculateForces()", "Atoms too close together.");
			const FloatType dist = sqrt(distSquared);
			
			// Compute the contribution of atom 2 to the electron density at the location of atom 1.
			rho += potential->Rho_of_r.eval(dist);
		}
		
		// Compute embedding energy derivative.
		FloatType derivativeF;
		potential->F_of_Rho.eval(rho, &derivativeF);
		atom1->setEmbeddingEnergyDerivative(derivativeF);
	}
	
	Q_FOREACH(const SamplingAtom* atom1, mesh().samplingAtoms()) {
		
		// Skip passive atoms.
		if(atom1->isPassive()) continue;
		
		// The force acting on atom 1. 
		Vector3 localInternalForce = NULL_VECTOR;
		
		// Loop over all neighbors.
		for(SamplingAtom** neighborIter = atom1->neighborsBegin(); neighborIter != atom1->neighborsEnd(); ++neighborIter) {
			const SamplingAtom* atom2 = *neighborIter;
			
			// Compute distance vector to neighbor.
			const Vector3 r = atom1->deformedPos() - atom2->deformedPos();			
			const FloatType distSquared = LengthSquared(r);
			if(distSquared >= potentialCutoffRadiusSquared) continue;			
			const FloatType dist = sqrt(distSquared);
			
			// Calculate pair energy term.
			FloatType derivative1;
			const FloatType Z = potential->U_of_r.eval(dist, &derivative1);
			const FloatType pairEnergy = Z / dist;
			derivative1 = (derivative1 - pairEnergy)/distSquared;

			// Compute gradient of pair energy.
			localInternalForce -= r * derivative1;
			
			// Compute the contribution of atom 2 to the electron density at the location of atom 1.
			FloatType rhoDerivative;
			potential->Rho_of_r.eval(dist, &rhoDerivative);
			Vector3 rhoGradient = (rhoDerivative / dist) * r;
			localInternalForce -= rhoGradient * (atom1->embeddingEnergyDerivative() + atom2->embeddingEnergyDerivative());
		}
		
		// Calculate forces imposed on this atom by the boundary conditions.
		for(ForceBoundaryConditionList::const_iterator forceBC = forceBCbegin; forceBC != forceBCend; ++forceBC) {
			if(atom1->belongsToGroup((*forceBC)->group()))
				(*forceBC)->calculateEnergyAndForces(atom1, localInternalForce);
		}
		
		// Get the central cluster atom.
		const RepAtom* repatom1 = atom1->clusterRepatom();		
			
		if(atom1->samplingNode()) {			
			MAFEM_ASSERT(repatom1 == atom1->samplingNode()->realRepatom());
			MAFEM_ASSERT(repatom1->index() >= 0 && repatom1->index() < output.forces().size());
			forceArray[repatom1->index()] += localInternalForce * repatom1->clusterWeight();
		}
		else {
			MAFEM_ASSERT(atom1->element() != NULL);
			for(int j=0; j<NEN; j++) {
				const RepAtom* vertex = atom1->element()->vertex(j)->realRepatom();
				const FloatType weight = atom1->barycentric()[j] * repatom1->clusterWeight();
				MAFEM_ASSERT(vertex->index() >= 0 && vertex->index() < output.forces().size());
				forceArray[vertex->index()] += localInternalForce * weight;
			}
		}
	}
}

}; // End of namespace MAFEM
