///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file AffineTransformation.h 
 * \brief Contains definition of Base::AffineTransformation class. 
 */

#ifndef __AFFINE_TRANSFORMATION_H
#define __AFFINE_TRANSFORMATION_H

#include <Base.h>
#include "Vector3.h"
#include "VectorN.h"
#include "Point3.h"
#include "Matrix3.h"
#include "Scaling.h"
#include "Rotation.h"
#include "Quaternion.h"

namespace Base {

/**
 * \brief A 3x4 matrix class that is used to describe affine transformations in space.
 *
 * This class stores an array of 3 times 4 floating-point value. 
 * It is therefore a matrix with 3 rows and 4 columns.
 * 
 * Such a matrix is used to describe affine transformations in 3d space
 * including any combination of rotation, shearing, scaling and translation.
 * 
 * The Matrix3 class can store affine transformations that do
 * not contain a translation component.
 * 
 * \author Alexander Stukowski
 * \sa Matrix3, Matrix4
 */
class AffineTransformation
{
private:
	/// \brief The four columns of the matrix.
	/// The fourth column contains the translation vector.
	Vector3 m[4];

public:

	/// \brief Constructs a matrix without initializing its elements.
	/// \note All elements are left uninitialized by this constructor and have therefore a random value!
	AffineTransformation() {}

	/// \brief Copy constructor that initializes the matrix elements from the elements of another matrix.
	AffineTransformation(const AffineTransformation& mat) { 
		memcpy(m, mat.m, sizeof(m)); 
	}

	/// \brief Constructor that initializes all 12 elements of the matrix to the given values.
	/// Values are given in row-major order, i.e. row by row.
	AffineTransformation(FloatType m11, FloatType m12, FloatType m13, FloatType m14,
		   FloatType m21, FloatType m22, FloatType m23, FloatType m24,
		   FloatType m31, FloatType m32, FloatType m33, FloatType m34)
	{
		m[0][0] = m11; m[0][1] = m21; m[0][2] = m31; 
		m[1][0] = m12; m[1][1] = m22; m[1][2] = m32; 
		m[2][0] = m13; m[2][1] = m23; m[2][2] = m33; 
		m[3][0] = m14; m[3][1] = m24; m[3][2] = m34; 
	}

	/// \brief Initializes the matrix to the null matrix.
	/// All matrix elements are set to zero by this constructor.
	AffineTransformation(const NullMatrix& NULL_MATRIX) {
		memset(m, 0, sizeof(m));
	}

	/// \brief Initializes the matrix to the identity matrix.
	/// All diagonal elements are set to one and all off-diagonal elements are set to zero.
	AffineTransformation(const IdentityMatrix& IDENTITY) {
		m[0].X = 1.0; m[0].Y = 0.0; m[0].Z = 0.0; 
		m[1].X = 0.0; m[1].Y = 1.0; m[1].Z = 0.0; 
		m[2].X = 0.0; m[2].Y = 0.0; m[2].Z = 1.0; 
		m[3].X = 0.0; m[3].Y = 0.0; m[3].Z = 0.0; 
	}

	/// \brief Initializes the matrix from a 3x3 matrix. 
	/// The translation component of the affine transformation is set to the null vector.
	AffineTransformation(const Matrix3& tm) {
        m[0] = tm.column(0);
		m[1] = tm.column(1);
		m[2] = tm.column(2);
		m[3] = NULL_VECTOR;
	}

	/// \brief Returns the value of a matrix element.
	/// \param row The row of the element to return (0-2). 
	/// \param col The column of the element to return (0-3).
	/// \return The value of the matrix element.  
	inline FloatType operator()(size_t row, size_t col) const { 
		MAFEM_ASSERT_MSG(row>=0 && row<3, "AffineTransformation matrix element access", "Row index out of range");
		MAFEM_ASSERT_MSG(col>=0 && col<4, "AffineTransformation matrix element access", "Column index out of range");
		return m[col][row];
	}

	/// \brief Returns a reference to a matrix element.
	/// \param row The row of the element to return (0-2). 
	/// \param col The column of the element to return (0-3).
	inline FloatType& operator()(size_t row, size_t col) { 
		MAFEM_ASSERT_MSG(row>=0 && row<3, "AffineTransformation matrix element access", "Row index out of range");
		MAFEM_ASSERT_MSG(col>=0 && col<4, "AffineTransformation matrix element access", "Column index out of range");
		return m[col][row];
	}

	/// \brief Returns a column from the matrix.
	/// \param i The column to return (0-3).
	/// \return The i-th column of the matrix as a vector.
	inline const Vector3& column(size_t i) const { 
		MAFEM_ASSERT_MSG(i>=0 && i<4, "AffineTransformation::column()", "Column index out of range.");
		MAFEM_STATIC_ASSERT(sizeof(m) == sizeof(Vector3)*4);
		return m[i];
	}

	/// \brief Returns a reference to a column in the matrix.
	/// \param i The column to return (0-3).
	/// \return The i-th column of the matrix as a vector reference. Modifying the vector modifies the matrix.
	inline Vector3& column(size_t i) {
		MAFEM_ASSERT_MSG(i>=0 && i<4, "AffineTransformation::column()", "Column index out of range.");
		MAFEM_STATIC_ASSERT(sizeof(m) == sizeof(Vector3)*4);
		return m[i];
	}
	
	/// \brief Returns a column from the matrix.
	/// \param i The column to return (0-3).
	/// \return The i-th column of the matrix as a vector.
	const Vector3& getColumn(size_t i) const { return column(i); }
	
	/// \brief Sets all elements in one column of the matrix.
	/// \param i The column to set (0-3).
	/// \param c The new element values as a vector.
	void setColumn(size_t i, const Vector3& c) { column(i) = c; }	

	/// \brief Returns a row from the matrix.
	/// \param i The row to return (0-2).
	/// \return The i-th row of the matrix as a vector.
	inline Vector4 row(size_t i) const {
		MAFEM_ASSERT_MSG(i>=0 && i<3, "AffineTransformation::row()", "Row index out of range.");
		return Vector4(m[0][i], m[1][i], m[2][i], m[3][i]);
	}

	/// \brief Sets all elements in one row of the matrix.
	/// \param i The row to set (0-2).
	/// \param r The new element values as a vector.
	inline void setRow(size_t i, const Vector4& r) {
		MAFEM_ASSERT_MSG(i>=0 && i<3, "AffineTransformation::setRow()", "Row index out of range.");
		m[0][i] = r[0]; m[1][i] = r[1]; m[2][i] = r[2]; m[3][i] = r[3];
	}	

	/// \brief Returns the translational part of this transformation matrix.
	/// \return A vector that specifies the translation.
	inline const Vector3& getTranslation() const { return column(3); }
	 
	/// \brief Sets the translational part of this transformation matrix.
	inline void setTranslation(const Vector3& t) { column(3) = t; } 

	////////////////////////////////// Comparison ///////////////////////////////////

	/// \brief Compares two affine transfromations for equality.
	/// \return true if each of the components in both matrices are equal.
	inline bool operator==(const AffineTransformation& mat) const { 
		for(size_t i=0; i<4; i++) 
			if(column(i) != mat.column(i)) return false; 
		return true; 
	}

	/// \brief Compares two affine transfromations for inequality.
	/// \return true if any of the components in both matrices are not equal.
	inline bool operator!=(const AffineTransformation& mat) const { return !(*this == mat); }

	/// \brief Checks whether this is the identity transformation.
	/// \return true if this is the identity matrix.
	inline bool operator==(IdentityMatrix) const { 
		return (m[0]==Vector3(1,0,0) && m[1]==Vector3(0,1,0) && m[2]==Vector3(0,0,1) && m[3]==NULL_VECTOR); 
	}

	/// \brief Checks whether this is NOT the identity transformation.
	/// \return true if this is NOT the identity matrix.
	inline bool operator!=(IdentityMatrix) const { return !(*this == IDENTITY); }

	/// \brief Computes the determinant of the matrix.
	inline FloatType determinant() const {
		return((m[0][0]*m[1][1] - m[0][1]*m[1][0])*(m[2][2])
			  -(m[0][0]*m[1][2] - m[0][2]*m[1][0])*(m[2][1])
			  +(m[0][1]*m[1][2] - m[0][2]*m[1][1])*(m[2][0]));
	}

	/// \brief Computes the inverse of the matrix. 
	/// \throw Exception if matrix is not invertible because it is singular.
	AffineTransformation inverse() const {
		// Compute inverse of 3x3 submatrix.
		// Then multiply with inverse translation.
		const FloatType det = determinant();
		MAFEM_ASSERT_MSG(det != 0, "AffineTransformation::inverse()", "Singular matrix cannot be inverted: determinant is zero.");
		if(det == 0) throw Exception("Affine transformation cannot be inverted: determinant is zero.");
		return AffineTransformation(	(m[1][1]*m[2][2] - m[1][2]*m[2][1])/det,
						(m[2][0]*m[1][2] - m[1][0]*m[2][2])/det,
						(m[1][0]*m[2][1] - m[1][1]*m[2][0])/det,
						0.0,
						(m[2][1]*m[0][2] - m[0][1]*m[2][2])/det,
						(m[0][0]*m[2][2] - m[2][0]*m[0][2])/det,
						(m[0][1]*m[2][0] - m[0][0]*m[2][1])/det,
						0.0,
						(m[0][1]*m[1][2] - m[1][1]*m[0][2])/det,
						(m[0][2]*m[1][0] - m[0][0]*m[1][2])/det,
						(m[0][0]*m[1][1] - m[1][0]*m[0][1])/det,
						0.0) * AffineTransformation::translation(-getTranslation());
	}

	/// \brief Returns a pointer to the first element of the matrix.
	/// \return A pointer to 12 matrix element in column-major order.
	/// \sa constData() 
	inline FloatType* data() {
        MAFEM_STATIC_ASSERT(sizeof(m) == sizeof(FloatType)*12);
		return (FloatType*)this;
	}

	/// \brief Returns a pointer to the first element of the matrix.
	/// \return A pointer to 12 matrix element in column-major order.
	/// \sa data() 
	inline const FloatType* constData() const {
        MAFEM_STATIC_ASSERT(sizeof(m) == sizeof(FloatType)*12);
		return (const FloatType*)this;
	}

	/// \brief Generates the identity matrix.
	/// \return A matrix with all diagonal elements set to one and all off-diagonal elements set to zero.
	static AffineTransformation identity() { return AffineTransformation(IDENTITY); }
	/// \brief Generates a pure rotation matrix around the X axis.
	/// \param angle The rotation angle in radians.
	static AffineTransformation rotationX(double angle);
	/// \brief Generates a pure rotation matrix around the Y axis.
	/// \param angle The rotation angle in radians.
	static AffineTransformation rotationY(double angle);
	/// \brief Generates a pure rotation matrix around the Z axis.
	/// \param angle The rotation angle in radians.
	static AffineTransformation rotationZ(double angle);
	/// \brief Generates a pure rotation matrix around a given axis.
	static AffineTransformation rotationAxis(Vector3& axis, double angle);
	/// \brief Generates a rotation matrix with given center of rotation.
	static AffineTransformation rotationPoint(AffineTransformation rotation, Point3& point);
	/// \brief Generates a pure rotation matrix around the given axis.
	static AffineTransformation rotation(const Rotation& rot);
	/// enerates a pure rotation matrix from a quaternion.
	static AffineTransformation rotation(const Quaternion& q);
	/// Generates a pure translation matrix.
	static AffineTransformation translation(double dx, double dy, double dz);
	/// Generates a pure diagonal scaling matrix.
	static AffineTransformation translation(const Vector3& translation);
	/// Generates a pure translation matrix.
	static AffineTransformation scaling(double scalingFactor);
	/// Generates a pure scaling matrix.
	static AffineTransformation scaling(double sx, double sy, double sz);
	/// Generates a pure scaling matrix.
	static AffineTransformation scaling(Vector3& scaling);
	/// Generates a pure scaling matrix.
	static AffineTransformation scaling(const Scaling& scaling);
	
	
	/// \brief Generates a look-at-matrix. 
	/// \param camera The position of the camera in space.
	/// \param target The position in space where to camera should point to.
	/// \param upVector A vector pointing to the upward direction (the sky) that defines the rotation of the camera
	///                 arround the viewing axis.
	/// \return The transformation from world space to view space.
	static AffineTransformation lookAt(const Point3& camera, const Point3& target, const Vector3& upVector);
	
	/// \brief Generates a matrix with pure shearing transfromation normal to the z-axis in the x- and y-direction.
	static AffineTransformation shear(FloatType gammaX, FloatType gammaY);

	Vector3 operator*(const Vector3& v) const;
	Point3 operator*(const Point3& p) const;
	Vector3 operator*(const Vector3I& v) const;
	Point3 operator*(const Point3I& p) const;
	AffineTransformation operator*(FloatType s) const;
	AffineTransformation operator*(const AffineTransformation& b) const;
	AffineTransformation operator*(const Matrix3& b) const;
	
	///////////////////////////////// Information ////////////////////////////////
	
	/// \brief Returns the number of rows in this matrix (the constant 3).
	inline size_t size1() const { return 3; }

	/// \brief Returns the number of columns in this matrix (the constant 4).
	inline size_t size2() const { return 4; }	
	
	/// \brief Returns a string representation of this matrix.
	QString toString() const { 
		return QString("%1\n%2\n%3").arg(row(0).toString(), row(1).toString(), row(2).toString());
	}
		
	friend class Matrix3;
	friend class Matrix4;
	friend class boost::serialization::access;
	
private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & m; }	
};

/// Multiplies a 3x4 matrix with a Vector3 (which is extended to a Vector4 with the last
/// element being 0).
inline Vector3 AffineTransformation::operator*(const Vector3& v) const 
{
	return Vector3(
		m[0][0]*v.X + m[1][0]*v.Y + m[2][0]*v.Z,
		m[0][1]*v.X + m[1][1]*v.Y + m[2][1]*v.Z,
		m[0][2]*v.X + m[1][2]*v.Y + m[2][2]*v.Z);
}

/// Multiplies a 3x4 matrix with a Vector3I (which is extended to a Vector4 with the last
/// element being 0).
inline Vector3 AffineTransformation::operator*(const Vector3I& v) const 
{
	return Vector3(
		m[0][0]*v.X + m[1][0]*v.Y + m[2][0]*v.Z,
		m[0][1]*v.X + m[1][1]*v.Y + m[2][1]*v.Z,
		m[0][2]*v.X + m[1][2]*v.Y + m[2][2]*v.Z);
}

/// Multiplies a 3x4 matrix with a Point3 (which is extended to a Vector4 with the last
/// element being 1).
inline Point3 AffineTransformation::operator*(const Point3& p) const
{
	return Point3(
		m[0][0]*p.X + m[1][0]*p.Y + m[2][0]*p.Z + m[3][0],
		m[0][1]*p.X + m[1][1]*p.Y + m[2][1]*p.Z + m[3][1],
		m[0][2]*p.X + m[1][2]*p.Y + m[2][2]*p.Z + m[3][2]);
}

/// Multiplies a 3x4 matrix with a Point3I (which is extended to a Vector4 with the last
/// element being 1).
inline Point3 AffineTransformation::operator*(const Point3I& p) const
{
	return Point3(
		m[0][0]*p.X + m[1][0]*p.Y + m[2][0]*p.Z + m[3][0],
		m[0][1]*p.X + m[1][1]*p.Y + m[2][1]*p.Z + m[3][1],
		m[0][2]*p.X + m[1][2]*p.Y + m[2][2]*p.Z + m[3][2]);
}

/// Multiplies a 3x4 matrix with a 3x4 Matrix.
inline AffineTransformation AffineTransformation::operator*(const AffineTransformation& b) const 
{
	AffineTransformation res;
	for(size_t i=0; i<3; i++) {
		for(size_t j=0; j<4; j++) {
			res(i,j) = (*this)(i,0)*b(0,j) + (*this)(i,1)*b(1,j) + (*this)(i,2)*b(2,j);
		}
		res(i,3) += (*this)(i,3);
	}
	return res;
}

/// Multiplies a 3x4 matrix with a scalar.
inline AffineTransformation AffineTransformation::operator*(FloatType s) const
{
	AffineTransformation res;
	for(size_t i=0; i<4; i++)
		res.column(i) = column(i) * s;
	return res;
}

/// Multiplies a 3x3 matrix with a 3x4 Matrix.
inline AffineTransformation operator*(const Matrix3& a, const AffineTransformation& b) 
{
	AffineTransformation res;
	for(size_t i=0; i<3; i++)
		for(size_t j=0; j<4; j++) 
			res(i,j) = a(i,0)*b(0,j) + a(i,1)*b(1,j) + a(i,2)*b(2,j);
	return res;
}

/// Multiplies a 3x4 matrix with a 3x3 matrix.
inline AffineTransformation AffineTransformation::operator*(const Matrix3& b) const 
{
	AffineTransformation res;
	for(size_t i=0; i<3; i++) {
		for(size_t j=0; j<3; j++) {
			res(i,j) = (*this)(i,0)*b(0,j) + (*this)(i,1)*b(1,j) + (*this)(i,2)*b(2,j);
		}
		res(i,3) = (*this)(i,3);
	}
	return res;
}

/// Writes the matrix to an output stream.
inline std::ostream& operator<<(std::ostream &os, const AffineTransformation& m) {
	using namespace std;
	return os << m.row(0) << endl << m.row(1) << endl << m.row(2);
}

/// Writes the matrix to a logging stream.
inline LoggerObject& operator<<(LoggerObject& log, const AffineTransformation& m)
{
	for(size_t row = 0; row < 3; row++)
		log.space() << m(row, 0) << m(row, 1) << m(row, 2) << m(row, 3) << endl;
	return log;
} 

/// Writes a matrix to an output stream.
inline QDataStream& operator<<(QDataStream& stream, const AffineTransformation& m)
{
	return stream << m.column(0) << m.column(1) << m.column(2) << m.column(3);
}

/// Reads a matrix from an input stream.
inline QDataStream& operator>>(QDataStream& stream, AffineTransformation& m)
{
	return stream >> m.column(0) >> m.column(1) >> m.column(2) >> m.column(3);
}

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::AffineTransformation)
Q_DECLARE_TYPEINFO(Base::AffineTransformation, Q_PRIMITIVE_TYPE);

#endif // __AFFINE_TRANSFORMATION_H
