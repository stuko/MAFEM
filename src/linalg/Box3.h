///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Box3.h 
 * \brief Contains the definition of the Base::Box_3 template class. 
 */

#ifndef __BOX3_H
#define __BOX3_H

#include <Base.h>
#include "Vector3.h"
#include "Point3.h"
#include "AffineTransformation.h"


namespace Base {

/**
 * \brief A bounding box in 3d space.
 *
 * This class stores an axis-aligned box in 3d.
 * It is defined by minimum and maximum coordinates in X, Y and Z direction.
 * 
 * There are two predefined instances of this template class: 
 * Box3 which is used for floating-point coordinates and \c Base::Box3I which is used
 * for integer coordinates. 
 * 
 * \author Alexander Stukowski
 * \sa Box_2
 */
template<typename T>
class Box_3
{
public:	
	/// The coordinates of the lower corner.
	Point_3<T> minc;
	/// The coordinates of the upper corner.
	Point_3<T> maxc;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief Creates an empty box.
	Box_3() : minc(std::numeric_limits<T>::max()), maxc(-std::numeric_limits<T>::max()) {}

	/// \brief Initializes the box with the minimum and maximum coordinates.
	/// \param minCorner A point that specifies the corner with minimum coordinates of the box.
	/// \param maxCorner A point that specifies the corner with maximum coordinates of the box.	
	Box_3(const Point_3<T>& minCorner, const Point_3<T>& maxCorner) : minc(minCorner), maxc(maxCorner) {
		MAFEM_ASSERT_MSG(minc.X <= maxc.X, "Box_3 constructor", "X component of the minimum corner point must not be larger than the maximum corner point.");
		MAFEM_ASSERT_MSG(minc.Y <= maxc.Y, "Box_3 constructor", "Y component of the minimum corner point must not be larger than the maximum corner point.");
		MAFEM_ASSERT_MSG(minc.Z <= maxc.Z, "Box_3 constructor", "Z component of the minimum corner point must not be larger than the maximum corner point.");
	}

	/// \brief Crates a cubic box with the given center and half edge length.
	/// \param center The center of the cubic box.
	/// \param halfEdgeLength The half size of the cube.
	Box_3(const Point_3<T>& center, T halfEdgeLength) {
		minc.X = center.X - halfEdgeLength;
		minc.Y = center.Y - halfEdgeLength;
		minc.Z = center.Z - halfEdgeLength;
		maxc.X = center.X + halfEdgeLength;
		maxc.Y = center.Y + halfEdgeLength;
		maxc.Z = center.Z + halfEdgeLength;
	}

	///////////////////////////////// Attributes /////////////////////////////////

	/// \brief Checks whether this is an empty box.
	///
	/// The box is considered empty when one of the maximum corner coodinates is less
	/// then the minimum corner coordinate.
	/// \return true if this box is empty; false otherwise.
	bool isEmpty() const {
        return (minc.X > maxc.X) || (minc.Y > maxc.Y) || (minc.Z > maxc.Z);
	}

	/// \brief Resets the box to the empty state.
	void setEmpty() {
		minc = Point_3<T>( std::numeric_limits<T>::max());
		maxc = Point_3<T>(-std::numeric_limits<T>::max());
	}

	/// \brief Computes the center of the box.
	/// \return The center of the box.
	Point_3<T> center() const {
		return (minc + maxc) / 2;
	}

	/// \brief Computes the size of the box.
	/// \return The difference between the maximum and minimum corner.
	Vector_3<T> size() const {
		return maxc - minc;
	}
	
	/// \brief Returns the size of the box in the given dimension.
	/// \param dimension The index of the dimension (0 - 2).
	T size(size_t dimension) const { 
		return maxc[dimension] - minc[dimension]; 
	}

	/// Returns the size in X direction (Max.X - Min.X) of the box.
	T sizeX() const { return maxc.X - minc.X; }

	/// Returns the size in Y direction (Max.Y - Min.Y) of the box.
	T sizeY() const { return maxc.Y - minc.Y; }

	/// Returns the size in Z direction (Max.Z - Min.Z) of the box.
	T sizeZ() const { return maxc.Z - minc.Z; }

	/// \brief Returns the position of one of the eight corners of the box corner.
	/// \param i The index of the corner (0 - 7).
	/// \return The coordinate of the i-th corner of the box.
	Point_3<T> operator[](size_t i) const {
		MAFEM_ASSERT_MSG(!isEmpty(), "Box_3::operator[]", "Cannot calculate the corner of an empty box.");
		switch(i) {
			case 0: return Point_3<T>(minc.X, minc.Y, minc.Z);
			case 1: return Point_3<T>(maxc.X, minc.Y, minc.Z);
			case 2: return Point_3<T>(minc.X, maxc.Y, minc.Z);
			case 3: return Point_3<T>(maxc.X, maxc.Y, minc.Z);
			case 4: return Point_3<T>(minc.X, minc.Y, maxc.Z);
			case 5: return Point_3<T>(maxc.X, minc.Y, maxc.Z);
			case 6: return Point_3<T>(minc.X, maxc.Y, maxc.Z);
			case 7: return Point_3<T>(maxc.X, maxc.Y, maxc.Z);
			default:				
				MAFEM_ASSERT_MSG(false, "Box3::operator[]", "Corner index out of range.");
                throw std::invalid_argument("Corner index out of range.");
				return ORIGIN;
		}
	}

	/////////////////////////////// Classification ///////////////////////////////
    
	/// \brief Checks whether a point is inside the box.
	/// \param p The point to test.
	/// \return true if the given point is inside or on the edge of the bounding box; false if it is completely outside the box.
	bool contains(const Point_3<T>& p, FloatType epsilon = FLOATTYPE_EPSILON) const {
		if(p.X + epsilon < minc.X || p.X - epsilon > maxc.X) return false;
		if(p.Y + epsilon < minc.Y || p.Y - epsilon > maxc.Y) return false;
		if(p.Z + epsilon < minc.Z || p.Z - epsilon > maxc.Z) return false;
		return true;
	}
	
	/// \brief Classifies the given point with respect to the box.
	///
	/// Returns -1 if the point is outside of the box.
	/// Returns 0 if the point is on the boundary of the box within the given tolerance.
	/// Returns +1 if the point is inside of the box.	
	int classifyPoint(const Point_3<T>& p, FloatType epsilon = FLOATTYPE_EPSILON) const {
		if(p.X > maxc.X + epsilon || p.Y > maxc.Y + epsilon || p.Z > maxc.Z + epsilon) return -1;
		if(p.X < minc.X - epsilon || p.Y < minc.Y - epsilon || p.Z < minc.Z - epsilon) return -1;
		if(p.X < maxc.X - epsilon && p.X > minc.X + epsilon && 
		   p.Y < maxc.Y - epsilon && p.Y > minc.Y + epsilon &&
		   p.Z < maxc.Z - epsilon && p.Z > minc.Z + epsilon) return 1;
		return 0;
	}

	/// \brief Checks whether another box is contained in this box.
	/// \return true if the given box is completely inside the bounding box.
	bool containsBox(const Box_3<T>& b) const {
		return (b.minc.X >= minc.X && b.maxc.X <= maxc.X) &&
			(b.minc.Y >= minc.Y && b.maxc.Y <= maxc.Y) &&
			(b.minc.Z >= minc.Z && b.maxc.Z <= maxc.Z);
	}

	/// \brief Checks wehther the intersection of two boxes is not empty.
	/// \return true if the given box is not completely outside of this box.
	bool intersects(const Box_3<T>& b) const {
		if(maxc.X <= b.minc.X || minc.X >= b.maxc.X) return false;
		if(maxc.Y <= b.minc.Y || minc.Y >= b.maxc.Y) return false;
		if(maxc.Z <= b.minc.Z || minc.Z >= b.maxc.Z) return false;
		if(isEmpty() || b.isEmpty()) return false;
		return true;
	}

    //////////////////////////////// Modification ////////////////////////////////

	/// \brief Enlarges the box to include the given point.
	/// \sa addPoints(), addBox()
	void addPoint(const Point_3<T>& p) {
		minc.X = std::min(minc.X, p.X); maxc.X = std::max(maxc.X, p.X);
		minc.Y = std::min(minc.Y, p.Y); maxc.Y = std::max(maxc.Y, p.Y);
		minc.Z = std::min(minc.Z, p.Z); maxc.Z = std::max(maxc.Z, p.Z);
	}

	/// \brief Enlarges the box to include the given point.
	/// \sa addPoint()
	Box_3& operator+=(const Point_3<T>& p) {
		addPoint(p);
		return *this;
	}

	/// \brief Enlarges the box to include the given points.
	/// \param points Pointer to the first element of an array of points.
	/// \param count The number of points in the array.
	/// \sa addPoint()
	void addPoints(const Point_3<T>* points, size_t count) {
		for(; count != 0; count--, points++) {
			minc.X = std::min(minc.X, points->X); maxc.X = std::max(maxc.X, points->X);
			minc.Y = std::min(minc.Y, points->Y); maxc.Y = std::max(maxc.Y, points->Y);
			minc.Z = std::min(minc.Z, points->Z); maxc.Z = std::max(maxc.Z, points->Z);
		}
	}

	/// \brief Enlarges this box to include the given box.
	/// \sa addPoint()
	void addBox(const Box_3& b) {
		minc.X = std::min(minc.X, b.minc.X); maxc.X = std::max(maxc.X, b.maxc.X);
		minc.Y = std::min(minc.Y, b.minc.Y); maxc.Y = std::max(maxc.Y, b.maxc.Y);
		minc.Z = std::min(minc.Z, b.minc.Z); maxc.Z = std::max(maxc.Z, b.maxc.Z);
	}

	/// \brief Computes the intersection of this box and a second box.
	/// 
	/// This box is clipped to the boundary of the given box.
	void clip(const Box_3& b) {
		minc.X = std::max(minc.X, b.minc.X); maxc.X = std::min(maxc.X, b.maxc.X);
		minc.Y = std::max(minc.Y, b.minc.Y); maxc.Y = std::min(maxc.Y, b.maxc.Y);
		minc.Z = std::max(minc.Z, b.minc.Z); maxc.Z = std::min(maxc.Z, b.maxc.Z);
	}

	/// \brief Computes the bounding box transformed by the given matrix.
	/// \return The axis-aligned bounding box that contains the transformed input box.
	Box_3 transformed(const AffineTransformation& tm) const {
		if(isEmpty()) return *this;
		Box_3 b;
		b.addPoint(tm * Point_3<T>(minc.X, minc.Y, minc.Z));
		b.addPoint(tm * Point_3<T>(maxc.X, minc.Y, minc.Z));
		b.addPoint(tm * Point_3<T>(minc.X, maxc.Y, minc.Z));
		b.addPoint(tm * Point_3<T>(maxc.X, maxc.Y, minc.Z));
		b.addPoint(tm * Point_3<T>(minc.X, minc.Y, maxc.Z));
		b.addPoint(tm * Point_3<T>(maxc.X, minc.Y, maxc.Z));
		b.addPoint(tm * Point_3<T>(minc.X, maxc.Y, maxc.Z));
		b.addPoint(tm * Point_3<T>(maxc.X, maxc.Y, maxc.Z));
		return b;
	}

	/// \brief Scales the box by the given scalar factor. 
	/// The center of the box is taken as scaling center.
	Box_3 centerScale(T factor) const {
		if(isEmpty()) return *this;
		Point_3<T> c = center();
		return Box_3(c + ((minc - c) * factor), c + ((maxc - c) * factor));
	}

	/// \brief Adds the given amount of padding to each side of the box.
	/// \return The padded box.
	Box_3 padBox(T amount) const {
		if(isEmpty()) return *this;		
		return Box_3(minc - Vector3(amount), maxc + Vector3(amount));
	}

    ////////////////////////////////// Utilities /////////////////////////////////
	
	/// Returns a string representation of this box.
	QString toString() const { return QString("[Min: %1 Max: %2]").arg(minc.toString()).arg(maxc.toString()); }	

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & minc & maxc; }	

	friend class boost::serialization::access;
};

/// \brief Transforms a box.
/// \sa Box_3::transformed()
template<typename T>
inline Box_3<T> operator*(const AffineTransformation& tm, const Box_3<T>& box) {
	return box.transformed(tm);
}

/// \brief Prints the box to a text output stream.
template<typename T>
inline std::ostream& operator<<(std::ostream &os, const Box_3<T> &b) {
	return os << '[' << b.minc << "] - [" << b.maxc << ']';
}

/// \brief Writes the box to a logging stream.
template<typename T>
inline LoggerObject& operator<<(LoggerObject& log, const Box_3<T>& b)
{
	log.nospace() << '[' << b.minc << "] - [" << b.maxc << ']';
	return log;
} 

/// \brief Writes a box to a binary output stream.
template<typename T>
inline QDataStream& operator<<(QDataStream& stream, const Box_3<T>& b)
{
	return stream << b.minc << b.maxc;
}

/// \brief Reads a box from a binary input stream.
template<typename T>
inline QDataStream& operator>>(QDataStream& stream, Box_3<T>& b)
{
	return stream >> b.minc >> b.maxc;
}

template class Box_3<FloatType>;
template class Box_3<int>;

/** 
 * \typedef Box_3<FloatType> Box3
 * \brief Template class instance of the Box_3 class used for floating-point calculations based on Point3. 
 */
typedef Box_3<FloatType> Box3;

/** 
 * \typedef Box3I
 * \brief Template class instance of the Box_3 class used for integer calculations based on Point3I. 
 */
typedef Box_3<int> Box3I;

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Box3)
Q_DECLARE_METATYPE(Base::Box3I)
Q_DECLARE_TYPEINFO(Base::Box3, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(Base::Box3I, Q_MOVABLE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Box3, boost::serialization::object_serializable)
BOOST_CLASS_IMPLEMENTATION(Base::Box3I, boost::serialization::object_serializable)

#endif // __BOX3_H
