///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __FLOATARRAY_H
#define __FLOATARRAY_H

#include <Base.h>

namespace Base {

/******************************************************************************
* A variable sized vector.
******************************************************************************/
class FloatArray : public QVector<FloatType> {
public:
	FloatArray() : QVector<FloatType>() {}
	FloatType Get(size_t i) const { return (*this)[i]; }
	void Set(size_t i, FloatType value) { (*this)[i] = value; }
	void CopyFrom(const FloatArray& other) {
		*this = other;
	}
	void Zero(size_t count) {
		fill(0, count);
	}
	void Mult(FloatType scale) {
		iterator iter = begin();
		iterator enditer = end();
		while(iter != enditer)
			*iter++ *= scale;
	}
	void MultAdd(const FloatArray& other, FloatType scale) {
		MAFEM_ASSERT(size() == other.size());
		iterator dest = begin();
		iterator enditer = end();
		const_iterator src = other.begin();
		while(dest != enditer)
			*dest++ += *src++ * scale;
	}
	void Multiplication(const FloatArray& a, const FloatArray& b) {
		MAFEM_ASSERT(size() == a.size());
		MAFEM_ASSERT(size() == b.size());
		iterator dest = begin();
		iterator enditer = end();
		const_iterator src1 = a.begin();
		const_iterator src2 = b.begin();
		while(dest != enditer)
			*dest++ = (*src1++) * (*src2++);
	}
	void WeightedSum(const FloatArray& a1, FloatType scale1, const FloatArray& a2, FloatType scale2) {
		MAFEM_ASSERT(size() == a1.size());
		MAFEM_ASSERT(size() == a2.size());
		iterator dest = begin();
		iterator enditer = end();
		const_iterator src1 = a1.begin();
		const_iterator src2 = a2.begin();
		while(dest != enditer)
			*dest++ = *src1++ * scale1 + *src2++ * scale2;
	}
	FloatType Dot(const FloatArray& a) const {
		MAFEM_ASSERT(size() == a.size());
		FloatType result = 0.0;
		const_iterator iter1 = begin();
		const_iterator enditer1 = end();
		const_iterator iter2 = a.begin();
		while(iter1 != enditer1)
			result += (*iter1++) * (*iter2++);
		return result;
	}
	FloatType Norm2() const {
		FloatType n = 0.0;
		const_iterator iter = begin();
		const_iterator enditer = end();
		while(iter != enditer)
			n += square(*iter++);
		return sqrt(n);
	}
	FloatType InfinityNorm() const {
		FloatType m = 0.0;
		const_iterator iter = begin();
		const_iterator enditer = end();
		while(iter != enditer)
			m = std::max(std::abs(*iter++), m);
		return m;
	}
};

}; // End of namespace Base

#endif // __FLOATARRAY_H
