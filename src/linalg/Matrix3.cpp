///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include <Base.h>
#include <linalg/Matrix3.h>

namespace Base {

/// This dummy instance should be passed to the AffineTransformation class constructor to initialize it to the identity matrix.
IdentityMatrix IDENTITY;
/// This dummy instance should be passed to the AffineTransformation class constructor to initialize it to the null matrix.
NullMatrix NULL_MATRIX;

/******************************************************************************
* Generates a rotation matrix around the X axis.
******************************************************************************/
Matrix3 Matrix3::rotationX(FloatType angle)
{
	FloatType c = cos(angle);
	FloatType s = sin(angle);
	return Matrix3(1.0, 0.0, 0.0,
				   0.0,   c,  -s,
				   0.0,   s,   c);
}

/******************************************************************************
* Generates a rotation matrix around the Y axis.
******************************************************************************/
Matrix3 Matrix3::rotationY(FloatType angle)
{
	FloatType c = cos(angle);
	FloatType s = sin(angle);
	return Matrix3(  c, 0.0,   s,
				   0.0, 1.0, 0.0,
				    -s, 0.0,   c);
}

/******************************************************************************
* Generates a rotation matrix around the Z axis.
******************************************************************************/
Matrix3 Matrix3::rotationZ(FloatType angle)
{
	FloatType c = cos(angle);
	FloatType s = sin(angle);
	return Matrix3(  c,  -s, 0.0,
				     s,   c, 0.0,
				   0.0, 0.0, 1.0);
}

/******************************************************************************
* Generates a rotation matrix around the given axis.
******************************************************************************/
Matrix3 Matrix3::rotation(const Rotation& rot)
{
	FloatType c = cos(rot.angle);
	FloatType s = sin(rot.angle);
	FloatType t = (FloatType)1 - c;
    const Vector3& a = rot.axis;
	MAFEM_ASSERT_MSG(abs(LengthSquared(a) - 1.0) <= FLOATTYPE_EPSILON, "Matrix3::rotation", "Rotation axis vector must be normalized.");
	return Matrix3(	t * a.X * a.X + c,       t * a.X * a.Y - s * a.Z, t * a.X * a.Z + s * a.Y,
					t * a.X * a.Y + s * a.Z, t * a.Y * a.Y + c,       t * a.Y * a.Z - s * a.X,
					t * a.X * a.Z - s * a.Y, t * a.Y * a.Z + s * a.X, t * a.Z * a.Z + c       );
}

/******************************************************************************
* Generates a rotation matrix from a quaternion.
******************************************************************************/
Matrix3 Matrix3::rotation(const Quaternion& q)
{
#if defined(_DEBUG)	
	if(abs(DotProduct(q,q) - 1.0) > FLOATTYPE_EPSILON) {
		MsgLogger() << "Invalid quaternion is: " << q;
		MsgLogger() << "abs(DotProduct(q,q) - 1.0): " << abs(DotProduct(q,q) - 1.0);
		MAFEM_ASSERT_MSG(false, "Matrix3::rotation", "Quaternion must be normalized.");
	}
#endif
	return Matrix3(1.0 - 2.0*(q.Y*q.Y + q.Z*q.Z),       2.0*(q.X*q.Y - q.W*q.Z),       2.0*(q.X*q.Z + q.W*q.Y),
				         2.0*(q.X*q.Y + q.W*q.Z), 1.0 - 2.0*(q.X*q.X + q.Z*q.Z),       2.0*(q.Y*q.Z - q.W*q.X),
			             2.0*(q.X*q.Z - q.W*q.Y),       2.0*(q.Y*q.Z + q.W*q.X), 1.0 - 2.0*(q.X*q.X + q.Y*q.Y));
}


/******************************************************************************
* Generates a scaling matrix.
******************************************************************************/
Matrix3 Matrix3::scaling(const Scaling& scaling)
{	
	Matrix3 U = Matrix3::rotation(scaling.Q);
	Matrix3 K = Matrix3(scaling.S.X, 0.0, 0.0,
						0.0, scaling.S.Y, 0.0,
						0.0, 0.0, scaling.S.Z);
	return U * K * U.transposed();
}

/******************************************************************************
* Balances the matrix.
*
* Replaces the matrix with a balanced matrix with identical eigenvalues.
* A symmetric matrix is already balanced and is unafected by this procedure.
******************************************************************************/
void Matrix3::balance()
{
	const FloatType RADIX = numeric_limits<FloatType>::radix;
	int i,j,last=0;
	FloatType s,r,g,f,c,sqrdx;

	sqrdx=RADIX*RADIX;
	while(last == 0) {
		last=1;
		for(i=0; i<3; i++) {
			r = c = 0.0;
			for(j=0; j<3; j++)
				if(j != i) {
					c += abs(m[j][i]);
					r += abs(m[i][j]);
				}
			if(c != 0.0 && r != 0.0) {
				g=r/RADIX;
				f=1.0;
				s=c+r;
				while(c<g) {
					f *= RADIX;
					c *= sqrdx;
				}
				g=r*RADIX;
				while(c>g) {
					f /= RADIX;
					c /= sqrdx;
				}
				if((c+r)/f < 0.95*s) {
					last=0;
					g=1.0/f;
					for(j=0;j<3;j++) m[i][j] *= g;
					for(j=0;j<3;j++) m[j][i] *= f;
				}
			}
		}
	}
}

/******************************************************************************
* Reduces matrix to Hessenberg form.
*
* Reduction to Hessenberg form by the elimination method.
* This real, nonsymmetric matrix is replaced by an upper Hessenberg matrix 
* with identical eigenvalues. 
* Recommended, but not required, is that this routine be preceded by Balance().
******************************************************************************/
void Matrix3::eliminateHessenberg()
{
	int i,j,mm;
	FloatType y,x;

	for(mm=1;mm<3-1;mm++) {
		x=0.0;
		i=mm;
		for(j=mm;j<3;j++) {
			if(abs(m[j][mm-1]) > abs(x)) {
				x=m[j][mm-1];
				i=j;
			}
		}
		if(i != mm) {
			for(j=mm-1;j<3;j++) swap(m[i][j],m[mm][j]);
			for(j=0;j<3;j++) swap(m[j][i],m[j][mm]);
		}
		if(x != 0.0) {
			for(i=mm+1;i<3;i++) {
				if((y=m[i][mm-1]) != 0.0) {
					y /= x;
					m[i][mm-1]=y;
					for(j=mm;j<3;j++) m[i][j] -= y*m[mm][j];
					for(j=0;j<3;j++) m[j][mm] += y*m[j][i];
				}
			}
		}
	}
}

template<class T>
inline const T SIGN(const T &a, const T &b)
	{return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);}

/******************************************************************************
* Computes the eigenvalues of the matrix.
*
* Finds all eigenvalues of an upper Hessenberg matrix.
* On input the matrix can be exactly as output from EliminateHessenberg(). 
* On output it is destroyed!
******************************************************************************/
void Matrix3::hqr(complex<FloatType> wri[3])
{
	int nn,mm,l,k,j,its,i,mmin;
	FloatType z,y,x,w,v,u,t,s,r,q,p,anorm;

	anorm=0.0;
	for(i=0;i<3;i++)
		for(j=max(i-1,0);j<3;j++)
			anorm += abs(m[i][j]);
	nn=3-1;
	t=0.0;
	while(nn >= 0) {
		its=0;
		do {
			for(l=nn;l>0;l--) {
				s=abs(m[l-1][l-1])+abs(m[l][l]);
				if (s == 0.0) s=anorm;
				if (abs(m[l][l-1]) + s == s) {
					m[l][l-1] = 0.0;
					break;
				}
			}
			x=m[nn][nn];
			if (l == nn) {
				wri[nn--]=x+t;
			} else {
				y=m[nn-1][nn-1];
				w=m[nn][nn-1]*m[nn-1][nn];
				if (l == nn-1) {
					p=0.5*(y-x);
					q=p*p+w;
					z=sqrt(abs(q));
					x += t;
					if (q >= 0.0) {
						z=p+SIGN(z,p);
						wri[nn-1]=wri[nn]=x+z;
						if(z != 0.0) wri[nn]=x-w/z;
					} else {
						wri[nn]=complex<FloatType>(x+p,z);
						wri[nn-1]=conj(wri[nn]);
					}
					nn -= 2;
				} else {
					if(its == 30) 
						throw runtime_error("Failed to compute matrix eigenvalues. Too many iterations in Matrix3::hqr().");
					if(its == 10 || its == 20) {
						t += x;
						for(i=0;i<nn+1;i++) m[i][i] -= x;
						s=abs(m[nn][nn-1])+abs(m[nn-1][nn-2]);
						y=x=0.75*s;
						w = -0.4375*s*s;
					}
					++its;
					for(mm=nn-2;mm>=l;mm--) {
						z=m[mm][mm];
						r=x-z;
						s=y-z;
						p=(r*s-w)/m[mm+1][mm]+m[mm][mm+1];
						q=m[mm+1][mm+1]-z-r-s;
						r=m[mm+2][mm+1];
						s=abs(p)+abs(q)+abs(r);
						p /= s;
						q /= s;
						r /= s;
						if(mm == l) break;
						u=abs(m[mm][mm-1])*(abs(q)+abs(r));
						v=abs(p)*(abs(m[mm-1][mm-1])+abs(z)+abs(m[mm+1][mm+1]));
						if(u+v == v) break;
					}
					for(i=mm;i<nn-1;i++) {
						m[i+2][i]=0.0;
						if (i != mm) m[i+2][i-1]=0.0;
					}
					for(k=mm;k<nn;k++) {
						if(k != mm) {
							p=m[k][k-1];
							q=m[k+1][k-1];
							r=0.0;
							if(k+1 != nn) r=m[k+2][k-1];
							if((x=abs(p)+abs(q)+abs(r)) != 0.0) {
								p /= x;
								q /= x;
								r /= x;
							}
						}
						if ((s=SIGN(sqrt(p*p+q*q+r*r),p)) != 0.0) {
							if (k == mm) {
								if (l != mm)
								m[k][k-1] = -m[k][k-1];
							} else
								m[k][k-1] = -s*x;
							p += s;
							x=p/s;
							y=q/s;
							z=r/s;
							q /= p;
							r /= p;
							for(j=k;j<nn+1;j++) {
								p=m[k][j]+q*m[k+1][j];
								if (k+1 != nn) {
									p += r*m[k+2][j];
									m[k+2][j] -= p*z;
								}
								m[k+1][j] -= p*y;
								m[k][j] -= p*x;
							}
							mmin = nn < k+3 ? nn : k+3;
							for (i=l;i<mmin+1;i++) {
								p=x*m[i][k]+y*m[i][k+1];
								if (k != (nn)) {
									p += z*m[i][k+2];
									m[i][k+2] -= p*r;
								}
								m[i][k+1] -= p*q;
								m[i][k] -= p;
							}
						}
					}
				}
			}
		} while (l+1 < nn);
	}
}

/******************************************************************************
* Finds the maximum eigenvalue of the matrix.
******************************************************************************/
FloatType Matrix3::maxEigenvalue() const
{
	// Calculate eigen-matrix B.
	Matrix3 b;
	for(int i=0; i<3; i++) {
		for(int j=0; j<3; j++) {
			b.m[i][j] = 0.0;
			for(int k=0; k<3; k++)
				b.m[i][j] += m[k][i] * m[k][j];
		}
	}

	// Balance matrix.
	b.balance();
	// Reduce matrix to Hessenberg form.
	b.eliminateHessenberg();
	// Compute eigenvalues.
	complex<FloatType> wri[3];
	b.hqr(wri);

	// Find maximum eigenvalue.
	FloatType dmax = 0.0;
	for(size_t i=0; i<3; i++) {
		if(abs(wri[i].imag()) < FLOATTYPE_EPSILON)
			dmax = max(dmax, wri[i].real());
	}

	// Return result
	return sqrt(dmax);
}

/******************************************************************************
* Finds the minimum eigenvalue of the matrix.
******************************************************************************/
FloatType Matrix3::minEigenvalue() const
{
	// Calculate eigen-matrix B.
	Matrix3 b;
	for(int i=0; i<3; i++) {
		for(int j=0; j<3; j++) {
			b.m[i][j] = 0.0;
			for(int k=0; k<3; k++)
				b.m[i][j] += m[k][i] * m[k][j];
		}
	}

	// Balance matrix.
	b.balance();
	// Reduce matrix to Hessenberg form.
	b.eliminateHessenberg();
	// Compute eigenvalues.
	complex<FloatType> wri[3];
	b.hqr(wri);

	// Find minimum eigenvalue.
	FloatType dmin = FLOATTYPE_MAX;
	for(size_t i=0; i<3; i++) {
		if(abs(wri[i].imag()) < FLOATTYPE_EPSILON)
			dmin = min(dmin, wri[i].real());
	}

	// Return result
	return sqrt(dmin);
}

};	// End of namespace Base
