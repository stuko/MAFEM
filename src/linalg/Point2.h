///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Point2.h 
 * \brief Contains definition of the Base::Point_2 template class and operators. 
 */

#ifndef __POINT2_H
#define __POINT2_H

#include <Base.h>
#include "Vector2.h"
#include "Point3.h"

namespace Base {

/**
 * \brief A point in two-dimensional space.
 * 
 * This is one of the basic vector algebra classes. It represents a two
 * dimensional point in space defined by the coordinates X and Y. 
 * There are two instances of this template point class: \c Point2 is for floating-point points 
 * and \c Point2I is for integer points with two components.
 * 
 * Note that there is also a class called Vector_2 that is used for vector
 * in a two-dimensional coordinate system.
 * 
 * \author Alexander Stukowski
 * \sa Vector_2
 * \sa Point_3
 */
template<typename T> 
class Point_2
{
public:	
	/// \brief The X coordinate of the point.
	T X;
	/// \brief The Y coordinate of the point.
	T Y;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief Constructs a point without initializing its coordinates.
	/// \note All coordinates are left uninitialized by this constructor and will therefore have a random value!
	Point_2() {}

	/// \brief Constructs a point with all coordinates set to the given value. 
	/// \param val The value to be assigned to each of the points's coordinates.
	Point_2(T val) { X = Y = val; }

	/// \brief Initializes the coordinates of the point with the given values.
	/// \param x The X coordinate of the new point.
	/// \param y The Y coordinate of the new point.
	Point_2(T x, T y) : X(x), Y(y) {}

	/// \brief Initializes the coordinates of the point with the values in the given array.
	/// \param val An array of two coordinates.
	Point_2(T val[2]) : X(val[0]), Y(val[1]) {}

	/// \brief Initializes the point to the origin. All coordinates are set to zero.
	/// \param ORIGIN A dummy parameter to distinguish this overloaded constructor from the others. 
	///        When using this constructor, jsut use the special value \c ORIGIN here.
	Point_2(Origin ORIGIN) : X((T)0), Y((T)0) {}

	/// \brief Converts a Qt point object to a Point2I.
	/// \param p The Qt point to convert.
	Point_2(const QPoint& p) : X(p.x()), Y(p.y()) {}
	
    ///////////////////////////// Component access ///////////////////////////////

	/// \brief Returns a reference to the i-th coordinate of the point.
	/// \param i The index specifying the coordinate to return (0=X, 1=Y).
	/// \return A reference to the i-th coordinate that can be used to change the coordinate's value. 
	T& operator[](size_t i) {
		MAFEM_STATIC_ASSERT(sizeof(Point_2<T>) == sizeof(T) * 2);
		MAFEM_ASSERT(i>=0 && i<size());
		return (&X)[i];
	}

	/// \brief Returns a constant reference to the i-th coordinate of the point.
	/// \param i The index specifying the coordinate to return (0=X, 1=Y).
	/// \return The value of the i-th coordinate. 
	const T& operator[](size_t i) const {
		MAFEM_STATIC_ASSERT(sizeof(Point_2<T>) == sizeof(T) * 2);
		MAFEM_ASSERT(i>=0 && i<size());
		return (&X)[i]; 
	}

	/// \brief Returns a pointer to the first coordinate (X) of the point.
	/// \sa constData() 
	T* data() { 		
        MAFEM_STATIC_ASSERT(sizeof(Point_2<T>) == sizeof(T) * 2);
		return (T*)this;
	}

	/// \brief Returns a pointer to the first coordinate of the point for read-only access.
	/// \sa data()
	const T* constData() const {
        MAFEM_STATIC_ASSERT(sizeof(Point_2<T>) == sizeof(T) * 2);
		return (const T*)this;
	}

	/// \brief Casts the point to another point type.
	template<typename T2>
	operator Point_2<T2>() const { return Point_2<T2>((T2)X, (T2)Y); }

	///////////////////////////// Assignment operators ///////////////////////////

	/// \brief Adds a vector to this point and stores the result in this point.
	/// \param v The vector to add to this point.
	/// \return A reference to \c this point, which has been changed.
	Point_2<T>& operator+=(const Vector_2<T>& v) { X += v.X; Y += v.Y; return *this; }

	/// \brief Substracts a vector from this point and stores the result in this point.
	/// \param v The vector to substract from this point.
	/// \return A reference to \c this point, which has been changed.
	Point_2<T>& operator-=(const Vector_2<T>& v) { X -= v.X; Y -= v.Y; return *this; }

	/// \brief Converts a Qt point object to a Point2I.
	/// \param p The Qt point to convert.
	/// \return This point object.
	Point_2<T>& operator=(const QPoint& p) { X = p.x(); Y = p.y(); return *this; }
	
	//////////////////////////// Coordinate read access //////////////////////////

	/// Returns the X coordinate of the point.
	const T& x() const { return X; }

	/// Returns the Y coordinate of the point.
	const T& y() const { return Y; }
	
	//////////////////////////// Coordinate write access //////////////////////////

	/// \brief Sets the X coordinate of this point to a new value.
	/// \param value The new value that is assigned to the point coordinate.
	void setx(const T& value) { X = value; }

	/// \brief Sets the Y coordinate of this point to a new value.
	/// \param value The new value that is assigned to the point coordinate.
	void sety(const T& value) { Y = value; }

	////////////////////////////////// Comparison ////////////////////////////////

	/// \brief Compares two points for equality.
	/// \param p The point this point should be compared to.
	/// \return \c true if each of the coordinates are equal; \c false otherwise.
	bool operator==(const Point_2<T>& p) const { return (p.X==X) && (p.Y==Y); }

	/// \brief Compares two points for inequality.
	/// \param p The point this point should be compared to.
	/// \return \c true if any of the coordinates are not equal; \c false if all are equal.
	bool operator!=(const Point_2<T>& p) const { return (p.X!=X) || (p.Y!=Y); }

	/// \brief Checks whether the point is the origin, i.e. all coordinates are zero.
	/// \param ORIGIN Just use the special value \c ORIGIN here.
	/// \return \c true if all of the null vector are zero; \c false otherwise
	bool operator==(const Origin& ORIGIN) const { return (X==(T)0) && (Y==(T)0); }

	/// \brief Checks whether the point is not the origin, i.e. any of the coordinates is nonzero.
	/// \param ORIGIN Just use the special value \c ORIGIN here.
	/// \return \c true if any of the coordinates is nonzero; \c false if this is the null vector otherwise
	bool operator!=(const Origin& ORIGIN) const { return (X!=(T)0) || (Y!=(T)0); }

	/// \brief Checks whether two points are equal within a given tolerance.
	/// \param p The point that should be compared to this point.
	/// \param tolerance A non-negative threshold for the equality test. The two points are considered equal when 
	///        the differences in the X and Y directions are both all smaller than this tolerance value.
	/// \return \c true if this point is equal to the given point \a p within the given tolerance.
	bool equals(const Point_2<T>& p, T tolerance) const { 
		return (abs(p.X - X) <= tolerance) && (abs(p.Y - Y) <= tolerance); 
	}

	/////////////////////////////// Binary operators /////////////////////////////

	/// \brief Computes the sum of two points.
	/// \param p The second operand.
	/// \return The sum of two points.
	Point_2<T> operator+(const Point_2<T>& p) const { return Point_2<T>(X + p.X, Y + p.Y); }

	/// \brief Computes the sum of a point and a vector.
	/// \param v The second operand.
	/// \return The new point.
	Point_2<T> operator+(const Vector_2<T>& v) const { return Point_2<T>(X + v.X, Y + v.Y); }

	/// \brief Computes the difference of two points.
	/// \param p The second operand.
	/// \return The vector connecting the two points.	
	Vector_2<T> operator-(const Point_2<T>& p) const { return Vector_2<T>(X - p.X, Y - p.Y); }

	/// \brief Substracts a vector from a point.
	/// \param v The second operand.
	/// \return The new point.
	Point_2<T> operator-(const Vector_2<T>& v) const { return Point_2<T>(X - v.X, Y - v.Y); }

	/// \brief Converts the point to a vector.
	/// \param ORIGIN Just use the special value \c ORIGIN here.
	/// \return A vector with its components equal to the coordinates of this point. 
	const Vector_2<T>& operator-(Origin ORIGIN) const { 
		MAFEM_STATIC_ASSERT(sizeof(Point_2<T>) == sizeof(Vector_2<T>));
		return *(Vector_2<T>*)this; 
	}

	/// \brief Multiplies all coordinates of a point with a scalar.
	/// \param s The scalar value.
	/// \return A new point with the scaled coordinates.
	Point_2<T> operator*(T s) const { return Point_2<T>(X*s, Y*s); }

	/// \brief Divides all coordinates of a point by a scalar.
	/// \param s The scalar value.
	/// \return A new point with the scaled coordinates.
	Point_2<T> operator/(T s) const { return Point_2<T>(X/s, Y/s); }

	///////////////////////////////// Information ////////////////////////////////
	
	/// \brief Returns the number of coordinates of this point.
	/// \return Always returns 2.
	size_t size() const { return 2; }

	/// \brief Returns the number of dimensions of this point.
	/// \return Always returns 2.
	size_t dimension() const { return 2; }

	/// \brief Gives a string representation of this point.
	/// \return A string that contains the coordinates of the point. 
	QString toString() const { return QString("(%1 %2)").arg(X).arg(Y); }

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & X & Y; }	

	friend class boost::serialization::access;
};

/// \brief Converts a vector to a point.
/// \param ORIGIN Just use the special value \c ORIGIN here.
/// \param v The vector to convert.
/// \return A point with its coordinates equal to the components of the vector \a v. 
template<typename T>
Point_2<T> operator+(const Origin& ORIGIN, const Vector_2<T>& v) { 
	return Point_2<T>(v.X, v.Y); 
}

/// \brief Calculates the squared distance between two points.
/// \param a The first point.
/// \param b The second point.
/// \return The squared distance between \a a and \a b.
template<typename T>
inline T DistanceSquared(const Point_2<T>& a, const Point_2<T>& b) {
	return square(a.X - b.X) + square(a.Y - b.Y);
}

/// \brief Calculates the distance between two points.
/// \param a The first point.
/// \param b The second point.
/// \return The distance between \a a and \a b.
template<typename T>
inline T Distance(const Point_2<T>& a, const Point_2<T>& b) {
	return (T)sqrt(DistanceSquared(a, b));
}

/// \brief Writes the point to a text output stream.
/// \param os The output stream.
/// \param p The point to write to the output stream \a os.
/// \return The output stream \a os.
template<typename T>
inline std::ostream& operator<<(std::ostream &os, const Point_2<T> &p) {
	return os << p.X << ' ' << p.Y;    
}

/// \brief Writes the point to a logging stream.
/// \param log The logging stream.
/// \param p The point to write to the logging stream \a log.
/// \return The logging stream \a log.
template<typename T>
inline LoggerObject& operator<<(LoggerObject& log, const Point_2<T>& p)
{
     log.space() << "(" << p.X << p.Y << ")";
     return log.space();
} 

/// \brief Writes the point to a binary output stream.
/// \param stream The output stream.
/// \param p The point to write to the output stream \a stream.
/// \return The output stream \a stream.
template<typename T>
inline QDataStream& operator<<(QDataStream& stream, const Point_2<T>& p)
{
	return stream << p.X << p.Y;
}

/// \brief Reads a point from a binary input stream.
/// \param stream The input stream.
/// \param p Reference to a point variable where the parsed data will be stored.
/// \return The input stream \a stream.
template<typename T>
inline QDataStream& operator>>(QDataStream& stream, Point_2<T>& p)
{
	return stream >> p.X >> p.Y;
}

template class Point_2<FloatType>;
template class Point_2<int>;

/** 
 * \fn typedef Point2
 * \brief Template class instance of the Point_2 class used for floating-point points. 
 */
typedef Point_2<FloatType>	Point2;

/** 
 * \fn typedef Point2I
 * \brief Template class instance of the Point_2 class used for integer points. 
 */
typedef Point_2<int>		Point2I;

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Point2)
Q_DECLARE_METATYPE(Base::Point2I)
Q_DECLARE_TYPEINFO(Base::Point2, Q_PRIMITIVE_TYPE);
Q_DECLARE_TYPEINFO(Base::Point2I, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Point2, boost::serialization::object_serializable)

#endif // __POINT2_H
