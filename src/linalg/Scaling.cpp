///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include <Base.h>
#include <linalg/Scaling.h>
#include <linalg/AffineTransformation.h>
#include <linalg/AffineDecomposition.h>

namespace Base {

/// This dummy instance should be passed to the Scaling class constructor to initialize it to the identity.
IdentityScaling IDENTITY_SCALING;

/******************************************************************************
* Performs the multiplication of two scaling structures.
******************************************************************************/
Scaling Scaling::operator*(const Scaling& s2) const 
{
	if(Q == s2.Q) {
		return Scaling(Vector3(S.X * s2.S.X, S.Y * s2.S.Y, S.Z * s2.S.Z), Q);
	}
	else {
		AffineDecomposition decomp(AffineTransformation::scaling(*this) * AffineTransformation::scaling(s2));
		return decomp.scaling;
	}
}

/******************************************************************************
* Calculates a linear interpolation between two scaling structures.
******************************************************************************/
Scaling Scaling::interpolate(const Scaling& s1, const Scaling& s2, FloatType t)
{
	return Scaling(t * s2.S + ((FloatType)1.0 - t) * s1.S, Quaternion::interpolate(s1.Q, s2.Q, t));
}

};	// End of namespace Base
