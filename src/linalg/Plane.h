///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Plane.h 
 * \brief Contains definition of the Base::Plane_3 template class. 
 */

#ifndef __PLANE_H
#define __PLANE_H

#include <Base.h>
#include "Vector3.h"
#include "Point3.h"
#include "Ray.h"
#include "AffineTransformation.h"

namespace Base {

/**
 * \brief A plane in three-dimensional space.
 * 
 * The plane is defined by a normal vector and a distance value that specifies
 * the distance of the plane from the orgin in the direction of the normal vector.
 * 
 * \note This is a template class for general data types. Usually one wants to
 *       use this template class with the floating-point data type. There is
 *       a template instance called \c Plane3 that should normally be used.
 * 
 * \author Alexander Stukowski
 */
template<typename T>
class Plane_3
{
public:	
	/// \brief The unit normal vector.
	Vector_3<T> normal;
	/// \brief The distance of the plane from the origin.
	T dist;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief This empty default constructor does not initialize the components!
	/// \note All components of the plane are left uninitialized by this constructor and 
	///       will therefore have a random value!
	Plane_3() {}

	/// \brief Initializes the plane from a normal vector and a distance.
	/// \param n The normal vector. This must be a unit vector.
	/// \param d The distance of the plane from the origin in the direction of the normal vector \a n.
	Plane_3(const Vector_3<T>& n, const T& d) : normal(n), dist(d) {}

	/// \brief Initializes the plane from a point and a normal vector.
	/// \param basePoint A point in the plane.
	/// \param n The normal vector. This must be a unit vector.
	Plane_3(const Point_3<T>& basePoint, const Vector_3<T>& n) : normal(n), dist(DotProduct(basePoint - ORIGIN, normal)) {}

	/// \brief Initializes the plane from three points (without normalization).
	/// \param p1 The first point in the plane.
	/// \param p2 The second point in the plane.
	/// \param p3 The third point in the plane.
	/// \note The three points must linearly independent of each other. 
	/// \note The normal vector computed from the three points is not normalized.
	///       You have to call normalizePlane() to do this.     
	Plane_3(const Point_3<T>& p1, const Point_3<T>& p2, const Point_3<T>& p3) {
		normal = CrossProduct(p2-p1, p3-p1);
		dist = DotProduct(normal, p1 - ORIGIN);
	}
	
	/// \brief Initializes the plane from three points with optional normalization.
	/// \param p1 The first point in the plane.
	/// \param p2 The second point in the plane.
	/// \param p3 The third point in the plane.
	/// \param normalize Controls the normalization of the calculated normal vector.
	///        If \a normalize is set to \c false then the normal vector can be normalized by
	///        a call to normalizePlane() at any later time.
	/// \note The three points must linearly independent of each other. 
	Plane_3(const Point_3<T>& p1, const Point_3<T>& p2, const Point_3<T>& p3, bool normalize) {
		if(normalize)
			normal = Normalize(CrossProduct(p2-p1, p3-p1));
		else
			normal = CrossProduct(p2-p1, p3-p1);
		dist = DotProduct(normal, p1 - ORIGIN);
	}

	/// \brief Initializes the plane from one point and two in-plane vectors with optional normalization.
	/// \param p The base point in the plane.
	/// \param v1 The first vector in the plane.
	/// \param v2 The second vector in the plane.
	/// \param normalize Controls the normalization of the calculated normal vector.
	///        If \a normalize is set to \c false then the normal vector can be normalized by
	///        a call to normalizePlane() at any later time.
	/// \note The two vectors must be linearly independent of each other. 
	Plane_3(const Point_3<T>& p, const Vector_3<T>& v1, const Vector_3<T>& v2, bool normalize = true) {
		if(normalize)
			normal = Normalize(CrossProduct(v1, v2));
		else
			normal = CrossProduct(v1, v2);
		dist = DotProduct(normal, p - ORIGIN);
	}
	
	/// \brief Scales the normal vector of the plane to unit length 1.
	void normalizePlane() {
		T len = Length(normal);
		MAFEM_ASSERT_MSG(len != (T)0, "Plane_3::normalizePlane()", "The normal vector of the plane must not be the null vector.");
		dist *= len;
		normal /= len;
		MAFEM_ASSERT(abs(LengthSquared(normal) - (T)1) <= (T)FLOATTYPE_EPSILON);
	}

    ////////////////////////////////// operators /////////////////////////////////

	/// \brief Flips the plane orientation.
	/// \return A new plane with reversed orientation.
	Plane_3<T> operator-() const { return(Plane_3<T>(-normal, -dist)); } 

	/// \brief Compares two planes for equality.
	/// \return \c true if the normal vectors and the distance value of both planes a equal; \c false otherwise.
	bool operator==(const Plane_3<T>& other) const { return normal == other.normal && dist == other.dist; }

	/////////////////////////////// Classification ///////////////////////////////
    
	/// \brief Classifies a point with respect to the plane.
	/// \param p The point to classify.
	/// \param tolerance A non-negative threshold value that is used to test whether the point is on the plane.
	/// \return 1 if \a p is on the POSITIVE side of the plane,
	///         -1 if \a p is on the NEGATIVE side or 0 if \a p is ON the plane within the tolerance.
	/// \sa pointDistance()
	int classifyPoint(const Point_3<T>& p, const T tolerance = (T)FLOATTYPE_EPSILON) const {
		MAFEM_ASSERT_MSG(tolerance >= 0, "Plane_3::classifyPoint()", "Tolerance value must be non-negative.");
        T d = pointDistance(p);
		//MsgLogger() << "Distance" << d << endl;
		//MsgLogger() << "*********************************" << endl;
		if(d < -tolerance) return -1;
		else if(d > tolerance) return 1;
		else return 0;
	}

	/// \brief Computes the signed distance of a point to the plane.
	/// \param p The input point.
	/// \note This method requires the plane normal to be a unit vector.
	/// \return The distance of the point to the plane. A positive value means that the point
	/// is on the positive side of the plane. A negative value means that the points is located in the 
	/// back of the plane.
	/// \sa classifyPoint()
	T pointDistance(const Point_3<T>& p) const {
		//return p.X*normal.X + p.Y*normal.Y + p.Z*normal.Z - dist;
		//MsgLogger() << p.X << p.Y << p.Z << normal.X << normal.Y << normal.Z <<  dist << endl;
		return DotProduct(p - ORIGIN, normal) - dist;
	}

	///////////////////////////////// Intersection ///////////////////////////////

	/// \brief Computes the intersection point of a ray with the plane.
	/// \param ray The input ray.
	/// \param epsilon A threshold value that determines whether the ray is considered parallel to the plane.
	/// \return The intersection point.
	/// \throw Exception if the plane and the ray direction are parallel.
	/// \note This method requires a unit plane normal vector.
	/// \sa intersectionT()
	Point_3<T> intersection(const Ray3& ray, T epsilon = (T)0) const {
		FloatType t = intersectionT(ray, epsilon);
		if(t == FLOATTYPE_MAX) throw Exception("Error in Plane_3::intersection(): There is no intersection point. Ray is parallel to plane.");
		return ray.point(t);
	}

	/// \brief Computes the t value for a ray-plane intersection.
	///
	/// The returned t value is choosen so that (ray.base + t*ray.dir) == point of intersection.	
	/// If there is no intersection point then the special value \c FLOATTYPE_MAX is returned.
	/// \param ray The input ray.
	/// \param epsilon A threshold value that determines whether the ray is considered parallel to the plane.
	/// \note This implementation requires a unit normal vector.
	/// \sa intersection()
	FloatType intersectionT(const Ray3& ray, T epsilon = (T)0) const {
		// The plane's normal vector should be normalized.
		MAFEM_ASSERT(abs(LengthSquared(normal) - (T)1) <= (T)FLOATTYPE_EPSILON);
		FloatType dot = DotProduct(normal, ray.dir);
		if(abs(dot) <= epsilon) return FLOATTYPE_MAX;
		return -pointDistance(ray.base) / dot;
	}

    ////////////////////////////////// Utilities /////////////////////////////////

	/// \brief Returns a string representation of this plane.
	QString toString() const { return QString("[Normal: %1 D: %2]").arg(normal.toString()).arg(dist); }

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & normal & dist; }	

	friend class boost::serialization::access;
};

/// \brief Transforms a plane.
/// \param tm The transformation matrix.
/// \param ray The plane to be transformed.
/// \return A new plane with transformed normal vector and origin distance.
///         The normal vector is normalized after the transformation.
template<typename T>
inline Plane_3<T> operator*(const AffineTransformation& tm, const Plane_3<T>& plane) {
	Plane_3<T> p2;
	p2.normal = Normalize(tm * plane.normal);
	Point_3<T> base = tm * (ORIGIN + plane.normal*plane.dist);
	p2.dist = DotProduct(base - ORIGIN, p2.normal);
	return p2;
}

/// \brief Writes the plane to an output stream.
/// \param os The output stream.
/// \param p The plane to write to the output stream \a os.
/// \return The output stream \a os.
template<typename T>
inline std::ostream& operator<<(std::ostream &os, const Plane_3<T>& p) {
	return os << '[' << p.normal.X << ' ' << p.normal.Y  << ' ' << p.normal.Z << "], " << p.dist;
}

/// \brief Writes the plane to a logging stream.
/// \param log The logging stream.
/// \param p The plane to write to the logging stream \a log.
/// \return The logging stream \a log.
template<typename T>
inline LoggerObject& operator<<(LoggerObject& log, const Plane_3<T>& p)
{
     log.space() << '[' << p.normal.X << ' ' << p.normal.Y  << ' ' << p.normal.Z << "], " << p.dist;
     return log.space();
} 

/// \brief Writes the plane to a binary output stream.
/// \param stream The output stream.
/// \param p The plane to write to the output stream \a stream.
/// \return The output stream \a stream.
template<typename T>
inline QDataStream& operator<<(QDataStream& stream, const Plane_3<T>& p)
{
	return stream << p.normal << p.dist;
}

/// \brief Reads a plane from a binary input stream.
/// \param stream The input stream.
/// \param p Reference to a plane variable where the parsed data will be stored.
/// \return The input stream \a stream.
template<typename T>
inline QDataStream& operator>>(QDataStream& stream, Plane_3<T>& p)
{
	return stream >> p.normal >> p.dist;
}
	
template class Plane_3<FloatType>;

/** 
 * \fn typedef Plane3
 * \brief Template class instance of the Plane_3 class used for floating-point planes. 
 */
typedef Plane_3<FloatType> Plane3;

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Plane3)
Q_DECLARE_TYPEINFO(Base::Plane3, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Plane3, boost::serialization::object_serializable)

#endif // __PLANE_H
