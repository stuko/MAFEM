///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include <Base.h>
#include <linalg/Rotation.h>
#include <linalg/AffineTransformation.h>
#include <linalg/Vector3.h>
#include <linalg/Quaternion.h>

namespace Base {

/// This dummy instance should be passed to the Rotation class constructor to initialize it to zero.
NullRotation NULL_ROTATION;

/******************************************************************************
* Initializes the object from rotational part of the matrix.
******************************************************************************/
Rotation::Rotation(const AffineTransformation& tm)
{
	axis.X = (tm(2,1) - tm(1,2));
	axis.Y = (tm(0,2) - tm(2,0));
	axis.Z = (tm(1,0) - tm(0,1));
	if(axis == NULL_VECTOR) {
		axis.Z = 1.0;
		angle = 0.0;
	}
	else {
		FloatType trace = (tm(0,0) + tm(1,1) + tm(2,2) - 1.0);
		FloatType s = Length(axis);
		axis /= s;
		angle = atan2(s, trace);
	}
}

inline Vector3 interpolateAxis(FloatType time, const Vector3& axis0, const Vector3& axis1) 
{
	// assert:  axis0 and axis1 are unit length
	// assert:  DotProduct(axis0, axis1) >= 0
	// assert:  0 <= time <= 1

	FloatType cos = DotProduct(axis0, axis1);  // >= 0 by assertion
	MAFEM_ASSERT(cos >= 0.0);
	if(cos > 1.0) cos = 1.0; // round-off error might create problems in acos call        

	FloatType angle = acos(cos);
	FloatType invSin = 1.0 / sin(angle);
	FloatType timeAngle = time * angle;
	FloatType coeff0 = sin(angle - timeAngle) * invSin;
	FloatType coeff1 = sin(timeAngle) * invSin;

	return (coeff0 * axis0 + coeff1 * axis1);
}

inline Quaternion slerpExtraSpins(FloatType t, const Quaternion& p, const Quaternion& q, int iExtraSpins) 
{
	FloatType fCos = DotProduct(p,q);
	MAFEM_ASSERT(fCos >= 0.0);

	// Numerical round-off error could create problems in call to acos.
	if(fCos < -1.0) fCos = -1.0;
	else if(fCos > 1.0) fCos = 1.0;

	FloatType fAngle = acos(fCos);
	FloatType fSin = sin(fAngle);  // fSin >= 0 since fCos >= 0

	if(fSin < 0.001) {
		return p;
	}
	else {
		FloatType fPhase = FLOATTYPE_PI * (FloatType)iExtraSpins * t;
		FloatType fInvSin = 1.0 / fSin;
		FloatType fCoeff0 = sin((1.0f - t) * fAngle - fPhase) * fInvSin;
		FloatType fCoeff1 = sin(t * fAngle + fPhase) * fInvSin;
		return Quaternion(fCoeff0*p.X + fCoeff1*q.X, fCoeff0*p.Y + fCoeff1*q.Y,
		                        fCoeff0*p.Z + fCoeff1*q.Z, fCoeff0*p.W + fCoeff1*q.W);
	}
}


/******************************************************************************
* Interpolates between the two rotation structures and handles multiple revolutions.
******************************************************************************/
Quaternion Rotation::interpolate(const Rotation& rot1, const Rotation& _rot2, FloatType t)
{
	MAFEM_ASSERT(t >= 0.0 && t <= 1.0);

	Rotation rot2;
	if(DotProduct(rot1.axis, _rot2.axis) < 0.0) {
		rot2.angle = -_rot2.angle;
		rot2.axis = -_rot2.axis;
	}
	else rot2 = _rot2;

	Quaternion q1 = (Quaternion)rot1;
	Quaternion q2 = (Quaternion)rot2;

	// Eliminate any non-acute angles between successive quaternions. This
	// is done to prevent potential discontinuities that are the result of
	// invalid intermediate value quaternions.
	if(DotProduct(q1,q2) < 0.0) {
		q2 = -q2;
	}

	// Clamp identity quaternions so that |w| <= 1 (avoids problems with
	// call to acos in SlerpExtraSpins).
	if(q1.W < -1.0) q1.W = -1.0; else if(q1.W > 1.0) q1.W = 1.0;
	if(q2.W < -1.0) q2.W = -1.0; else if(q2.W > 1.0) q2.W = 1.0;

	// Determine interpolation type, compute extra spins, and adjust angles accordingly.
	FloatType fDiff = rot1.angle - rot2.angle;
	int iExtraSpins;
	if(abs(fDiff) < FLOATTYPE_PI*2.0) {
		return Quaternion::interpolate(q1, q2, t);
	}
	else {
		iExtraSpins = (int)(fDiff/(FLOATTYPE_PI*2.0));
		
		if(rot1.axis.equals(rot2.axis, FLOATTYPE_EPSILON)) {
			Rotation result(rot1.axis, (1.0f-t) * rot1.angle + t * rot2.angle);
			return (Quaternion)result;
		}
		else if(rot1.angle != 0.0)
			return slerpExtraSpins(t, q1, q2, iExtraSpins);
		else {
			Rotation result(interpolateAxis(t, rot1.axis, rot2.axis), (1.0 - t) * rot1.angle + t * rot2.angle);
			return (Quaternion)result;
		}
	}
}

};	// End of namespace Base
