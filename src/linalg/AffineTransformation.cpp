///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include <Base.h>
#include <linalg/AffineTransformation.h>

namespace Base {

/******************************************************************************
* Generates a rotation matrix around the given axis.
******************************************************************************/
AffineTransformation AffineTransformation::rotation(const Rotation& rot)
{
	FloatType c = cos(rot.angle);
	FloatType s = sin(rot.angle);
	FloatType t = 1.0 - c;
    const Vector3& a = rot.axis;
	MAFEM_ASSERT_MSG(abs(LengthSquared(a) - 1.0) <= FLOATTYPE_EPSILON, "AffineTransformation::rotation", "Rotation axis vector must be normalized.");
	return AffineTransformation(	t * a.X * a.X + c,       t * a.X * a.Y - s * a.Z, t * a.X * a.Z + s * a.Y, 0.0, 
					t * a.X * a.Y + s * a.Z, t * a.Y * a.Y + c,       t * a.Y * a.Z - s * a.X, 0.0, 
					t * a.X * a.Z - s * a.Y, t * a.Y * a.Z + s * a.X, t * a.Z * a.Z + c      , 0.0);
}

/******************************************************************************
* Generates a rotation matrix from a quaternion.
******************************************************************************/
AffineTransformation AffineTransformation::rotation(const Quaternion& q)
{
#if defined(_DEBUG)	
	if(abs(DotProduct(q,q) - 1.0) > FLOATTYPE_EPSILON) {
		MsgLogger() << "Invalid quaternion is: " << q;
		MAFEM_ASSERT_MSG(false, "AffineTransformation::rotation", "Quaternion must be normalized.");
	}
#endif
	return AffineTransformation(1.0 - 2.0*(q.Y*q.Y + q.Z*q.Z),       2.0*(q.X*q.Y - q.W*q.Z),       2.0*(q.X*q.Z + q.W*q.Y), 0.0,
				        2.0*(q.X*q.Y + q.W*q.Z), 1.0 - 2.0*(q.X*q.X + q.Z*q.Z),       2.0*(q.Y*q.Z - q.W*q.X), 0.0,
			            2.0*(q.X*q.Z - q.W*q.Y),       2.0*(q.Y*q.Z + q.W*q.X), 1.0 - 2.0*(q.X*q.X + q.Y*q.Y), 0.0);
}

/******************************************************************************
* Generates a rotation matrix around the X axis.
******************************************************************************/
AffineTransformation AffineTransformation::rotationX(double angle)
{
	FloatType c = cos(angle);
	FloatType s = sin(angle);
	return AffineTransformation(1.0, 0.0, 0.0, 0.0,
				  0.0,   c,  -s, 0.0,
				  0.0,   s,   c, 0.0);
}

/******************************************************************************
* Generates a rotation matrix around the Y axis.
******************************************************************************/
AffineTransformation AffineTransformation::rotationY(double angle)
{
	FloatType c = cos(angle);
	FloatType s = sin(angle);
	return AffineTransformation(  c, 0.0,   s, 0.0,
				  0.0, 1.0, 0.0, 0.0,
				   -s, 0.0,   c, 0.0);
}

/******************************************************************************
* Generates a rotation matrix around the Z axis.
******************************************************************************/
AffineTransformation AffineTransformation::rotationZ(double angle)
{
	FloatType c = cos(angle);
	FloatType s = sin(angle);
	return AffineTransformation(c,  -s, 0.0, 0.0,
								s,   c, 0.0, 0.0,
								0.0, 0.0, 1.0, 0.0);
}

/******************************************************************************
* Generates a rotation matrix around a given axis.
******************************************************************************/
AffineTransformation AffineTransformation::rotationAxis(Vector3& axis, double angle)
{
	Vector3 nomalizedAxis = Normalize(axis);
	
	Matrix3 rotationMatrix = Matrix3::identity() * cos(angle) + sin(angle) * crossProductMatrix(nomalizedAxis) + (1 - cos(angle)) * Dyadic(nomalizedAxis, nomalizedAxis);
	
	return AffineTransformation(rotationMatrix);
}

/******************************************************************************
* Generates a rotation matrix woth a given center of rotation.
******************************************************************************/
AffineTransformation AffineTransformation::rotationPoint(AffineTransformation rotation, Point3& point)
{
	return translation(point - Point3(ORIGIN)) * rotation * translation(Point3(ORIGIN) - point);
}

/******************************************************************************
* Generates a translation matrix.
******************************************************************************/
AffineTransformation AffineTransformation::translation(double dx, double dy, double dz)
{
	return AffineTransformation(1.0, 0.0, 0.0, dx,
								0.0, 1.0, 0.0, dy,
								0.0, 0.0, 1.0, dz);
}

/******************************************************************************
* Generates a translation matrix.
******************************************************************************/
AffineTransformation AffineTransformation::translation(const Vector3& t)
{
	return AffineTransformation(1.0, 0.0, 0.0, t.X,
								0.0, 1.0, 0.0, t.Y,
								0.0, 0.0, 1.0, t.Z);
}

/******************************************************************************
* Generates a scaling matrix.
******************************************************************************/
AffineTransformation AffineTransformation::scaling(double sx, double sy, double sz)
{
	return AffineTransformation(  sx, 0.0, 0.0, 0.0,
				  0.0,   sy, 0.0, 0.0,
				  0.0, 0.0,   sz, 0.0);
}

/******************************************************************************
* Generates a scaling matrix.
******************************************************************************/
AffineTransformation AffineTransformation::scaling(Vector3& s)
{
	return AffineTransformation(  s.X, 0.0, 0.0, 0.0,
				  0.0,   s.Y, 0.0, 0.0,
				  0.0, 0.0,   s.Z, 0.0);
}

/******************************************************************************
* Generates a diagonal scaling matrix.
******************************************************************************/
AffineTransformation AffineTransformation::scaling(double s)
{
	return AffineTransformation(  s, 0.0, 0.0, 0.0,
				  0.0,   s, 0.0, 0.0,
				  0.0, 0.0,   s, 0.0);
}

/******************************************************************************
* Generates a scaling matrix.
******************************************************************************/
AffineTransformation AffineTransformation::scaling(const Scaling& scaling)
{	
	Matrix3 U = Matrix3::rotation(scaling.Q);
	Matrix3 K = Matrix3(scaling.S.X, 0.0, 0.0,
						0.0, scaling.S.Y, 0.0,
						0.0, 0.0, scaling.S.Z);
	return (U * K * U.transposed());
}

/******************************************************************************
* Generates a look at matrix.
******************************************************************************/
AffineTransformation AffineTransformation::lookAt(const Point3& camera, const Point3& target, const Vector3& upVector)
{	
	Vector3 zaxis = Normalize(camera - target);
	Vector3 xaxis = CrossProduct(upVector, zaxis);
	if(xaxis == NULL_VECTOR) {
		xaxis = CrossProduct(Vector3(0,1,0), zaxis);
		if(xaxis == NULL_VECTOR) {
			xaxis = CrossProduct(Vector3(0,0,1), zaxis);
			MAFEM_ASSERT(xaxis != NULL_VECTOR);
		}
	}
	xaxis = Normalize(xaxis);
	Vector3 yaxis = CrossProduct(zaxis, xaxis);

	return AffineTransformation(	xaxis.X, xaxis.Y, xaxis.Z, -DotProduct(xaxis, camera - ORIGIN), 
					yaxis.X, yaxis.Y, yaxis.Z, -DotProduct(yaxis, camera - ORIGIN), 
					zaxis.X, zaxis.Y, zaxis.Z, -DotProduct(zaxis, camera - ORIGIN));
}


/******************************************************************************
* Generates a matrix with shearing normal to the z-axis.
******************************************************************************/
AffineTransformation AffineTransformation::shear(FloatType gammaX, FloatType gammaY)
{
	return AffineTransformation(1.0, 0.0, gammaX, 0.0,
				  0.0, 1.0, gammaY, 0.0,
				  0.0, 0.0, 1.0, 0.0);
}


};	// End of namespace Base
