///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file LinAlg.h 
 * \brief This header file includes other header files
 *        to import the most important linear algebra classes. 
 */

#ifndef __LINALG_H
#define __LINALG_H

#include <Base.h>

#include "Vector3.h"
#include "Vector2.h"
#include "VectorN.h"
#include "Point3.h"
#include "Point2.h"
#include "AffineTransformation.h"
#include "Matrix3.h"
#include "Matrix4.h"
#include "Scaling.h"
#include "Rotation.h"
#include "Quaternion.h"
#include "Box2.h"
#include "Box3.h"
#include "Ray.h"
#include "Plane.h"
#include "Tensor.h"

#endif // __LINALG_H
