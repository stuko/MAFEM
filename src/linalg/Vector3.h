///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Vector3.h 
 * \brief Contains definition of the Base::Vector_3 template class and operators. 
 */

#ifndef __VECTOR3_H
#define __VECTOR3_H

#include <Base.h>

namespace Base {

// Empty tag class.
class NullVector {};
// This dummy instance should be passed to the Vector_3/Vector_2 constructor to initialize it to the null vector.
extern NullVector NULL_VECTOR;

/**
 * \brief A vector with three components X, Y, Z.
 * 
 * This is one of the basic vector algebra classes. It represents a three
 * dimensional vector in space. There are two instances of this template
 * vector class: \c Vector3 is for floating-point vectors and \c Vector3I is
 * for integer vectors with three components.
 * 
 * Note that there is also a class called Point_3 that is used for points
 * in a three dimensional coordinate system.
 * 
 * \author Alexander Stukowski
 * \sa Point_3
 * \sa Vector_2
 */
template<typename ValueType> 
class Vector_3
{
public:	
	/// \brief The X component of the vector.
	ValueType X;
	/// \brief The Y component of the vector.
	ValueType Y;
	/// \brief The Z component of the vector.
	ValueType Z;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief Constructs a vector without initializing its component.
	/// \note All components are left uninitialized by this constructor and will therefore have a random value!
	Vector_3() {}

	/// \brief Constructs a vector with all components set to the given value. 
	/// \param val The value to be assigned to each of the vector's components.
	explicit Vector_3(ValueType val) { X = Y = Z = val; }

	/// \brief Initializes the components of the vector with the given component values.
	/// \param x The X component of the new vector.
	/// \param y The Y component of the new vector.
	/// \param z The Z component of the new vector.
    inline Vector_3(ValueType x, ValueType y, ValueType z) : X(x), Y(y), Z(z) {}

	/// \brief Initializes the components of the vector with the values in the given array.
	/// \param val The array that contains the vector components.
    Vector_3(ValueType val[3]) : X(val[0]), Y(val[1]), Z(val[2]) {}

	/// \brief Initializes the vector to the null vector. All components are set to zero.
	/// \param NULL_VECTOR A dummy parameter to distinguish this overloaded constructor from the others. 
	///        When using this constructor, jsut use the special value \c NULL_VECTOR here.
    inline Vector_3(NullVector NULL_VECTOR) : X((ValueType)0), Y((ValueType)0), Z((ValueType)0) {}

    ///////////////////////////// Component access ///////////////////////////////

	/// \brief Returns a reference to the i-th component of the vector.
	/// \param i The index specifying the component to return (0=X, 1=Y, 2=Z).
	/// \return A reference to the i-th component that can be used to change the component's value. 
	inline ValueType& operator[](size_t i) {
		MAFEM_STATIC_ASSERT(sizeof(Vector_3<ValueType>) == sizeof(ValueType) * 3);
		MAFEM_ASSERT_MSG(i>=0 && i<size(), "Vector3 operator[]", "Index out of range.");
		return (&X)[i]; 
	}

	/// \brief Returns a reference to the i-th component of the vector.
	/// \param i The index specifying the component to return (0=X, 1=Y, 2=Z).
	/// \return The i-th component of the vector. 
	inline const ValueType& operator[](size_t i) const {
		MAFEM_STATIC_ASSERT(sizeof(Vector_3<ValueType>) == sizeof(ValueType) * 3);
		MAFEM_ASSERT_MSG(i>=0 && i<size(), "Vector3 operator[]", "Index out of range.");
		return (&X)[i]; 
	}

	/// \brief Returns a pointer to the first element of the vector.
	/// \return A pointer to three consecutive floating-point numbers: X, Y and Z.
	/// \sa constData() 
	inline ValueType* data() { 		
        MAFEM_STATIC_ASSERT(sizeof(Vector_3<ValueType>) == sizeof(ValueType) * 3);
		return (ValueType*)this;
	}

	/// \brief Returns a pointer to the first element of the vector for read-only access.
	/// \return A pointer to three consecutive floating-point numbers: X, Y and Z.
	/// \sa data()
	inline const ValueType* constData() const {
        MAFEM_STATIC_ASSERT(sizeof(Vector_3<ValueType>) == sizeof(ValueType) * 3);
		return (const ValueType*)this;
	}

	/// \brief Casts the vector to a vector with another data type.
	template<typename T2>
	operator Vector_3<T2>() const { return Vector_3<T2>((T2)X, (T2)Y, (T2)Z); }

    /////////////////////////////// Unary operators //////////////////////////////

	/// \brief Returns the inverse of the vector.
	/// \return A vector with negated components: (-X, -Y, -Z).
	inline Vector_3<ValueType> operator-() const { return(Vector_3<ValueType>(-X, -Y, -Z)); } 

	///////////////////////////// Assignment operators ///////////////////////////

	/// \brief Adds another vector to this vector and stores the result in this vector.
	/// \param v The vector to add to this vector.
	/// \return A reference to \c this vector, which has been changed.
	inline Vector_3<ValueType>& operator+=(const Vector_3<ValueType>& v) { X += v.X; Y += v.Y; Z += v.Z; return *this; }

	/// \brief Substracts another vector from this vector and stores the result in this vector.
	/// \param v The vector to substract from this vector.
	/// \return A reference to \c this vector, which has been changed.
	inline Vector_3<ValueType>& operator-=(const Vector_3<ValueType>& v) { X -= v.X; Y -= v.Y; Z -= v.Z; return *this; }

	/// \brief Multplies each component of the vector with a scalar value and stores the result in this vector.
	/// \param s The scalar value to multiply this vector with.
	/// \return A reference to \c this vector, which has been changed.
	inline Vector_3<ValueType>& operator*=(ValueType s) { X *= s; Y *= s; Z *= s; return *this; }

	/// \brief Divides each component of the vector by a scalar value and stores the result in this vector.
	/// \param s The scalar value.
	/// \return A reference to \c this vector, which has been changed.
	inline Vector_3<ValueType>& operator/=(ValueType s) { X /= s; Y /= s; Z /= s; return *this; }

	//////////////////////////// Component read access //////////////////////////

	/// \brief Returns the value of the X component of this vector.
	const ValueType& x() const { return X; }

	/// \brief Returns the value of the Y component of this vector.
	const ValueType& y() const { return Y; }

	/// \brief Returns the value of the Z component of this vector.
	const ValueType& z() const { return Z; }

	//////////////////////////// Component write access //////////////////////////

	/// \brief Sets the X component of this vector to a new value.
	/// \param value The new value that is assigned to the vector component.
	void setx(const ValueType& value) { X = value; }

	/// \brief Sets the Y component of this vector to a new value.
	/// \param value The new value that is assigned to the vector component.
	void sety(const ValueType& value) { Y = value; }

	/// \brief Sets the Z component of this vector to a new value.
	/// \param value The new value that is assigned to the vector component.
	void setz(const ValueType& value) { Z = value; }

	////////////////////////////////// Comparison ////////////////////////////////

	/// \brief Compares two vectors for equality.
	/// \return true if each of the components are equal; false otherwise.
	inline bool operator==(const Vector_3<ValueType>& v) const { return (v.X==X) && (v.Y==Y) && (v.Z==Z); }

	/// \brief Compares two vectors for inequality.
	/// \return true if any of the components are not equal; false if all are equal.
	inline bool operator!=(const Vector_3<ValueType>& v) const { return (v.X!=X) || (v.Y!=Y) || (v.Z!=Z); }

	/// \brief Checks whether the vector is the null vector, i.e. all components are zero.
	/// \return true if all of the components are zero; false otherwise
	inline bool operator==(const NullVector& NULL_VECTOR) const { return (X==(ValueType)0) && (Y==(ValueType)0) && (Z==(ValueType)0); }

	/// \brief Checks whether the vector is not a null vector, i.e. any of the components is nonzero.
	/// \return true if any of the components is nonzero; false if this is the null vector otherwise
	inline bool operator!=(const NullVector& NULL_VECTOR) const { return (X!=(ValueType)0) || (Y!=(ValueType)0) || (Z!=(ValueType)0); }

	/// \brief Checks whether two vectors are equal within a given tolerance.
	/// \param v The vector that should be compared to this vector.
	/// \param tolerance A non-negative threshold for the equality test. The two vectors are considered equal when 
	///        the differences in the X, Y, and Z directions are all smaller than this tolerance value.
	/// \return true if this vector is equal to the given vector within the given tolerance.
	inline bool equals(const Vector_3<ValueType>& v, ValueType tolerance) const {
		MAFEM_ASSERT(tolerance >= 0);
		return abs(v.X - X) <= tolerance && abs(v.Y - Y) <= tolerance && abs(v.Z - Z) <= tolerance; 
	}

	/////////////////////////////// Binary operators /////////////////////////////

	/// \brief Computes the sum of two vectors.
	/// \param v The second operand.
	/// \return The sum of two vectors.
	inline Vector_3<ValueType> operator+(const Vector_3<ValueType>& v) const { return Vector_3<ValueType>(X + v.X, Y + v.Y, Z + v.Z); }

	/// \brief Computes the difference of two vectors.
	/// \param v The second operand.
	/// \return The difference of two vectors.
	inline Vector_3<ValueType> operator-(const Vector_3<ValueType>& v) const { return Vector_3<ValueType>(X - v.X, Y - v.Y, Z - v.Z); }

	/// \brief Computes the product of a vector and a scalar value. All
	///        components of the vector are multiplied by the scalar \a s.
	/// \param s The second operand.		
	/// \return The product of the vector with a scalar \a s: (s*X, s*Y, s*Z).
	inline Vector_3<ValueType> operator*(ValueType s) const { return Vector_3<ValueType>(X*s, Y*s, Z*s); }

	/// \brief Computes the division of a vector by a scalar value. All
	///        components of the vector are divided by the scalar \a s.
	/// \param s The second operand.		
	/// \return The division of the vector by a scalar \a s: (X/s, Y/s, Z/s).
	inline Vector_3<ValueType> operator/(ValueType s) const { return Vector_3<ValueType>(X/s, Y/s, Z/s); }

	///////////////////////////////// Information ////////////////////////////////
	
	/// \brief Returns the number of components of this vector.
	/// \return Always returns 3.
	inline size_t size() const { return 3; }

	/// \brief Returns the dimension of this vector (the constant 3).
	/// \return Always returns 3.
	inline size_t dimension() const { return 3; }

	/// \brief Gives a string representation of this vector.
	/// \return A string that contains the components of the vector. 
	QString toString() const { return QString("(%1 %2 %3)").arg(X).arg(Y).arg(Z); }

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & X & Y & Z; }	

	friend class boost::serialization::access;
};

/// \brief Returns the product of the vector with a scalar: (v.X*s, v.Y*s, v.Z*s).
template<typename ValueType>
inline Vector_3<ValueType> operator*(const ValueType& s, const Vector_3<ValueType>& v) { return v * s; }

/// \brief Computes the scalar product of two vectors.
/// \param a The first vector.
/// \param b The second vector.
/// \return The scalar vector product: a.X*b.X + a.Y*b.Y + a.Z*b.Z 
template<typename ValueType>
inline ValueType DotProduct(const Vector_3<ValueType>& a, const Vector_3<ValueType>& b) { 
	return a.X*b.X + a.Y*b.Y + a.Z*b.Z;
}

/// \brief Computes the cross product of two vectors.
/// \param a The first vector.
/// \param b The second vector.
/// \return The cross product \a a x \a b.
template<typename ValueType>
inline Vector_3<ValueType> CrossProduct(const Vector_3<ValueType>& a, const Vector_3<ValueType>& b) {
	return Vector_3<ValueType>(a.Y * b.Z - a.Z * b.Y,
				       a.Z * b.X - a.X * b.Z,
				       a.X * b.Y - a.Y * b.X);
}

/// \brief Returns the squared length of a vector.
/// \param a The input vector.
/// \return The squared length of vector \a a: X*X + Y*Y + Z*Z
/// \sa Length()
template<typename ValueType>
inline ValueType LengthSquared(const Vector_3<ValueType>& a) {
	return a.X*a.X + a.Y*a.Y + a.Z*a.Z;
}

/// \brief Returns the length of a vector.
/// \param a The input vector.
/// \return The length of vector \a a: sqrt(X*X + Y*Y + Z*Z)
/// \sa LengthSquared(), Normalize()
template<typename ValueType>
inline ValueType Length(const Vector_3<ValueType>& a) {
	return (ValueType)sqrt(LengthSquared(a));
}

/// \brief Normalizes a vector to unit length.
/// \param a The input vector.
/// \return The vector \a a divided by its length.
template<typename ValueType>
inline Vector_3<ValueType> Normalize(const Vector_3<ValueType>& a) {
	MAFEM_ASSERT_MSG(a != NULL_VECTOR, "Normalize(const Vector3&)", "Cannot normalize the null vector.");
	return a / Length(a);
}

/// \brief Finds the index of the component with the maximum value.
/// \param a The input vector.
/// \return The index (0-2) with the largest value.
template<typename ValueType>
inline size_t MaxComponent(const Vector_3<ValueType>& a) {
    return ((a.X >= a.Y) ? ((a.X >= a.Z) ? 0 : 2) : ((a.Y >= a.Z) ? 1 : 2));	
}

/// \brief Finds the index of the component with the minimum value.
/// \param a The input vector.
/// \return The index (0-2) with the smallest value.
template<typename ValueType>
inline size_t MinComponent(const Vector_3<ValueType>& a) {
    return ((a.X <= a.Y) ? ((a.X <= a.Z) ? 0 : 2) : ((a.Y <= a.Z) ? 1 : 2));
}

/// \brief Finds the index of the component with the maximum absolute value.
/// \param a The input vector.
/// \return The index (0-2) with the largest absolute value.
template<typename ValueType>
inline size_t MaxAbsComponent(const Vector_3<ValueType>& a) {
    return ((abs(a.X) >= abs(a.Y)) ? ((abs(a.X) >= abs(a.Z)) ? 0 : 2) : ((abs(a.Y) >= abs(a.Z)) ? 1 : 2));	
}

/// \brief Writes the vector to a text output stream.
/// \param os The output stream.
/// \param v The vector to write to the output stream \a os.
/// \return The output stream \a os.
template<typename ValueType>
inline std::ostream& operator<<(std::ostream &os, const Vector_3<ValueType> &v) {
	return os << v.X << ' ' << v.Y  << ' ' << v.Z;
}

/// \brief Writes the vector to a logging stream.
/// \param log The logging stream.
/// \param v The vector to write to the logging stream \a log.
/// \return The logging stream \a log.
template<typename ValueType>
inline LoggerObject& operator<<(LoggerObject& log, const Vector_3<ValueType>& v)
{
	log.space() << "(" << v.X << v.Y << v.Z << ")";
	return log.space();
} 

/// \brief Writes a vector to a binary Qt output stream.
/// \param stream The output stream.
/// \param v The vector to write to the output stream \a stream.
/// \return The output stream \a stream.
template<typename ValueType>
inline QDataStream& operator<<(QDataStream& stream, const Vector_3<ValueType>& v)
{
	return stream << v.X << v.Y << v.Z;
}

/// \brief Writes a vector to a text Qt output stream.
/// \param stream The output stream.
/// \param v The vector to write to the output stream \a stream.
/// \return The output stream \a stream.
template<typename ValueType>
inline QTextStream& operator<<(QTextStream& stream, const Vector_3<ValueType>& v)
{
	return stream << "(" << v.X << " " << v.Y << " " << v.Z << ")";
}

/// \brief Reads a vector from a binary input stream.
/// \param stream The input stream.
/// \param v Reference to a vector variable where the parsed data will be stored.
/// \return The input stream \a stream.
template<typename ValueType>
inline QDataStream& operator>>(QDataStream& stream, Vector_3<ValueType>& v)
{
	return stream >> v.X >> v.Y >> v.Z;
}

// Define the default Vector_3 template instance.
template class Vector_3<FloatType>;
template class Vector_3<int>;

/** 
 * \fn typedef Vector3
 * \brief Template class instance of the Vector_3 class used for floating-point vectors. 
 */
typedef Vector_3<FloatType>		Vector3;

/** 
 * \fn typedef Vector3I
 * \brief Template class instance of the Vector_3 class used for integer vectors. 
 */
typedef Vector_3<int>			Vector3I;

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Vector3)
Q_DECLARE_METATYPE(Base::Vector3I)
Q_DECLARE_TYPEINFO(Base::Vector3, Q_PRIMITIVE_TYPE);
Q_DECLARE_TYPEINFO(Base::Vector3I, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Vector3, boost::serialization::object_serializable)
BOOST_CLASS_IMPLEMENTATION(Base::Vector3I, boost::serialization::object_serializable)

#endif // __VECTOR3_H
