///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include <Base.h>
#include <linalg/Quaternion.h>
#include <linalg/AffineTransformation.h>

namespace Base {

/// This dummy instance should be passed to the Quaternion class constructor to initialize it to the identity quaternion.
IdentityQuaternion IDENTITY_QUAT;

/******************************************************************************
* Initializes the quaternion from rotational part of the matrix.
******************************************************************************/
Quaternion::Quaternion(const AffineTransformation& tm)
{
	// Make sure this is a pure rotation matrix.
    MAFEM_ASSERT_MSG(abs((tm(0,0)*tm(1,1) - tm(0,1)*tm(1,0))*(tm(2,2))
					-(tm(0,0)*tm(1,2) - tm(0,2)*tm(1,0))*(tm(2,1))
					+(tm(0,1)*tm(1,2) - tm(0,2)*tm(1,1))*(tm(2,0)) - 1.0) <= FLOATTYPE_EPSILON,
					"Quaternion constructor" , "Quaternion::Quaternion(const AffineTransformation& tm) accepts only pure rotation matrices.");

	// Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
    // article "Quaternion Calculus and Fast Animation".
	FloatType trace = tm(0,0) + tm(1,1) + tm(2,2);
	FloatType root;

	if(trace > 0.0) {
		root = sqrt(trace + 1.0);
		W = 0.5 * root;
		root = 0.5 / root;
		X = (tm(2,1) - tm(1,2)) * root;
		Y = (tm(0,2) - tm(2,0)) * root;
		Z = (tm(1,0) - tm(0,1)) * root;
	}
	else {
		const int next[] = { 1, 2, 0 };
		int i = 0;
		if(tm(1,1) > tm(0,0)) i = 1;
		if(tm(2,2) > tm(i,i)) i = 2;
		int j = next[i];
		int k = next[j];
		root = sqrt(tm(i,i) - tm(j,j) - tm(k,k) + 1.0);
		(*this)[i] = 0.5 * root;
		root = 0.5 / root;
		W = (tm(k,j) - tm(j,k)) * root;
		(*this)[j] = (tm(j,i) + tm(i,j)) * root;
		(*this)[k] = (tm(k,i) + tm(i,k)) * root;
	}

	// Since we represent a rotation, make sure we are unit length.
	MAFEM_ASSERT(abs(DotProduct(*this, *this)-1.0) <= FLOATTYPE_EPSILON);
	//*this = Normalize(*this);
}

/******************************************************************************
* Interpolates between the two quaternions.
******************************************************************************/
Quaternion Quaternion::interpolate(const Quaternion& q1, const Quaternion& q2, FloatType alpha)
{
	MAFEM_ASSERT_MSG(abs(DotProduct(q1,q1) - 1.0) <= FLOATTYPE_EPSILON, "Quaternion::Interpolate", "Quaternions must be normalized.");
	MAFEM_ASSERT_MSG(abs(DotProduct(q2,q2) - 1.0) <= FLOATTYPE_EPSILON, "Quaternion::Interpolate", "Quaternions must be normalized.");

	FloatType cos_t = DotProduct(q1, q2);

	// same quaternion (avoid domain error)
	if(1.0 <= abs(cos_t))
		return q1;

	// t is now theta
	FloatType theta = acos(cos_t);

	FloatType sin_t = sin(theta);

	// same quaternion (avoid zero-div)
	if(sin_t == 0.0)
		return q1;

	FloatType s = sin((1.0-alpha)*theta)/sin_t;
	FloatType t = sin(alpha*theta)/sin_t;

	// Set values.
	return Normalize(Quaternion(s*q1.X + t*q2.X, s*q1.Y + t*q2.Y, s*q1.Z + t*q2.Z, s*q1.W + t*q2.W));
}


};	// End of namespace Base
