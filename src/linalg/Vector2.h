///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Vector2.h 
 * \brief Contains definition of the Base::Vector_2 template class and operators.  
 */

#ifndef __VECTOR2_H
#define __VECTOR2_H

#include <Base.h>
#include "Vector3.h"

namespace Base {

/**
 * \brief A vector with two components X and Y.
 * 
 * This is one of the basic vector algebra classes. It represents a two
 * dimensional vector in space. There are two instances of this template
 * vector class: \c Vector2 is for floating-point vectors and \c Vector2I is
 * for integer vectors with two components.
 * 
 * Note that there is also a class called Point_2 that is used for points
 * in a two dimensional coordinate system.
 * 
 * \author Alexander Stukowski
 * \sa Point_2
 * \sa Vector_3
 */
template<typename ValueType> 
class Vector_2
{
public:	
	/// The X component of the vector.
	ValueType X;
	/// The Y component of the vector.
	ValueType Y;

	/////////////////////////////// Constructors /////////////////////////////////

	/// \brief Constructs a vector without initializing its component.
	/// \note All components are left uninitialized by this constructor!
	Vector_2() {}

	/// \brief Constructs a vector with all components set to the given value. 
	/// \param val The value to be assigned to each of the vector's components.
	explicit Vector_2(ValueType val) { X = Y = val; }

	/// Initializes the components of the vector with the given component values.
    Vector_2(ValueType _x, ValueType _y) : X(_x), Y(_y) {}

	/// \brief Initializes the components of the vector with the values in the given array.
	/// \param val The array that contains the vector components.
    Vector_2(ValueType val[2]) : X(val[0]), Y(val[1]) {}

	/// Initializes the vector to the null vector. All components are set to zero.
    Vector_2(NullVector NULL_VECTOR) : X((ValueType)0), Y((ValueType)0) {}

    ///////////////////////////// Component access ///////////////////////////////

	/// \brief Returns a reference to the i-th component of the vector.
	/// \param i The index specifying the component to return (0=X, 1=Y).
	/// \return A reference to the i-th component that can be used to change the component's value. 
	ValueType& operator[](size_t i) {
		MAFEM_STATIC_ASSERT(sizeof(Vector_2<ValueType>) == sizeof(ValueType) * 2);
		MAFEM_ASSERT(i>=0 && i<size());
		return (&X)[i]; 
	}

	/// \brief Returns a reference to the i-th component of the vector.
	/// \param i The index specifying the component to return (0=X, 1=Y).
	/// \return The i-th component of the vector. 
	const ValueType& operator[](size_t i) const {
		MAFEM_STATIC_ASSERT(sizeof(Vector_2<ValueType>) == sizeof(ValueType) * 2);
		MAFEM_ASSERT(i>=0 && i<size());
		return (&X)[i]; 
	}

	/// \brief Returns a pointer to the first element of the vector.
	/// \sa constData() 
	ValueType* data() { 		
        MAFEM_STATIC_ASSERT(sizeof(Vector_2<ValueType>) == sizeof(ValueType) * 2);
		return (ValueType*)this;
	}

	/// \brief Returns a pointer to the first element of the vector for read-only access.
	/// \sa data()
	const ValueType* constData() const {
        MAFEM_STATIC_ASSERT(sizeof(Vector_2<ValueType>) == sizeof(ValueType) * 2);
		return (const ValueType*)this;
	}

	/// \brief Casts the vector to a vector with another data type.
	template<typename T2>
	operator Vector_2<T2>() const { return Vector_2<T2>((T2)X, (T2)Y); }

    /////////////////////////////// Unary operators //////////////////////////////

	/// \brief Returns the inverse of the vector.
	/// \return A vector with negated components: (-X, -Y).
	Vector_2<ValueType> operator-() const { return(Vector_2<ValueType>(-X, -Y)); } 

	///////////////////////////// Assignment operators ///////////////////////////

	/// \brief Adds another vector to this vector and stores the result in this vector.
	Vector_2<ValueType>& operator+=(const Vector_2<ValueType>& v) { X += v.X; Y += v.Y; return *this; }

	/// \brief Substracts another vector from this vector and stores the result in this vector.
	Vector_2<ValueType>& operator-=(const Vector_2<ValueType>& v) { X -= v.X; Y -= v.Y; return *this; }

	/// \brief Multplies each component of the vector with a scalar value and stores the result in this vector.
	Vector_2<ValueType>& operator*=(ValueType s) { X *= s; Y *= s; return *this; }

	/// \brief Divides each component of the vector by a scalar value and stores the result in this vector.
	Vector_2<ValueType>& operator/=(ValueType s) { X /= s; Y /= s; return *this; }

	//////////////////////////// Coordinate read access //////////////////////////

	/// \brief Returns the value of the X component of this vector.
	const ValueType& x() const { return X; }

	/// \brief Returns the value of the Y component of this vector.
	const ValueType& y() const { return Y; }

	//////////////////////////// Coordinate write access //////////////////////////

	/// \brief Sets the X component of this vector to a new value.
	/// \param value The new value that is assigned to the vector component.
	void setx(const ValueType& value) { X = value; }

	/// \brief Sets the Y component of this vector to a new value.
	/// \param value The new value that is assigned to the vector component.
	void sety(const ValueType& value) { Y = value; }

	////////////////////////////////// Comparison ////////////////////////////////

	/// \brief Compares two vector for equality.
	/// \return true if each of the components are equal; false otherwise.
	bool operator==(const Vector_2<ValueType>& v) const { return (v.X==X) && (v.Y==Y); }

	/// \brief Compares two vector for inequality.
	/// \return true if any of the components are not equal; false if all are equal.
	bool operator!=(const Vector_2<ValueType>& v) const { return (v.X!=X) || (v.Y!=Y); }

	/// \brief Checks whether the vector is the null vector, i.e. all components are zero.
	/// \return true if all of the components are zero; false otherwise
	bool operator==(const NullVector& NULL_VECTOR) const { return (X==(ValueType)0) && (Y==(ValueType)0); }

	/// \brief Checks whether the vector is not a vector, i.e. any of the components is nonzero.
	/// \return true if any of the components is nonzero; false if this is the null vector otherwise
	bool operator!=(const NullVector& NULL_VECTOR) const { return (X!=(ValueType)0) || (Y!=(ValueType)0); }

	/// \brief Checks whether two vectors are equal within a given tolerance.
	/// \param v The vector that should be compraed to this vector.
	/// \param tolerance A non-negative threshold for the equality test. The two vectors are considered equal when 
	///        the differences in the X and Y directions are all smaller than this tolerance value.
	/// \return true if this vector is equal to the given vector within the given tolerance.
	bool equals(const Vector_2<ValueType>& v, ValueType tolerance) const {
		MAFEM_ASSERT(tolerance >= 0);
		return abs(v.X - X) <= tolerance && abs(v.Y - Y) <= tolerance; 
	}

	/////////////////////////////// Binary operators /////////////////////////////

	/// \brief Computes the sum of two vectors.
	/// \return The sum of two vectors.
	Vector_2<ValueType> operator+(const Vector_2<ValueType>& v) const { return Vector_2<ValueType>(X + v.X, Y + v.Y); }

	/// \brief Computes the difference of two vectors.
	/// \return The difference of two vectors.
	Vector_2<ValueType> operator-(const Vector_2<ValueType>& v) const { return Vector_2<ValueType>(X - v.X, Y - v.Y); }

	/// \brief Computes the product of a vector and a scalar value. All
	///        components of the vector are multiplied by the scalar.
	/// \return The product of the vector with a scalar: (s*X, s*Y).
	Vector_2<ValueType> operator*(ValueType s) const { return Vector_2<ValueType>(X*s, Y*s); }

	/// \brief Computes the division of a vector by a scalar value. All
	///        components of the vector are divided by the scalar.
	/// \return The division of the vector by a scalar: (X/s, Y/s).
	Vector_2<ValueType> operator/(ValueType s) const { return Vector_2<ValueType>(X/s, Y/s); }

	///////////////////////////////// Information ////////////////////////////////
	
	/// \brief Returns the number of components of this vector.
	/// \return Always returns 2.
	size_t size() const { return 2; }

	/// \brief Returns the dimension of this vector (the constant 2).
	/// \return Always returns 2.
	size_t dimension() const { return 2; }

	/// \brief Gives a string representation of this vector.
	/// \return A string that contains the components of the vector. 
	QString toString() const { return QString("(%1 %2)").arg(X).arg(Y); }

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { ar & X & Y; }	

	friend class boost::serialization::access;
};

/// \brief Returns the product of the vector with a scalar: (v.X*s, v.Y*s).
template<typename ValueType>
inline Vector_2<ValueType> operator*(const ValueType& s, const Vector_2<ValueType>& v) { return v * s; }

/// \brief Computes the scalar product of two vectors.
/// \return The scalar vector product: a.X*b.X + a.Y*b.Y 
template<typename T>
inline T DotProduct(const Vector_2<T>& a, const Vector_2<T>& b) { 
	return a.X*b.X + a.Y*b.Y;
}

/// \brief Returns the squared length of a vector.
/// \return X*X + Y*Y
/// \sa Length()
template<typename T>
inline T LengthSquared(const Vector_2<T>& a) {
	return a.X*a.X + a.Y*a.Y;
}

/// \brief Returns the length of a vector.
/// \return sqrt(X*X + Y*Y)
/// \sa LengthSquared()
template<typename T>
inline T Length(const Vector_2<T>& a) {
	return (T)sqrt(LengthSquared(a));
}

/// \brief Normalizes a vector to unit length.
/// \return The vector divided by its length.
template<typename T>
inline Vector_2<T> Normalize(const Vector_2<T>& a) {
	MAFEM_ASSERT(a != NULL_VECTOR);
	return a / Length(a);
}

/// \brief Finds the index of the component with the maximum value.
/// \return The index (0-1) with the largest value.
template<typename T>
inline size_t MaxComponent(const Vector_2<T>& a) {
	return ((a.X >= a.Y) ? 0 : 1);	
}

/// \brief Finds the index of the component with the minimum value.
/// \return The index (0-2) with the smallest value.
template<typename T>
inline size_t MinComponent(const Vector_2<T>& a) {
	return ((a.X <= a.Y) ? 0 : 1);
}

/// \brief Writes the vector to a text output stream.
template<typename T>
inline std::ostream& operator<<(std::ostream &os, const Vector_2<T> &p) {
	return os << p.X << ' ' << p.Y;
}

/// \brief Writes the vector to a logging stream.
template<typename T>
inline LoggerObject& operator<<(LoggerObject& log, const Vector_2<T>& v)
{
     log.space() << "(" << v.X << v.Y << ")";
     return log.space();
} 

/// \brief Writes a vector to a binary output stream.
template<typename T>
inline QDataStream& operator<<(QDataStream& stream, const Vector_2<T>& v)
{
	return stream << v.X << v.Y;
}

/// \brief Reads a vector from a binary input stream.
template<typename T>
inline QDataStream& operator>>(QDataStream& stream, Vector_2<T>& v)
{
	return stream >> v.X >> v.Y;
}


// Define the default Vector_2 template instance.
template class Vector_2<FloatType>;
template class Vector_2<int>;

/** 
 * \fn typedef Vector2
 * \brief Template class instance of the Vector_2 class used for floating-point vectors. 
 */
typedef Vector_2<FloatType>			Vector2;

/** 
 * \fn typedef Vector2I
 * \brief Template class instance of the Vector_2 class used for integer vectors. 
 */
typedef Vector_2<int>				Vector2I;

};	// End of namespace Base


Q_DECLARE_METATYPE(Base::Vector2)
Q_DECLARE_METATYPE(Base::Vector2I)
Q_DECLARE_TYPEINFO(Base::Vector2, Q_PRIMITIVE_TYPE);
Q_DECLARE_TYPEINFO(Base::Vector2I, Q_PRIMITIVE_TYPE);

// We don't need versioning info for this type when serializing.
BOOST_CLASS_IMPLEMENTATION(Base::Vector2, boost::serialization::object_serializable)
BOOST_CLASS_IMPLEMENTATION(Base::Vector2I, boost::serialization::object_serializable)


#endif // __VECTOR2_H
