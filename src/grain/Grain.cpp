///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "Grain.h"

BOOST_CLASS_EXPORT(MAFEM::Grain)

namespace MAFEM {

/******************************************************************************
* Constructs a grain with the given properties.
******************************************************************************/
Grain::Grain(LatticeType latticeType, int atomType, const AffineTransformation& bravaisSystem) :
	_lattice(latticeType, bravaisSystem), _atomType(atomType)
{	
}

}; // End of namespace MAFEM
