///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// 
// Copyright (C) 2005, Alexander Stukowski
//
// E-mail:  alex@stukowski.de
// Web:     www.stukowski.com
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __MAFEM_GRAIN_BOUNDARY_H
#define __MAFEM_GRAIN_BOUNDARY_H

#include "MAFEM.h"
#include <simulation/SimulationResource.h>



namespace MAFEM {

class BoundaryVertex;			// defined below
class BoundaryFace;				// defined below
class Grain;

///////////////////////////////////////////////////////////////////
/// A vertex that is part of a GrainBoundary.
///////////////////////////////////////////////////////////////////
class BoundaryVertex
{
public:
	/// \brief Deserialization constructor.
	///
	/// This constructor should not be used.
	BoundaryVertex() {}

	/// Returns the position of the vertex in world space.
	const Point3& GetWorldPos() const { return worldPos; }

	/// Returns the position of the vertex in Bravais lattice space.
	const Point3& GetBravaisPos() const { return bravaisPos; }

	/// Returns the position of the vertex in lattice space.
	const Point3& GetLatticePos() const { return latticePos; }

private:

	/// Creates a new vertex at the given position.
	BoundaryVertex(const Point3& _worldPos, const Point3& _bravaisPos, const Point3& _latticePos) :
		worldPos(_worldPos), bravaisPos(_bravaisPos), latticePos(_latticePos) {}

	/// The position of the vertex in world space.
	Point3 worldPos;

	/// The position of the vertex in Bravais lattice space.
	Point3 bravaisPos;

	/// The position of the vertex in lattice space.
	Point3 latticePos;

	template<class Archive>
	    void serialize(Archive &ar, const unsigned int version) {
			ar & worldPos;
			ar & bravaisPos;
			ar & latticePos;
		}

	friend class boost::serialization::access;

	friend class GrainBoundary;

};

///////////////////////////////////////////////////////////////////
/// An edge that is part of a GrainBoundary.
///////////////////////////////////////////////////////////////////
class BoundaryEdge
{
public:

	/// \brief Deserialization constructor.
	///
	/// This constructor should not be used.
	BoundaryEdge() {}

	/// Returns the first vertex of this edge.
	BoundaryVertex* Vertex1() const { return v1; }

	/// Returns the second vertex of this edge.
	BoundaryVertex* Vertex2() const { return v2; }

private:

	/// Edge constructor.
	BoundaryEdge(BoundaryVertex* vertex1, BoundaryVertex* vertex2) : v1(vertex1), v2(vertex2) {}

	/// The first vertex of the edge.
	BoundaryVertex* v1;

	/// The second vertex of the edge.
	BoundaryVertex* v2;

	template<class Archive>
		void serialize(Archive &ar, const unsigned int version) {

			ar & v1;
			ar & v2;

		}

	friend class boost::serialization::access;

	friend class GrainBoundary;
};

///////////////////////////////////////////////////////////////////
/// A face that is part of a GrainBoundary.
///////////////////////////////////////////////////////////////////
class BoundaryFace
{
public:

	/// \brief Deserialization constructor.
	///
	/// This constructor should not be used.
	BoundaryFace() {}

	/// Returns the normal vector of this face in world space.
	const Vector3& GetWorldNormal() const { return worldNormal; }

	/// Returns the normal vector of this face in Bravais lattice space.
	const Vector3& GetBravaisNormal() const { return bravaisNormal; }

	/// Returns the list of vertices of this face.
	const QVector<BoundaryVertex*>& Vertices() const { return verts; }

	/// Returns true if the given point is on the face.
	bool PointOnFace(const Point3& point, FloatType epsilon = FLOATTYPE_EPSILON) const;



private:

	/// Face constructor.
	BoundaryFace(const QVector<BoundaryVertex*> vertices, const QVector<BoundaryEdge*> _edges) : verts(vertices), edges(_edges) {
		MAFEM_ASSERT(vertices.size() >= 3);
		ComputeFaceNormal();
	}

	/// Computes the normal vector of the face.
	void ComputeFaceNormal();

	/// This is the list of vertices that make up the corners of this polygonal face. 
	QVector<BoundaryVertex*> verts;	

	/// This is the list of edges. 
	QVector<BoundaryEdge*> edges;	

	/// The triangulation of the face in Bravais space.
    QVector<Point3*> triangulation;

    /// The normal vector of this face in world space.
	Vector3 worldNormal;

	/// The normal vector of this face in Bravais space.
	Vector3 bravaisNormal;

	template<class Archive>
		void serialize(Archive &ar, const unsigned int version) {

			ar & verts;
			ar & edges;
			ar & triangulation;
			ar & worldNormal;
			ar & bravaisNormal;

		}

	friend class boost::serialization::access;

	friend class GrainBoundary;
};

class GrainBoundary : public SimulationResource
{
public:
		/// \brief Deserialization constructor.
		///
		/// This constructor should not be used.
		GrainBoundary() {}
	/********************************** Property Access ********************************/

	/// Returns the list of vertices.
	const QVector<BoundaryVertex*>& Vertices() const { return vertices; }

    /// Returns the list of faces.
	const QVector<BoundaryFace*>& Faces() const { return faces; }

	/// Returns the list of edges.
	const QVector<BoundaryEdge*>& Edges() const { return edges; }

    /// Returns the bounding box of the grain. This axis-aligned
	/// box contains all vertices of the boundary shape.
	const Box3& GetWorldBoundingBox() const { return worldBoundingBox; }

	/// Returns the bounding box of the grain in Bravais space.
	const Box3I& GetBravaisBoundingBox() const { return bravaisBoundingBox; }

	/// Returns the bounding box of the grain in lattice space.
	const Box3I& GetLatticeBoundingBox() const { return latticeBoundingBox; }

    /***************************** Adding new components ******************************/

	/// Creates a new vertex and sets it to the specified world point.
	BoundaryVertex* CreateVertex(const shared_ptr<Grain>& grain, const Point3& p);

	/// Create a new triangle face with three vertices given by index.
	BoundaryFace* CreateTriFace(int a, int b, int c);

	/// Create a new quad face with four vertices given by index.
	BoundaryFace* CreateQuadFace(int a, int b, int c, int d);

	/// Creates an edge that connects the two vertices with the given indices.
	/// Returns an existing edge if there is one.
	BoundaryEdge* CreateEdge(int a, int b);

    /************************************ Utilities ************************************/

	/// Tests whether a point lies inside the boundary. Returns true if the given point
	/// is inside the boundary or on the boundary. If the point
	/// is on the boundary and 'face' is not NULL the 
	/// face that contains the point is returned in the output parameter 'face'.
	bool LocatePoint(const Point3& point, BoundaryFace** onFace = NULL, FloatType epsilon = FLOATTYPE_EPSILON) const;

	/// Returns true if the given axis-aligned box intersects with any of the faces.
	bool IntersectsBravaisBox(const Box3I& box) const;

/**//// sets the associated grain reference
	void setGrainIndex(int index) {grainIndex = index;}

/**//// returns the associated grain reference
	int getGrainIndex() { return grainIndex; }


private:

	int grainIndex;

	/// Array of vertices.
	QVector<BoundaryVertex*> vertices;

	/// Array of edges.
	QVector<BoundaryEdge*> edges;

	/// Array of faces.
	QVector<BoundaryFace*> faces;

	/// The bounding box in world space.
	Box3 worldBoundingBox;

	/// The bounding box in Bravais space.
	Box3I bravaisBoundingBox;

	/// The bounding box in lattice space.
	Box3I latticeBoundingBox;

	template<class Archive>
	    void serialize(Archive &ar, const unsigned int version) {

			ar & grainIndex;
			ar & vertices;
			ar & edges;
			ar & faces;
			ar & worldBoundingBox;
			ar & bravaisBoundingBox;
			ar & latticeBoundingBox;
		}
	friend class boost::serialization::access;

};
};
#endif // __MAFEM_GRAIN_BOUNDARY_H
