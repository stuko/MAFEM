///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "CrystalLattice.h"

namespace MAFEM {

// Computes the lattice vectors for the simple unit cell of the lattice.
// Takes the specified Bravais vectors and computes the lattice vectors
// of the simple unit cell based on the lattice type.
void CrystalLattice::init()
{
	// Derive the primitive lattice vectors from the Bravais cell and the lattice type.
	switch(_latticeType) {
		case SC:
			_latticeVectors = bravaisVectors();
			_numNearestNeighbors = 6;
			_nearestNeighbors[0] = bravaisVector(0);
			_nearestNeighbors[1] = -_nearestNeighbors[0];
			_nearestNeighbors[2] = bravaisVector(1);
			_nearestNeighbors[3] = -_nearestNeighbors[2];
			_nearestNeighbors[4] = bravaisVector(2);
			_nearestNeighbors[5] = -_nearestNeighbors[4];
			break;
		case FCC:
			_latticeVectors.column(0) = (bravaisVector(0) + bravaisVector(1)) / 2.0;
			_latticeVectors.column(1) = (bravaisVector(1) + bravaisVector(2)) / 2.0;
			_latticeVectors.column(2) = (bravaisVector(2) + bravaisVector(0)) / 2.0;
			_latticeVectors.column(3) = translation();
			_numNearestNeighbors = 12;
			_nearestNeighbors[0] = latticeVector(0);
			_nearestNeighbors[1] = -_nearestNeighbors[0];
			_nearestNeighbors[2] = (bravaisVector(0) - bravaisVector(1)) / 2.0;
			_nearestNeighbors[3] = -_nearestNeighbors[2];
			_nearestNeighbors[4] = latticeVector(1);
			_nearestNeighbors[5] = -_nearestNeighbors[4];
			_nearestNeighbors[6] = (bravaisVector(1) - bravaisVector(2)) / 2.0;
			_nearestNeighbors[7] = -_nearestNeighbors[6];
			_nearestNeighbors[8] = latticeVector(2);
			_nearestNeighbors[9] = -_nearestNeighbors[8];
			_nearestNeighbors[10] = (bravaisVector(2) - bravaisVector(0)) / 2.0;
			_nearestNeighbors[11] = -_nearestNeighbors[10];
			break;
		case BCC:
			_latticeVectors.column(0) = bravaisVector(0);
			_latticeVectors.column(1) = bravaisVector(1);
			_latticeVectors.column(2) = (bravaisVector(0) + bravaisVector(1) + bravaisVector(2)) / 2.0;
			_latticeVectors.column(3) = translation();
			_numNearestNeighbors = 8;
                       	_nearestNeighbors[0] = (bravaisVector(0) + bravaisVector(1) + bravaisVector(2)) / 2.0;
                        _nearestNeighbors[1] = -_nearestNeighbors[0];
                        _nearestNeighbors[2] = (-bravaisVector(0) + bravaisVector(1) + bravaisVector(2)) / 2.0;
                        _nearestNeighbors[3] = -_nearestNeighbors[2];
                        _nearestNeighbors[4] = (bravaisVector(0) - bravaisVector(1) + bravaisVector(2)) / 2.0;
                        _nearestNeighbors[5] = -_nearestNeighbors[4];
                        _nearestNeighbors[6] = (bravaisVector(0) + bravaisVector(1) - bravaisVector(2)) / 2.0;
                        _nearestNeighbors[7] = -_nearestNeighbors[6];
			break;			
		default:
			_numNearestNeighbors = 0;
			throw Exception("Invalid lattice type.");
	}

	// Compute the volume of the primitive unit cell.
	// This is just the determinant of the three lattice vectors.
	_volume = latticeVectors().determinant();
	if(_volume <= 0.0)
		throw Exception("Invalid Bravais lattice vectors. They must form a right-handed basis set.");

	// Compute magnitudes and reciprocal vectors.
	for(size_t i=0; i<3; i++)
		_latticeMagnitudes[i] = Length(latticeVector(i));

	_inverseBravaisVectors = bravaisVectors().inverse();
	_inverseLatticeVectors = latticeVectors().inverse();

	/// Compute the square root of the minimum eigenvalue of the 
	/// symmetric matrix C = L^T * L with L being the 3x3 matrix of lattice vectors.
	Tensor2 L;
	L.column(0) = latticeVector(0);
	L.column(1) = latticeVector(1);
	L.column(2) = latticeVector(2);
	SymmetricTensor2 C = Product_AtA(L);
	_minEigenvalue = sqrt(C.minEigenvalue());
}


}; // End of namespace MAFEM
