///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_LATTICE_H
#define __MAFEM_LATTICE_H

#include <MAFEM.h>

namespace MAFEM {

/**
 * \brief Describes a cubic Bravais lattice.
 * 
 * A lattice is defined by its oriantation and location in space,
 * its type (FCC, BCC etc.) and its lattice constant.
 *
 * There are two different types of vectors that define a lattice:
 * <ul>
 * <li> Three \e Bravais vectors specify the Bravais cell. A Bravais
 *      cell can contain more than one atom (an FCC bravais cell for instance)</li>
 * <li> Three \e lattice vectors specify the simple unit cell.
 *      This simple cell contains exactly one atom. </li>
 * </ul>
 * The \e lattice vectors are derived from the \e Bravais vectors based on the lattice type.
 * 
 * \author Alexander Stukowski
 */

/**//// The list of CrystalLattice types.
	enum LatticeType {
		SC,		///< Simple cubic lattice.
		FCC,	///< Face-centered cubic lattice.
		BCC,	///< Body-centered cubic lattice.
	};



class CrystalLattice : boost::noncopyable
{
public:
	
	/// \brief Deserialization constructor.
	CrystalLattice() {}
	
	/// \brief Defines a lattice with the given properties.
	/// \param type				The Bravais type of the lattice.
	/// \param bravaisSystem	This transformation matrix specifies both the translation of the lattice
	///							and the magnitude and orientation of the three Bravais lattice vectors.
	///                         \note They must form a right-handed basis set.
	CrystalLattice(LatticeType type, const AffineTransformation& bravaisSystem) : _latticeType(type), _bravaisVectors(bravaisSystem) { init(); }

	/// \brief Gets one of the three Bravais vectors of this lattice.
	/// \param i	The index of the vector to return (0 <= i <= 2).
	const Vector3& bravaisVector(size_t i) const { return _bravaisVectors.column(i); }

	/// \brief Gets one of the three lattice vectors of this lattice.
	/// \param i	The index of the vector to return (0 <= i <= 2).
	const Vector3& latticeVector(size_t i) const { return _latticeVectors.column(i); }

	/// \brief Returns the length of a lattice vector.
	/// \param i	The index of the vector for which the lnegth should be returned (0 <= i <= 2).
	FloatType latticeVectorLength(size_t i) const { return _latticeMagnitudes[i]; }

	/// \brief Returns the three lattice vectors that define the primitive unit cell of the lattice.
	///
	/// The returned matrix contains the three lattice vectors as columns plus
	/// a fourth column that contains the translation vector of the lattice.
	const AffineTransformation& latticeVectors() const { return _latticeVectors; }

	/// \\brief Returns the reciprocal lattice vectors.
	///
	/// The returned matrix contains the three reciprocal lattice vectors as columns.
	const AffineTransformation& inverseLatticeVectors() const { return _inverseLatticeVectors; }

	/// \brief Returns the three Bravais vectors that define the Bravais unit cell of the lattice.
	///
	/// The returned matrix contains the three Bravais vectors as columns plus
	/// a fourth column that contains the translation vector of the lattice.
	const AffineTransformation& bravaisVectors() const { return _bravaisVectors; }

	/// \brief Returns the reciprocal Bravais vectors.
	///
	/// The returned matrix contains the three reciprocal Bravais vectors as columns.
	const AffineTransformation& inverseBravaisVectors() const { return _inverseBravaisVectors; }

	/// \brief Get the translation of this lattice.
	///
	/// The returned vector is the translation from the world space origin
	/// to the origin of the lattice.
	const Vector3& translation() const { return _bravaisVectors.getTranslation(); }

	/// \brief Returns the Bravais type of the lattice.
	LatticeType latticeType() const { return _latticeType; }

	/// \brief Returns the volume of the primitive unit cell.
	///
	/// This is the inverse of the density of atoms.
	FloatType unitCellVolume() const { return _volume; }

	/// \brief Return the square root of the minimum eigenvalue of the 
	///        symmetric matrix C = L^T * L with L being the 3x3 matrix of lattice vectors.
	FloatType minEigenvalue() const { return _minEigenvalue; }

	/// \brief Returns the numer of nearest neighbor atoms for the current lattice type.
	size_t numNearestNeighbors() const { return _numNearestNeighbors; }

	/// \brief Returns the vector from a central atom to the given nearest neighbor.
	const Vector3& nearestNeighbor(size_t index) const { 
		MAFEM_ASSERT(index < numNearestNeighbors()); 
		return _nearestNeighbors[index]; 
	}

protected:

	/// Computes the lattice vectors for the simple unit cell of the lattice.
	/// Takes the specified Bravais vectors and computes the lattice vectors
	/// of the simple unit cell based on the lattice type.
	void init();

	/// The type of the lattice Bravais cells.
	LatticeType _latticeType;

	/// Stores the translation and orientation of the lattice.
	/// The transformation matrix has the following structure:
	///   - \c bravaisVectors.Column(0) contains the first Bravais vector.
	///   - \c bravaisVectors.Column(1) contains the second Bravais vector.
	///   - \c bravaisVectors.Column(2) contains the third Bravais vector.
	///   - \c bravaisVectors.Column(3) contains the translation of the lattice in space.
	AffineTransformation _bravaisVectors;

	/// Stores the translation and orientation of the lattice.
	/// The transformation matrix has the following structure:
	///   - \c latticeVectors.Column(0) contains the first lattice vector.
	///   - \c latticeVectors.Column(1) contains the second lattice vector.
	///   - \c latticeVectors.Column(2) contains the third lattice vector.
	///   - \c latticeVectors.Column(3) contains the translation of the lattice in space.
	AffineTransformation _latticeVectors;

	/// Stores the reciprocal Bravais vectors.
	AffineTransformation _inverseBravaisVectors;

	/// Stores the reciprocal lattice vectors.
	AffineTransformation _inverseLatticeVectors;

	/// Stores the magnitudes of the three lattice vectors.
	FloatType _latticeMagnitudes[3];

	/// The volume of the primitive unit cell. 
	/// This is the inverse of the density of atoms.
	FloatType _volume;

	/// The square root of the minimum eigenvalue of the 
	/// symmetric matrix C = L^T * L with L being the 3x3 matrix of lattice vectors.
	FloatType _minEigenvalue;

	/// The precalculated vectors to the nearest neighbors of an atom.
	Vector3 _nearestNeighbors[12];

	/// The number of nearest neighbors.
	size_t _numNearestNeighbors;

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) { 
		ar & _latticeType;
		ar & _bravaisVectors;
		ar & _latticeVectors;
		ar & _inverseBravaisVectors;
		ar & _inverseLatticeVectors;
		ar & _latticeMagnitudes;
		ar & _volume;
		ar & _minEigenvalue;
		ar & _nearestNeighbors;
		ar & _numNearestNeighbors;
    }	

	friend class boost::serialization::access;
};

}; // End of namespace MAFEM

BOOST_CLASS_TRACKING(MAFEM::CrystalLattice, boost::serialization::track_always)

#endif // __MAFEM_LATTICE_H

