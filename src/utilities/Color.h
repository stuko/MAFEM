///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Color.h 
 * \brief Contains the definitions of the Base::Color and Base::ColorA classes. 
 */

#ifndef __COLOR_H
#define __COLOR_H

#include <Base.h>
#include <linalg/Vector3.h>

namespace Base {

/**
 * \brief A color class with floating-point RGB values.
 * 
 * The Color class stores three floating-point values, one for the red
 * component, one for the green component and one for the blue component.
 * 
 * \author Alexander Stukowski
 * \sa ColorA
 */
class Color
{
public:
	/// The components of the color value.
	/// Red, green and blue values are in the range [0.0 - 1.0].
	FloatType r, g, b;

	/// \brief Constructs a color structure without initializing its component.
	/// \note All components are left uninitialized by this constructor and will therefore have a random value!
	Color() {}
	
	/// \brief Initializes the color width the given red, green and blue components.
	/// \param red The red value of the RGB color in the range 0 to 1.
	/// \param green The green value of the RGB color in the range 0 to 1.
	/// \param blue The blue value of the RGB color in the range 0 to 1.
	Color(FloatType red, FloatType green, FloatType blue) : r(red), g(green), b(blue) {}
	
	/// \brief Copy constructor.
	/// \param c The color to copy.
	Color(const Color& c) : r(c.r), g(c.g), b(c.b) {}
	
	/// \brief Converts a vector to a color structure.
	/// \param v The input vector. Its X, Y and Z components are taken as red, green and blue components 
	///          respectively.
	explicit Color(const Vector3& v) { r = v.X; g = v.Y; b = v.Z; }
	
	/// \brief Conversion from a Qt color object.
	/// \param c The Qt color to convert to a floating-point representation.
	explicit Color(const QColor& c) : r(c.redF()), g(c.greenF()), b(c.blueF()) {}

	/// \brief Sets all components to zero.
	void setBlack() { r = g = b = 0; }
	
	/// \brief Sets all components to one.
	void setWhite() { r = g = b = 1; }

	/// \brief Converts this color to a Qt color object.
	/// \return A Qt color object. All color components are clamped to the [0,1] range before the conversion.
	operator QColor() const { 
		return QColor::fromRgbF(qMin(qMax(r, (FloatType)0), (FloatType)1), 
				qMin(qMax(g, (FloatType)0), (FloatType)1),
				qMin(qMax(b, (FloatType)0), (FloatType)1)); 
	}
	
	/// \brief Converts this color to a vector with three components.
	/// \return A vector.
	operator const Vector3&() const { 
		return *(const Vector3*)this; 
	}	
	
	/// \brief Returns a reference to the i-th component of the color.
	/// \param i The index specifying the component to return (0=red, 1=green, 2=blue).
	/// \return A reference to the i-th component that can be used to change the component's value. 
	FloatType& operator[](size_t i) {
		MAFEM_STATIC_ASSERT(sizeof(Color) == sizeof(FloatType) * 3);
		MAFEM_ASSERT(i>=0 && i<3);
		return (&r)[i]; 
	}

	/// \brief Returns a reference to the i-th component of the color.
	/// \param i The index specifying the component to return (0=red, 1=green, 2=blue).
	/// \return The i-th component of the color. 
	const FloatType& operator[](size_t i) const {
		MAFEM_STATIC_ASSERT(sizeof(Color) == sizeof(FloatType) * 3);
		MAFEM_ASSERT(i>=0 && i<3);
		return (&r)[i]; 
	}

	/// \brief Returns a pointer to the first element of the color.
	/// \return A pointer to three consecutive floating-point numbers: red, green and blue.
	/// \sa constData() 
	FloatType* data() { 		
        MAFEM_STATIC_ASSERT(sizeof(Color) == sizeof(FloatType) * 3);
		return &r;
	}

	/// \brief Returns a pointer to the first element of the color for read-only access.
	/// \return A pointer to three consecutive floating-point numbers: red, green and blue.
	/// \sa data()
	const FloatType* constData() const {
        MAFEM_STATIC_ASSERT(sizeof(Color) == sizeof(FloatType) * 3);
		return &r;
	}	

	/// \brief Compares this color with another color for equality.
	/// \param rgb The second color.
	/// \return \c true if all three color components are exactly equal; \c false otherwise.
	bool operator==(const Color& rgb) const { return (r == rgb.r && g == rgb.g && b == rgb.b); }
	
	/// \brief Compares this color with another color for inequality.
	/// \param rgb The second color.
	/// \return \c true if any of the three color components are not equal; \c false otherwise.		
	bool operator!=(const Color& rgb) const { return (r != rgb.r || g != rgb.g || b != rgb.b); }

	/// Adds the components of another color to this color.
	Color& operator+=(const Color& c) { r += c.r; g += c.g; b += c.b; return (*this); }
	
	/// Multiplies the components of another color with the components of this color.
	Color& operator*=(const Color& c) { r *= c.r; g *= c.g; b *= c.b; return (*this); }

	/// Converts a vector to a color assigns it to this object.
	Color& operator=(const Vector3& v) { r = v.X; g = v.Y; b = v.Z; return (*this); }
	
	/// \brief Ensures that all components are not greater than one.
	/// 
	/// If any of the R, G and B components is greater than 1 then it is set to 1.
	/// \sa clampMin(), clampMinMax()
	void clampMax() { if(r > 1.0) r = 1.0; if(g > 1.0) g = 1.0; if(b > 1.0) b = 1.0; }
	
	/// \brief Ensures that all components are not less than zero.
	/// 
	/// If any of the R, G and B components is less than 0 then it is set to 0.
	/// \sa clampMax(), clampMinMax()
	void clampMin() { if(r < 0.0) r = 0.0; if(g < 0.0) g = 0.0; if(b < 0.0) b = 0.0; }
	
	/// \brief Ensures that all components are not greater than one and not less than zero.
	/// \sa clampMin(), clampMax()
	void clampMinMax() { 
		if(r > 1.0) r = 1; 
		else if(r < 0.0) r = 0;
		if(g > 1.0) g = 1; 
		else if(g < 0.0) g = 0;
		if(b > 1.0) b = 1; 
		else if(b < 0.0) b = 0;
	}

	/// \brief Converts a color from hue, saturation, value representation to RGB representation.
	/// \param hue The hue value between zero and one.
	/// \param saturation The saturation value between zero and one.
	/// \param value The value of the color between zero and one.
	/// \return The color in RGB representation.
	static Color fromHSV(FloatType hue, FloatType saturation, FloatType value) {
		if(saturation == 0.0) {
			return Color(value, value, value);
		}
		else {
			FloatType f, p, q, t;
			int i;
			if(hue >= 1.0 || hue < 0.0) hue = 0.0;
			hue *= 6.0;
			i = (int)floor(hue);
			f = hue - (FloatType)i;
			p = value * (1.0 - saturation);
			q = value * (1.0 - (saturation * f));
			t = value * (1.0 - (saturation * (1.0 - f)));
			switch(i) {
				case 0: return Color(value, t, p);
				case 1: return Color(q, value, p);
				case 2: return Color(p, value, t);
				case 3: return Color(p, q, value);
				case 4: return Color(t, p, value);
				case 5: return Color(value, p, q);
				default:
					return Color(value, value, value);
			}
		}
	}

	/// Multplies all three components of a color with a scalar value.
	Color operator*(FloatType f) const { return(Color(r*f, g*f, b*f)); }
	/// Computes the sum of each color component.
	Color operator+(const Color& c2) const { return(Color(r+c2.r, g+c2.g, b+c2.b)); }
	/// Computes the product of each color component.
	Color operator*(const Color& c2) const { return(Color(r*c2.r, g*c2.g, b*c2.b)); }
	
	///////////////////////////////// Information ////////////////////////////////
	
	/// \brief Returns the number of components of this color.
	/// \return Always returns 3.
	size_t size() const { return 3; }

	/// \brief Gives a string representation of this color.
	/// \return A string that contains the components of the color. 
	QString toString() const { return QString("(%1 %2 %3)").arg(r).arg(g).arg(b); }	
};

/// Multplies all three components of a color with a scalar value.
inline Color operator*(FloatType f, const Color& c) { return(Color(c.r*f, c.g*f, c.b*f)); }

/// \brief Writes a color to a text output stream.
/// \param os The destination stream.
/// \param c The color to write.
/// \return The destination stream \a os.
inline std::ostream& operator<<(std::ostream &os, const Color& c) {
	return os << c.r << ' ' << c.g  << ' ' << c.b;
}

/// \brief Writes a color to a binary output stream.
/// \param stream The destination stream.
/// \param c The color to write.
/// \return The destination \a stream.
inline QDataStream& operator<<(QDataStream& stream, const Color& c)
{
	return stream << c.r << c.g << c.b;
}

/// \brief Reads a color from an input stream.
/// \param stream The source stream.
/// \param c[out] The destination variable.
/// \return The source \a stream.
inline QDataStream& operator>>(QDataStream& stream, Color& c)
{
	return stream >> c.r >> c.g >> c.b;
}

/**
 * \brief A color class with floating-point RGBA values.
 * 
 * The ColorA class stores four floating-point values, one for the red
 * component, one for the green component, one for the blue component and
 * one for the alpha component which controls the opacity of the color.
 * 
 * \author Alexander Stukowski
 * \sa Color
 */
class ColorA
{
public:
	/// The components of the color value.
	/// Red, green, blue and alpha values are in the range [0.0 - 1.0].
	FloatType r, g, b, a;

	/// The default constructor does not initialize the components of the color value.
	ColorA(void) {}
	/// Initializes the color width the given red, green and blue components.
	ColorA(FloatType red, FloatType green, FloatType blue, FloatType alpha) : r(red), g(green), b(blue), a(alpha) {}
	/// Copy constructor.
	ColorA(const ColorA& c) { r = c.r; g = c.g; b = c.b; a = c.a; }
	/// Takes the RGB components from the given color value and sets alpha to one.
	ColorA(const Color& c) { r = c.r; g = c.g; b = c.b; a = 1; }
	/// Conversion from a Qt color object,
	explicit ColorA(const QColor& c) : r(c.redF()), g(c.greenF()), b(c.blueF()), a(c.alphaF()) {}

	/// Sets RGB components to zero and alpha to one.
	void setBlack() { r = g = b = 0.0; a=1.0; }
	/// Sets all components to one.
	void setWhite() { r = g = b = 1.0; a=1.0; }

	/// Converts this to a RGB value.
	operator Color() const { return Color(r,g,b); }
	/// Converts this color to a Qt color object.
	operator QColor() const { 
		return QColor::fromRgbF(qMin(qMax(r, (FloatType)0), (FloatType)1), 
				qMin(qMax(g, (FloatType)0), (FloatType)1),
				qMin(qMax(b, (FloatType)0), (FloatType)1),
				qMin(qMax(a, (FloatType)0), (FloatType)1)); 
	}

	/// \brief Returns a reference to the i-th component of the color.
	/// \param i The index specifying the component to return (0=red, 1=green, 2=blue, 3=alpha).
	/// \return A reference to the i-th component that can be used to change the component's value. 
	FloatType& operator[](size_t i) {
		MAFEM_STATIC_ASSERT(sizeof(ColorA) == sizeof(FloatType) * 4);
		MAFEM_ASSERT(i>=0 && i<4);
		return (&r)[i]; 
	}

	/// \brief Returns a reference to the i-th component of the color.
	/// \param i The index specifying the component to return (0=red, 1=green, 2=blue, 3=alpha).
	/// \return The i-th component of the color. 
	const FloatType& operator[](size_t i) const {
		MAFEM_STATIC_ASSERT(sizeof(ColorA) == sizeof(FloatType) * 4);
		MAFEM_ASSERT(i>=0 && i<4);
		return (&r)[i]; 
	}

	/// \brief Returns a pointer to the first element of the color.
	/// \return A pointer to four consecutive floating-point numbers: red, green, blue and alpha.
	/// \sa constData() 
	FloatType* data() { 		
        MAFEM_STATIC_ASSERT(sizeof(ColorA) == sizeof(FloatType) * 4);
		return &r;
	}

	/// \brief Returns a pointer to the first element of the color for read-only access.
	/// \return A pointer to four consecutive floating-point numbers: red, green, blue and alpha.	
	/// \sa data()
	const FloatType* constData() const {
        MAFEM_STATIC_ASSERT(sizeof(ColorA) == sizeof(FloatType) * 4);
		return &r;
	}	

	/// \brief Compares this color with another color for equality.
	/// \param rgba The second color.
	/// \return \c true if all four color components are exactly equal; \c false otherwise.
	bool operator==(const ColorA& rgba) const { return (r == rgba.r && g == rgba.g && b == rgba.b && a == rgba.a); }
	
	/// \brief Compares this color with another color for inequality.
	/// \param rgba The second color.
	/// \return \c true if any of the four color components are not equal; \c false otherwise.		
	bool operator!=(const ColorA& rgba) const { return (r != rgba.r || g != rgba.g || b != rgba.b || a != rgba.a); }

	/// Adds the components of another color to this color.
	ColorA& operator+=(const ColorA& c) { r += c.r; g += c.g; b += c.b; a += c.a; return (*this); }
	/// Multiplies the components of another color with the components of this color.
	ColorA& operator*=(const ColorA& c) { r *= c.r; g *= c.g; b *= c.b; a *= c.a; return (*this); }

	/// \brief Ensures that all components are not greater than one.
	/// 
	/// If any of the R, G, B and A components is greater than 1 then it is set to 1.
	/// \sa clampMin(), clampMinMax()
	void clampMax() { if(r > 1.0) r = 1.0; if(g > 1.0) g = 1.0; if(b > 1.0) b = 1.0; if(a > 1.0) a = 1.0; }

	/// \brief Ensures that all components are not less than zero.
	/// 
	/// If any of the R, G, B and A components is less than 0 then it is set to 0.
	/// \sa clampMax(), clampMinMax()
	void clampMin() { if(r < 0.0) r = 0.0; if(g < 0.0) g = 0.0; if(b < 0.0) b = 0.0; if(a < 0.0) a = 0.0; }

	/// \brief Ensures that all components are not greater than one and not less than zero.
	/// \sa clampMin(), clampMax()
	void clampMinMax() { 
		if(r > 1.0) r = 1; 
		else if(r < 0.0) r = 0;
		if(g > 1.0) g = 1; 
		else if(g < 0.0) g = 0;
		if(b > 1.0) b = 1; 
		else if(b < 0.0) b = 0;
		if(a > 1.0) a = 1; 
		else if(a < 0.0) a = 0;
	}

	/// Multplies all components of a color with a scalar value.
	ColorA operator*(FloatType f) const { return(ColorA(r*f, g*f, b*f, a*f)); }
	/// Computes the sum of each color component.
	ColorA operator+(const ColorA& c2) const { return(ColorA(r+c2.r, g+c2.g, b+c2.b, a+c2.a)); }
	/// Computes the product of each color component.
	ColorA operator*(const ColorA& c2) const { return(ColorA(r*c2.r, g*c2.g, b*c2.b, a*c2.a)); }

	///////////////////////////////// Information ////////////////////////////////
	
	/// \brief Returns the number of components of this color.
	/// \return Always returns 4.
	size_t size() const { return 4; }

	/// \brief Gives a string representation of this color.
	/// \return A string that contains the components of the color. 
	QString toString() const { return QString("(%1 %2 %3 %4)").arg(r).arg(g).arg(b).arg(a); }	
};

/// Multplies all components of a color with a scalar value.
inline ColorA operator*(FloatType f, const ColorA& c) { return(ColorA(c.r*f, c.g*f, c.b*f, c.a*f)); }

/// \brief Writes a color to a text output stream.
/// \param os The destination stream.
/// \param c The color to write.
/// \return The destination stream \a os.
inline std::ostream& operator<<(std::ostream &os, const ColorA& c) {
	return os << c.r << ' ' << c.g  << ' ' << c.b << ' ' << c.a;
}

/// \brief Writes a color to a binary output stream.
/// \param stream The destination stream.
/// \param c The color to write.
/// \return The destination \a stream.
inline QDataStream& operator<<(QDataStream& stream, const ColorA& c)
{
	return stream << c.r << c.g << c.b << c.a;
}

/// \brief Reads a color from an input stream.
/// \param stream The source stream.
/// \param c[out] The destination variable.
/// \return The source \a stream.
inline QDataStream& operator>>(QDataStream& stream, ColorA& c)
{
	return stream >> c.r >> c.g >> c.b >> c.a;
}

};	// End of namespace Base

Q_DECLARE_METATYPE(Base::Color)
Q_DECLARE_METATYPE(Base::ColorA)
Q_DECLARE_TYPEINFO(Base::Color, Q_PRIMITIVE_TYPE);
Q_DECLARE_TYPEINFO(Base::ColorA, Q_PRIMITIVE_TYPE);

#endif // __COLOR_H
