///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include <Base.h>
#include <utilities/Exception.h>

namespace Base {
	
/// The exception handler for displaying error messages.
Exception::ExceptionHandler Exception::exceptionHandler = NULL;	

Exception::Exception() 
{ 
	_messages.push_back("An exception has occured."); 
}

Exception::Exception(const QString& message)
{
	_messages.push_back(message);
}

Exception& Exception::appendDetailMessage(const QString& message) 
{
	_messages.push_back(message);
	return *this;
}

Exception& Exception::prependGeneralMessage(const QString& message) 
{
	_messages.push_front(message);
	return *this;
}

/// Logs the error message to the console/debugger.
void Exception::logError() const 
{
	for(int i=0; i<_messages.size(); i++)
		qCritical(qPrintable(_messages[i]));
}

/// Displays the error message using the current message handler.
void Exception::showError() const 
{
	if(exceptionHandler == NULL) {
		logError();
		return;
	}	
	
	exceptionHandler(*this);
}

OutOfMemoryException::OutOfMemoryException() : Exception("Out of memory")
{ 
}

};	// End of namespace Base
