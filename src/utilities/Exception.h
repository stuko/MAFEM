///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
/** 
 * \file Exception.h 
 * \brief Contains the definition of the Base::Exception class. 
 */

#ifndef __EXCEPTION_H
#define __EXCEPTION_H

#include <Base.h>

namespace Base {

/**
 * \brief The base exception class used by this application.
 * 
 * \author Alexander Stukowski
 */
class Exception
{		
public:
	
	/// \brief Default constructor that creates an exception object with a default error message.
	Exception();

	/// \brief Constructor that initializes the exception object with a message string describing 
	///        the error that has occured.
	/// \param message The human-readable (and translated) message that describes the error.
	Exception(const QString& message);
	
	/// \brief Copy constructor.
	/// \param ex The exception object whose message strings are copied into the new exception object.
	Exception(const Exception& ex) : _messages(ex.messages()) {}

	/// \brief Multi-message constructor that initializes the exception object with multiple message string.
	/// \param errorMessages The list of message strings that describe the error. The list should be ordered with
	///                      the most general error description coming first followed by the more detailed information.
	Exception(const QStringList& errorMessages) : _messages(errorMessages) {}

	/// \brief Appends a message to this exception object that describes the error in more detail.
	/// \param message The human-readable and translated description string.
	/// \return A reference to this Exception object.
	Exception& appendDetailMessage(const QString& message);

	/// \brief Prepends a message to this exception object's list of messages that describes the error in a 
	///        more general way than the existing message strings in the exception object.
	/// \param message The human-readable and translated description string.
	/// \return A reference to this Exception object.
	Exception& prependGeneralMessage(const QString& message);
	
	/// \brief Sets the error messages of this exception object.
	/// \param messages The new list of messages that completely replaces the old messages.
	void setMessages(const QStringList& messages) { this->_messages = messages; }
	
	/// \brief Returns the most general message string in the exception object that describes the error.
	/// \return The first message from the object's list of messages.
	const QString& message() const { return _messages.front(); }

	/// \brief Returns the message strings in the exception object that describe the error.
	/// \return The list of message string stored.
	const QStringList& messages() const { return _messages; }
	
	/// \brief Logs the error message to the active log target.
	/// \sa showError();
	void logError() const;

	/// \brief Displays the error message using the current error message handler.
	/// \sa setExceptionHandler()
	/// \sa logError()
	void showError() const;
	
public:	
	
	/// Prototype for a handler function for exceptions. 
	typedef void (*ExceptionHandler)(const Exception& exception);
	
	/// \brief Installs a handler for exceptions.
	/// \param handler The new handler routine. This handler is responsible for showing the error message to the user.
	///
	/// \sa showError();
	static void setExceptionHandler(ExceptionHandler handler) { exceptionHandler = handler; }
	
private:

	/// The message strings describing the exception.
	/// This list is ordered with the most general error description coming first followed by the more detailed information.
	QStringList _messages;
	
	/// The exception handler for displaying error messages.
	static ExceptionHandler exceptionHandler;	
};

/******************************************************************************
* This exception is thrown by the collection classes if there is not 
* enough memory available.
******************************************************************************/
class OutOfMemoryException : public Exception
{
public:
	/// Default constructor.
	OutOfMemoryException();
};

};	// End of namespace Base

#endif // __EXCEPTION_H
