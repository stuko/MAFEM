///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_REPATOM_H
#define __MAFEM_REPATOM_H

#include <MAFEM.h>
#include "Atom.h"
#include <mesh/ElementOctree.h>

#include <boost/archive/detail/iserializer.hpp>

namespace MAFEM {

/**
 * \brief A representative QC atom.
 * 
 * The representative atoms are the nodes of the FE mesh. 
 * They have three degrees of freedom.
 * 
 * When periodic boundary conditions are applied then some of the nodes 
 * can be periodic images of others. In this case they do not
 * have their own degrees of freedom but follow the position of the
 * repsepctive master node.  
 * 
 * \author Alexander Stukowski
 */
class RepAtom : public Atom
{
public:

	/// \brief Deserialization constructor.
	/// 
	/// This constructor should not be used. 
	RepAtom() : _numSamplingAtoms(0), _clusterWeight(0),maxDevStrainSecInvL2Norm(0){}

	/// \brief Constructs an atom located at the given lattice site.
	/// \param site The lattice site where the new atom should be located.
	inline RepAtom(const LatticeSite& site) : Atom(site), _periodicImageMaster(NULL), _numSamplingAtoms(0), _index(-1), _clusterWeight(0),maxDevStrainSecInvL2Norm(0){}
	
	/// If this repatom is the periodic image of another repatom that this
	/// points to the master repatom.
	inline RepAtom* periodicImageMaster() const { return _periodicImageMaster; }
	
	/// Sets the master repatom of which this repatom a periodic image.
	inline void setPeriodicImageMaster(RepAtom* masterRepatom) { _periodicImageMaster = masterRepatom; }
	
	/// Returns the real repatom if this node is a periodic image of a repatom. 
	/// Otherwise returns itself.
	inline RepAtom* realRepatom() const { return _periodicImageMaster ? _periodicImageMaster : const_cast<RepAtom*>(this); }

	/// Returns the index of the repatom.
	inline int index() const { return _index; }

	/// Sets the index of the repatom.
	inline void setIndex(int index) { _index = index; }

	/// Returns the number of the sampling atoms that belong to this repatom's cluster.
	inline int numSamplingAtoms() const { return _numSamplingAtoms; }

	/// Sets the value of the internal sampling atom counter. 
	inline void setNumSamplingAtoms(int n) { _numSamplingAtoms = n; MAFEM_ASSERT(n>=0); }
	
	/// Returns the weight of the sampling cluster that belongs to this repatom.
	inline FloatType clusterWeight() const { return _clusterWeight; }

	/// Sets the weight of the sampling cluster that belongs to this repatom.
	inline void setClusterWeight(FloatType w) { _clusterWeight = w; }

	/**//// Returns the next repatom in the linked list of repatoms that
	/// are in the same leaf cell of the element octree.
	RepAtom* GetNextRepAtomInCell() const { return nextRepAtomInCell; }

	/**//// Sets the next repatom in the linked list of repatoms that
	/// are in the same leaf cell of the element octree.
	/// This method is called by the octree class to build the linked list.
	void SetNextRepAtomInCell(RepAtom* next) { nextRepAtomInCell = next; }

	/**//// Sets the binary location code of this repatom. This information can be used to
	/// quickly locate the repatom in the octree.
	void SetOctreeLocationCode(const OctreeLocationCode& code) { octreeLocationCode = code; }

	/**//// Gets the binary location code of this repatom. This information can be used to
	/// quickly locate the repatom in the octree.
	const OctreeLocationCode& GetOctreeLocationCode() const { return octreeLocationCode; }

	/**//// Indicates that the repatom has to be updated.
	bool IsDirty() const { return isDirty; }

	/**//// Sets the value of the dirty flag for this repatom.
	void SetDirty(bool dirty) { isDirty = dirty; }

	/**//// sets maximum value of dev strain second invariant L2 norm
	void setMaxDevStrainSecInvL2Norm(FloatType value){maxDevStrainSecInvL2Norm = value;}

	/**//// returns maximum value of dev strain second invariant L2 norm
	FloatType getMaxDevStrainSecInvL2Norm() { return maxDevStrainSecInvL2Norm;}
	


protected:
	
	/// If this repatom is the periodic image of another repatom that this
	/// points to the master repatom.
	RepAtom* _periodicImageMaster;
	
	/// The index of the repatom.
	int _index;
	
	/// The number of sampling atoms that belong to this repatom.
	int _numSamplingAtoms;	
	
	/// The weight of the sampling cluster that belongs to this repatom.
	FloatType _clusterWeight;
	
	/**//// The linked list of repatoms that are in the same leaf cell of the element octree.
	RepAtom* nextRepAtomInCell;

	/**//// The binary location code of this repatom. This information can be used to
	/// quickly locate the repatom in the octree.
	OctreeLocationCode octreeLocationCode;

	/**//// Indicates that the repatom has to be updated.
	bool isDirty;

	/**//// stores maximum value of dev strain second invariant L2 norm
	FloatType maxDevStrainSecInvL2Norm;
	


private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
    	ar & boost::serialization::base_object<Atom>(*this);
		ar & _periodicImageMaster;
		ar & _index;
		ar & nextRepAtomInCell;
		ar & octreeLocationCode;
	}

	friend class boost::serialization::access;	
};

}; // End of namespace MAFEM

// This allocator class ensures that all RepAtom instances created by the Boost serialization
// code are allocated from the memory pool of the Mesh class. 
namespace boost {
namespace archive {
namespace detail {
#if buildLocationCluster
	template<>
	struct heap_allocation<MAFEM::RepAtom>
	{
		static MAFEM::RepAtom* invoke_new();
		static void invoke_delete(MAFEM::RepAtom* p);
	    explicit heap_allocation(){
	        m_p = invoke_new();
	    }
	    ~heap_allocation(){
	        if(m_p) invoke_delete(m_p);
	    }
	    MAFEM::RepAtom* get() const {
	        return m_p;
	    }
	    MAFEM::RepAtom* release() {
	    	MAFEM::RepAtom* p = m_p;
	        m_p = 0;
	        return p;
	    }
	private:
	    MAFEM::RepAtom* m_p;
	};
#else
	template<>
	struct heap_allocator<MAFEM::RepAtom>	
	{
		// Allocates a new RepAtom instance from the memory pool in the Mesh class.
		static MAFEM::RepAtom* invoke();	// Implementation is in MeshTriangulation.cpp
	};
#endif
};
};
};

#endif // __MAFEM_REPATOM_H
