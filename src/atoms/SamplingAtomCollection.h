///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_SAMPLING_ATOM_COLLECTION_H
#define __MAFEM_SAMPLING_ATOM_COLLECTION_H

#include <MAFEM.h>
#include <atoms/AtomCollectionBase.h>
#include <atoms/SamplingAtom.h>

namespace MAFEM {

/// Defines the maximum number of neighbors one atom may have. 
#define MAFEM_MAXIMUM_NEIGHBOR_COUNT		800 //256

/**
 * \brief Stores all atoms that are used by the QC-model to compute the
 *        approximate forces and total energy.
 * 
 * \author Alexander Stukowski
 */
class SamplingAtomCollection : public AtomCollectionBase<SamplingAtom>
{
public:
	
	/// \brief Default constructor that creates an empty collection.
	SamplingAtomCollection() {}
	
	/// \brief Copy constructor.
	/// \note This copy constructor is only needed for the Q_FOREACH macro. It does
	///       not copy the hashtable, only the plain list of atoms.
	SamplingAtomCollection(const SamplingAtomCollection& other) : AtomCollectionBase<SamplingAtom>(other) {}
	
	/// This rebuilds the neighbor lists for the sampling atoms.
	void buildNeighborLists(Simulation* sim);
	
private:

	/// Controls how many neighbor list pointers are stored in one memory page.
	static const int neighborListPageSize;
	
	/// Stores the memory pages for the neighbor lists.
	QVector< shared_array<SamplingAtom*> > _memoryPages;
};

}; // End of namespace MAFEM

#endif // __MAFEM_SAMPLING_ATOM_COLLECTION_H
