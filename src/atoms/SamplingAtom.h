///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_SAMPLING_ATOM_H
#define __MAFEM_SAMPLING_ATOM_H

#include <MAFEM.h>
#include "Atom.h"
#include <mesh/Element.h>

namespace MAFEM {
	
class RepAtom;		// defined in RepAtom

/**
 * \brief A sampling atoms.
 * 
 * \author Alexander Stukowski
 */
class SamplingAtom : public Atom
{
public:

	/// \brief Constructs an atom located at the given lattice site.
	/// \param site The lattice site where the new atom is located.
	SamplingAtom(const LatticeSite& site) : Atom(site), _neighborsBegin(NULL), _neighborsEnd(NULL), _weight(0) {}
	
	/// \brief Performs the final initialization.
	/// \param element The element that contains the site or NULL.
	/// \param wrappedPos The wrapped around position of the atom in the undeformed configuration.
	/// \param imageShift Specifies the peridic image shift vector for this atom.
	/// \param clusterRepatom The repatom if this sampling atom belongs to a repatom's cluster.
	/// \param samplingNode The mesh node if this sampling atom is located exactly on a node.
	/// \note This extra initialization function is required because the constructor 
	///       cannot accept more than three parameters due to a limitation in the Boost memory pool library. 
	void initialize(Element* element, const Point3& wrappedPos, const Vector3I& imageShift, RepAtom* clusterRepatom, RepAtom* samplingNode, FloatType squaredDistanceToCenter) {
		_element = element;
		_clusterRepatom = clusterRepatom;
		_imageShift = imageShift;
		_samplingNode = samplingNode;
		_squaredDistanceToCenter = squaredDistanceToCenter;
		MAFEM_ASSERT((_samplingNode!=NULL) != (_element!=NULL));
		// Compute barycentric coordinates of atom.
		if(element != NULL) {
			if(element->vertex(0) == samplingNode) _baryc = Vector4(1,0,0,0);
			else if(element->vertex(1) == samplingNode) _baryc = Vector4(0,1,0,0);
			else if(element->vertex(2) == samplingNode) _baryc = Vector4(0,0,1,0);
			else if(element->vertex(3) == samplingNode) _baryc = Vector4(0,0,0,1);
			else { 
				_baryc = element->computeBarycentric(wrappedPos);
				MAFEM_ASSERT_MSG(_baryc[0] >= -FLOATTYPE_EPSILON && _baryc[1] >= -FLOATTYPE_EPSILON && _baryc[2] >= -FLOATTYPE_EPSILON && _baryc[3] >= -FLOATTYPE_EPSILON, 
						"SamplingAtom::initialize()", QString("Negative barycentric coordinate detected: %1 (lattice site: %2)").arg(_baryc.toString()).arg(wrappedPos.toString()).toAscii().constData());
				MAFEM_ASSERT_MSG(_baryc[0] <= 1.0+FLOATTYPE_EPSILON && _baryc[1] <= 1.0+FLOATTYPE_EPSILON && _baryc[2] <= 1.0+FLOATTYPE_EPSILON && _baryc[3] <= 1.0+FLOATTYPE_EPSILON, 
						"SamplingAtom::initialize()", QString("Barycentric coordinate out of range: %1 (lattice site: %2)").arg(_baryc.toString()).arg(wrappedPos.toString()).toAscii().constData());
				_baryc[3] = 1.0 - _baryc[0] - _baryc[1] - _baryc[2];
				// Clamp barycentric coordinates to the [0,1] range.
				for(int i=0; i<4; i++) {
					if(_baryc[i] < 0) _baryc[i] = 0;
					else if(_baryc[i] > 1) _baryc[i] = 1;
				}
			}
		}
	}

	/// Returns the element the atom is located in.
	Element* element() const { return _element; }
	
	/// Returns the barycentric coordinates of the atom in the containing element.
	const Vector4& barycentric() const { return _baryc; }

	/// Returns the peridic image shift vector for this atom.
	const Vector3I& imageShift() const { return _imageShift; }		

	/// Returns the central repatom if this sampling atom is part of an atom cluster.
	RepAtom* clusterRepatom() const { return _clusterRepatom; }
	
	/// Associates the sampling atom with the given repatom.
	void setClusterRepatom(RepAtom* repatom, FloatType squaredDistance) {
		_clusterRepatom = repatom; 
		_squaredDistanceToCenter = squaredDistance;
	}

	/// Returns the squared distance of the sampling atom to the central repatom of its cluster.
	FloatType squaredDistanceToCenter() const { return _squaredDistanceToCenter; }
	
	/// Returns the mesh node if this sampling atom is located exactly on a node.
	RepAtom* samplingNode() const { return _samplingNode; }
	
	/// Returns true if this sampling atom does not belong to a cluster. 
	bool isPassive() const { return _clusterRepatom == NULL; }
	
	/// Returns true if this sampling atom belongs to a cluster. 
	bool isActive() const { return _clusterRepatom != NULL; }
	
	/// Returns the number of neighbors of this sampling atom.
	int numNeighbors() const { return _neighborsEnd - _neighborsBegin; }

	/// Returns the pointer to the first neighbor of this atom..
	SamplingAtom** neighborsBegin() const { return _neighborsBegin; }

	/// Returns a pointer that points to the end of the neighbor list of this atom. 
	SamplingAtom** neighborsEnd() const { return _neighborsEnd; }

	/// Returns the EAM embedding energy derivative.
	/// This method is only used for the force-based QC scheme.
	FloatType embeddingEnergyDerivative() const { return _embeddingEnergyDerivative; }

	/// Stores the EAM embedding energy derivative.
	/// This method is only used for the force-based QC scheme.
	void setEmbeddingEnergyDerivative(FloatType d) { _embeddingEnergyDerivative = d; }

	/// Returns the weight of the sampling atom's contribution to the total energy.
	inline FloatType weight() const { return _weight; }

	/// Sets the weight of the sampling atom's contribution to the total energy.
	inline void setWeight(FloatType w) { _weight = w; }
	
	/**//// sets the centrosymmetry value
	void setCentroSymmetry(FloatType cenSym) { centroSymmetry = cenSym; }

	/**//// returns the centrosymmetry value
	FloatType getCentroSymmetry() {return centroSymmetry;}

private:

	/// The element that contains this atom or NULL if it is not inside an element.
	Element* _element;
	
	/// The barycentric coordinates of the atom in the containing element.
	Vector4 _baryc;
	
	/// Contains the central repatom if this sampling atom is part of an atom cluster.
	RepAtom* _clusterRepatom;
	
	/// This contains the mesh node if this sampling atom is located exactly on a node.
	RepAtom* _samplingNode;
	
	/// Specifies the peridic image shift vector for this atom.
	Vector3I _imageShift;
	
	/// Pointer to the first neighbor atom. 
	SamplingAtom** _neighborsBegin;

	/// Points to the end of the neighbor list of this atom. 
	SamplingAtom** _neighborsEnd;
	
	/// Pointer to the next atom in the linked list of a bin.
	SamplingAtom* _nextInBin;

	/// The weight of the sampling atom's contribution to the total energy.
	FloatType _weight;
	
	/// The squared distance of the sampling atom to the central repatom of its cluster.
	FloatType _squaredDistanceToCenter;
	
	/// This stores the EAM embedding energy derivative.
	/// This field is only used for the force-based QC scheme.
	FloatType _embeddingEnergyDerivative;
	
	/// This stores the centroSymmetry check
	bool centroSymmetryCheck;

	/**//// stores the centroSymmetry parameter
	FloatType centroSymmetry;

	friend class SamplingAtomCollection;
	
};

}; // End of namespace MAFEM

#endif // __MAFEM_SAMPLING_ATOM_H
