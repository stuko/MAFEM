///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_GROUP_H
#define __MAFEM_GROUP_H

#include <MAFEM.h>
#include <simulation/SimulationResource.h>
#include "Region.h"

namespace MAFEM {

/// The maximum number of groups that can be defined in a simulation.
#define MAX_GROUP_COUNT		32

/**
 * \brief A user defined set of repatoms.
 * 
 * \author Alexander Stukowski
 */
class Group : public SimulationResource
{
private:

	friend class Simulation;
		
	/// \brief Constructs a new group object.
	Group(Simulation* sim, const shared_ptr<Region>& region, int groupIndex) : 
		SimulationResource(sim), _region(region), _groupIndex(groupIndex) {}
	
	/// \brief The destructor.
	~Group() {}
	
public:

	/// \brief Returns the geometric region that specifies which atoms belong to the group.
	/// \return The geometric region or \c NULL if this is the special ALL group.
	const shared_ptr<Region>& region() const { return _region; }

	/// \brief Returns the index of this group in the list of simulation groups.
	int groupIndex() const { return _groupIndex; }

private:

	/// The index of this group in the list of simulation groups.
	int _groupIndex;

	/// The geometric region that defines which atoms belong to this group.
	shared_ptr<Region> _region;
};

}; // End of namespace MAFEM

#endif // __MAFEM_GROUP_H
