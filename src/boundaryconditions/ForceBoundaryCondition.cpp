///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "ForceBoundaryCondition.h"
#include <simulation/Simulation.h>
#include <atoms/SamplingAtom.h>

namespace MAFEM {
	
/******************************************************************************
* Constructor.
******************************************************************************/
ForceBoundaryCondition::ForceBoundaryCondition(Group* group) : SimulationResource(group->simulation()), _group(group) 
{
	// Register this BC with the global simulation object.
	//this->simulation()->_forceBC.push_back(shared_ptr<ForceBoundaryCondition>(this));	
}

/******************************************************************************
* This is called by the system to let the boundary condition calculate the 
( potential energy and the force acting on an atom within the indentor force field.
******************************************************************************/
FloatType IndentorForceField::calculateEnergyAndForces(const SamplingAtom* atom, Vector3& force)
{
	if(_strength == 0) return 0;
	Vector3 vec = atom->deformedPos() - _center;
	FloatType r2 = LengthSquared(vec);
	if(r2 >= square(_radius)) return 0;
	FloatType r = sqrt(r2);
	FloatType diff = _radius - r;
	FloatType temp = _strength * diff * diff;
	Vector3 f = (3.0 * temp / r) * vec;
	force += f;	
	_appliedForce += f;
	return temp * diff;
}

/******************************************************************************
* This is called by the system to let the boundary condition calculate the 
* potential energy of the atoms within the indentor force field.
******************************************************************************/
FloatType IndentorForceField::calculateEnergy(const SamplingAtom* atom)
{
	if(_strength == 0) return 0;
	Vector3 vec = atom->deformedPos() - _center;
	FloatType r2 = LengthSquared(vec);
	if(r2 >= square(_radius)) return 0;
	FloatType r = sqrt(r2);
	FloatType diff = _radius - r;	
	return _strength * diff * diff * diff;
}

/******************************************************************************
* This is called by the system to let the boundary condition calculate the 
( potential energy and the force acting on an atom within the planar force field.
******************************************************************************/
FloatType PlanarForceField::calculateEnergyAndForces(const SamplingAtom* atom, Vector3& force)
{
	if(_strength == 0) return 0;
	FloatType d = DotProduct(atom->deformedPos() - _center, Normalize(_normal));
	if(d >= 0) return 0;
	FloatType diff = -d;
	FloatType temp = _strength * diff * diff;
	Vector3 f = 3.0 * temp * Normalize(_normal);
	force += f;	
	_appliedForce += f;
	return temp * diff;
}

/******************************************************************************
* This is called by the system to let the boundary condition calculate the 
* potential energy of the atoms within the planar force field.
******************************************************************************/
FloatType PlanarForceField::calculateEnergy(const SamplingAtom* atom)
{
	if(_strength == 0) return 0;
	FloatType d = DotProduct(atom->deformedPos() - _center, Normalize(_normal));
	if(d >= 0) return 0;
	FloatType diff = -d;
	return _strength * diff * diff * diff;
}

/******************************************************************************
* This is called by the system to let the boundary condition calculate the 
( potential energy and the force acting on an atom within the distributed load.
******************************************************************************/
FloatType DistributedLoad::calculateEnergyAndForces(const SamplingAtom* atom, Vector3& force)
{
	if(_force == NULL_VECTOR) return 0;
	force += _atomForce;
	return 0;
}

FloatType DistributedLoad::calculateEnergy(const SamplingAtom* atom)
{
	return 0;
}

void DistributedLoad::prepare(Mesh& mesh)
{
	int numSamplingAtoms = 0;
	for(int i = 0; i < mesh.samplingAtoms().size(); ++i)
	{
		SamplingAtom* atom =  mesh.samplingAtoms()[i];
		if(atom->belongsToGroup(group()))
			++numSamplingAtoms;
	}
	_atomForce = _force / numSamplingAtoms;
}

}; // End of namespace MAFEM
