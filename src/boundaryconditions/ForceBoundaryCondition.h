///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_FORCE_BOUNDARY_CONDITION_H
#define __MAFEM_FORCE_BOUNDARY_CONDITION_H

#include <MAFEM.h>
#include <simulation/SimulationResource.h>
#include "Group.h"

namespace MAFEM {

class Mesh;							// defined in Mesh.h
class SamplingAtom;					// defined in SamplingAtom.h

/**
 * \brief Base class for all boundary condition objects that excert 
 *        a force on atoms.
 * 
 * \author Alexander Stukowski
 */
class ForceBoundaryCondition : public SimulationResource
{	
protected:

	/// \brief Constructor.
	/// \param group The group of atoms that should be affected by this boundary condition.
	ForceBoundaryCondition(Group* group);
	
public:

	/// \brief Returns the group of atoms on which this boundary condition acts.
	Group* group() const { return _group; }
	
	/// \brief Initializes the boundary condition.
	/// \param mesh The mesh.
	///
	/// This method is called by the system before every force calculation pass.	
	virtual void prepare(Mesh& mesh) {}

	/// \brief This is called by the system to let the boundary condition calculate the 
	///        potential energy of an atom within the force field.
	/// \param atom The atom for which to calculate the energy. 
	/// \return The contribution of this boundary condition to the potential energy of the given atom.
	virtual FloatType calculateEnergy(const SamplingAtom* atom) = 0;

	/// \brief This is called by the system to let the boundary condition calculate the 
	///        potential energy and the force acting on an atom within the force field.
	/// \param atom The atom for which to calculate the energy and force. 
	/// \param force The boundary conditions adds its force to the given vector.
	/// \return The contribution of this boundary condition to the potential energy of the given atom.
	virtual FloatType calculateEnergyAndForces(const SamplingAtom* atom, Vector3& force) = 0;

private:

	/// The group of atoms to which this BC is applied.
	Group* _group;
};

typedef QVector< shared_ptr<ForceBoundaryCondition> > ForceBoundaryConditionList;

/**
 * \brief A spherical indentor force field.
 * 
 * \author Alexander Stukowski
 */
class IndentorForceField : public ForceBoundaryCondition
{
public:

	/// \brief Constructor.
	/// \param group The group of atoms that should be affected by this boundary condition.
	IndentorForceField(Group* group, FloatType strength, FloatType radius, const Point3& center = ORIGIN) : ForceBoundaryCondition(group),
		_strength(strength), _radius(radius), _center(center) {}

	/// \brief Initializes the boundary condition.
	/// \param mesh The mesh.
	virtual void prepare(Mesh& mesh)
	{
		// Reset the internal force measure.
		_appliedForce = NULL_VECTOR; 
	}

	/// \brief This is called by the system to let the boundary condition calculate the 
	///        potential energy of an atom within the force field.
	/// \param atom The atom for which to calculate the energy. 
	/// \return The contribution of this boundary condition to the potential energy of the given atom.
	virtual FloatType calculateEnergy(const SamplingAtom* atom);

	/// \brief This is called by the system to let the boundary condition calculate the 
	///        potential energy and the force acting on an atom within the force field.
	/// \param atom The atom for which to calculate the energy and force. 
	/// \param force The boundary conditions adds its force to the given vector.
	/// \return The contribution of this boundary condition to the potential energy of the given atom.
	virtual FloatType calculateEnergyAndForces(const SamplingAtom* atom, Vector3& force);

	/// \brief Returns the total force vector applied by the indentor. 
	const Vector3& appliedForce() const { return _appliedForce; }

	/// \brief Returns current center of the indentor. 
	const Point3& center() const { return _center; }
	
	/// \brief Sets the center location of the indentor. 
	void setCenter(const Point3& center) { _center = center; }

private:

	/// The strength parameter of the force field.
	FloatType _strength;
	
	/// The radius of the spherical indentor.
	FloatType _radius;

	/// The center of the spherical indentor.
	Point3 _center;
	
	/// This measures the total force applied by the indentor to the crystal.
	Vector3	_appliedForce;
};

class PlanarForceField : public ForceBoundaryCondition
{
public:

	/// \brief Constructor.
	/// \param group The group of atoms that should be affected by this boundary condition.
	PlanarForceField(Group* group, FloatType strength, const Point3& center = ORIGIN, const Vector3& normal = Vector3(0,0,-1)) : ForceBoundaryCondition(group),
		_strength(strength), _center(center), _normal(normal) {}

	/// \brief Initializes the boundary condition.
	/// \param mesh The mesh.
	virtual void prepare(Mesh& mesh)
	{
		// Reset the internal force measure.
		_appliedForce = NULL_VECTOR;
	}

	/// \brief This is called by the system to let the boundary condition calculate the
	///        potential energy of an atom within the force field.
	/// \param atom The atom for which to calculate the energy.
	/// \return The contribution of this boundary condition to the potential energy of the given atom.
	virtual FloatType calculateEnergy(const SamplingAtom* atom);

	/// \brief This is called by the system to let the boundary condition calculate the
	///        potential energy and the force acting on an atom within the force field.
	/// \param atom The atom for which to calculate the energy and force.
	/// \param force The boundary conditions adds its force to the given vector.
	/// \return The contribution of this boundary condition to the potential energy of the given atom.
	virtual FloatType calculateEnergyAndForces(const SamplingAtom* atom, Vector3& force);

	/// \brief Returns the total force vector applied by the plane.
	const Vector3& appliedForce() const { return _appliedForce; }

	/// \brief Returns current center of the plane.
	const Point3& center() const { return _center; }

	/// \brief Sets the center location of the plane.
	void setCenter(const Point3& center) { _center = center; }
	
	/// \brief Returns current normal vector of the plane.
	const Vector3& normal() const { return _normal; }

	/// \brief Sets the normal vector of the plane.
	void setNormal(const Vector3& normal) { _normal = normal; }

private:

	/// The strength parameter of the force field.
	FloatType _strength;

	/// The center of the spherical plane.
	Point3 _center;
	
	/// The center of the spherical plane.
	Vector3 _normal;

	/// This measures the total force applied by the plane to the crystal.
	Vector3	_appliedForce;
};

class DistributedLoad : public ForceBoundaryCondition
{
public:

	/// \brief Constructor.
	/// \param group The group of atoms that should be affected by this boundary condition.
	DistributedLoad(Group* group, const Vector3& force = Vector3(NULL_VECTOR)) : ForceBoundaryCondition(group), _force(force) {}

	/// \brief Initializes the boundary condition.
	/// \param mesh The mesh.
	virtual void prepare(Mesh& mesh);

	/// \brief This is called by the system to let the boundary condition calculate the
	///        potential energy of an atom within the force field.
	/// \param atom The atom for which to calculate the energy.
	/// \return The contribution of this boundary condition to the potential energy of the given atom.
	virtual FloatType calculateEnergy(const SamplingAtom* atom);

	/// \brief This is called by the system to let the boundary condition calculate the
	///        potential energy and the force acting on an atom within the force field.
	/// \param atom The atom for which to calculate the energy and force.
	/// \param force The boundary conditions adds its force to the given vector.
	/// \return The contribution of this boundary condition to the potential energy of the given atom.
	virtual FloatType calculateEnergyAndForces(const SamplingAtom* atom, Vector3& force);
	
	/// \brief Returns current normal vector of the plane.
	const Vector3& force() const { return _force; }

	/// \brief Sets the normal vector of the plane.
	void setForce(const Vector3& force) { _force = force; }

private:
	
	/// The distributed load.
	Vector3 _force;
	
	/// The load on a sampling atom.
	Vector3 _atomForce;
};

}; // End of namespace MAFEM

#endif // __MAFEM_FORCE_BOUNDARY_CONDITION_H
