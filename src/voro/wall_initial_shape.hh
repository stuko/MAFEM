#include "voro++.hh"
using namespace voro;

class wall_initial_shape : public wall {

	public:
 
               wall_initial_shape() {

			const double Phi=0.5*(1+sqrt(5.0));
			const double phi=0.5*(1-sqrt(5.0));

			// Create a dodecahedron
			v.init(-6,6,-6,6,-6,6);
			v.plane(0,Phi,1);v.plane(0,-Phi,1);v.plane(0,Phi,-1);
			v.plane(0,-Phi,-1);v.plane(1,0,Phi);v.plane(-1,0,Phi);
			v.plane(1,0,-Phi);v.plane(-1,0,-Phi);v.plane(Phi,1,0);
			v.plane(-Phi,1,0);v.plane(Phi,-1,0);v.plane(-Phi,-1,0);
		};
		bool point_inside(double x,double y,double z) {return true;}
		bool cut_cell(voronoicell &c,double x,double y,double z) {
 
                         // Set the cell to be equal to the dodecahedron
                         c=v;
                         return true;
                 }
                 bool cut_cell(voronoicell_neighbor &c,double x,double y,double z) {
 
                         // Set the cell to be equal to the dodecahedron
                         c=v;
                         return true;
                 }
         private:
                 voronoicell v;
 };
