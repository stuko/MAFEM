///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "ElementCollection.h"

namespace MAFEM {

/******************************************************************************
* This rebuilds the 3d segment tree data structure.
******************************************************************************/
void ElementCollection::rebuildSegmentTree()
{
	// First clear old tree structure.
    delete _tree.segment_tree_3;
    delete _tree.segment_tree_2;
    delete _tree.segment_tree_1;
    delete _tree.anchor;
    // Allocate new structure.
    _tree.anchor = new CGAL_Segment_tree_3::Tree_anchor_type;
    _tree.segment_tree_1 = new CGAL_Segment_tree_3::Segment_tree_1_type(*_tree.anchor);
    _tree.segment_tree_2 = new CGAL_Segment_tree_3::Segment_tree_2_type(*_tree.segment_tree_1);
    _tree.segment_tree_3 = new CGAL_Segment_tree_3::Segment_tree_3_type(*_tree.segment_tree_2);
    
    // Feed the elements into the segment tree.
    _tree.segment_tree_3->make_tree_impl(
    	make_transform_iterator(constBegin(), &intervalFromElement),
    	make_transform_iterator(constEnd(), &intervalFromElement));
    	
    // The tree is now up-to-date.
    _treeValid = true;
}

// This is an output iterator that receives the elements found by the
// segment tree structure. It does the final point-in-tetrahedron tests.
// It returns the first element that contains the lattice site.
template<typename Interval3D>
struct SegmentTreeOutputIterator : public std::iterator<output_iterator_tag, void, void, void, void> 
{		
	// The query point.
	const LatticeSite& site;
	// Pointer to the result variable.
	Element** result;
	
	// Constructor.
	SegmentTreeOutputIterator(const LatticeSite& _site, Element** _result) : site(_site), result(_result) {}
	 
	SegmentTreeOutputIterator& operator*() { return *this; }
	SegmentTreeOutputIterator& operator=(const Interval3D& iv) {
		// Only look for elements if none has been found yet. 
		if(*result == NULL) {
			// Do the final point-in-tetrahedron test.
			if(iv.second->lattice() == site.lattice() && iv.second->containsSite(site.indices())) {
				// Store element in result variable for pick-up by the findAt() method. 
				*result = iv.second;
			}
		}
		return *this;
	}	
	SegmentTreeOutputIterator& operator++() { return *this; }
	SegmentTreeOutputIterator& operator++(int) { return *this; }
};

// This is an output iterator that receives the elements found by the
// segment tree structure. It does the final point-in-tetrahedron tests.
// It returns all elements that contains the lattice site.
template<typename Interval3D>
struct SegmentTreeOutputIterator2 : public std::iterator<output_iterator_tag, void, void, void, void> 
{		
	// The query point.
	const LatticeSite& site;
	// Pointer to the result variable.
	QVector<Element*>& result;
	
	// Constructor.
	SegmentTreeOutputIterator2(const LatticeSite& _site, QVector<Element*>& _result) : site(_site), result(_result) {}
	 
	SegmentTreeOutputIterator2& operator*() { return *this; }
	SegmentTreeOutputIterator2& operator=(const Interval3D& iv) {
		// Do the final point-in-tetrahedron test.
		if(iv.second->lattice() == site.lattice() && iv.second->containsSite(site.indices())) {
			// Store element in result variable for pick-up by the findContainingElements() method. 
			result.push_back(iv.second);
		}
		return *this;
	}	
	SegmentTreeOutputIterator2& operator++() { return *this; }
	SegmentTreeOutputIterator2& operator++(int) { return *this; }
};

/******************************************************************************
* Finds the first element that constains the given lattice site.
******************************************************************************/
Element* ElementCollection::findAt(const LatticeSite& site) const 
{
	// Make this function non-const.
	ElementCollection* myself = const_cast<ElementCollection*>(this);
	
	// Make sure the segment tree is up-to-date.
	if(_treeValid == false) {
		myself->rebuildSegmentTree();
	}

	// Make a quick check with last found element to exploit spatial coherence
	// of point queries.
	if(_lastFoundElement && _lastFoundElement->lattice() == site.lattice()) {
		if(_lastFoundElement->containsSite(site.indices())) {
			return _lastFoundElement;
		}
		// Make second quick check with all neighbors of the last found element.
		for(int i=0; i<NEN; i++) {
			if(_lastFoundElement->neighbor(i)) {
				MAFEM_ASSERT(_lastFoundElement->neighbor(i)->lattice() == site.lattice());
				if(_lastFoundElement->neighbor(i)->containsSite(site.indices())) {
					myself->_lastFoundElement = _lastFoundElement->neighbor(i);
					return _lastFoundElement;
				}
			}
		}
	}

	// Let the 3d segment tree find all element whose bounding box contains the query point.
	
	// The segment tree can only look for intervals. So we have to make an interval out of the query point.
	Interval3D window(Pure_interval3D(
		Tree_Key(site.pos().X - FLOATTYPE_EPSILON, site.pos().Y - FLOATTYPE_EPSILON, site.pos().Z - FLOATTYPE_EPSILON), 
		Tree_Key(site.pos().X + FLOATTYPE_EPSILON, site.pos().Y + FLOATTYPE_EPSILON, site.pos().Z + FLOATTYPE_EPSILON)), NULL);
	
	// This variable will store the result element.
	Element* result = NULL;
	
	// This is an output iterator that receives the elements found by the
	// segment tree structure. It will do the final point-in-tetrahedron tests.
	SegmentTreeOutputIterator<Interval3D> out(site, &result);

	// Let the tree search for elements.
	myself->_tree.segment_tree_3->window_query_impl(window, out);
	
	// Save this element for later quick tests.
	myself->_lastFoundElement = result;
	
	return result;
}

/******************************************************************************
* Finds all elements that contain the given lattice site.
******************************************************************************/
QVector<Element*> ElementCollection::findContainingElements(const LatticeSite& site) const
{
	// Make sure the segment tree is up-to-date.
	if(_treeValid == false) {
		ElementCollection* myself = const_cast<ElementCollection*>(this);
		myself->rebuildSegmentTree();
	}
	// Let the 3d segment tree find all element whose bounding box contains the query point.
	
	// The segment tree can only look for intervals. So we have to make an interval out of the query point.
	Interval3D window(Pure_interval3D(
		Tree_Key(site.pos().X - FLOATTYPE_EPSILON, site.pos().Y - FLOATTYPE_EPSILON, site.pos().Z - FLOATTYPE_EPSILON), 
		Tree_Key(site.pos().X + FLOATTYPE_EPSILON, site.pos().Y + FLOATTYPE_EPSILON, site.pos().Z + FLOATTYPE_EPSILON)), NULL);
	
	QVector<Element*> list;
	
	// This is an output iterator that receives the elements found by the
	// segment tree structure. It will do the final point-in-tetrahedron tests.
	SegmentTreeOutputIterator2<Interval3D> out(site, list);

	// Let the tree search for elements.
	_tree.segment_tree_3->window_query_impl(window, out);
	
	return list;
}

}; // End of namespace MAFEM


