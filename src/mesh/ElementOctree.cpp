///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "MAFEM.h"
#include "Base.h"
#include "ElementOctree.h"
#include <simulation/Simulation.h>
#include <grain/GrainBoundary.h>
#include <atoms/RepAtom.h>
#include <mesh/Mesh.h>

namespace MAFEM {

/////////////////// Lookup tables used by the Subdivide() method //////////////////////

/// The coordinates are specified in the local coordinate system of the cell in the range [0 - 2].
static const Vector3I SC_CELL_TEMPLATE[] = {
	Vector3I(0,0,0), Vector3I(2,0,0), Vector3I(2,2,0), Vector3I(0,2,0), 
	Vector3I(0,0,2), Vector3I(2,0,2), Vector3I(2,2,2), Vector3I(0,2,2)
};

/// The coordinates are specified in the local coordinate system of the cell in the range [0 - 2].
static const Vector3I FCC_CELL_TEMPLATE[] = {
	Vector3I(0,0,0), Vector3I(2,0,0), Vector3I(2,2,0), Vector3I(0,2,0), 
	Vector3I(0,0,2), Vector3I(2,0,2), Vector3I(2,2,2), Vector3I(0,2,2),
	Vector3I(1,0,1), Vector3I(2,1,1), Vector3I(1,2,1), Vector3I(0,1,1),
	Vector3I(1,1,0), Vector3I(1,1,2)
};

/// The coordinates are specified in the local coordinate system of the cell in the range [0 - 2].
static const Vector3I BCC_CELL_TEMPLATE[] = {
	Vector3I(0,0,0), Vector3I(2,0,0), Vector3I(2,2,0), Vector3I(0,2,0), 
	Vector3I(0,0,2), Vector3I(2,0,2), Vector3I(2,2,2), Vector3I(0,2,2),
	Vector3I(1,0,1), Vector3I(2,1,1), Vector3I(1,2,1), Vector3I(0,1,1),
	Vector3I(1,1,0), Vector3I(1,1,2)
};


static const Vector3I BCC_LEVEL0_CELL_TEMPLATE[] = {
	Vector3I(0,0,0), Vector3I(2,0,0), Vector3I(2,2,0), Vector3I(0,2,0), 
	Vector3I(0,0,2), Vector3I(2,0,2), Vector3I(2,2,2), Vector3I(0,2,2),
	Vector3I(1,1,1)
};

/***********************************************************************
* Constructor of the ElementOctree class.
************************************************************************/
ElementOctree::ElementOctree()
	: nodePool(sizeof(ElementOctreeNode)), root(NULL)
{
}







/***********************************************************************
* Creates the initial octree.
************************************************************************/
void ElementOctree::CreateInitialMesh()
{	

	Vector3 diagonal = GetSimulation()->grains()[grainIndex]->Boundary().GetBravaisBoundingBox().size();

	FloatType size = diagonal[MaxComponent(diagonal)]; 

	if(size < 1.0)
		throw Exception("Grain is too small. It must contain at least one Bravais cell.");
	//int levels = (int)ceil((log(size) / log(2.0))-0.5);
	
	int levels = (int)ceil(log(size) / log(2.0));
	MsgLogger() << "levels" <<  levels << (int)ceil(log(size) / log(2.0)) << size << endl;
	MAFEM_ASSERT(levels >= 0);


	Box3I octreeBB;
	octreeBB.minc = GetSimulation()->grains()[grainIndex]->Boundary().GetBravaisBoundingBox().minc;
	octreeBB.maxc = octreeBB.minc + Vector3I(1 << levels);
	
	MAFEM_ASSERT(octreeBB.size().X == (1<<levels));
	MAFEM_ASSERT(octreeBB.size().Y == (1<<levels));
	MAFEM_ASSERT(octreeBB.size().Z == (1<<levels));

	// Create the root node.
	root = AllocNewNode();
	root->box = octreeBB;
	root->center = octreeBB.center();
	root->level = levels;
   	root->locationCode = OctreeLocationCode(0,0,0);
	root->CreateBravaisPoints();

  	int refinementLevel = std::max(levels - (int)GetSimulation()->settings().InitialRefinementLevel, 0);
	RefineBoundary(&GetSimulation()->grains()[grainIndex]->Boundary(), refinementLevel);
}

/******************************************************************************
* Locally subdivides the octree until the given boundary is matched.
******************************************************************************/
void ElementOctree::RefineBoundary(GrainBoundary* boundary, unsigned int toLevel)
{
	MAFEM_ASSERT(toLevel >= 0);
	
	// Create the initial subdivision.
	root->RefineBoundary(boundary, toLevel);
	
	// Create the tetrahedron elements in the leaf nodes.
	// Fill in the neighbor fields of every element.
	OnTreeChanged();
}

void ElementOctree::OnTreeChanged()
{
	CHECK_POINTER(root);

	// Create the tetrahedron elements in the new leaf nodes.
	// This has to be done in a top-down way.
	for(int level = root->level; level >= 0; level--)
	{
		root->CreateElements(level);
	}

	// Fill in the neighbor fields of every element.
	root->LinkElements();
}

/***********************************************************************
* Subdivides those cells that contain at least one element that
* is flagged for adaption.
* Returns true if at least one cell has been subdivided.
************************************************************************/
bool ElementOctreeNode::RefineFlaggedElements()
{



	if(level == 0) return false;	// This cell can't be refined any further.

	if(IsLeaf()) {
		// Determine whether this cell needs to be subdivided.
		bool needsRefinement = false;
		for(Element* el = elements; el != NULL; el = el->GetNextElementInCell()) {
			if(el->GetFlag(Element::ADAPTION_FLAG)) {
				needsRefinement = true;
				break;
			}
		}



		if(!needsRefinement)
			return false;

		Subdivide();
		return true;
	}
	else {
		bool result = false;
		for(int i=0; i<8; i++) {
			if(subnodes[i] == NULL) continue;
			if(subnodes[i]->RefineFlaggedElements()) 
				result = true;
		}
		return result;
	}
}
/***********************************************************************
* Subdivides those cells that contain at least one element that
* is flagged for adaption.
* Returns true if at least one cell has been subdivided.
************************************************************************/
bool ElementOctree::RefineFlaggedElements()
{
	if(root->RefineFlaggedElements()) {

		// Some cells have been refined.
		// So we have to remesh them.
		OnTreeChanged();
		return true;
	}
	else return false;
}

/******************************************************************************
* Searches this cell for an element that has the three given vertices.
******************************************************************************/
Element* ElementOctreeNode::FindElementFace(Element* faceElement, RepAtom* faceVertices[3], int& side)
{
	MAFEM_ASSERT(IsLeaf());
	Element* el = elements;
	while(el) {
		if(el != faceElement) {
			side = el->findFace(faceVertices);
			if(side != -1) break;
		}
		el = el->GetNextElementInCell();
	}
	return el;
}

/******************************************************************************
* Updates the neighbor fields for all elements below this node.
******************************************************************************/
void ElementOctreeNode::LinkElements()
{
	if(IsLeaf()) {

		if(GetFlag(ELEMENT_LINKS_VALID))
			return;		// Links are up to date.

		// Retrieve the adjacent octree cell for each of the six cube sides.
		ElementOctreeNode* neighborCells[7];
		neighborCells[0] = this;
		neighborCells[1] = tree->GetLeafNode(locationCode + Vector3I(1<<level, 0, 0), level);
		neighborCells[2] = tree->GetLeafNode(locationCode - Vector3I(1<<level, 0, 0), level);
		neighborCells[3] = tree->GetLeafNode(locationCode + Vector3I(0, 1<<level, 0), level);
		neighborCells[4] = tree->GetLeafNode(locationCode - Vector3I(0, 1<<level, 0), level);
		neighborCells[5] = tree->GetLeafNode(locationCode + Vector3I(0, 0, 1<<level), level);
		neighborCells[6] = tree->GetLeafNode(locationCode - Vector3I(0, 0, 1<<level), level);

#ifdef _DEBUG
		for(int i=1; i<=6; i++) {
			MAFEM_ASSERT(neighborCells[i] == NULL || (neighborCells[i]->level >= level && neighborCells[i]->level <= level+1));
			MAFEM_ASSERT(neighborCells[i] != this || IsRoot());
		}
#endif

		// Iterator over the cell elements.
		Element* el = elements;
		while(el != NULL) {
			CHECK_POINTER(el);

			// Iterate over the element faces.
			for(size_t side = 0; side < 4; side++) {

				// Lookup the cell that contains the neighbor element.
				MAFEM_ASSERT(el->GetFaceDirections()[side] >= 0 && el->GetFaceDirections()[side] <= 6);
				ElementOctreeNode* nc = neighborCells[(size_t)el->GetFaceDirections()[side]];
				if(nc == NULL || IsRoot()) {
					// Element face is a boundary face.
					el->setNeighbor(side, NULL);
				}
				else {
					// Get the three face vertices.
					RepAtom* faceVertices[3];
					el->faceVertices(side, &faceVertices[0], &faceVertices[1], &faceVertices[2]);
					
					// Search the neighbor cell for the neighbor element.
					Element* neighborElement;
					int otherSide;
					if(nc->IsLeaf()) {
						neighborElement = nc->FindElementFace(el, faceVertices, otherSide);
					}
					else {
						MAFEM_ASSERT(nc->level == this->level);
						neighborElement = NULL;
						for(size_t i=0; i<8; i++) {
							if(nc->subnodes[i] != NULL && nc->subnodes[i]->IsLeaf()) {
								neighborElement = nc->subnodes[i]->FindElementFace(el, faceVertices, otherSide);
								if(neighborElement) break;
							}
						}
					}
					// Link the two elements.
					el->setNeighbor(side, neighborElement);
					el->SetBoundaryFace(side, NULL);
					if(neighborElement != NULL) {
						neighborElement->setNeighbor(otherSide, el);
						neighborElement->SetBoundaryFace(otherSide, NULL);
					}
				}

				if(el->neighbor(side) == NULL) {
					// This is an element side at the grain boundary.
					// The element side will be associated with the face of
					// the grain boundary shape that is closest to the element side.

					FloatType minDistance = FLOATTYPE_MAX;
					BoundaryFace* bestMatch = NULL;

					// Get the three vertices.
					RepAtom* v[3];
					el->faceVertices(side, &v[0], &v[1], &v[2]);
					Point3 faceCenter = (v[0]->pos() + v[1]->pos() + v[2]->pos()) / 3.0;

					// Iterate over the faces of the boundary shape.

                   for(int n = 0; n < tree->GetSimulation()->grains()[tree->GrainIndex()]->Boundary().Faces().size(); n++){

                         BoundaryFace* face = tree->GetSimulation()->grains()[tree->GrainIndex()]->Boundary().Faces().at(n);
                        
						// Compute the plane equation of the boundary face.
						Plane3 plane(face->Vertices()[0]->GetWorldPos(), face->GetWorldNormal());

						// Compute distance measure.
						FloatType totalDistance = 0.0;
						for(size_t i=0; i<3; i++)
							totalDistance += plane.pointDistance(v[i]->pos());

						totalDistance = std::abs(totalDistance);
						if(totalDistance > minDistance) continue;

						// At least the center has to be inside the face polygon.
						// Project point onto plane and check if it is in the face polygon.
						if(!face->PointOnFace(faceCenter - plane.pointDistance(faceCenter) * plane.normal))
							continue;

						if(totalDistance < minDistance) {
							minDistance = totalDistance;
							bestMatch = face;
						}
					}
					el->SetBoundaryFace(side, bestMatch);
				}
			}			

			el = el->GetNextElementInCell();
		}

		// The neighbor information is now up to date for this cell.
		SetFlag(ELEMENT_LINKS_VALID);
	}
	else {
		for(size_t i=0; i<8; i++)
			if(subnodes[i] != NULL) subnodes[i]->LinkElements();
	}
	
}



/******************************************************************************
* Allocates a new octree node.
******************************************************************************/
ElementOctreeNode* ElementOctree::AllocNewNode()
{
	ElementOctreeNode* node = new ElementOctreeNode();
	if(node == NULL)
		throw OutOfMemoryException();
	CHECK_POINTER(node);
	node->parent = NULL;
	node->tree = this;
	node->CopyFlags(ElementOctreeNode::IS_LEAF);
	node->elements = NULL;
	node->meshnodes = NULL;
	memset(node->subnodes, 0, sizeof(node->subnodes));
	memset(node->bravaisPoints, 0, sizeof(node->bravaisPoints));
	nodePool.push_back(node);
	return node;
}
/******************************************************************************
* Inserts a new repatom into the mesh.
******************************************************************************/
RepAtom* ElementOctree::InsertMeshNode(const Point3& pos) 
{
	// Back transform world point to Bravais space.
	Point3 u = GetSimulation()->grains()[grainIndex]->lattice().inverseBravaisVectors() * pos;
	
	// Round to integer position.
	Point3I r((int)floor(u.X+FLOATTYPE_EPSILON), (int)floor(u.Y+FLOATTYPE_EPSILON), (int)floor(u.Z+FLOATTYPE_EPSILON));
	if(!root->box.contains(r)) return NULL;

	// Compute location code.
	OctreeLocationCode locationCode;
	locationCode.X = (r.X - root->box.minc.X);
	locationCode.Y = (r.Y - root->box.minc.Y);
	locationCode.Z = (r.Z - root->box.minc.Z);

	

	return InsertMeshNode(pos, locationCode);
}
/******************************************************************************
* Inserts a new repatom into the mesh.
******************************************************************************/
RepAtom* ElementOctree::InsertMeshNode(const Point3& worldPos, OctreeLocationCode locationCode)
{

	
	
	LatticeSite site(&GetSimulation()->grains()[grainIndex]->lattice(), worldPos);
	MAFEM_ASSERT(site.pos().equals(worldPos, FLOATTYPE_EPSILON));
	//MsgLogger() << worldPos  << locationCode << endl;
	int size = (1<<root->level);
	if(locationCode.X >= size) locationCode.X--;
	if(locationCode.Y >= size) locationCode.Y--;
	if(locationCode.Z >= size) locationCode.Z--;
	MAFEM_ASSERT(locationCode.X >= 0 && locationCode.X < size);
	MAFEM_ASSERT(locationCode.Y >= 0 && locationCode.Y < size);
	MAFEM_ASSERT(locationCode.Z >= 0 && locationCode.Z < size);
	
	// Try to find an existing repatom at that world position.
	
	ElementOctreeNode* leaf = GetSmallestNode(locationCode);
	
	
	MAFEM_ASSERT(leaf != NULL);
	for(RepAtom* mnode = leaf->meshnodes; mnode != NULL; mnode = mnode->GetNextRepAtomInCell()) {
		MAFEM_ASSERT(mnode->site().lattice() == site.lattice());
		
		if(mnode->site().indices() == site.indices()){
		    return mnode;
		}
	}
	
	// Make sure the location is inside the grain.
	if(!GetSimulation()->grains()[grainIndex]->Boundary().LocatePoint(site.pos(), NULL))
	return NULL;

	
	RepAtom* oldAtom = GetSimulation()->mesh().nodes().findAt(site);
	if(oldAtom) {
	  
	       switch(site.lattice()->latticeType()){
		  case BCC: 
			  oldAtom->SetOctreeLocationCode(locationCode);
			  oldAtom->SetNextRepAtomInCell(leaf->meshnodes);
			  leaf->meshnodes = oldAtom;
			  return oldAtom; 
		}		
		GetSimulation()->mesh().nodes().remove(GetSimulation()->mesh().nodes().indexOf(oldAtom));
		
	}
	
	// Have to create a new mesh node.
	RepAtom* mnode;
		
	mnode = GetSimulation()->mesh().createNode(site);
	// Insert repatom into cell.
	mnode->SetOctreeLocationCode(locationCode);
	mnode->SetNextRepAtomInCell(leaf->meshnodes);
	
	leaf->meshnodes = mnode;
	return mnode;
	
}

/// Applies periodic boundary conditions.
bool ElementOctree::MakeCodeInside(OctreeLocationCode& code) const
{
	int size = (1<<root->level);
	for(size_t i=0; i<NDOF; i++) {
		if(GetSimulation()->settings().periodicBC[i]) {
			code[i] %= size;
			if(code[i] < 0)
				code[i] += size;
			MAFEM_ASSERT(code[i] >= 0 && code[i] < size);
		}
		else {
			if(code[i] < 0 || code[i] >= size) 
				return false;
		}
	}
	return true;
}
/******************************************************************************
* Returns a node in the tree.
******************************************************************************/
ElementOctreeNode* ElementOctree::GetNodeAtLevel(OctreeLocationCode locationCode, unsigned int level, bool createOnDemand)
{
	CHECK_POINTER(root);
	MAFEM_ASSERT(level <= root->level);

	// Make sure the location is inside the octree region.
	if(!MakeCodeInside(locationCode))
		return NULL;
	
	ElementOctreeNode* cell = root;
	unsigned int nextLevel = cell->level - 1;
	unsigned int n = cell->level - level;
	while(n--) {
		MAFEM_ASSERT(nextLevel < root->level);
		if(createOnDemand)
			cell->Subdivide();
		else if(cell->IsLeaf())
			return NULL;
		unsigned int childBranchBit = (1<<nextLevel);
		unsigned int childIndex = ((locationCode.X & childBranchBit) >> nextLevel)
			| (((locationCode.Y & childBranchBit) << 1) >> nextLevel)
			| (((locationCode.Z & childBranchBit) << 2) >> nextLevel);
		cell = cell->subnodes[childIndex];
		if(cell == NULL) return NULL;
		MAFEM_ASSERT((cell->locationCode.X & childBranchBit) == (locationCode.X & childBranchBit));
		MAFEM_ASSERT((cell->locationCode.Y & childBranchBit) == (locationCode.Y & childBranchBit));
		MAFEM_ASSERT((cell->locationCode.Z & childBranchBit) == (locationCode.Z & childBranchBit));
		nextLevel--;
	}
	return cell;
}
/******************************************************************************
* Returns the leaf node that is closest to the given location code.
******************************************************************************/
ElementOctreeNode* ElementOctree::GetLeafNode(OctreeLocationCode locationCode, unsigned int stopAtLevel)
{
	CHECK_POINTER(root);

	// Make sure the location is inside the octree region.
	if(!MakeCodeInside(locationCode))
		return NULL;

	ElementOctreeNode* cell = root;
	unsigned int nextLevel = cell->level - 1;
	while(cell != NULL && !cell->IsLeaf() && nextLevel >= stopAtLevel) {
		unsigned int childBranchBit = (1<<nextLevel);
		unsigned int childIndex = ((locationCode.X & childBranchBit) >> nextLevel)
			| (((locationCode.Y & childBranchBit) << 1) >> nextLevel)
			| (((locationCode.Z & childBranchBit) << 2) >> nextLevel);
		cell = cell->subnodes[childIndex];		
		MAFEM_ASSERT(cell == NULL || (cell->locationCode.X & childBranchBit) == (locationCode.X & childBranchBit));
		MAFEM_ASSERT(cell == NULL || (cell->locationCode.Y & childBranchBit) == (locationCode.Y & childBranchBit));
		MAFEM_ASSERT(cell == NULL || (cell->locationCode.Z & childBranchBit) == (locationCode.Z & childBranchBit));

		nextLevel--;
	}
	return cell;
}

/******************************************************************************
* Returns the smallest node in the hierarchy that contains the given location code.
******************************************************************************/
ElementOctreeNode* ElementOctree::GetSmallestNode(const OctreeLocationCode& locationCode)
{
	CHECK_POINTER(root);

	// Make sure the location is inside the octree region.
	MAFEM_ASSERT(locationCode.X >= 0 && locationCode.X < (1<<root->level));
	MAFEM_ASSERT(locationCode.Y >= 0 && locationCode.Y < (1<<root->level));
	MAFEM_ASSERT(locationCode.Z >= 0 && locationCode.Z < (1<<root->level));

	ElementOctreeNode* cell = root;
	unsigned int nextLevel = cell->level - 1;
	while(cell != NULL) {
		if(cell->IsLeaf()) return cell;
		unsigned int childBranchBit = (1<<nextLevel);
		unsigned int childIndex = ((locationCode.X & childBranchBit) >> nextLevel)
			| (((locationCode.Y & childBranchBit) << 1) >> nextLevel)
			| (((locationCode.Z & childBranchBit) << 2) >> nextLevel);
		if(cell->subnodes[childIndex] == NULL) return cell;
		cell = cell->subnodes[childIndex];		
		nextLevel--;
	}
	return cell;
}
/******************************************************************************
* Subdivides this cell if it intersects the boundary.
******************************************************************************/
void ElementOctreeNode::RefineBoundary(GrainBoundary* boundary, unsigned int toLevel)
{

	if(level == 0) return;	// This cell can't be refined any further.
	
	// Determine whether this cell needs to be subdivided since it intersects with the grain boundary.

	if(boundary->IntersectsBravaisBox(box) == false)
	{
		int numNodes = 0;
		switch(GetLatticeType())
		{
		case SC: numNodes = sizeof(SC_CELL_TEMPLATE) / sizeof(SC_CELL_TEMPLATE[0]); break; //std::cout << "numNodes" << numNodes << "\n";
		case FCC: numNodes = sizeof(FCC_CELL_TEMPLATE) / sizeof(FCC_CELL_TEMPLATE[0]); break; //std::cout << "numNodes" << numNodes << "\n";
		case BCC: numNodes = sizeof(BCC_CELL_TEMPLATE) / sizeof(BCC_CELL_TEMPLATE[0]) ; break;
		}
		
		// Check if cell is completely inside or outside the boundary.
		bool isOutside = true;
		bool isInside = true;
		for(size_t j=0; j<numNodes; j++)
		{
			if(bravaisPoints[j] == NULL || boundary->LocatePoint(bravaisPoints[j]->pos(), NULL) == false)
				isInside = false;
			else
			{
				BoundaryFace* onFace;
				if(boundary->LocatePoint(bravaisPoints[j]->pos(), &onFace) && onFace == NULL)
					isOutside = false;
			}
		}
		
		if(isOutside && !isInside)
			return;	// no more subdivision.

		if(isInside && toLevel >= level)
			return;
	}

	Subdivide();

	// Keep subdivding child nodes.
		for(size_t i=0; i<8; i++)
		{
			 if(subnodes[i] != NULL) subnodes[i]->RefineBoundary(boundary, toLevel);
		}
}

/******************************************************************************
* Creates the repatoms that make up the Bravais cell.
******************************************************************************/
void ElementOctreeNode::CreateBravaisPoints()
{
	CHECK_POINTER(tree);
	
	
	// Compute the world coordinates of the lower corner of the cell.
	Point3 worldPos = tree->GetSimulation()->grains()[tree->GrainIndex()]->lattice().bravaisVectors() * (Point3)box.minc;

	
	FloatType scaling = (FloatType)(1<<level);
	AffineTransformation bv = tree->GetSimulation()->grains()[tree->GrainIndex()]->lattice().bravaisVectors() * scaling;
	AffineTransformation lv = tree->GetSimulation()->grains()[tree->GrainIndex()]->lattice().latticeVectors() * scaling;

	
	OctreeLocationCode nextCell = locationCode + OctreeLocationCode(1<<level);
	OctreeLocationCode middleCell = locationCode + OctreeLocationCode((1<<level)>>1);
	
	switch(GetLatticeType()) {
		case SC:
			
			// Create the element nodes for a SC cell.
			bravaisPoints[0] = tree->InsertMeshNode(worldPos, locationCode);
			bravaisPoints[1] = tree->InsertMeshNode(worldPos+ bv.getColumn(0), OctreeLocationCode(nextCell.X, locationCode.Y, locationCode.Z));
			bravaisPoints[2] = tree->InsertMeshNode(worldPos + bv.getColumn(0) + bv.getColumn(1), OctreeLocationCode(nextCell.X, nextCell.Y, locationCode.Z));
			bravaisPoints[3] = tree->InsertMeshNode(worldPos + bv.getColumn(1), OctreeLocationCode(locationCode.X, nextCell.Y, locationCode.Z));
			bravaisPoints[4] = tree->InsertMeshNode(worldPos + bv.getColumn(2), OctreeLocationCode(locationCode.X, locationCode.Y, nextCell.Z));
			bravaisPoints[5] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + bv.getColumn(0), OctreeLocationCode(nextCell.X, locationCode.Y, nextCell.Z));
			bravaisPoints[6] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + bv.getColumn(0) + bv.getColumn(1), OctreeLocationCode(nextCell.X, nextCell.Y, nextCell.Z));
			bravaisPoints[7] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + bv.getColumn(1), OctreeLocationCode(locationCode.X, nextCell.Y, nextCell.Z));
			bravaisPoints[8] = NULL;
			bravaisPoints[9] = NULL;
			bravaisPoints[10] = NULL;
			bravaisPoints[11] = NULL;
			bravaisPoints[12] = NULL;
			bravaisPoints[13] = NULL;
			bravaisPoints[14] = NULL;
			break;
		case FCC:
			
			
			// Create the element nodes for a FCC cell.
			bravaisPoints[0] = tree->InsertMeshNode(worldPos, locationCode);
			bravaisPoints[1] = tree->InsertMeshNode(worldPos + bv.getColumn(0), OctreeLocationCode(nextCell.X, locationCode.Y, locationCode.Z));
			bravaisPoints[2] = tree->InsertMeshNode(worldPos + bv.getColumn(0) + bv.getColumn(1), OctreeLocationCode(nextCell.X, nextCell.Y, locationCode.Z));
			bravaisPoints[3] = tree->InsertMeshNode(worldPos + bv.getColumn(1), OctreeLocationCode(locationCode.X, nextCell.Y, locationCode.Z));
			bravaisPoints[4] = tree->InsertMeshNode(worldPos + bv.getColumn(2), OctreeLocationCode(locationCode.X, locationCode.Y, nextCell.Z));
			bravaisPoints[5] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + bv.getColumn(0), OctreeLocationCode(nextCell.X, locationCode.Y, nextCell.Z));
			bravaisPoints[6] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + bv.getColumn(0) + bv.getColumn(1), OctreeLocationCode(nextCell.X, nextCell.Y, nextCell.Z));
			bravaisPoints[7] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + bv.getColumn(1), OctreeLocationCode(locationCode.X, nextCell.Y, nextCell.Z));
			bravaisPoints[8] = tree->InsertMeshNode(worldPos + lv.getColumn(2), OctreeLocationCode(middleCell.X, locationCode.Y, middleCell.Z));
			bravaisPoints[9] = tree->InsertMeshNode(worldPos + bv.getColumn(0) + lv.getColumn(1), OctreeLocationCode(nextCell.X, middleCell.Y, middleCell.Z));
			bravaisPoints[10] = tree->InsertMeshNode(worldPos + bv.getColumn(1) + lv.getColumn(2), OctreeLocationCode(middleCell.X, nextCell.Y, middleCell.Z));
			bravaisPoints[11] = tree->InsertMeshNode(worldPos + lv.getColumn(1), OctreeLocationCode(locationCode.X, middleCell.Y, middleCell.Z));
			bravaisPoints[12] = tree->InsertMeshNode(worldPos + lv.getColumn(0), OctreeLocationCode(middleCell.X, middleCell.Y, locationCode.Z));
			bravaisPoints[13] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + lv.getColumn(0), OctreeLocationCode(middleCell.X, middleCell.Y, nextCell.Z));
			bravaisPoints[14] = NULL;
			break;
			
		case BCC:
		  
		  
			// Create the element nodes for a BCC cell.
			bravaisPoints[0] = tree->InsertMeshNode(worldPos, locationCode);
			bravaisPoints[1] = tree->InsertMeshNode(worldPos + bv.getColumn(0), OctreeLocationCode(nextCell.X, locationCode.Y, locationCode.Z));
			bravaisPoints[2] = tree->InsertMeshNode(worldPos + bv.getColumn(0) + bv.getColumn(1), OctreeLocationCode(nextCell.X, nextCell.Y, locationCode.Z));
			bravaisPoints[3] = tree->InsertMeshNode(worldPos + bv.getColumn(1), OctreeLocationCode(locationCode.X, nextCell.Y, locationCode.Z));
			bravaisPoints[4] = tree->InsertMeshNode(worldPos + bv.getColumn(2), OctreeLocationCode(locationCode.X, locationCode.Y, nextCell.Z));
			bravaisPoints[5] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + bv.getColumn(0), OctreeLocationCode(nextCell.X, locationCode.Y, nextCell.Z));
			bravaisPoints[6] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + bv.getColumn(0) + bv.getColumn(1), OctreeLocationCode(nextCell.X, nextCell.Y, nextCell.Z));
			bravaisPoints[7] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + bv.getColumn(1), OctreeLocationCode(locationCode.X, nextCell.Y, nextCell.Z));
			bravaisPoints[8] = tree->InsertMeshNode(worldPos + lv.getColumn(2), OctreeLocationCode(middleCell.X, locationCode.Y, middleCell.Z));
			bravaisPoints[9] = tree->InsertMeshNode(worldPos + bv.getColumn(0) + lv.getColumn(1), OctreeLocationCode(nextCell.X, middleCell.Y, middleCell.Z));
			bravaisPoints[10] = tree->InsertMeshNode(worldPos + bv.getColumn(1) + lv.getColumn(2), OctreeLocationCode(middleCell.X, nextCell.Y, middleCell.Z));
			bravaisPoints[11] = tree->InsertMeshNode(worldPos + lv.getColumn(1), OctreeLocationCode(locationCode.X, middleCell.Y, middleCell.Z));
			bravaisPoints[12] = tree->InsertMeshNode(worldPos + lv.getColumn(0), OctreeLocationCode(middleCell.X, middleCell.Y, locationCode.Z));
			bravaisPoints[13] = tree->InsertMeshNode(worldPos + bv.getColumn(2) + lv.getColumn(0), OctreeLocationCode(middleCell.X, middleCell.Y, nextCell.Z));
			bravaisPoints[14] = tree->InsertMeshNode(worldPos + (lv.getColumn(2)), OctreeLocationCode(nextCell.X,nextCell.Y,nextCell.Z));
			break;
			
			

		default:
			throw Exception("Unsupported Bravais cell type.");
	}
}

/******************************************************************************
* Returns the Bravais cell type.
******************************************************************************/
LatticeType ElementOctreeNode::GetLatticeType(){
	return tree->GetSimulation()->grains()[tree->GrainIndex()]->lattice().latticeType();
}

/// Returns the element that constains the given lattice site.
Element* ElementOctree::FindElement(const LatticeSite& site)
{
	
/**///	MAFEM_ASSERT(GetGrain()->lattice() == site.lattice());

	// Back transform world point to Bravais space.
	Point3 u = site.lattice()->inverseBravaisVectors() * site.pos();

	// Quick out-of-bounds test.
	if(u.X > root->box.maxc.X + FLOATTYPE_EPSILON || u.X < root->box.minc.X - FLOATTYPE_EPSILON) return NULL;
	if(u.Y > root->box.maxc.Y + FLOATTYPE_EPSILON || u.Y < root->box.minc.Y - FLOATTYPE_EPSILON) return NULL;
	if(u.Z > root->box.maxc.Z + FLOATTYPE_EPSILON || u.Z < root->box.minc.Z - FLOATTYPE_EPSILON) return NULL;

	// Round to integer position.
	Point3I r((int)floor(u.X+FLOATTYPE_EPSILON), (int)floor(u.Y+FLOATTYPE_EPSILON), (int)floor(u.Z+FLOATTYPE_EPSILON));
	MAFEM_ASSERT(root->box.contains(r));
	//MsgLogger() << "r" << r << endl;
	// Compute location code.
	OctreeLocationCode locationCode;
	locationCode.X = (r.X - root->box.minc.X);
	locationCode.Y = (r.Y - root->box.minc.Y);
	locationCode.Z = (r.Z - root->box.minc.Z);

	// Clip location code.
	unsigned int maxPos = (1<<root->level);
	if(locationCode.X == maxPos) locationCode.X--;
	if(locationCode.Y == maxPos) locationCode.Y--;
	if(locationCode.Z == maxPos) locationCode.Z--;
	
	ElementOctreeNode* leaf = GetSmallestNode(locationCode);
	MAFEM_ASSERT(leaf != NULL);
	for(Element* element = leaf->elements; element != NULL; element = element->GetNextElementInCell()) {
/**///	MAFEM_ASSERT(grain->lattice() == site.lattice());
		if(element->containsSite(site.indices()))
			return element;
	}

	return NULL;
}
/******************************************************************************
* Subdivides this node if it is a leaf node.
******************************************************************************/
void ElementOctreeNode::Subdivide()
{

	CHECK_POINTER(this);
	MAFEM_ASSERT(level > 0);


	// Create child nodes if this is a leaf node.
	if(IsLeaf()) {
		CreateSubNode(0, Box3I(Point3I(box.minc.X, box.minc.Y, box.minc.Z), Point3I(center.X, center.Y, center.Z)));		
		CreateSubNode(1, Box3I(Point3I(center.X, box.minc.Y, box.minc.Z), Point3I(box.maxc.X, center.Y, center.Z)));		
		CreateSubNode(2, Box3I(Point3I(box.minc.X, center.Y, box.minc.Z), Point3I(center.X, box.maxc.Y, center.Z)));		
		CreateSubNode(3, Box3I(Point3I(center.X, center.Y, box.minc.Z), Point3I(box.maxc.X, box.maxc.Y, center.Z)));		
		CreateSubNode(4, Box3I(Point3I(box.minc.X, box.minc.Y, center.Z), Point3I(center.X, center.Y, box.maxc.Z)));		
		CreateSubNode(5, Box3I(Point3I(center.X, box.minc.Y, center.Z), Point3I(box.maxc.X, center.Y, box.maxc.Z)));		
		CreateSubNode(6, Box3I(Point3I(box.minc.X, center.Y, center.Z), Point3I(center.X, box.maxc.Y, box.maxc.Z)));		
		CreateSubNode(7, Box3I(Point3I(center.X, center.Y, center.Z), Point3I(box.maxc.X, box.maxc.Y, box.maxc.Z)));		

		// Compute the world coordinates of the lower corner of the cell.
	
       	RepAtom* subatoms[15 * 8];
		int numSubatoms = 0;

		// Copy old nodes.
		for(size_t i = 0; i<15; i++) {
			if(bravaisPoints[i] != NULL) 
				subatoms[numSubatoms++] = bravaisPoints[i];
		}

		// Transfer old repatoms to child cells.
		while(meshnodes != NULL) {
			RepAtom* next = meshnodes->GetNextRepAtomInCell();
			CHECK_POINTER(meshnodes);

			unsigned int childLevel = level - 1;
			unsigned int childBranchBit = (1<<childLevel);
			unsigned int childIndex = ((meshnodes->GetOctreeLocationCode().X & childBranchBit) >> childLevel)
				| (((meshnodes->GetOctreeLocationCode().Y & childBranchBit) << 1) >> childLevel)
				| (((meshnodes->GetOctreeLocationCode().Z & childBranchBit) << 2) >> childLevel);
			MAFEM_ASSERT(subnodes[childIndex] != NULL);
			meshnodes->SetNextRepAtomInCell(subnodes[childIndex]->meshnodes);
			subnodes[childIndex]->meshnodes = meshnodes;
			
			meshnodes = next;
		}
		MAFEM_ASSERT(meshnodes == NULL);

		SetFlag(IS_LEAF, false);

		// Create any nodes required by the subcells.
		for(int i=0; i<8; i++) {
			ElementOctreeNode* subnode = subnodes[i];
			if(subnode == NULL) continue;
			subnode->SetFlag(IS_INTERIOR_CELL);
			subnode->SetFlag(IS_EXTERIOR_CELL);

			// Select the template for the child nodes.
			const Vector3I* nodeTemplate;
			int nodeTemplateSize;

			switch(subnode->GetLatticeType()) {
				case SC:
					nodeTemplate = SC_CELL_TEMPLATE;
					nodeTemplateSize = sizeof(SC_CELL_TEMPLATE) / sizeof(SC_CELL_TEMPLATE[0]);
					break;
				case FCC:
					nodeTemplate = FCC_CELL_TEMPLATE;
					nodeTemplateSize = sizeof(FCC_CELL_TEMPLATE) / sizeof(FCC_CELL_TEMPLATE[0]);
					break;
				case BCC:
					if(level != 1){
					  nodeTemplate = BCC_CELL_TEMPLATE;
					  nodeTemplateSize = sizeof(BCC_CELL_TEMPLATE) / sizeof(BCC_CELL_TEMPLATE[0]);
					  break;
					}else{
					  nodeTemplate = BCC_LEVEL0_CELL_TEMPLATE;
					  nodeTemplateSize = sizeof(BCC_LEVEL0_CELL_TEMPLATE) / sizeof(BCC_LEVEL0_CELL_TEMPLATE[0]);
					  break;
					}
				default:
					throw Exception("Unsupported Bravais cell type.");
			}

			// Build the coordinate system for the new nodes.
			Point3 origin = tree->GetSimulation()->grains()[tree->GrainIndex()]->lattice().bravaisVectors() * (Point3)subnode->box.minc;
			AffineTransformation bv = tree->GetSimulation()->grains()[tree->GrainIndex()]->lattice().bravaisVectors() * ((FloatType)(1<<subnode->level) / 2.0);
			
			for(size_t j=0; j<nodeTemplateSize; j++) {
				Point3 pos = origin + bv * (Vector3)nodeTemplate[j];
				OctreeLocationCode code = subnode->locationCode + ((nodeTemplate[j] * (1<<subnode->level)) / 2);

				// Look for an existing node.
				RepAtom* node = NULL;
							
				for(size_t k=0; k<numSubatoms; k++) {
					if(subatoms[k]->GetOctreeLocationCode() == code && subatoms[k]->pos().equals(pos, FLOATTYPE_EPSILON)) {
						node = subatoms[k];
						//MsgLogger() << node->pos() << endl;
						break;
					}
				}
				if(node == NULL ) {
				  
					// Create a new node.
					node = tree->InsertMeshNode(pos, code);
					if(node != NULL) {
						subatoms[numSubatoms++] = node;
						CHECK_POINTER(node->site().lattice());
		
						// Determine deformed position by interpolation.
						if(elements != NULL)
							node->setDeformedPos(ComputeDeformedPosition(node->site()));

					}
				}

				// Assign node to the subcell.
				subnode->bravaisPoints[j] = node;

				
				if(node == NULL)
					subnode->ClearFlag(IS_INTERIOR_CELL);
				else
					subnode->ClearFlag(IS_EXTERIOR_CELL);
			}
		}

		// Remove old meshing of this cell.
		PurgeElements();
		
		
		// Subdivide neighbor cells to ensure there is a smooth level transition.
		GetNeighbor(Vector3I(-1,  0,  0), true);
		GetNeighbor(Vector3I( 1,  0,  0), true);
		GetNeighbor(Vector3I( 0, -1,  0), true);
		GetNeighbor(Vector3I( 0,  1,  0), true);
		GetNeighbor(Vector3I( 0,  0, -1), true);
		GetNeighbor(Vector3I( 0,  0,  1), true);
		GetNeighbor(Vector3I(-1, -1,  0), true);
		GetNeighbor(Vector3I( 1, -1,  0), true);
		GetNeighbor(Vector3I( 1,  1,  0), true);
		GetNeighbor(Vector3I(-1,  1,  0), true);
		GetNeighbor(Vector3I(-1,  0, -1), true);
		GetNeighbor(Vector3I( 1,  0, -1), true);
		GetNeighbor(Vector3I( 1,  0,  1), true);
		GetNeighbor(Vector3I(-1,  0,  1), true);
		GetNeighbor(Vector3I( 0, -1, -1), true);
		GetNeighbor(Vector3I( 0,  1, -1), true);
		GetNeighbor(Vector3I( 0,  1,  1), true);
		GetNeighbor(Vector3I( 0, -1,  1), true);
		
				
	}
	else {
		MAFEM_ASSERT(elements == NULL);
		MAFEM_ASSERT(meshnodes == NULL);
	}
}

/******************************************************************************
* Returns a neighbor of this cell that is on the same tree level as this one.
******************************************************************************/
ElementOctreeNode* ElementOctreeNode::GetNeighbor(const Vector3I& direction, bool createOnDemand)
{
	CHECK_POINTER(this);

	// Compute the location code of the neighbor.
	OctreeLocationCode neighborCode;
	int size = (1<<level);
	neighborCode.X = (int)locationCode.X + size*direction.X;
	neighborCode.Y = (int)locationCode.Y + size*direction.Y;
	neighborCode.Z = (int)locationCode.Z + size*direction.Z;

	return tree->GetNodeAtLevel(neighborCode, level, createOnDemand);
}

/******************************************************************************
* Creates the i-th subnode.
******************************************************************************/
void ElementOctreeNode::CreateSubNode(unsigned int index, const Box3I& bb)
{
	MAFEM_ASSERT(subnodes[index] == NULL);
	MAFEM_ASSERT(level != 0);
	MAFEM_ASSERT(index < 8);

	unsigned int sublevel = level - 1;
	ElementOctreeNode* subnode = subnodes[index] = tree->AllocNewNode();
	subnode->parent = this;
	subnode->level = sublevel;
	subnode->box = bb;
	subnode->center = subnode->box.center();

	// Compute the location code of the new cell.
	int code = index;
	
	subnode->locationCode[0] = locationCode[0] | ((code & 1) << sublevel);
	code >>= 1;
	
	subnode->locationCode[1] = locationCode[1] | ((code & 1) << sublevel);
	code >>= 1;
	
	subnode->locationCode[2] = locationCode[2] | ((code & 1) << sublevel);

}

/******************************************************************************
* Creates the elements in the mesh for this cell.
******************************************************************************/
void ElementOctreeNode::CreateElements(unsigned int meshLevel)
{
	
	if(IsLeaf()) {

		
		if(elements != NULL || level != meshLevel)
			return;	// This cell is already meshed.
	

		// Create the tetrahedra. 
		switch(GetLatticeType()) {
		case SC:
#if 1		// This is the 5-element triangulation.
			if((((locationCode.X>>level) + (locationCode.Y>>level) + (locationCode.Z>>level)) & 1) == 0) {
				CreateElement(bravaisPoints[0], bravaisPoints[1], bravaisPoints[2], bravaisPoints[5], 1, 0, 4, 6);
				CreateElement(bravaisPoints[2], bravaisPoints[6], bravaisPoints[7], bravaisPoints[5], 5, 0, 1, 3);
				CreateElement(bravaisPoints[7], bravaisPoints[3], bravaisPoints[2], bravaisPoints[0], 6, 0, 2, 3);
				CreateElement(bravaisPoints[0], bravaisPoints[4], bravaisPoints[5], bravaisPoints[7], 5, 0, 2, 4);
				CreateElement(bravaisPoints[0], bravaisPoints[2], bravaisPoints[7], bravaisPoints[5], 0, 0, 0, 0);
			}
			else {
				CreateElement(bravaisPoints[0], bravaisPoints[1], bravaisPoints[3], bravaisPoints[4], 0, 2, 4, 6);
				CreateElement(bravaisPoints[1], bravaisPoints[2], bravaisPoints[3], bravaisPoints[6], 3, 0, 1, 6);
				CreateElement(bravaisPoints[1], bravaisPoints[6], bravaisPoints[4], bravaisPoints[5], 5, 4, 1, 0);
				CreateElement(bravaisPoints[3], bravaisPoints[6], bravaisPoints[7], bravaisPoints[4], 5, 2, 0, 3);
				CreateElement(bravaisPoints[3], bravaisPoints[1], bravaisPoints[6], bravaisPoints[4], 0, 0, 0, 0);
			}
#else		// This is the 6-element triangulation.
			CreateElement(bravaisPoints[3], bravaisPoints[6], bravaisPoints[7], bravaisPoints[5], 5, 0, 0, 3);
			CreateElement(bravaisPoints[3], bravaisPoints[5], bravaisPoints[7], bravaisPoints[4], 5, 2, 0, 0);
			CreateElement(bravaisPoints[0], bravaisPoints[5], bravaisPoints[3], bravaisPoints[4], 0, 2, 4, 0);
			CreateElement(bravaisPoints[3], bravaisPoints[2], bravaisPoints[6], bravaisPoints[5], 1, 0, 0, 3);
			CreateElement(bravaisPoints[3], bravaisPoints[1], bravaisPoints[2], bravaisPoints[5], 1, 0, 0, 6);
			CreateElement(bravaisPoints[3], bravaisPoints[1], bravaisPoints[5], bravaisPoints[0], 4, 0, 6, 0);
#endif
			break;

		case FCC:
			CreateElement(bravaisPoints[0], bravaisPoints[1], bravaisPoints[12], bravaisPoints[8], 0, 0, 4, 6);
			CreateElement(bravaisPoints[1], bravaisPoints[2], bravaisPoints[12], bravaisPoints[9], 0, 0, 1, 6);
			CreateElement(bravaisPoints[2], bravaisPoints[3], bravaisPoints[12], bravaisPoints[10], 0, 0, 3, 6);
			CreateElement(bravaisPoints[3], bravaisPoints[0], bravaisPoints[12], bravaisPoints[11], 0, 0, 2, 6);
			CreateElement(bravaisPoints[4], bravaisPoints[5], bravaisPoints[8], bravaisPoints[13], 0, 0, 5, 4);
			CreateElement(bravaisPoints[5], bravaisPoints[6], bravaisPoints[9], bravaisPoints[13], 0, 0, 5, 1);
			CreateElement(bravaisPoints[6], bravaisPoints[7], bravaisPoints[10], bravaisPoints[13], 0, 0, 5, 3);
			CreateElement(bravaisPoints[7], bravaisPoints[4], bravaisPoints[11], bravaisPoints[13], 0, 0, 5, 2);
			CreateElement(bravaisPoints[0], bravaisPoints[4], bravaisPoints[8], bravaisPoints[11], 0, 0, 2, 4);
			CreateElement(bravaisPoints[1], bravaisPoints[5], bravaisPoints[9], bravaisPoints[8], 0, 0, 4, 1);
			CreateElement(bravaisPoints[2], bravaisPoints[6], bravaisPoints[10], bravaisPoints[9], 0, 0, 1, 3);
			CreateElement(bravaisPoints[3], bravaisPoints[7], bravaisPoints[11], bravaisPoints[10], 0, 0, 3, 2);
			CreateElement(bravaisPoints[1], bravaisPoints[8], bravaisPoints[9], bravaisPoints[12], 0, 0, 0, 0);
			CreateElement(bravaisPoints[2], bravaisPoints[9], bravaisPoints[10], bravaisPoints[12], 0, 0, 0, 0);
			CreateElement(bravaisPoints[3], bravaisPoints[10], bravaisPoints[11], bravaisPoints[12], 0, 0, 0, 0);
			CreateElement(bravaisPoints[0], bravaisPoints[11], bravaisPoints[8], bravaisPoints[12], 0, 0, 0, 0);
			CreateElement(bravaisPoints[5], bravaisPoints[9], bravaisPoints[8], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[6], bravaisPoints[10], bravaisPoints[9], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[7], bravaisPoints[11], bravaisPoints[10], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[4], bravaisPoints[8], bravaisPoints[11], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[12], bravaisPoints[8], bravaisPoints[9], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[12], bravaisPoints[9], bravaisPoints[10], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[12], bravaisPoints[10], bravaisPoints[11], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[12], bravaisPoints[11], bravaisPoints[8], bravaisPoints[13], 0, 0, 0, 0);
			break;
			
			
		case BCC:
		  
		  if(level == 0){
			CreateElement(bravaisPoints[3], bravaisPoints[0], bravaisPoints[1], bravaisPoints[14], 0, 0, 0, 6); 
			CreateElement(bravaisPoints[1], bravaisPoints[2], bravaisPoints[3], bravaisPoints[14], 0, 0, 0, 6); 
			CreateElement(bravaisPoints[5], bravaisPoints[1], bravaisPoints[2], bravaisPoints[14], 0, 0, 0, 3); 
			CreateElement(bravaisPoints[5], bravaisPoints[2], bravaisPoints[6], bravaisPoints[14], 0, 0, 0, 3); 
			CreateElement(bravaisPoints[7], bravaisPoints[4], bravaisPoints[5], bravaisPoints[14], 0, 0, 0, 5); 
			CreateElement(bravaisPoints[5], bravaisPoints[6], bravaisPoints[7], bravaisPoints[14], 0, 0, 0, 5); 
			CreateElement(bravaisPoints[0], bravaisPoints[3], bravaisPoints[4], bravaisPoints[14], 0, 0, 0, 4); 
			CreateElement(bravaisPoints[3], bravaisPoints[7], bravaisPoints[4], bravaisPoints[14], 0, 0, 0, 4); 
			CreateElement(bravaisPoints[0], bravaisPoints[1], bravaisPoints[5], bravaisPoints[14], 0, 0, 0, 1);
			CreateElement(bravaisPoints[5], bravaisPoints[0], bravaisPoints[4], bravaisPoints[14], 0, 0, 0, 1);
			CreateElement(bravaisPoints[2], bravaisPoints[6], bravaisPoints[3], bravaisPoints[14], 0, 0, 0, 2);
			CreateElement(bravaisPoints[3], bravaisPoints[7], bravaisPoints[6], bravaisPoints[14], 0, 0, 0, 2);
		  
		  
			break;
		  
		  
		  }else{
		  
			CreateElement(bravaisPoints[0], bravaisPoints[1], bravaisPoints[12], bravaisPoints[8], 0, 0, 4, 6);
			CreateElement(bravaisPoints[1], bravaisPoints[2], bravaisPoints[12], bravaisPoints[9], 0, 0, 1, 6);
			CreateElement(bravaisPoints[2], bravaisPoints[3], bravaisPoints[12], bravaisPoints[10], 0, 0, 3, 6);
			CreateElement(bravaisPoints[3], bravaisPoints[0], bravaisPoints[12], bravaisPoints[11], 0, 0, 2, 6);
			CreateElement(bravaisPoints[4], bravaisPoints[5], bravaisPoints[8], bravaisPoints[13], 0, 0, 5, 4);
			CreateElement(bravaisPoints[5], bravaisPoints[6], bravaisPoints[9], bravaisPoints[13], 0, 0, 5, 1);
			CreateElement(bravaisPoints[6], bravaisPoints[7], bravaisPoints[10], bravaisPoints[13], 0, 0, 5, 3);
			CreateElement(bravaisPoints[7], bravaisPoints[4], bravaisPoints[11], bravaisPoints[13], 0, 0, 5, 2);
			CreateElement(bravaisPoints[0], bravaisPoints[4], bravaisPoints[8], bravaisPoints[11], 0, 0, 2, 4);
			CreateElement(bravaisPoints[1], bravaisPoints[5], bravaisPoints[9], bravaisPoints[8], 0, 0, 4, 1);
			CreateElement(bravaisPoints[2], bravaisPoints[6], bravaisPoints[10], bravaisPoints[9], 0, 0, 1, 3);
			CreateElement(bravaisPoints[3], bravaisPoints[7], bravaisPoints[11], bravaisPoints[10], 0, 0, 3, 2);
			CreateElement(bravaisPoints[1], bravaisPoints[8], bravaisPoints[9], bravaisPoints[12], 0, 0, 0, 0);
			CreateElement(bravaisPoints[2], bravaisPoints[9], bravaisPoints[10], bravaisPoints[12], 0, 0, 0, 0);
			CreateElement(bravaisPoints[3], bravaisPoints[10], bravaisPoints[11], bravaisPoints[12], 0, 0, 0, 0);
			CreateElement(bravaisPoints[0], bravaisPoints[11], bravaisPoints[8], bravaisPoints[12], 0, 0, 0, 0);
			CreateElement(bravaisPoints[5], bravaisPoints[9], bravaisPoints[8], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[6], bravaisPoints[10], bravaisPoints[9], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[7], bravaisPoints[11], bravaisPoints[10], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[4], bravaisPoints[8], bravaisPoints[11], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[12], bravaisPoints[8], bravaisPoints[9], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[12], bravaisPoints[9], bravaisPoints[10], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[12], bravaisPoints[10], bravaisPoints[11], bravaisPoints[13], 0, 0, 0, 0);
			CreateElement(bravaisPoints[12], bravaisPoints[11], bravaisPoints[8], bravaisPoints[13], 0, 0, 0, 0);
			break;  
					  
		  }
		  
		      
			
		}

		ClearFlag(MODIFIED_MESH | ELEMENT_LINKS_VALID);

		if(IsRoot())
			return;	

		  const SimulationSettings& settings = tree->GetSimulation()->settings();
		
		// Split the tetrahedra in the neighbors cells if necessary.
		Vector3I dir;
		int size = 1<<level;
		int rootSize = 1<<(tree->GetRoot()->level);
				
		for(dir.X = -1; dir.X <= 1; dir.X++) {
			for(dir.Y = -1; dir.Y <= 1; dir.Y++) {
				for(dir.Z = -1; dir.Z <= 1; dir.Z++) {
					
					// Skip corner neighbor cells.
					if(dir.X != 0 && dir.Y != 0 && dir.Z != 0) continue;
					
					// Compute the location code of the neighbor.
					OctreeLocationCode neighborCode;
					neighborCode.X = (int)locationCode.X + size*dir.X;
					neighborCode.Y = (int)locationCode.Y + size*dir.Y;
					neighborCode.Z = (int)locationCode.Z + size*dir.Z;
					//MsgLogger() << neighborCode.X << neighborCode.Y << neighborCode.Z << endl;
					// Lookup the neighbor cell.
					ElementOctreeNode* neighbor = tree->GetNodeAtLevel(neighborCode, level+1, false);
					if(neighbor == NULL) continue;
					if(neighbor->IsLeaf() == false) continue;
					MAFEM_ASSERT(neighbor != this->parent);

					for(size_t i=0; i<15; i++) {
						if(bravaisPoints[i] != NULL) {
							
							Point3 worldPos = bravaisPoints[i]->pos();
							Point3 nodeLocationCode = bravaisPoints[i]->GetOctreeLocationCode();
							//MsgLogger() << nodeLocationCode;
							for(size_t k=0; k<NDOF; k++) {
								if(neighborCode[k] < 0) {
									MAFEM_ASSERT(settings.periodicBC[k]);
									worldPos += tree->GetSimulation()->grains()[tree->GrainIndex()]->lattice().bravaisVector(k) * (FloatType)rootSize;
									nodeLocationCode[k] += rootSize;
									
								}
								else if(neighborCode[k] >= rootSize) {
									MAFEM_ASSERT(settings.periodicBC[k]);
									worldPos -= tree->GetSimulation()->grains()[tree->GrainIndex()]->lattice().bravaisVector(k) * (FloatType)rootSize;
									nodeLocationCode[k] -= rootSize;
									
								}
							}
							
							if(nodeLocationCode == bravaisPoints[i]->GetOctreeLocationCode()) {
								neighbor->InsertSplitPoint(bravaisPoints[i]);								
							}
							else {
								RepAtom* pbcPoint = tree->InsertMeshNode(worldPos);
								if(pbcPoint != NULL) {
									MAFEM_ASSERT(neighbor->box.contains(pbcPoint->site().indices()));
									neighbor->InsertSplitPoint(pbcPoint);
								}
							}
						}
					}
					
				}
			}
		} 
	}
	else if(level > meshLevel) {
		for(size_t i=0; i<8; i++) {
			if(subnodes[i] != NULL) subnodes[i]->CreateElements(meshLevel);
		}
	}
}
/******************************************************************************
* Deletes all elements contained in this cell.
******************************************************************************/
void ElementOctreeNode::PurgeElements()
{
	Element* el = elements;
	while(el != NULL) {
		CHECK_POINTER(el);
		Element* next = el->GetNextElementInCell();
		tree->GetSimulation()->mesh().elements().remove(tree->GetSimulation()->mesh().elements().indexOf(el));
		el = next;
	}
	elements = NULL;
}

/******************************************************************************
* Transforms the given point from reference to deformed configuration using linear interpolation.
******************************************************************************/
Point3 ElementOctreeNode::ComputeDeformedPosition(const LatticeSite& site) const
{
	for(Element* el = elements; el != NULL; el = el->GetNextElementInCell()) {
		CHECK_POINTER(el);
/**///		MAFEM_ASSERT(&el->lattice() == site.lattice());
		if(el->containsSite(site.indices())) {
			Vector4 baryc = el->computeBarycentric(site.pos());
			Point3 defpos(ORIGIN);
			for(size_t j=0; j<NEN; j++) {
				defpos += baryc[j] * (el->vertex(j)->deformedPos() - ORIGIN);
			}
			return defpos;
		}
	}
	//DEBUG_FAIL;
	return site.pos();
}



/******************************************************************************
* Create a single tetrahedron element inside this octree cell.
******************************************************************************/
Element* ElementOctreeNode::CreateElement(RepAtom* a1, RepAtom* a2, RepAtom* a3, RepAtom* a4,
										  int fd1, int fd2, int fd3, int fd4)
{
	if(a1 == NULL || a2 == NULL || a3 == NULL || a4 == NULL)
		return NULL;

   
	// If this cell is not completely inside or outside of the grain
	// then we have to make sure that at least this element is inside the grain.
	if(!GetFlag(IS_INTERIOR_CELL) && !GetFlag(IS_EXTERIOR_CELL)) {
		// Compute center of element.
		Point3 center = (a1->pos() + a2->pos() + a3->pos() + a4->pos()) * 0.25;
		// Test whether center point is inside the grain boundary.
		if(!tree->GetSimulation()->grains()[tree->GrainIndex()]->Boundary().LocatePoint(center))
			return NULL;
	}
	
	array<RepAtom*, NEN> vertices;
	vertices[0] = a1;
	vertices[1] = a2;
	vertices[2] = a3;
	vertices[3] = a4;
		

	Element element(vertices);
	if(element.isUnnecessaryElement())
		return NULL;
	Element* el = tree->GetSimulation()->mesh().addElement(element);
	el->SetFaceDirections(fd1, fd2, fd3, fd4);
	if(level == 0) el->SetFlag(Element::SMALL_FLAG);
	el->SetNextElementInCell(elements);
	elements = el;
	return el;

}

/******************************************************************************
* Splits any tetrahedra in this cell whos edges are bisected by the given point.
******************************************************************************/
void ElementOctreeNode::InsertSplitPoint(RepAtom* point)
{
  

	CHECK_POINTER(point);
	MAFEM_ASSERT(IsLeaf());

	Element* el = elements;
	while(el != NULL) {
		CHECK_POINTER(el);
/**///		MAFEM_ASSERT(&el->lattice() == point->sittree->GetSimulation()->mesh()e().lattice());

		static int edgeMap[6][2] = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};

		// Test all six edges of the tetrahedron.
		for(int e = 0; e < 6; e++) {

			const Point3I& vi1 = el->vertexLatticeIndices(edgeMap[e][0]);
			const Point3I& vi2 = el->vertexLatticeIndices(edgeMap[e][1]);
			
			// Test whether the point is on the midpoint edge.
			if(vi1 - point->site().indices() == point->site().indices() - vi2) {

				// Check if this is the longest edge of the element.
#if 0
				int edgeLength = DistanceSquared(vi1, vi2);
				int longestEdge = e;
				int longestEdgeLength = edgeLength;
				for(size_t e2 = 0; e2 < 6; e2++) {
					const Point3I& vi1 = el->vertexLatticeIndices(edgeMap[e2][0]);
					const Point3I& vi2 = el->vertexLatticeIndices(edgeMap[e2][1]);
					int edgeLength2 = DistanceSquared(vi1, vi2);
					if(edgeLength2 > longestEdgeLength) {
						Point3I center = (vi1 + vi2) / 2;
						if(vi1 - center == center - vi2) {

							LatticeSite site(el->lattice(), center);
							// Back transform world point to Bravais space.
							Point3 u = tree->GetSimulation()->grains()[tree->GrainIndex()]->lattice().inverseBravaisVectors() * site.pos();
							// Round to integer position.
							Point3I r((int)floor(u.X+FLOATTYPE_EPSILON), (int)floor(u.Y+FLOATTYPE_EPSILON), (int)floor(u.Z+FLOATTYPE_EPSILON));
							// Compute location code.
							OctreeLocationCode code;
							code.X = (r.X - tree->GetRoot()->box.minc.X);
							code.Y = (r.Y - tree->GetRoot()->box.minc.Y);
							code.Z = (r.Z - tree->GetRoot()->box.minc.Z);
							if(code.X == box.minc.X || code.X == box.maxc.X || 
								code.Y == box.minc.Y || code.Y == box.maxc.Y || 
								code.Z == box.minc.Z || code.Z == box.maxc.Z) {
								longestEdgeLength = edgeLength2;
								longestEdge = e2;
							}
						}
					}
				}
				if(longestEdge != e) {
					// The longest edge has to be split first.
					const Point3I& vi1 = el->vertexLatticeIndices(edgeMap[longestEdge][0]);
					const Point3I& vi2 = el->vertexLatticeIndices(edgeMap[longestEdge][1]);
					Point3I center = (vi1 + vi2) / 2;
					if(vi1 - center == center - vi2) {el->GetNextElementInCell()
						LatticeSite site(el->lattice(), center);
						MAFEM_ASSERT(vi1 - site.indices() == site.indices() - vi2);
						// Create a new mesh node.
						RepAtom* node = tree->InsertMeshNode(site.pos());
						// Split the tetrahedra in the neighbor cells if necessary.
						Vector3I dir;
						int size = 1<<level;
						for(dir.X = -1; dir.X <= 1; dir.X++) {
							for(dir.Y = -1; dir.Y <= 1; dir.Y++) {
								for(dir.Z = -1; dir.Z <= 1; dir.Z++) {
									
									// Skip corner neighbor cells.
									if(dir.X != 0 && dir.Y != 0 && dir.Z != 0) continue;
									
									// Compute the location code of the neighbor.
									OctreeLocationCode neighborCode;
									neighborCode.X = (int)locationCode.X + size*dir.X;
									neighborCode.Y = (int)locationCode.Y + size*dir.Y;
									neighborCode.Z = (int)locationCode.Z + size*dir.Z;

									// Lookup the neighbor cell.
									ElementOctreeNode* neighbor = tree->GetLeafNode(neighborCode, 0);
									if(neighbor == NULL) continue;
									if(neighbor->IsLeaRepAtom* pointf() == false) continue;
									MAFEM_ASSERT(neighbor != this->parent);

									neighbor->InsertSplitPoint(node);
								}
							}
						}	
						InsertSplitPoint(point);
						return;
					}
				}
#endif
				
				// Subdivide the element.
				Element* newElement = el->BisectElementEdge(edgeMap[e][0], edgeMap[e][1], point, &(tree->GetSimulation()->mesh()));
				if(newElement==NULL) continue;
				// Insert new element into linked list of cell elements.
				newElement->SetNextElementInCell(el->GetNextElementInCell());					
				el->SetNextElementInCell(newElement);
				
				// Skip new element.
				el = newElement; 
				 
				// This cell has now a modified mesh.
				SetFlag(MODIFIED_MESH, true);
				ClearFlag(ELEMENT_LINKS_VALID);

				break;
			}
		}

		el = el->GetNextElementInCell();
	}
	
	

	
}





};

