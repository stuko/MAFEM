///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_ELEMENT_COLLECTION_H
#define __MAFEM_ELEMENT_COLLECTION_H

#include <MAFEM.h>
#include "Element.h"
#include <atoms/LatticeSite.h>

// Include the CGAL library headers.
#include <CGAL/Cartesian.h>
#include <CGAL/Segment_tree_k.h>
#include <CGAL/Range_segment_tree_traits.h>

namespace MAFEM {

/**
 * \brief Class that stores a list of elements and provides
 *        a fast lookup mechanism for point location queries.
 * 
 * This class is essentially a QVector of Element pointers.
 * Additionally it stores a so called 3d segment tree to allow
 * for fast point location queries in the mesh, i.e. to quickly
 * locate the element that contains a given lattice site.
 * 
 * The 3d segment tree algorithm is provided by the CGAL library. 
 * 
 * \author Alexander Stukowski
 */
class ElementCollection : public QVector<Element*>
{
public:

	/// \brief Default constructor that creates an empty collection.
	ElementCollection() : _lastFoundElement(NULL), _treeValid(false) {}
	
	/// \brief Copy constructor.
	/// \note This copy constructor is only needed for the Q_FOREACH macro. It does
	///       not copy the internal data, only the plain list of elements.
	ElementCollection(const ElementCollection& other) : QVector<Element*>(other), _lastFoundElement(NULL), _treeValid(false) {}

	/// \brief Adds an element to the collection.
	/// \param element The element to be added to the collection.
	void push_back(Element* element) {
		CHECK_POINTER(element);
		QVector<Element*>::push_back(element);
		
		// Rebuild the segment tree on the next call to findAt(). 
		_treeValid = false;
	}

	/// Removes all elements from the collection.
	void clear() {
		QVector<Element*>::clear();

		// We have to reset the cached element.
		_lastFoundElement = NULL;

		// Rebuild the segment tree on the next call to findAt(). 
		_treeValid = false;
	}

	/// \brief Finds the first element that constains the given lattice site.
	/// \param site A lattice site.
	/// \return The containing element or NULL if the lattice site is not inside
	///			an element.
	Element* findAt(const LatticeSite& site) const;
	
	/// \brief Finds all elements that contain the given lattice site.
	/// \param site A lattice site.
	/// \return The list of elements that contain the query point.
	QVector<Element*> findContainingElements(const LatticeSite& site) const;
	
private:

	/// This stores the last element that has been returned by the findAt() method.
	/// It is used in the findAt() method to exploit the spatial coherence of point queries. 
	Element* _lastFoundElement;

	/// Indicates whether the 3d segment tree data stucture is up to date.
	bool _treeValid;	
	
	// Some typedefinitions that configure the 3d segment tree.
	typedef CGAL::Cartesian<FloatType> CGAL_Kernel;
	typedef CGAL::Segment_tree_map_traits_3<CGAL_Kernel, Element*> CGAL_Traits;
	typedef CGAL::Segment_tree_3<CGAL_Traits> CGAL_Segment_tree_3;	
	typedef CGAL_Traits::Interval Interval3D;
	typedef CGAL_Traits::Pure_interval Pure_interval3D;	
	typedef CGAL_Traits::Key Tree_Key;
	
	// This is the CGAL segment tree.
	CGAL_Segment_tree_3 _tree;
	
	// This rebuilds the 3d segment tree.
	void rebuildSegmentTree();

	// This helper function computes the bounding box of an element.    
    static Interval3D& intervalFromElement(Element* element) {
    	static Interval3D iv;
    	Box3 bb;
		for(int i=0; i<NEN; i++)
			bb.addPoint(element->vertex(i)->pos());
		iv = Interval3D(Pure_interval3D(Tree_Key(bb.minc.X, bb.minc.Y, bb.minc.Z), Tree_Key(bb.maxc.X, bb.maxc.Y, bb.maxc.Z)), element);
		return iv;			
	}	

private:  // serialization code

	template<class Archive>
    void serialize(Archive &ar, const unsigned int version) {
    	// Save/load elements from the vector.
    	ar & boost::serialization::base_object< QVector<Element*> >(*this);
    }
	friend class boost::serialization::access;
};

}; // End of namespace MAFEM

#endif // __MAFEM_ELEMENT_COLLECTION_H
