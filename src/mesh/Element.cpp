///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "Element.h"
#include "Mesh.h"
#include <simulation/SimulationResource.h>

namespace MAFEM {

/// Lookup table for tetrahedron face vertices.
const int Element::elementFaceMap[4][3] = {
	{1, 3, 2}, {0, 2, 3}, {0, 3, 1}, {0, 1, 2}
};

/// Lookup table for tetrahedron edge vertices.
const int Element::elementEdgeMap[6][2] = {
		{0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 3}
};

/******************************************************************************
* Initializes the element.
******************************************************************************/
Element::Element(const array<RepAtom*, NEN>& vertices) : _isMarked(false),devStrainSecInvL2Norm(0)
{
	// Copy vertices and initialize the neighbor list to NULL.
	_vertices = vertices;
	for(int i=0; i<NEN; i++) {
		CHECK_POINTER(_vertices[i]);
		_neighbors[i] = NULL;
		
		// Make sure all vertices are from the same lattice.
		MAFEM_ASSERT_MSG(vertices[i]->site().lattice() == vertices[0]->site().lattice(), "Element constructor", "All element vertices must belong to the same crystal lattice.");
	}
	_lattice = _vertices[0]->site().lattice();
	
	initialize();
}

/******************************************************************************
* Initialize the data fields of the class.
******************************************************************************/
void Element::initialize()
{
	Matrix4 J;
	J(0, 0) = J(0, 1) = J(0, 2) = J(0, 3) = 1;
	for(int i=0; i<NDOF; i++)
		for(int j=0; j<NEN; j++)
			J(i+1, j) = vertex(j)->pos()[i];

	_det = J.determinant();

	if(_det < -FLOATTYPE_EPSILON) {
		// Flip elements with negative volume.
		swap(_vertices[0], _vertices[1]);
		_det = -_det;
		for(int i=1; i<=NDOF; i++)
			swap(J(i, 0), J(i, 1));
	}
	if(_det > FLOATTYPE_EPSILON) {
		Matrix4 jinv = J.inverse();
		for(int i=0; i<4; i++)
			_dsh[i] = Vector4(jinv(i, 1), jinv(i, 2), jinv(i, 3), jinv(i, 0));
	}
	else {
		_dsh[0] = _dsh[1] = _dsh[2] = _dsh[3] = NULL_VECTOR;
	}
	_volume = _det / 6.0;

	// Compute the planes of the element sides.
	for(int face=0; face<NEN; face++) {
		const Point3I& index1 = _vertices[elementFaceMap[face][0]]->site().indices();
		const Point3I& index2 = _vertices[elementFaceMap[face][1]]->site().indices();
		const Point3I& index3 = _vertices[elementFaceMap[face][2]]->site().indices();
        _facePlanes[face] = Plane_3<int>(index1, index2, index3);
	}
}

/******************************************************************************
* Determines whether this is an unnecessary element in a fully atomistic region.
******************************************************************************/
bool Element::isUnnecessaryElement() const
{
	//return false;
	
	// Skip zero volume elements.
	if(volume() < FLOATTYPE_EPSILON)
		return true;
/*	
	// Check if all elements should be kept
	if(simulation()->settings().keepFullyAtomisticElements)
		return false;
*/
	// Check if the element contains at least one interior lattice site.
	bool hasInteriorSites = false;
	
	// First, do a quick test with the center point of the element.
	Point3I center = (vertexLatticeIndices(0) + vertexLatticeIndices(1) + vertexLatticeIndices(2) + vertexLatticeIndices(3)) / 4;
	if(containsSite(center)) {
		if(vertexLatticeIndices(0) != center && vertexLatticeIndices(1) != center &&
				vertexLatticeIndices(2) != center && vertexLatticeIndices(3) != center)
			return false;
	}					

	// Compute the bounding box of the element in lattice space.
	Box3I latticeBB;
	for(int v=0; v<NEN; v++)
		latticeBB.addPoint(vertexLatticeIndices(v));

	// Iterate over all lattice sites in the bounding box.
	Point3I indices;
	for(indices.X = latticeBB.minc.X; indices.X <= latticeBB.maxc.X; indices.X++) {
		for(indices.Y = latticeBB.minc.Y; indices.Y <= latticeBB.maxc.Y; indices.Y++) {
			for(indices.Z = latticeBB.minc.Z; indices.Z <= latticeBB.maxc.Z; indices.Z++) {
				
				// Skip corner points of the element.
				if(indices == vertexLatticeIndices(0)) continue;
				if(indices == vertexLatticeIndices(1)) continue;
				if(indices == vertexLatticeIndices(2)) continue;
				if(indices == vertexLatticeIndices(3)) continue;
				
				// Is the site inside the tetrahedron element?
				if(containsSite(indices)) {
					hasInteriorSites = true;
					break;
				}
			}
			if(hasInteriorSites) break;
		}
		if(hasInteriorSites) break;
	}	
	if(hasInteriorSites == false)
		return true;
	
	return false;
}

/***********************************************************************
* Bisects an edge of this element with the given point.
* The newly created second element is returned.
************************************************************************/
Element* Element::BisectElementEdge(int i1, int i2, RepAtom* splitPoint,Mesh* mesh)
{
	CHECK_POINTER(splitPoint);
	MAFEM_ASSERT(vertex(i1) != splitPoint && vertex(i2) != splitPoint);
    array<RepAtom*, NEN> v1;
	// Determine the vertex order of the new element.
    RepAtom* vert1[4];
    for(int i= 0; i < 4; i++){
    vert1[i] = _vertices[i];
    }
    vert1[i1] = splitPoint;

	char fd1[4];
    for(int i= 0; i < 4; i++){
    fd1[i] = faceDirections[i];
    }
   	fd1[i2] = 0;

	// Create a second element.
    for(int i= 0; i < 4; i++){
    v1[i] = vert1[i];
    }

    Element el(v1);
    	Element* newElement = mesh->addElement(el);
	newElement->SetFaceDirections(fd1[0],fd1[1],fd1[2],fd1[3]);

	// Modify this element.
	setVertex(i2,splitPoint);
	initialize();
	faceDirections[i1] = 0;

	return newElement;
}


}; // End of namespace MAFEM

