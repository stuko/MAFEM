///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "Mesh.h"
#include <simulation/Simulation.h>

namespace MAFEM {

/******************************************************************************
* Splits those elements marked for refinement.
******************************************************************************/
bool Mesh::splitMarkedElements()
{
	MsgLogger() << logdate << "Refining marked elements." << endl;
	int oldNodeCount = nodes().size();
	
	Q_FOREACH(Element* element, elements()) {
		if(!element->isMarked()) continue;
		
		// Determine the longest edge of the element.
		ElementEdge longestEdge = element->edge(0);
		FloatType maxLengthSq = DistanceSquared(longestEdge.first->pos(), longestEdge.second->pos());
		for(int e=1; e<6; e++) {
			ElementEdge edge = element->edge(1);
			FloatType lengthSq = DistanceSquared(edge.first->pos(), edge.second->pos());
			MAFEM_ASSERT(lengthSq > 0.0);
			if(lengthSq > maxLengthSq) {
				maxLengthSq = lengthSq;
				longestEdge = edge;
			}
		}
		
		// Determine the lattice site that splits the longest edge.
		// If the edge mid point is right between two or more lattice sites then we have to check all.
		Point3 midPoint = (longestEdge.first->pos() + longestEdge.second->pos()) * 0.5;
		
		// Back transform world point to lattice space.
		CrystalLattice* lattice = longestEdge.first->site().lattice();
		Point3 u = lattice->inverseLatticeVectors() * midPoint;

		bool foundValidSite = false;
		// Jitter midpoint coordinates by a little amount in each direction to find valid nearby sites.
		for(FloatType dx = -FLOATTYPE_EPSILON; dx <= FLOATTYPE_EPSILON*2; dx += 2*FLOATTYPE_EPSILON) {
			for(FloatType dy = -FLOATTYPE_EPSILON; dy <= FLOATTYPE_EPSILON*2; dy += 2*FLOATTYPE_EPSILON) {
				for(FloatType dz = -FLOATTYPE_EPSILON; dz <= FLOATTYPE_EPSILON*2; dz += 2*FLOATTYPE_EPSILON) {
					// Round to integer position.
					Point3I r((int)floor(u.X+0.5+dx), (int)floor(u.Y+0.5+dy), (int)floor(u.Z+0.5+dz));
					LatticeSite midSite(lattice, r);
		
					if(tryToAddNode(midSite)) {
						foundValidSite = true;
						break;
					}
				}
				if(foundValidSite) break;
			}
			if(foundValidSite) break;
		}
	}
	
	// If a new nodes have been created then we have to do the triangulation again.
	int numNewNodes = nodes().size() - oldNodeCount;
	if(numNewNodes != 0) {
		MsgLogger() << "Added" << numNewNodes << "nodes to the mesh during refinement. Triangulation will be updated." << endl;
		triangulate();
		MsgLogger() << "Number of mesh nodes:" << nodes().size() << endl;
		MsgLogger() << "Number of real repatoms:" << repatoms().size() << endl;
		MsgLogger() << "Number of periodic image repatoms:" << (nodes().size() - repatoms().size()) << endl;
		MsgLogger() << "Number of necessary elements:" << elements().size() << endl;
		// Rebuild the sampling clusters.
		buildSamplingClusters();
	}
	else {
		MsgLogger() << "No elements have been split." << endl;
	}
	
	return numNewNodes != 0;
}

/******************************************************************************
* Tries to add a new node to the mesh.
******************************************************************************/
RepAtom* Mesh::tryToAddNode(const LatticeSite& site)
{
	// Check if new node is inside simulation cell.
	Point3 reducedPoint = simulation()->inverseSimulationCell() * site.pos();
	if(reducedPoint[0] < -FLOATTYPE_EPSILON || reducedPoint[0] > 1.0+FLOATTYPE_EPSILON || 
		reducedPoint[1] < -FLOATTYPE_EPSILON || reducedPoint[1] > 1.0+FLOATTYPE_EPSILON ||
		reducedPoint[2] < -FLOATTYPE_EPSILON || reducedPoint[2] > 1.0+FLOATTYPE_EPSILON) {
		
		return NULL;
	}
			
	// Check if there is another node at the given site.		
	if(nodes().findAt(site)) {
		return NULL;
	}
	// Create a new one.
	RepAtom* newNode = createNode(site);
	_repatoms.push_back(newNode);
		
	// If the new node is at the simulation cell boundary then we might have to create a periodic images as well.
	const array<bool,NDOF> pbc = simulation()->settings().periodicBC;
	for(int k=0; k<NDOF; k++) {
		if(pbc[k]) {
			Vector3 delta;
			if(reducedPoint[k] < FLOATTYPE_EPSILON)
				delta = simulation()->simulationCell().column(k);
			if(reducedPoint[k] > 1.0 - FLOATTYPE_EPSILON)
				delta = -simulation()->simulationCell().column(k);
			else
				continue;
			
			LatticeSite imageSite(newNode->site().lattice(), newNode->site().pos() + delta);
			RepAtom* imageNode = nodes().findAt(imageSite);
			if(imageNode == NULL)
				imageNode = createNode(imageSite);
			imageNode->setPeriodicImageMaster(newNode);
		}
	}
	
	return newNode;
}

}; // End of namespace MAFEM
