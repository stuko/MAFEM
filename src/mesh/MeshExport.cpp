///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "Mesh.h"
#include <simulation/Simulation.h>
#include <force/CalculationResult.h>

namespace MAFEM {

/******************************************************************************
* Exports the QC mesh to the given file for visualization with Tecplot.
******************************************************************************/
void Mesh::exportMeshToTecplot(const QString& filename, CalculationResult* forces)
{
	MsgLogger() << logdate << "Saving mesh to Tecplot file" << filename << endl;
	
	if(forces && forces->forces().size() != repatoms().size()) 
		forces = NULL;
		
	
	Box3 simCell = simulation()->simulationCell() * Box3(Point3(0,0,0),Point3(1,1,1));
	AdaptiveRefinement(simCell,1e100);

	// Open output file.
	QFile tecplotFile(filename);
	if(!tecplotFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh to file %1. Cannot open file for writing: %2").arg(filename).arg(tecplotFile.errorString()));
	QTextStream fileStream(&tecplotFile);

	fileStream << "VARIABLES=\"X\" \"Y\" \"Z\" \"UX\" \"UY\" \"UZ\"";
	if(forces) fileStream << " \"Force X\" \"Force Y\" \"Force Z\" \"InternalEnergy\" \"ExternalEnergy\"" ;
	fileStream << " \"RefIndicator\" \"Cluster Weight\" \"Sampling Atom Count\" \"Repatom Index\"  \"sigma11\"  \"sigma22\"  \"sigma33\"   \"sigma12\"  \"sigma13\"   \"sigma23\"  \"Veq\"  \"AtomicVolume\"" << endl;
	if(elements().size() > 0)
		fileStream << "ZONE T=\"Mesh\" N=" << nodes().size() << " E=" << elements().size() << " DATAPACKING=POINT ZONETYPE=FETETRAHEDRON" << endl;
	else
		fileStream << "ZONE T=\"Mesh\" I=" << nodes().size() << " DATAPACKING=POINT" << endl;

	// Write nodes to Tecplot file.
	QMap<RepAtom*, int> nodeIndices;
	for(int i=0; i<nodes().size(); i++) {
		RepAtom* node = nodes()[i];
		nodeIndices[node] = i+1;
		Vector3 displacement = node->displacement();
		fileStream << node->deformedPos().X << " " << node->deformedPos().Y << " " << node->deformedPos().Z;
		fileStream << " " << displacement.X << " " << displacement.Y << " " << displacement.Z;
		RepAtom* repatom = node->realRepatom();
		if(forces) {
			const Vector3& force = forces->forces()[repatom->index()];
			const FloatType& intEnergy = forces->internalEnergy()[repatom->index()];
			const FloatType& extEnergy = forces->externalEnergy()[repatom->index()];
			fileStream << " " << force.X << " " << force.Y << " " << force.Z << " " << intEnergy << " " << extEnergy;
		}
		fileStream << " " << repatom->getMaxDevStrainSecInvL2Norm();
	
		fileStream << " " << repatom->clusterWeight();
		fileStream << " " << repatom->numSamplingAtoms();
		fileStream << " " << repatom->index();
		for (int s = 0; s<6; s++){
		  fileStream << " " << repatom->getStressComponent(s);
		}
		
		FloatType VonMisesEqStress = 0.0;
		VonMisesEqStress += (repatom->getStressComponent(0)-repatom->getStressComponent(1))*(repatom->getStressComponent(0)-repatom->getStressComponent(1));
		VonMisesEqStress += (repatom->getStressComponent(1)-repatom->getStressComponent(2))*(repatom->getStressComponent(1)-repatom->getStressComponent(2));
		VonMisesEqStress += (repatom->getStressComponent(2)-repatom->getStressComponent(0))*(repatom->getStressComponent(2)-repatom->getStressComponent(0));
		VonMisesEqStress += 6*((repatom->getStressComponent(3)*repatom->getStressComponent(3)) + (repatom->getStressComponent(4)*repatom->getStressComponent(4)) + (repatom->getStressComponent(5)*repatom->getStressComponent(5)));
		VonMisesEqStress = sqrt(0.5*VonMisesEqStress);

		fileStream << " " << VonMisesEqStress;
		fileStream << " " << repatom->getAtomicVolume();
		
		
		
		
		
		fileStream << endl;
	}

	// Write elements to Tecplot file.
	for(int i=0; i<elements().size(); i++) {
		Element* el = elements()[i];
		for(int j=0; j<NEN; j++)
			fileStream << nodeIndices[el->vertex(j)] << " ";
		fileStream << endl;
	}
	
	if(!samplingAtoms().empty()) {
		// Count the number of active/passive sampling atoms.
		int nactive = 0;
		Q_FOREACH(const SamplingAtom* atom, samplingAtoms())
			if(atom->isActive()) nactive++;
				
		fileStream << "ZONE T=\"Active Sampling Atoms\" I=" << nactive << " DATAPACKING=POINT" << endl;
		Q_FOREACH(const SamplingAtom* atom, samplingAtoms()) {
			if(!atom->isActive()) continue;
			Vector3 displacement = atom->displacement();
			fileStream << atom->deformedPos().X << " " << atom->deformedPos().Y << " " << atom->deformedPos().Z;
			fileStream << " " << displacement.X << " " << displacement.Y << " " << displacement.Z;
			RepAtom* repatom = atom->clusterRepatom()->realRepatom();
			if(forces) {
				const Vector3& force = forces->forces()[repatom->index()];
				const FloatType& intEnergy = forces->internalEnergy()[repatom->index()];
				const FloatType& extEnergy = forces->externalEnergy()[repatom->index()];
				fileStream << " " << force.X << " " << force.Y << " " << force.Z << " " << intEnergy << " " << extEnergy;
			}
			//if(forces) fileStream << " 0 0 0 0 0";
			fileStream << " " << atom->clusterRepatom()->getMaxDevStrainSecInvL2Norm();
			if(simulation()->settings().quadratureMethod==true) { fileStream << " " << atom->weight();}else{
			fileStream << " " << atom->clusterRepatom()->clusterWeight();}
			fileStream << " " << atom->clusterRepatom()->numSamplingAtoms();
			fileStream << " " << atom->clusterRepatom()->index() ;
			for (int s = 0; s<6; s++){
			  fileStream << " " << atom->clusterRepatom()->getStressComponent(s);
			}
		
			FloatType VonMisesEqStress = 0.0;
			VonMisesEqStress += (atom->clusterRepatom()->getStressComponent(0)-atom->clusterRepatom()->getStressComponent(1))*(atom->clusterRepatom()->getStressComponent(0)-atom->clusterRepatom()->getStressComponent(1));
			VonMisesEqStress += (atom->clusterRepatom()->getStressComponent(1)-atom->clusterRepatom()->getStressComponent(2))*(atom->clusterRepatom()->getStressComponent(1)-atom->clusterRepatom()->getStressComponent(2));
			VonMisesEqStress += (atom->clusterRepatom()->getStressComponent(2)-atom->clusterRepatom()->getStressComponent(0))*(atom->clusterRepatom()->getStressComponent(2)-atom->clusterRepatom()->getStressComponent(0));
			VonMisesEqStress += 6*((atom->clusterRepatom()->getStressComponent(3)*atom->clusterRepatom()->getStressComponent(3)) + (atom->clusterRepatom()->getStressComponent(4)*atom->clusterRepatom()->getStressComponent(4)) + (atom->clusterRepatom()->getStressComponent(5)*atom->clusterRepatom()->getStressComponent(5)));
			VonMisesEqStress = sqrt(0.5*VonMisesEqStress);
			
			fileStream << " " << VonMisesEqStress;
			fileStream << " " << atom->clusterRepatom()->getAtomicVolume();
			
			
			fileStream << endl;
		}

		int npassive = samplingAtoms().size() - nactive;
		if(npassive>0) fileStream << "ZONE T=\"Passive Sampling Atoms\" I=" << npassive << " DATAPACKING=POINT" << endl;
		Q_FOREACH(const SamplingAtom* atom, samplingAtoms()) {
			if(atom->isActive()) continue;
			Vector3 displacement = atom->displacement();
			fileStream << atom->deformedPos().X << " " << atom->deformedPos().Y << " " << atom->deformedPos().Z;
			fileStream << " " << displacement.X << " " << displacement.Y << " " << displacement.Z;
			if(forces) fileStream << " 0 0 0 0 0";
			fileStream << " 0 0 0 -1" ;
			for (int s = 0; s<8; s++){
			  fileStream << " " << "0" ;
			}
			fileStream << endl;
		}
	}
	tecplotFile.close();
}

/******************************************************************************
* Exports the QC mesh to the given file for visualization with ParaView.
******************************************************************************/
void Mesh::exportMeshToVTK(const QString& filename, CalculationResult* output)
{
	MsgLogger() << logdate << "Saving mesh to VTK file " << filename << "." << endl;

	if (output && output->forces().size() != repatoms().size())
		output = NULL;

	// Open output file.
	QFile vtkFile(filename);
	if (!vtkFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh to file %1. Cannot open file for writing: %2").arg(filename).arg(vtkFile.errorString()));
	QTextStream fileStream(&vtkFile);

	fileStream << "# vtk DataFile Version 3.0" << endl;
	fileStream << "# QC mesh" << endl;
	fileStream << "ASCII" << endl;
	fileStream << "DATASET UNSTRUCTURED_GRID" << endl;
	
	
/*
	size_t numCells = 0;
	vector<DelaunayTriangulation::Cell_handle> cells;
	for(DelaunayTriangulation::Cells_iterator cell = triangulation.cells_begin(); cell != triangulation.cells_end(); ++cell) {

		// Skip elements with nearly zero initial volume.
		Matrix3 volmat;
		for(int v = 0; v < 3; v++)
			volmat.setColumn(v, triangulation.getVertex(cell, 3)->pos() - triangulation.getVertex(cell, v)->pos());
		if(volmat.determinant() < 1e-6) continue;

		if(clipPlane != NULL) {
			bool clipped = false;
			for(int v = 0; v < NEN; v++) {
				if(clipPlane->pointDistance(triangulation.getVertex(cell, v)->deformedPos()) < 0) {
					clipped = true;
					break;
				}
			}
			if(clipped) continue;
		}
		numCells++;
		cells.push_back(cell);
	}
*/

	// Write node positions to VTK file.
	int numNodes = nodes().size();
	fileStream << "POINTS " << numNodes << " double" << endl;
	map<const RepAtom*, int> nodeIndices;
	int index = 0;
	
	for (int i = 0; i < numNodes; ++i)
	{
		RepAtom* node = nodes()[i];
		nodeIndices[node] = index++;
		fileStream << node->deformedPos().X << " " << node->deformedPos().Y << " " << node->deformedPos().Z << endl;
	}
	
	// Write elements to VTK file.
	int numCells = elements().size();
	fileStream << endl << "CELLS " << numCells + numNodes << " " << (numCells * 5 + numNodes * 2) << endl;
	for (int i = 0; i < elements().size(); ++i)
	{
		fileStream << "4";
		for (int j=0; j<NEN; j++)
			fileStream << " " << nodeIndices[elements()[i]->vertex(j)];
		fileStream << endl;
	}

	for (int i = 0; i < numNodes; ++i)
		fileStream << "1 " << i << endl;

	// Write cell types.
	fileStream << endl << "CELL_TYPES " << numCells + numNodes << endl;
	for (int i = 0; i < numCells; ++i)
		fileStream << "10" << endl;	// Tetrahedron
	for(int i = 0; i < numNodes; ++i)
		fileStream << "1" << endl; // Vertext

	// Write point data
	fileStream << endl << "POINT_DATA " << nodes().size() << endl;

	// Deformed Position
	fileStream << endl << "VECTORS def_pos double" << endl;
	for (int i = 0; i < numNodes; ++i)
	{
		Point3 def_pos = nodes()[i]->deformedPos();
		fileStream << def_pos.X << " " << def_pos.Y << " " << def_pos.Z << endl;
	}
		
	// Original Position
	fileStream << endl << "VECTORS org_pos double" << endl;
	for (int i = 0; i < numNodes; ++i)
	{
		Point3 pos = nodes()[i]->pos();
		fileStream << pos.X << " " << pos.Y << " " << pos.Z << endl;
	}
	
	// Displacement
	fileStream << endl << "VECTORS displacement double" << endl;
	for (int i = 0; i < numNodes; ++i)
	{
		Vector3 displacement = nodes()[i]->displacement();
		fileStream << displacement.X << " " << displacement.Y << " " << displacement.Z << endl;
	}
	
	// Energy and forces
	if (output->forces().size())
	{
		// Internal forces
		fileStream << endl << "VECTORS forces double" << endl;
		for (int i = 0; i < numNodes; ++i)
		{
			Vector3& force = output->forces()[nodes()[i]->realRepatom()->index()];
			fileStream << force.X << " " << force.Y << " " << force.Z << endl;
		}
		
		// External forces
		fileStream << endl << "VECTORS reforces double" << endl;
		for (int i = 0; i < numNodes; ++i)
		{
			Vector3& reforce = output->reforces()[nodes()[i]->realRepatom()->index()];
			fileStream << reforce.X << " " << reforce.Y << " " << reforce.Z << endl;
		}
	
		// Internal Energy
		fileStream << endl << "SCALARS InternalEnergy double 1" << endl;
		fileStream << "LOOKUP_TABLE default" << endl;
		for (int i = 0; i < numNodes; ++i)
			fileStream << output->internalEnergy()[nodes()[i]->realRepatom()->index()] << endl;
		
		// External Energy
		fileStream << endl << "SCALARS ExternalEnergy double 1" << endl;
		fileStream << "LOOKUP_TABLE default" << endl;
		for (int i = 0; i < numNodes; ++i)
			fileStream << output->externalEnergy()[nodes()[i]->realRepatom()->index()] << endl;
	}
	
	// Censtrosymmetry parameter
	if (output->centrosymmetry().size())
	{	
		fileStream << endl << "SCALARS centrosymmetry double 1" << endl;
		fileStream << "LOOKUP_TABLE default" << endl;
		for (int i = 0; i < numNodes; ++i)
			fileStream << output->centrosymmetry()[nodes()[i]->realRepatom()->index()] << endl;
	}
	
	// Eigenvectors
	if (output->evecs().size())
	{	
		int numEvecs = output->evecs().size() / numNodes;
		for (int i = 0; i < numEvecs; ++i)
		{
			fileStream << endl << "VECTORS eigenvectors[" << i << "] double" << endl;
			for (int j = 0; j < numNodes; ++j)
			{
				Vector3& evec = output->evecs()[i * numNodes + nodes()[j]->realRepatom()->index()];
				fileStream << evec.X << " " << evec.Y << " " << evec.Z << endl;
			}
		}
	}
	
	// Refinement Indicator
	fileStream << endl << "SCALARS RefIndicator double 1" << endl;
	fileStream << "LOOKUP_TABLE default" << endl;
	for (int i = 0; i < numNodes; ++i)
		fileStream << nodes()[i]->realRepatom()->getMaxDevStrainSecInvL2Norm() << endl;
	
	// Cluster Weight
	fileStream << endl << "SCALARS cluster_weight double 1" << endl;
	fileStream << "LOOKUP_TABLE default" << endl;
	for (int i = 0; i < numNodes; ++i)
		fileStream << nodes()[i]->realRepatom()->clusterWeight() << endl;
	
	// Sampling Atom Count
	fileStream << endl << "SCALARS sampling_atom_count int 1" << endl;
	fileStream << "LOOKUP_TABLE default" << endl;
	for (int i = 0; i < numNodes; ++i)
		fileStream << nodes()[i]->realRepatom()->numSamplingAtoms() << endl;
	
	// Repatom Index
	fileStream << endl << "SCALARS Repatom_Index int 1" << endl;
	fileStream << "LOOKUP_TABLE default" << endl;
	for (int i = 0; i < numNodes; ++i)
		fileStream << nodes()[i]->realRepatom()->index() << endl;

	// Stresses
	if (nodes()[0]->realRepatom()->getStressComponent(0))
	{
		// Equivalent Stress
		fileStream << endl << "SCALARS sigma_v double 1" << endl;
		fileStream << "LOOKUP_TABLE default" << endl;
		for (int i = 0; i < numNodes; ++i)
		{
			RepAtom* repatom = nodes()[i]->realRepatom();
		
			FloatType VonMisesEqStress = 0.0;
			VonMisesEqStress += (repatom->getStressComponent(0)-repatom->getStressComponent(1))*(repatom->getStressComponent(0)-repatom->getStressComponent(1));
			VonMisesEqStress += (repatom->getStressComponent(1)-repatom->getStressComponent(2))*(repatom->getStressComponent(1)-repatom->getStressComponent(2));
			VonMisesEqStress += (repatom->getStressComponent(2)-repatom->getStressComponent(0))*(repatom->getStressComponent(2)-repatom->getStressComponent(0));
			VonMisesEqStress += 6*((repatom->getStressComponent(3)*repatom->getStressComponent(3)) + (repatom->getStressComponent(4)*repatom->getStressComponent(4)) + (repatom->getStressComponent(5)*repatom->getStressComponent(5)));
			VonMisesEqStress = sqrt(0.5*VonMisesEqStress);
		
			if (VonMisesEqStress == VonMisesEqStress) // Prevent NAN values
				fileStream << VonMisesEqStress << endl;
			else
				fileStream << 0 << endl;
		}
		
		// Stress Components
		fileStream << endl << "SCALARS sigma_components double 6" << endl;
		fileStream << "LOOKUP_TABLE default" << endl;
		int PVTensorComponents[] = {0, 1, 2, 3, 5, 4}; // ParaView: XX YY ZZ XY YZ XZ
		
		for (int i = 0; i < numNodes; ++i)
		{
			for (int j = 0; j < 6; ++j)
			{
				FloatType sigma = nodes()[i]->realRepatom()->getStressComponent(PVTensorComponents[j]);
				if(sigma == sigma) // Prevent NAN values
					fileStream << sigma << " ";
				else
					fileStream << "0 ";
			}
			fileStream << endl;	
		}
		
		// Hydrostatic Pressure
		fileStream << endl << "SCALARS p_hyd double 1" << endl;
		fileStream << "LOOKUP_TABLE default" << endl;
		for (int i = 0; i < numNodes; ++i)
		{
			RepAtom* repatom = nodes()[i]->realRepatom();
			FloatType p_hyd = (repatom->getStressComponent(0) + repatom->getStressComponent(1) + repatom->getStressComponent(2)) / 3;
			fileStream << p_hyd << endl;
		}
	}
	
	// Atomic Volume
	fileStream << endl << "SCALARS Atomic_Volume double 1" << endl;
	fileStream << "LOOKUP_TABLE default" << endl;
	for (int i = 0; i < numNodes; ++i)
		fileStream << nodes()[i]->realRepatom()->getAtomicVolume() << endl;
		
/*
	// Centrosymmetry
	if(output->centroatoms.size() > 0)
	{
		fileStream << endl << "SCALARS Centrosymmetry double 1" << endl;
		fileStream << "LOOKUP_TABLE default" << endl;
		Q_FOREACH(const RepAtom* node, nodes())
			Q_FOREACH(SamplingAtom* atom, output->centroatoms)
				if(node == atom->samplingNode())
					fileStream << atom->getCentroSymmetry() << endl;
	}
*/

	// Write cell data
	fileStream << endl << "CELL_DATA " << numCells + numNodes << endl;
	
	// Element Volume
	fileStream << endl << "SCALARS element_volume double 1" << endl;
	fileStream << "LOOKUP_TABLE default" << endl;
	for (int i = 0; i < elements().size(); ++i)
		fileStream << elements()[i]->volume() << endl;
	
	for(int i = 0; i<numNodes; i++)
		fileStream << "0" << endl; //vertex-element
/*
	// Write element state
	fileStream << endl << "SCALARS is_element int 1" << endl;
	fileStream << "LOOKUP_TABLE default" << endl;
	for(vector<DelaunayTriangulation::Cell_handle>::const_iterator cell = cells.begin(); cell != cells.end(); ++cell)
		fileStream << (triangulation.getElement(*cell) != NULL) << endl;

	// Write shear strain
	fileStream << endl << "SCALARS shear_strain float 1" << endl;
	fileStream << "LOOKUP_TABLE default" << endl;
	for(ElementCollection::iterator element = grain->elements().begin(); element != grain->elements().end(); ++element) {
			fileStream << element->shearStrain() << endl;
	}

	for(vector<DelaunayTriangulation::Cell_handle>::const_iterator cell = cells.begin(); cell != cells.end(); ++cell) {
		fileStream << cell->shearStrain() << endl;
	}
	
*/
	vtkFile.close();
}

/******************************************************************************
* Exports the QC mesh only without data to the given file for visualization with ParaView.
******************************************************************************/
void Mesh::exportMeshOnlyToVTK(const QString& filename)
{
	MsgLogger() << logdate << "Saving mesh to VTK file " << filename << "." << endl;

	// Open output file.
	QFile vtkFile(filename);
	if (!vtkFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh to file %1. Cannot open file for writing: %2").arg(filename).arg(vtkFile.errorString()));
	QTextStream fileStream(&vtkFile);

	fileStream << "# vtk DataFile Version 3.0" << endl;
	fileStream << "# QC mesh" << endl;
	fileStream << "ASCII" << endl;
	fileStream << "DATASET UNSTRUCTURED_GRID" << endl;
	
	// Write node positions to VTK file.
	int numNodes = nodes().size();
	fileStream << "POINTS " << numNodes << " double" << endl;
	map<const RepAtom*, int> nodeIndices;
	int index = 0;
	
	for (int i = 0; i < numNodes; ++i)
	{
		RepAtom* node = nodes()[i];
		nodeIndices[node] = index++;
		fileStream << node->deformedPos().X << " " << node->deformedPos().Y << " " << node->deformedPos().Z << endl;
	}
	
	// Write elements to VTK file.
	int numCells = elements().size();
	fileStream << endl << "CELLS " << numCells + numNodes << " " << (numCells * 5 + numNodes * 2) << endl;
	for (int i = 0; i < elements().size(); ++i)
	{
		fileStream << "4";
		for (int j=0; j<NEN; j++)
			fileStream << " " << nodeIndices[elements()[i]->vertex(j)];
		fileStream << endl;
	}

	for (int i = 0; i < numNodes; ++i)
		fileStream << "1 " << i << endl;

	// Write cell types.
	fileStream << endl << "CELL_TYPES " << numCells + numNodes << endl;
	for (int i = 0; i < numCells; ++i)
		fileStream << "10" << endl;	// Tetrahedron
	for(int i = 0; i < numNodes; ++i)
		fileStream << "1" << endl; // Vertext

	vtkFile.close();
}

/******************************************************************************
* Exports the QC mesh as triangle surfaces without data to the given file for visualization with Ovito.
******************************************************************************/
void Mesh::exportMeshTriasToVTK(const QString& filename)
{
	MsgLogger() << logdate << "Saving mesh to VTK file " << filename << "." << endl;

	// Open output file.
	QFile vtkFile(filename);
	if (!vtkFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh to file %1. Cannot open file for writing: %2").arg(filename).arg(vtkFile.errorString()));
	QTextStream fileStream(&vtkFile);

	fileStream << "# vtk DataFile Version 3.0" << endl;
	fileStream << "# QC mesh" << endl;
	fileStream << "ASCII" << endl;
	fileStream << "DATASET POLYDATA" << endl;
	
	// Write node positions to VTK file.
	int numNodes = nodes().size();
	fileStream << "POINTS " << numNodes << " double" << endl;
	map<const RepAtom*, int> nodeIndices;
	int index = 0;
	
	for (int i = 0; i < numNodes; ++i)
	{
		RepAtom* node = nodes()[i];
		nodeIndices[node] = index++;
		fileStream << node->deformedPos().X << " " << node->deformedPos().Y << " " << node->deformedPos().Z << endl;
	}
	
	// Write polygons to VTK file.
	int numCells = elements().size();
	fileStream << endl << "POLYGONS " << numCells * 4 << " " << numCells * 4 * 4 << endl;
	double triaIndices[4][3] = {{0,2,1},{0,1,3},{0,3,2},{1,2,3}};
	for (int i = 0; i < elements().size(); ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			fileStream << "3";
			for (int k = 0; k < 3; ++k)
				fileStream << " " << nodeIndices[elements()[i]->vertex(triaIndices[j][k])];
			fileStream << endl;
		}
	}
	vtkFile.close();
}

/******************************************************************************
* Exports the sampling atoms to the given file for visualization with ParaView.
******************************************************************************/
void Mesh::exportSamplingAtomsToVTK(const QString& filename, CalculationResult* output)
{
	MsgLogger() << logdate << "Saving sampling atoms to VTK file " << filename << "." << endl;

	if(output && output->forces().size() != repatoms().size())
		output = NULL;

	// Open output file.
	QFile vtkFile(filename);
	if(!vtkFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh to file %1. Cannot open file for writing: %2").arg(filename).arg(vtkFile.errorString()));
	QTextStream fileStream(&vtkFile);

	fileStream << "# vtk DataFile Version 3.0" << endl;
	fileStream << "# QC mesh" << endl;
	fileStream << "ASCII" << endl;
	fileStream << "DATASET UNSTRUCTURED_GRID" << endl;
	

	if(!samplingAtoms().empty())
	{
		// Count the number of active/passive sampling atoms.
//		int nactive = 0;
//		Q_FOREACH(const SamplingAtom* atom, samplingAtoms())
//			if(atom->isActive()) nactive++;

		int numAtoms = samplingAtoms().size();
		
		// Write node positions to VTK file.
		fileStream << "POINTS " << numAtoms << " double" << endl;
		Q_FOREACH(const SamplingAtom* atom, samplingAtoms())
		{
			fileStream << atom->deformedPos().X << " " << atom->deformedPos().Y << " " << atom->deformedPos().Z << endl;
		}
		
		// Write Vertices to VTK file.
		fileStream << endl << "CELLS " << numAtoms << " " << numAtoms * 2 << endl;
		for(int i = 0; i<numAtoms; i++)
		fileStream << "1 " << i << endl;

		// Write cell types.
		fileStream << endl << "CELL_TYPES " << numAtoms << endl;
		for(int i = 0; i < numAtoms; i++)
			fileStream << "1" << endl; // Vertext

		// Write point data
		fileStream << endl << "POINT_DATA " << numAtoms << endl;
		
		// Displacement
		fileStream << endl << "VECTORS displacement double" << endl;
		Q_FOREACH(const SamplingAtom* atom, samplingAtoms())
		{
			Vector3 displacement = atom->displacement();
			fileStream << displacement.X << " " << displacement.Y << " " << displacement.Z << endl;
		}
		
		// IsActive State
		fileStream << endl << "SCALARS isActive int 1" << endl;
		fileStream << "LOOKUP_TABLE default" << endl;
		Q_FOREACH(const SamplingAtom* atom, samplingAtoms())
		{
			if(atom->isActive()) fileStream << 1 << endl;
			else fileStream << 0 << endl;
		}
/*
		fileStream << "ZONE T=\"Active Sampling Atoms\" I=" << nactive << " DATAPACKING=POINT" << endl;
		Q_FOREACH(const SamplingAtom* atom, samplingAtoms()) {
			if(!atom->isActive()) continue;
			Vector3 displacement = atom->displacement();
			fileStream << atom->deformedPos().X << " " << atom->deformedPos().Y << " " << atom->deformedPos().Z;
			fileStream << " " << displacement.X << " " << displacement.Y << " " << displacement.Z;
			RepAtom* repatom = atom->clusterRepatom()->realRepatom();
			if(output) {
				const Vector3& force = output->forces()[repatom->index()];
				const FloatType& intEnergy = output->internalEnergy()[repatom->index()];
				const FloatType& extEnergy = output->externalEnergy()[repatom->index()];
				fileStream << " " << force.X << " " << force.Y << " " << force.Z << " " << intEnergy << " " << extEnergy;
			}
			//if(output) fileStream << " 0 0 0 0 0";
			fileStream << " " << atom->clusterRepatom()->getMaxDevStrainSecInvL2Norm();
			if(simulation()->settings().quadratureMethod==true) { fileStream << " " << atom->weight();}else{
			fileStream << " " << atom->clusterRepatom()->clusterWeight();}
			fileStream << " " << atom->clusterRepatom()->numSamplingAtoms();
			fileStream << " " << atom->clusterRepatom()->index() ;
			for (int s = 0; s<6; s++){
			  fileStream << " " << atom->clusterRepatom()->getStressComponent(s);
			}
		
			FloatType VonMisesEqStress = 0.0;
			VonMisesEqStress += (atom->clusterRepatom()->getStressComponent(0)-atom->clusterRepatom()->getStressComponent(1))*(atom->clusterRepatom()->getStressComponent(0)-atom->clusterRepatom()->getStressComponent(1));
			VonMisesEqStress += (atom->clusterRepatom()->getStressComponent(1)-atom->clusterRepatom()->getStressComponent(2))*(atom->clusterRepatom()->getStressComponent(1)-atom->clusterRepatom()->getStressComponent(2));
			VonMisesEqStress += (atom->clusterRepatom()->getStressComponent(2)-atom->clusterRepatom()->getStressComponent(1))*(atom->clusterRepatom()->getStressComponent(2)-atom->clusterRepatom()->getStressComponent(1));
			VonMisesEqStress += 6*((atom->clusterRepatom()->getStressComponent(3)*atom->clusterRepatom()->getStressComponent(3)) + (atom->clusterRepatom()->getStressComponent(4)*atom->clusterRepatom()->getStressComponent(4)) + (atom->clusterRepatom()->getStressComponent(5)*atom->clusterRepatom()->getStressComponent(5)));
			VonMisesEqStress = sqrt(0.5*VonMisesEqStress);
			
			fileStream << " " << VonMisesEqStress;
			fileStream << " " << atom->clusterRepatom()->getAtomicVolume();
			
			
			fileStream << endl;
		}

		int npassive = samplingAtoms().size() - nactive;
		if(npassive>0) fileStream << "ZONE T=\"Passive Sampling Atoms\" I=" << npassive << " DATAPACKING=POINT" << endl;
		Q_FOREACH(const SamplingAtom* atom, samplingAtoms()) {
			if(atom->isActive()) continue;
			Vector3 displacement = atom->displacement();
			fileStream << atom->deformedPos().X << " " << atom->deformedPos().Y << " " << atom->deformedPos().Z;
			fileStream << " " << displacement.X << " " << displacement.Y << " " << displacement.Z;
			if(output) fileStream << " 0 0 0 0 0";
			fileStream << " 0 0 0 -1" ;
			for (int s = 0; s<8; s++){
			  fileStream << " " << "0" ;
			}
			fileStream << endl;
		}
*/
	}
}

/******************************************************************************
* Exports the QC mesh to the given file for visualization with Tecplot.
******************************************************************************/
/*
void Mesh::exportMeshToTecplot(const QString& filename, CalculationResult* forces)
{
	MsgLogger() << logdate << "Saving mesh to Tecplot file" << filename << endl;
	
	if(forces && forces->forces().size() != repatoms().size()) 
		forces = NULL;
		
	
	// Open output file.
	QFile tecplotFile(filename);
	if(!tecplotFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh to file %1. Cannot open file for writing: %2").arg(filename).arg(tecplotFile.errorString()));
	QTextStream fileStream(&tecplotFile);

	fileStream << "VARIABLES=\"UX\" \"UY\" \"UZ\" \"Index\" ";
	fileStream << endl;
	// Write nodes to Tecplot file.
		
	if(!samplingAtoms().empty()) {
		// Count the number of active/passive sampling atoms.
		for(int i=0; i<samplingAtoms().size(); i++) {	
			SamplingAtom* atom =  samplingAtoms()[i];
		//Q_FOREACH(const SamplingAtom* atom, samplingAtoms()) {
			if(!atom->isActive()) continue;
			Vector3 displacement = atom->displacement();
			//fileStream << " " << atom->pos().X << " " << atom->pos().Y << " " << atom->pos().Z;
			fileStream << " " << displacement.X << " " << displacement.Y << " " << displacement.Z;
			fileStream << " " << atom->getAtomicIndex();
			fileStream << endl;
		}

		for(int i=0; i<samplingAtoms().size(); i++) {	
			SamplingAtom* atom =  samplingAtoms()[i];
		//Q_FOREACH(const SamplingAtom* atom, samplingAtoms()) {
			if(atom->isActive()) continue;
			Vector3 displacement = atom->displacement();
			//fileStream << " " << atom->pos().X << " " << atom->pos().Y << " " << atom->pos().Z;
			fileStream << " " << displacement.X << " " << displacement.Y << " " << displacement.Z;
			fileStream << " " << atom->getAtomicIndex();
			fileStream << endl;
		}
	}
}
*/

/******************************************************************************
* Exports the repatom/sampling atoms to the given LAMMPS dump file for visualization with the OVITO program.
******************************************************************************/
void Mesh::exportMeshToDump(const QString& filename, CalculationResult* output)
{
	MsgLogger() << logdate << "Saving mesh to dump file" << filename << endl;
	
	if(output && output->forces().size() != repatoms().size()) 
		output = NULL;
	
	// Open output file.
	QFile dumpFile(filename);
	if(!dumpFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh to file %1. Cannot open file for writing: %2").arg(filename).arg(dumpFile.errorString()));
	QTextStream fileStream(&dumpFile);

	fileStream << "ITEM: TIMESTEP" << endl;
	fileStream << "0" << endl;
	fileStream << "ITEM: NUMBER OF ATOMS" << endl;
	fileStream << repatoms().size() << endl;
	fileStream << "ITEM: BOX BOUNDS" << endl;
	Box3 simCell = simulation()->simulationCell() * Box3(Point3(0,0,0),Point3(1,1,1));
	for(size_t k=0; k<3; k++)
		fileStream << simCell.minc[k] << " " << simCell.maxc[k] << endl;
	fileStream << "ITEM: ATOMS id type x y z displacement.x displacement.y displacement.z displacement.mag clusterWeight numSamplingAtoms";
	if(output)
		fileStream << " fx fy fz reforce.x reforce.y reforce.z internalEnergy externalEnergy";

	if (output->centrosymmetry().size())
		fileStream << " centrosymmetry";
	
	if (output->evecs().size())
	{
		int numEvecs = output->evecs().size() / nodes().size();
		for (int i = 0; i < numEvecs; ++i)
			fileStream << " evec[" << i << "].x" << " evec[" << i << "].y" << " evec[" << i << "].z";
	}
	if (nodes()[0]->realRepatom()->getStressComponent(0))
		fileStream << " sigma_v sigma_xx sigma_yy sigma_zz sigma_xy sigma_xz sigma_yz p_hyd";
	
	
	fileStream << endl;
	// Write repatoms to dumpfile.
	for (int i = 0; i < repatoms().size(); ++i)
	{
		RepAtom* repatom = repatoms()[i];
		
		fileStream << (repatom->index() + 1) << " 1 ";
		fileStream << repatom->deformedPos().X << " " << repatom->deformedPos().Y << " " << repatom->deformedPos().Z;
		Vector3 displacement = repatom->displacement();
		fileStream << " " << displacement.X << " " << displacement.Y << " " << displacement.Z;
		fileStream << " " << sqrt(displacement.X * displacement.X + displacement.Y * displacement.Y + displacement.Z * displacement.Z);
		fileStream << " " << repatom->clusterWeight();
		fileStream << " " << repatom->numSamplingAtoms();
		if(output)
		{
			const Vector3& force = output->forces()[repatom->index()];
			fileStream << " " << force.X << " " << force.Y << " " << force.Z;
			const Vector3& reforce = output->reforces()[repatom->index()];
			fileStream << " " << reforce.X << " " << reforce.Y << " " << reforce.Z;
			fileStream << " " << output->internalEnergy()[repatom->index()] << " " << output->externalEnergy()[repatom->index()];
		}
		if (output->centrosymmetry().size())
			fileStream << " " << output->centrosymmetry()[repatom->index()];
		if (output->evecs().size())
		{	
			int numEvecs = output->evecs().size() / nodes().size();
			for (int j = 0; j < numEvecs; ++j)
			{
				Vector3& evec = output->evecs()[j * nodes().size() + repatom->index()];
				fileStream << " " << evec.X << " " << evec.Y << " " << evec.Z;
			}
		}
		if (nodes()[0]->realRepatom()->getStressComponent(0))
		{
			double VonMisesEqStress = 0.0;
			VonMisesEqStress += (repatom->getStressComponent(0)-repatom->getStressComponent(1))*(repatom->getStressComponent(0)-repatom->getStressComponent(1));
			VonMisesEqStress += (repatom->getStressComponent(1)-repatom->getStressComponent(2))*(repatom->getStressComponent(1)-repatom->getStressComponent(2));
			VonMisesEqStress += (repatom->getStressComponent(2)-repatom->getStressComponent(0))*(repatom->getStressComponent(2)-repatom->getStressComponent(0));
			VonMisesEqStress += 6*((repatom->getStressComponent(3)*repatom->getStressComponent(3)) + (repatom->getStressComponent(4)*repatom->getStressComponent(4)) + (repatom->getStressComponent(5)*repatom->getStressComponent(5)));
			VonMisesEqStress = sqrt(0.5*VonMisesEqStress);
		
			if (VonMisesEqStress == VonMisesEqStress) // Prevent NAN values
				fileStream << " " << VonMisesEqStress;
			else
				fileStream << " 0";
			
			for (int j = 0; j < 6; ++j)
			{
				FloatType sigma = repatom->getStressComponent(j);
				if(sigma == sigma) // Prevent NAN values
					fileStream << " " << sigma;
				else
					fileStream << " 0";
			}
			
			double p_hyd = (repatom->getStressComponent(0) + repatom->getStressComponent(1) + repatom->getStressComponent(2)) / 3;
			fileStream << " " << p_hyd;
		}
		
		fileStream << endl;	
	}
}

/******************************************************************************
* Exports the QC mesh to the given file for visualization with the program TetView.
******************************************************************************/
void Mesh::exportMeshToTetView(const QString& filename)
{
	// Write .node file with all mesh nodes.
	QString nodeFilename = filename + ".node";
	MsgLogger() << logdate << "Saving mesh nodes to TetView file" << nodeFilename << endl;
	QFile nodeFile(nodeFilename);
	if(!nodeFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh nodes to file %1. Cannot open file for writing: %2").arg(nodeFilename).arg(nodeFile.errorString()));
	QTextStream nodeFileStream(&nodeFile);

	QMap<RepAtom*, int> nodeIndices;
	// Write header of .node file
	nodeFileStream << nodes().size() << " 3 0 0" << endl;
	// Write one line per node
	for(int i=0; i<nodes().size(); i++) {
		RepAtom* node = nodes()[i];
		nodeIndices[node] = i+1;
		nodeFileStream << (i+1) << " " << node->deformedPos().X << " " << node->deformedPos().Y << " " << node->deformedPos().Z << endl;
	}
	
	// Write .ele file with all elements.
	QString elementFilename = filename + ".ele";
	MsgLogger() << "Saving mesh elements to TetView file" << elementFilename << endl;
	QFile elementFile(elementFilename);
	if(!elementFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh elements to file %1. Cannot open file for writing: %2").arg(elementFilename).arg(nodeFile.errorString()));
	QTextStream elementFileStream(&elementFile);

	// Write header of .ele file
	elementFileStream << elements().size() << " " << NEN << " 0" << endl;
	// Write one line per element
	for(int i=0; i<elements().size(); i++) {
		Element* el = elements()[i];
		elementFileStream << (i+1);
		for(int j=0; j<NEN; j++)
			elementFileStream << " " << nodeIndices[el->vertex(j)];
		elementFileStream << endl;
	}
}

void Mesh::exportAtoms(const QString& filename, CalculationResult* forces)
{
	MsgLogger() << logdate << "Saving centroSymmetry parameter to Tecplot file" << filename << endl;

	// Open output file.
	QFile tecplotFile(filename);
	if(!tecplotFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh to file %1. Cannot open file for writing: %2").arg(filename).arg(tecplotFile.errorString()));
	QTextStream fileStream(&tecplotFile);

	fileStream << "VARIABLES=\"X\" \"Y\" \"Z\" \"UX\" \"UY\" \"UZ\" \"CentroSymmetry\"  \"sigma11\"  \"sigma22\"  \"sigma33\"   \"sigma12\"  \"sigma13\"   \"sigma23\"  \"Veq\"  \"AtomicVolume\" " << endl;
	fileStream << "ZONE T=\"Atoms\" I=" << forces->getCentroSymmetryAtoms() << " DATAPACKING=POINT" << endl;

	MsgLogger() << forces->getCentroSymBox().minc << forces->getCentroSymBox().maxc << endl;
	// Write nodes to Tecplot file.
		if(!samplingAtoms().empty()) {

				Q_FOREACH(SamplingAtom* atom, forces->centroatoms) {
					if(!forces->getCentroSymBox().contains(atom->pos(),FLOATTYPE_EPSILON)) continue;
					fileStream << atom->deformedPos().X << " " << atom->deformedPos().Y << " " << atom->deformedPos().Z << " ";
					fileStream << atom->displacement().X << " " << atom->displacement().Y << " " << atom->displacement().Z;
					fileStream << " " << atom->getCentroSymmetry() ;
					for (int s = 0; s<6; s++){
					  fileStream << " " << atom->getStressComponent(s);
					}
					FloatType VonMisesEqStress = 0.0;
					VonMisesEqStress += (atom->getStressComponent(0)-atom->getStressComponent(1))*(atom->getStressComponent(0)-atom->getStressComponent(1));
					VonMisesEqStress += (atom->getStressComponent(1)-atom->getStressComponent(2))*(atom->getStressComponent(1)-atom->getStressComponent(2));
					VonMisesEqStress += (atom->getStressComponent(2)-atom->getStressComponent(1))*(atom->getStressComponent(2)-atom->getStressComponent(1));
					VonMisesEqStress += 6*((atom->getStressComponent(3)*atom->getStressComponent(3)) + (atom->getStressComponent(4)*atom->getStressComponent(4)) + (atom->getStressComponent(5)*atom->getStressComponent(5)));
					VonMisesEqStress = sqrt(0.5*VonMisesEqStress);
			
					fileStream << " " << VonMisesEqStress;

					fileStream << " " << atom->getAtomicVolume();
					fileStream <<  " " << endl;




				}

		}
}


void Mesh::exportDXAdump(const QString& filename, const Box3& box, CalculationResult* forces)
{
  
	
	MsgLogger() << logdate << "Saving mesh to dump file" << filename << endl;
	int counter = 0;
	
	// Open output file.
	QFile dumpFile(filename);
	if(!dumpFile.open(QIODevice::WriteOnly | QIODevice::Text))
		throw Exception(QString("Failed to export mesh to file %1. Cannot open file for writing: %2").arg(filename).arg(dumpFile.errorString()));
	QTextStream fileStream(&dumpFile);

	fileStream << "ITEM: TIMESTEP" << endl;
	fileStream << "0" << endl;
	fileStream << "ITEM: NUMBER OF ATOMS" << endl;
	fileStream << forces->getCentroSymmetryAtoms() << endl;
	fileStream << "ITEM: BOX BOUNDS" << endl;
	Box3 simCell = simulation()->simulationCell() * Box3(Point3(0,0,0),Point3(1,1,1));
	for(size_t k=0; k<3; k++)
		fileStream << simCell.minc[k] << " " << simCell.maxc[k] << endl;
	fileStream << "ITEM: ATOMS x y z" << endl;
	int index = 1;
	// Write repatoms to dumpfile.
	if(!samplingAtoms().empty()) {
		Q_FOREACH(SamplingAtom* atom, samplingAtoms()) {
					  if(!box.contains(atom->pos(),FLOATTYPE_EPSILON)) continue;
					  counter++;
					  fileStream << atom->deformedPos().X << " " << atom->deformedPos().Y << " " << atom->deformedPos().Z << " ";
					  fileStream << " " << endl;
					  index++;
		}  
	}
}


void Mesh::exportDislocation(const QString& filename,const Box3& box)
{
  
    MsgLogger() << logdate << "Saving dislocation data file" << filename << endl;
    // Open output file.
    QFile dumpFile(filename);
    if(!dumpFile.open(QIODevice::WriteOnly | QIODevice::Text))
	    throw Exception(QString("Failed to export mesh to file %1. Cannot open file for writing: %2").arg(filename).arg(dumpFile.errorString()));
    QTextStream fileStream(&dumpFile);
    
    for(int i = 0; i < nodes().size(); i++){
	RepAtom* rep = nodes()[i];
	if(!box.contains(rep->pos(),FLOATTYPE_EPSILON)) continue;
	
	fileStream << rep->site().indices().X << " " << rep->site().indices().Y << " " << rep->site().indices().Z << " ";
	fileStream << rep->displacement().X << " " << rep->displacement().Y << " " << rep->displacement().Z << endl;      
	
	
    }
}

void Mesh::deflectOnEvec(CalculationResult* output, int index, FloatType scale)
{
	for(int i = 0; i < repatoms().size(); ++i)
	{
		RepAtom* repatom = repatoms()[i];
		repatom->setDeformedPos(repatom->deformedPos() + output->evecs()[repatoms().size() * index + i] * scale);
	}
}

}; // End of namespace MAFEM
