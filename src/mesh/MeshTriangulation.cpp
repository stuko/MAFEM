///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "Mesh.h"
#include <simulation/Simulation.h>
#include <force/CalculationResult.h>
#include "tetgen.cxx"
#include "predicates.cxx"

namespace MAFEM {

/******************************************************************************
* Creates a new repatom at the given lattice site.
******************************************************************************/
RepAtom* Mesh::createNode(const LatticeSite& site)
{
	// Allocate memory for another repatom/node and initialize it with the site. 
	RepAtom* atom = _repatomPool.construct(site);
	if(atom == NULL) throw OutOfMemoryException();		
	_nodes.push_back(atom);
	
	// Add new node to the groups if it is inside their regions.
	for(int g = 1; g < MAX_GROUP_COUNT; g++) {
		Group* group = simulation()->group(g);
		if(!group) continue;
		if(!group->region())
			atom->addToGroup(group);			
		else if(group->region()->contains(site.pos()))
			atom->addToGroup(group);
	}
	
	return atom;
}

/******************************************************************************
* Create a new element and adds it to the mesh.
******************************************************************************/
Element* Mesh::createElement(const array<RepAtom*, NEN>& vertices)
{
	Element* el = _elementPool.construct(vertices);
	if(el == NULL) throw OutOfMemoryException();		
	_elements.push_back(el);
	return el;
}

/******************************************************************************
* Adds an existing element to the mesh.
******************************************************************************/
Element* Mesh::addElement(const Element& element) 
{
	Element* el = _elementPool.malloc();
	if(el == NULL) throw OutOfMemoryException();
	memcpy(el, &element, sizeof(Element));
	_elements.push_back(el);
	return el;
}

/******************************************************************************
* Creates the mesh by tetgen that connects the previously added RepAtoms.
******************************************************************************/
void Mesh::triangulateTetgen()
{
	if(nodes().size() == 0)
			throw Exception("Cannot build mesh. You have to add some repatoms first.");

		// Check simulation box geometry.
		const AffineTransformation& simCell = simulation()->simulationCell();
		if(simCell.determinant() <= 0.0)
			throw Exception("Simulation box is empty or has not been defined. Use Simulation::setSimulationCell() first.");

		const array<bool,NDOF> pbc = simulation()->settings().periodicBC;


			// Measure the time it taskes to generate the mesh.
			timer triangulationTimer;

			MsgLogger() << logdate << "Sending" << nodes().size() << "nodes to external Tetgen program for triangulation." << endl;

			ofstream myfile;
			myfile.open ("mesh.node");
			//myfile.clear();
			myfile << nodes().size() << " " << 3 << " " << 0 << " " <<  0 << endl;
			for(int i = 0; i < nodes().size(); i++){
				RepAtom* node = nodes()[i];
				myfile << i << " " << node->pos().X << " " << node->pos().Y << " " << node->pos().Z << " " << endl;
			}
			myfile.close();

			//system("./tetgen -n mesh.node");

			int n,num;
			fstream file;
			 file.open ("mesh.1.ele");
			 file >> num >> n >> n >> n;
			 file.close();

			int **e;
			e = new int*[num];
			int n1,n2,n3,n4;
			 array<RepAtom*, NEN> vertices;

			  QVector<Element*> tetgenElements(num);
			  fstream myfile1;
			  fstream myfile2;

			for(int i = 0; i < num; ++i) {
			e[i] = new int[4];
			}

			  myfile1.open ("mesh.1.ele");
			  myfile1 >> n >> n >> n;
			  for(int i = 0; i < num; i++){

			  myfile1 >> n >> e[i][0] >> e[i][1] >> e[i][2] >> e[i][3];
				n1 = e[i][0] ;
				n2 = e[i][1];
				n3 = e[i][2];
				n4 = e[i][3];

				//MsgLogger() << n1 << " " << n2 << " " << n3 << " " << n4 << " " << endl;
				vertices[0] = nodes()[n1];
				vertices[1] = nodes()[n2];
				vertices[2] = nodes()[n3];
				vertices[3] = nodes()[n4];

				Element element(vertices);
				if(element.isUnnecessaryElement()) {
					tetgenElements[i] = NULL;
				}
				else{
					tetgenElements[i] = addElement(element);
				}

			  }


			 myfile2.open ("mesh.1.neigh");
			 myfile2 >> n >> n;
			for(int i = 0; i < num; i++){
			  myfile2 >> n >> e[i][0] >> e[i][1] >> e[i][2] >> e[i][3];

				if (tetgenElements[i] == NULL) {

			}else{

			  Element* element = tetgenElements[i];
				for(int j = 0; j < 4;j++){
					int k = e[i][j];
					if(k == -1) {
					element->setNeighbor(j, NULL);
					}
					else {
					Element* neighbor = tetgenElements[e[i][j]];
					element->setNeighbor(j, neighbor);
					}

				}
			}
		}

			for(int i = 0; i < num; ++i) {
			delete[] e[i];
			}

			delete[] e;
			myfile1.close();
			myfile2.close();

}

/******************************************************************************
* Creates the mesh that connects the previously added RepAtoms.
******************************************************************************/
void Mesh::triangulate()
{	
	if(nodes().size() == 0)
		throw Exception("Cannot build mesh. You have to add some repatoms first.");

	if(simulation()->settings().octreeElementSearch == true)
		throw Exception("Cannot use octree element search method. You have to set segment tree element search method.");

	// Measure the time it taskes to generate the mesh.
	timer triangulationTimer;	

	MsgLogger() << logdate << "Sending" << nodes().size() << "nodes to external Qhull program for triangulation." << endl;

	// Clear previous triangulation.
	if(!elements().empty()) {
		MsgLogger() << "  Clearing previous triangulation." << endl;
		Q_FOREACH(Element* el, elements())
			_elementPool.destroy(el);
		_elements.clear();
	}
	
	// Invoke the Qhull package for Delaunay triangulation.
	QProcess process;
	process.start("qdelaunay", QStringList() << "QJ" << "Fv" << "Fn");
	if(!process.waitForStarted())
		throw Exception("Failed to invoke the qdelaunay program. Please check your Qhull installation.");
	
	// Send nodes to QHull process.
	QTextStream ostream(&process);
	ostream << "3" << endl;	// Write number of dimensions
	ostream << nodes().size() << endl;
	Q_FOREACH(const RepAtom* node, nodes()) {
		ostream << node->pos().X << " " << node->pos().Y << " " << node->pos().Z << endl;
	}
	ostream.flush();
	process.closeWriteChannel();
	
	// Let Qhull process the input.
	if(!process.waitForFinished(-1))
		throw Exception("The qdelaunay process did not finish properly.");

	// Parse th Qhull output.
	QByteArray delaunayOutput = process.readAllStandardOutput();
	QTextStream istream(delaunayOutput);
	int numDelaunayRegions, vertexCount;
	istream >> numDelaunayRegions;
	if(numDelaunayRegions <= 0) {
		MsgLogger() << "The qdelaunay output was:" << endl << delaunayOutput << endl;
		MsgLogger() << "The qdelaunay error output was:" << endl << process.readAllStandardError() << endl;
		throw Exception("Meshing error: qdelaunay returned no Delaunay regions.");
	}
	
	MsgLogger() << "  Number of Delaunay regions:" << numDelaunayRegions << endl; 
	QVector<Element*> qhullElements(numDelaunayRegions);
	for(int i=0; i<numDelaunayRegions; i++) {
		int numVertices;
		istream >> numVertices;
		if(numVertices != NEN) { 
			MsgLogger() << "The qdelaunay output was:" << endl << delaunayOutput << endl;
			MsgLogger() << "The qdelaunay error output was:" << endl << process.readAllStandardError() << endl;
			throw Exception("Meshing error: qdelaunay returned a Delaunay region that does not have 4 vertices.");
		}
		array<RepAtom*, NEN> vertices;
		for(int j=0; j<NEN; j++) {
			int index;
			istream >> index;
			if(index < 0 || index >= nodes().size()) {
				MsgLogger() << "The qdelaunay output was:" << endl << delaunayOutput << endl;
				MsgLogger() << "The qdelaunay error output was:" << endl << process.readAllStandardError() << endl;
				throw Exception(QString("Meshing error: Vertex index in Delaunay region %1 is out of range.").arg(i));
			}
			vertices[j] = nodes()[index];
		}		
		Element element(vertices);
		if(simulation()->settings().keepFullyAtomisticElements) {
			// Skip only zero volume elements.
			if(element.volume() < FLOATTYPE_EPSILON)
				continue;
		}				
		else if(element.isUnnecessaryElement()) {
			// Skip elements in fully atomistic regions.
			continue;			
		}
		qhullElements[i] = addElement(element);
	}
	
	// Parse neighbor information.
	int temp;
	istream >> temp;
	if(temp != numDelaunayRegions) {
		MsgLogger() << "The qdelaunay output was:" << endl << delaunayOutput << endl;
		MsgLogger() << "The qdelaunay error output was:" << endl << process.readAllStandardError() << endl;
		throw Exception("Meshing error: qdelaunay returned no valid element neighbor information.");
	}
	for(int i=0; i<numDelaunayRegions; i++) {
		Element* element = qhullElements[i];
		int numNeighbors;
		istream >> numNeighbors;
		if(numNeighbors != NEN) { 
			MsgLogger() << "The qdelaunay output was:" << endl << delaunayOutput << endl;
			MsgLogger() << "The qdelaunay error output was:" << endl << process.readAllStandardError() << endl;
			throw Exception("Meshing error: qdelaunay returned a Delaunay region that does not have 4 neighbors.");
		}
		for(int j=0; j<NEN; j++) {
			int index;
			istream >> index;
			if(index < 0 || element == NULL) continue;
			if(index >= qhullElements.size()) {
				MsgLogger() << "The qdelaunay output was:" << endl << delaunayOutput << endl;
				MsgLogger() << "The qdelaunay error output was:" << endl << process.readAllStandardError() << endl;
				throw Exception(QString("Meshing error: Index of neighbor element of Delaunay region %1 is out of range.").arg(i));
			}
			Element* neighbor = qhullElements[index];
			if(!neighbor) continue;
			// Find out which 3 vertices are shared by the neighbor element.
			int nshared = 0;
			int neighborIndex;
			for(int k=0; k<NEN; k++) {
				if(neighbor->hasVertex(element->vertex(k)))
					nshared++;
				else
					neighborIndex = k;
			}
			MAFEM_ASSERT(nshared == 3);
			// The fourth non-shared vertex determines the shared face.
			element->setNeighbor(neighborIndex, neighbor);
		}
	}
	
	MsgLogger() << "  Total time for triangulation:" << triangulationTimer.elapsed() << "sec." << endl;
}
/******************************************************************************
* Creates the Octree mesh
******************************************************************************/
void Mesh::octreeMesh()
{
	if(simulation()->settings().octreeElementSearch == false && simulation()->settings().segmentTreeElementSearch == false  )
			throw Exception("set one of the element search method. either Octree search or segment tree search");

	MAFEM_ASSERT(_octrees.empty());

	// Create the element octree for each grain.
	int g = 0;
	Q_FOREACH(const shared_ptr<Grain>& grain, simulation()->grains())
	{
		if(!simulation()->settings().latticeStaticsMode)
		{
			// Create an octree for the grain.
			ElementOctree* octree = new ElementOctree();
			octree->setSimulation(simulation());
			octree->setGrainIndex(g);
			octree->CreateInitialMesh();
			_octrees.push_back(octree);
			++g;
		}
	}
}

/******************************************************************************
* Checks the topology of the mesh. Throws an exception if the mesh is not valid.
******************************************************************************/
void Mesh::checkTriangulation()
{
	MsgLogger() << logdate << "Checking triangulation." << endl;
	
	Q_FOREACH(const RepAtom* node, nodes())
	{
		// Make sure the node is not inside a non-adjacent element.
		QVector<Element*> containingElements = elements().findContainingElements(node->site());
		Q_FOREACH(const Element* e, containingElements)
			if(!e->hasVertex(node))
				throw Exception(QString("Node at position %1 is inside non-adjacent element.").arg(node->pos().toString()));
	}
	
	MsgLogger() << "  Mesh is valid." << endl;
}

/******************************************************************************
* Builds the initial mesh and checks whether the mesh is compatible with any 
* periodic boundary conditions.
******************************************************************************/
void Mesh::generateInitialMesh()
{
	// Check simulation box geometry.
	const AffineTransformation& simCell = simulation()->simulationCell();
	if(simCell.determinant() <= 0.0)
		throw Exception("Simulation box is empty or has not been defined. Use Simulation::setSimulationCell() first.");

	if(!simulation()->settings().latticeStaticsMode)
	{
		// Makes shure the initial triangulation has been generated.
		if(simulation()->settings().qhullMesh) triangulate();
		if(simulation()->settings().tetgenMesh) triangulateTetgen();
		if(simulation()->settings().octreeMesh) octreeMesh();
	}	

	UpdateMesh(1);
}

/******************************************************************************
* updates the mesh
******************************************************************************/
void Mesh::UpdateMesh(int cluster)
{

if(simulation()->settings().octreeMesh)
{
	// Set the dirty flag for nodes and elements.
	for(int i = 0; i < repatoms().size(); ++i)
	{
		RepAtom* r = repatoms().at(i);
		(r)->SetDirty(true);
		(r)->pos();
	}

	int index = 0;
	for(int i = 0; i < elements().size(); ++i)
	{
		Element* el = elements().at(i);
	   (el)->SetDirty(true);
	   (el)->SetIndex(index++);
	}
}

		const AffineTransformation& simCell = simulation()->simulationCell();
		const array<bool,NDOF> pbc = simulation()->settings().periodicBC;

	    // Check whether all nodes are inside the simulation cell.
		MsgLogger() << logdate << "Checking node positions and simulation cell geometry." << endl;
		MsgLogger() << "  Periodic boundary conditions: X=" << pbc[0] << "Y=" << pbc[1] << "Z=" << pbc[2] << endl;

		/*for(int i=0; i<nodes().size(); i++) {
				RepAtom* a1 = nodes()[i];
				if(i > (nodes().size()-51)) MsgLogger() << i << a1->pos() << endl;
		}*/

		for(int i=0; i<nodes().size(); ++i)
		{
			RepAtom* a1 = nodes()[i];
			Point3 reducedPoint1 = simulation()->inverseSimulationCell() * a1->pos();
			int mind[3], maxd[3];

			for(int k=0; k<NDOF; k++)
			{
				if(reducedPoint1[k] < -FLOATTYPE_EPSILON || reducedPoint1[k] > 1.0+FLOATTYPE_EPSILON)
					throw Exception(QString("Detected node outside simulation cell at coordinates %1").arg(a1->pos().toString()));
					
				mind[k] = (pbc[k] && reducedPoint1[k] > 1.0 + FLOATTYPE_EPSILON) ? -1 : 0;
				maxd[k] = (pbc[k] && reducedPoint1[k] < FLOATTYPE_EPSILON) ? 1 : 0;
			}

			// Check if periodic images for this node exist.
			for(int dx = mind[0]; dx <= maxd[0]; ++dx)
			{
				for(int dy = mind[1]; dy <= maxd[1]; ++dy)
				{
					for(int dz = mind[2]; dz <= maxd[2]; ++dz)
					{
						Point3 imagePos = a1->pos() + simCell * Vector3(dx, dy, dz);
						LatticeSite imageSite(a1->site().lattice(), imagePos);
						if(imageSite.pos().equals(imagePos, FLOATTYPE_EPSILON) == false)
							throw Exception("Non-periodic lattice detected. Lattice is not compatible with periodic boundary conditions and simulation cell size.");

						RepAtom* imageNode = nodes().findAt(imageSite);
						if(dx == 0 && dy == 0 && dz == 0)
						{
							 if(imageNode != a1)
								 throw Exception(QString("Detected two repatoms at the same coordinates: %1").arg(a1->pos().toString()));
							 else
							 	continue;
						}
						if(!imageNode)
							throw Exception(QString("Repatom %1 at coordinates %2 at the simulation cell boundary does not have enough periodic images.\nExpected periodic image at coordinates %3.").arg(i).arg(a1->pos().toString()).arg(imagePos.toString()));

						// Associate all periodic images with the master repatom.
						if(a1->periodicImageMaster() == NULL)
							imageNode->setPeriodicImageMaster(a1);
					}
				}
			}
		}


		// Collect real repatoms.
		_repatoms.clear();
		
		bool rcheck = false;
		for (int n=0; n < nodes().size(); n++) {
			RepAtom* node = nodes()[n];
			rcheck = false;
			if(node->periodicImageMaster() == NULL){
				for(int i=0; i<_removeSite.size(); i++){
					if(_removeSite[i]->equals(node->pos(),FLOATTYPE_EPSILON)) { rcheck=true; break;}
				}
				//_repatoms.push_back(node);
				if(rcheck) {node->SetDirty(true);}
				if(!rcheck) {node->SetDirty(false);}
			}
		}


		for (int n=0; n < nodes().size(); n++) {
			RepAtom* node = nodes()[n];
			if(!node->IsDirty()) _repatoms.push_back(node);
		}
		


		MsgLogger() << "  Number of mesh nodes:" << nodes().size() << endl;
		MsgLogger() << "  Number of real repatoms:" << repatoms().size() << endl;
		MsgLogger() << "  Number of periodic image repatoms:" << (nodes().size() - repatoms().size()) << endl;
		MsgLogger() << "  Number of necessary elements:" << elements().size() << endl;

		MsgLogger() << "  Lattice statics mode:" << simulation()->settings().latticeStaticsMode << endl;
		MsgLogger() << "  Force-based QC mode:" << simulation()->settings().QCForceMode << endl;

		// Finally build the sampling clusters.
		if(cluster == 1){
		    if(simulation()->settings().quadratureMethod){
			    buildQuadPoints();
		    }else{
			    buildSamplingClusters();
		    }
		}

if(simulation()->settings().octreeMesh)
{
	// Reset the dirty flag for nodes and elements.
	for(int i = 0; i < repatoms().size(); i++)
		repatoms().at(i)->SetDirty(false);

	for(int i = 0; i < elements().size(); i++)
		elements().at(i)->SetDirty(false);
}

}
/******************************************************************************
* Deletes all elements that have been marked for deletion.
******************************************************************************/
void Mesh::deleteMarkedElements()
{
	throw Exception("Mesh::deleteMarkedElements(): This function is not supported yet.");
	
	MsgLogger() << logdate << "Deleting marked elements." << endl;
	boost::progress_display progress(elements().size());
	
	int j = 0;
	for(int i = 0; i < elements().size(); ++i, ++progress) {
		Element* element = elements()[i];
		if(element->isMarked()) {
			// Disconnect element from neighbors.
			for(int n=0; n<NEN; n++) {
				if(element->neighbor(n) != NULL) {
					element->neighbor(n)->setNeighbor(element->neighbor(n)->neighborIndex(element), NULL);
				}
			}
			// Delete element.			
			_elementPool.destroy(element);
		}
		else {
			_elements[j] = element;
			j++;
		}
	}
	_elements.resize(j);
}




/// Octree method development
void Mesh::RefineLocally(const Box3& box, int toLevel)
{
	MsgLogger() << "Refining mesh locally" << endl;

	for (int n = 0; n < octrees().size(); ++n)
	{
		ElementOctree* octree = octrees().at(n);

		GrainBoundary* boundary = new GrainBoundary();
		int index = 0;
		for (int i = 0; i < simulation()->grains().size(); ++i)
			if (octree->GetSimulation()->grains()[octree->GrainIndex()] == simulation()->grains()[i])
				index = i;

		boundary->setGrainIndex(index);
		
		if (!octree->GetSimulation()->grains()[octree->GrainIndex()]->Boundary().GetWorldBoundingBox().intersects(box))
			continue;		// Refinement region is completely outside the grain boundary.

		// Create a box boundary object.
		for (int i = 0; i < 8; ++i)
			boundary->CreateVertex(octree->GetSimulation()->grains()[octree->GrainIndex()],box[i]);
		
		boundary->CreateQuadFace(0, 1, 5, 4);
		boundary->CreateQuadFace(1, 3, 7, 5);
		boundary->CreateQuadFace(3, 2, 6, 7);
		boundary->CreateQuadFace(2, 0, 4, 6);
		boundary->CreateQuadFace(4, 5, 7, 6);
		boundary->CreateQuadFace(0, 2, 3, 1);

		// Refine octree.
		octree->RefineBoundary(boundary, toLevel);
	}
/*	
	for (int i = 0; i < nodes().size(); i++)
	{
		Element* el = elements().findAt(nodes()[i]->site());
		if (el == NULL)
		{
			nodes().remove(nodes().indexOf(nodes()[i]));
	   
			MsgLogger() << "I am here" << endl;
		}
	}
*/
	UpdateMesh(1);
}

/// Refines the mesh if necessary.
bool Mesh::AdaptiveRefinement(const Box3& box, FloatType tolerance)
{
	int numRefine = 0;
	FloatType maxIndicator = 0.0;
	bool boxCheck = false;


	for(int e = 0; e < elements().size(); e++){

		Element* element = elements()[e];
		
		boxCheck = false;
		for(int n = 0; n <4; n++){
		  if(box.contains(element->vertex(n)->pos())) { boxCheck=true; break;}
		}
		if(!boxCheck) continue;

		Tensor2 H(NULL_MATRIX);
		for(size_t k=0; k<NEN; k++){
			for(size_t i=0; i<NDOF; i++) {
				if(e == 1e100) MsgLogger() << " " << endl;
				Vector3 displacement = element->vertex(k)->deformedPos() - element->vertex(k)->pos();
				H.column(i) += element->GetDSh(k)[i] * displacement;
			}
		}

		// Compute deformation gradient F = H + 1
		Tensor2 I(IDENTITY);
		Tensor2 F(NULL_MATRIX);
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				F(i,j) = H(i,j) + I(i,j);
			}
		}


		// Compute Green-Lagrange strain tensor: E = 1/2 * (F^T * F - 1)
		SymmetricTensor2 E = 0.5 * (Product_AtA(F) - IDENTITY);
		SymmetricTensor2 devE;
		FloatType trE = E.get(0,0) + E.get(1,1) + E.get(2,2);
		for(int i = 0; i < NDOF; i++)
			devE.set(i,i,E.get(i,i)-(trE/3.0));

		devE.set(0,1,E.get(0,1));
		devE.set(0,2,E.get(0,2));
		devE.set(1,2,E.get(1,2));
		devE.set(1,0,E.get(0,1));
		devE.set(2,0,E.get(0,2));
		devE.set(2,1,E.get(1,2));

		// Compute the adaption indicator.
		FloatType indicator = std::sqrt(std::abs(devE.secondInvariant()));   //// second invariant of deviotoric strain
		//FloatType indicator = std::sqrt(std::abs(E.secondInvariant())) / element->volume(); //// refinement indicator based on the element volume
		element->setDevStrainSecInvL2Norm(indicator);

		for(int j = 0; j < NEN; j++){
			if(indicator > element->vertex(j)->getMaxDevStrainSecInvL2Norm()) element->vertex(j)->setMaxDevStrainSecInvL2Norm(indicator);
		}


		// Skip elements that are too small for refinement.
		if(element->GetFlag(Element::SMALL_FLAG))
					continue;
		//MsgLogger() << e << indicator << endl;
		maxIndicator = std::max(maxIndicator, indicator);

		if(indicator > tolerance) {
			// Element is targeted for refinement.
			element->SetFlag(Element::ADAPTION_FLAG);
			numRefine++;
		}
	}

	_maxRefIndicator =  maxIndicator;

	if(tolerance < 1e50){
		MsgLogger() << "Maximum refinement indicator: " << _maxRefIndicator << endl;
		MsgLogger() << numRefine << " elements targeted for refinement." << endl;
	}

	if(numRefine) {
		bool hasRefined = false;
		for (int o = 0; o < octrees().size(); o++){
			ElementOctree* octree  =  octrees()[o];
			hasRefined = octree->RefineFlaggedElements() || hasRefined;
		}

		if(hasRefined) {
			/*UpdateMesh(0);
			bool check = AdaptiveRefinement(box,tolerance);

			if(check == false) { UpdateMesh(1); return true;}
			return true;*/

			UpdateMesh(1);
			return true;	
		}
	}
	return false;
}

/// finds the element for given site through octree structure
Element* Mesh::FindElement(const LatticeSite& site, const shared_ptr<Grain>& grain)
{
	for(int i = 0; i < octrees().size(); i++){
		ElementOctree* octree = octrees().at(i);
		if(octree->GetSimulation()->grains()[octree->GrainIndex()] == grain) {
			return octree->FindElement(site);
		}
	}
	return NULL;
}

Point3 Mesh::trackNode(int nodeNum)
{
	if(nodeNum > nodes().size()) throw Exception("Mesh::trackNode(): node number exceeds the total number of representative atom.");
	return nodes()[nodeNum]->deformedPos();
}


void Mesh::createVertex(int grainIdx, Point3 p)
{
	BoundaryVertex* v = simulation()->grains()[grainIdx]->Boundary().CreateVertex(simulation()->grains()[grainIdx],p);
}

void Mesh::createTriFace(int grainIdx, int face1, int face2, int face3)
{
	BoundaryFace* f = simulation()->grains()[grainIdx]->Boundary().CreateTriFace(face1,face2,face3);
}

void Mesh::createQuadFace(int grainIdx, int face1, int face2, int face3, int face4)
{
	BoundaryFace* f = simulation()->grains()[grainIdx]->Boundary().CreateQuadFace(face1,face2,face3,face4);
}

void Mesh::importVTK(const QString& filename, int grainIdx, const AffineTransformation& transformation, bool faceInversion)
{
	MsgLogger() << logdate << "Importing VTK file" << filename << "." << endl;
	
	QString line;
	QStringList lineList;
	int numVertsInGrain = simulation()->grains()[grainIdx]->Boundary().Vertices().size();
	
	// Open file for reading
	QFile vtkFile(filename);
	if(!vtkFile.open(QIODevice::ReadOnly | QIODevice::Text))
		throw Exception(QString("Failed to import geometry from file %1. Cannot open file for reading: %2").arg(filename).arg(vtkFile.errorString()));
	QTextStream fileStream(&vtkFile);
	
	// Skip header
	for(int i = 0; i < 4; ++i)
		fileStream.readLine();
	
	// Create vertices
	int numPoints = fileStream.readLine().split(" ")[1].toInt();
	int i = 0;
	while(i < numPoints)
	{
		line = fileStream.readLine().replace(QString(","),QString(".")).trimmed();
		lineList = line.split(" ");
		for(int j = 0; j < lineList.length() / 3; ++j)
		{
			Point3 point;
			for(int k = 0; k < 3; ++k)
				point[k] = lineList[j * 3 + k].toDouble();
			createVertex(grainIdx, transformation * point);
			++i;
		}
	}
	
	// Skip empty lines
	line = fileStream.readLine();
	while(line.isEmpty())
		line = fileStream.readLine();
		
	// Create faces
	lineList = line.split(" ");
	int numFaces = lineList[1].toInt();
	if(lineList[0] == QString("POLYGONS"))
	{
		for(int i = 0; i < numFaces; ++i)
		{
			line = fileStream.readLine().trimmed();
			lineList = line.split(" ");
			int numFaceVerts = lineList[0].toInt();
			for(int j = 2; j < numFaceVerts; ++j)
				createTriFace(grainIdx, lineList[1].toInt() + numVertsInGrain, lineList[j + (int)faceInversion].toInt() + numVertsInGrain, lineList[j + 1 - (int)faceInversion].toInt() + numVertsInGrain);
		}
	}

	vtkFile.close();
}

void Mesh::setSiteToBeRemoved(const Point3& site)
{
	/*Point3* atom = _removeSitePool.construct(site);
	if(atom == NULL) throw OutOfMemoryException();
	_removeSite.push_back(atom);*/

	Point3* atom = _removeSitePool.malloc();
	if(atom == NULL) throw OutOfMemoryException();
	memcpy(atom, &site, sizeof(Point3));
	_removeSite.push_back(atom);

}

void Mesh::selectMethod()
{
	if(simulation()->settings().quadratureMethod)
	{
	    buildQuadPoints();
    }
    else
    {
	    buildSamplingClusters();
    }
}

}; // End of namespace MAFEM

namespace boost {
namespace archive {
namespace detail {
#if buildLocationCluster
MAFEM::RepAtom* heap_allocation<MAFEM::RepAtom>::invoke_new()
{
	MAFEM_ASSERT(MAFEM::Simulation::serializationInstance() != NULL);
	MAFEM::Mesh& mesh = MAFEM::Simulation::serializationInstance()->mesh();
	return mesh._repatomPool.malloc();
}

void heap_allocation<MAFEM::RepAtom>::invoke_delete(MAFEM::RepAtom* p)
{
	MAFEM_ASSERT(MAFEM::Simulation::serializationInstance() != NULL);
	MAFEM::Mesh& mesh = MAFEM::Simulation::serializationInstance()->mesh();
	return mesh._repatomPool.free(p);
}

MAFEM::Element* heap_allocation<MAFEM::Element>::invoke_new()
{
	MAFEM_ASSERT(MAFEM::Simulation::serializationInstance() != NULL);
	MAFEM::Mesh& mesh = MAFEM::Simulation::serializationInstance()->mesh();
	return mesh._elementPool.malloc();
}

void heap_allocation<MAFEM::Element>::invoke_delete(MAFEM::Element* p)
{
	MAFEM_ASSERT(MAFEM::Simulation::serializationInstance() != NULL);
	MAFEM::Mesh& mesh = MAFEM::Simulation::serializationInstance()->mesh();
	return mesh._elementPool.free(p);
}
#else
MAFEM::RepAtom* heap_allocator<MAFEM::RepAtom>::invoke() 
{
	MAFEM_ASSERT(MAFEM::Simulation::serializationInstance() != NULL);
	MAFEM::Mesh& mesh = MAFEM::Simulation::serializationInstance()->mesh();
	return mesh._repatomPool.malloc();
}

MAFEM::Element* heap_allocator<MAFEM::Element>::invoke() 
{
	MAFEM_ASSERT(MAFEM::Simulation::serializationInstance() != NULL);
	MAFEM::Mesh& mesh = MAFEM::Simulation::serializationInstance()->mesh();
	return mesh._elementPool.malloc();
}
#endif
};
};
};
