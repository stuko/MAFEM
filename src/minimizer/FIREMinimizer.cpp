///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#include "FIREMinimizer.h"

namespace MAFEM {

/******************************************************************************
* Constructor that initializes the minimizer.
******************************************************************************/
FIREMinimizer::FIREMinimizer(const shared_ptr<Calculator>& calculator, FloatType convergenceNorm, 
		FloatType initialStepSize, FloatType maxStepSize) : Minimizer(calculator),
	_convergenceNorm(convergenceNorm), _numForceEvaluations(0), 
	_x(NDOF * calculator->mesh().repatoms().size()),
	_gradient(NDOF * calculator->mesh().repatoms().size()),
	_v(ublas::zero_vector<FloatType>(NDOF * calculator->mesh().repatoms().size())),
	_stepSize(initialStepSize), _maxStepSize(maxStepSize), _alpha(0.2), _alpha0(0.2), _nsteps(0)
{
	MAFEM_ASSERT(_convergenceNorm >= 0);
	MAFEM_ASSERT(_stepSize > 0);
		
	MsgLogger() << logdate << "Initializing FIRE minimizer (convergenceNorm:" << 
		convergenceNorm << ", initialStepSize:" << initialStepSize << ", maxStepSize:" << _maxStepSize << ")" << endl; 
	
	// Copy current repatom positions to internal position vector.
	ublas::vector<FloatType>::iterator ptr = _x.begin();
	Q_FOREACH(const RepAtom* repatom, calculator->mesh().repatoms()) {
		MAFEM_ASSERT(repatom->index() * NDOF == ptr - _x.begin());
		*ptr++ = repatom->deformedPos().X;
		*ptr++ = repatom->deformedPos().Y;
		*ptr++ = repatom->deformedPos().Z;
	}
	
	computeGradient();
}

/******************************************************************************
* Destructor.
******************************************************************************/
FIREMinimizer::~FIREMinimizer()
{
}

/******************************************************************************
* Performs one iteration step.
******************************************************************************/
void FIREMinimizer::iterate()
{
	int Nmin = 5;
	int Nmax = 100;
	FloatType _finc = 1.1;
	FloatType _fdec = 0.5;
	FloatType _falpha = 0.99;
	
	// Velocity Verlet step
	_v -= _gradient * (0.5 * _stepSize);
	_x += _v * _stepSize;
	computeGradient();
	
	_v -= _gradient * (0.5 * _stepSize);
	
	// FIRE step 1
	FloatType P = -inner_prod(_gradient, _v);
	
	// FIRE step 2
	FloatType normv = norm_2(_v);
	_v *= 1.0 - _alpha;
	_v -= _gradient * (_alpha * normv / _gradientNorm2);

	_statusString = QString("StepSize=%1 Alpha=%2").arg(_stepSize, 0, 'e', 3).arg(_alpha, 0, 'g', 3);
	
	// FIRE step 3
	_nsteps++;
	if(P > 0.0 && _nsteps >= Nmin)
	{
		_stepSize = min(_stepSize * _finc, _maxStepSize);
		_alpha *= _falpha;

/*		if(_nsteps > Nmax)
		{
			_v = ublas::zero_vector<FloatType>(_v.size());
			_alpha = _alpha0;
			_nsteps = 0;
			_statusString += " nreset";
		}
*/
		
			
	}
	else if(P <= 0.0) {
		_stepSize *= _fdec;
		_v = ublas::zero_vector<FloatType>(_v.size());
		_alpha = _alpha0;
		_nsteps = 0;
		_statusString += " vreset";
	}
}

/******************************************************************************
* This calculates the gradient for the current configuration.
******************************************************************************/
void FIREMinimizer::computeGradient()
{
	// The function dimensionality is the number of repatoms times the number of degrees of freedom.
	size_t n = NDOF * calculator()->mesh().repatoms().size();
	MAFEM_ASSERT_MSG(_x.size() == n, "FIREMinimizer", "The number of repatoms has changed during minimization.");
	
	// Write internal configuration vector back to repatom positions.
	ublas::vector<FloatType>::const_iterator ptr = _x.begin();
	Q_FOREACH(RepAtom* repatom, calculator()->mesh().repatoms()) {
		MAFEM_ASSERT(repatom->index() * NDOF == ptr - _x.begin());
		repatom->setDeformedPosComponent(0, *ptr++);
		repatom->setDeformedPosComponent(1, *ptr++);
		repatom->setDeformedPosComponent(2, *ptr++);
	}
	
	// Let the sampling atoms follow the repatoms.
	calculator()->mesh().deformationUpdate();
	
	// Calculate forces.
	calculator()->calculateForces(_output);
	_numForceEvaluations++;
	
	// Copy nodal forces to internal gradient vector.
	MAFEM_ASSERT(_output.forces().size() == n/NDOF);
	ublas::vector<FloatType>::iterator gptr = _gradient.begin();
	_gradientNorm2 = 0;
	Q_FOREACH(const Vector3& f, _output.forces()) {
		// Don't forget not negate the forces to get the gradient.
		*gptr++ = -f.X;
		*gptr++ = -f.Y;
		*gptr++ = -f.Z;
		_gradientNorm2 += LengthSquared(f);
	}
	_gradientNorm2 = sqrt(_gradientNorm2);
}

/******************************************************************************
* Tests whether the desired accuracy has been reached.
******************************************************************************/
bool FIREMinimizer::isConverged()
{
	return _gradientNorm2 <= _convergenceNorm;
}

/******************************************************************************
* Returns the value of the energy function and the current point.
******************************************************************************/
FloatType FIREMinimizer::minimumValue()
{
	return _output.totalEnergy();
}

/******************************************************************************
* Returns the current norm of the force vector.
******************************************************************************/
FloatType FIREMinimizer::forceNorm()
{
	return _gradientNorm2;
}

/******************************************************************************
* Performs all iteration steps to reach convergence norm.
******************************************************************************/
int FIREMinimizer::minimize()
{
	MsgLogger() << logdate << "Starting minimization." << endl;
	int iterations = 0;
	while (isConverged() == false)
	{
		iterate();
		++iterations;
		
		cout << "NI = " << iterations << " E = " << _output.totalEnergy() << " F = " << _gradientNorm2 << " NE = " << 0 << " NF = " << _numForceEvaluations << endl; // status string
	}
	if (isConverged() == true)
		MsgLogger() << logdate << "Minimizer converged after " << iterations << " steps." << endl;
	else
		MsgLogger() << logdate << "Minimizer didn't converge." << endl;
	return iterations;
}

}; // End of namespace MAFEM

