/*
 * iomatrix.h
 *
 *  Created on: Sep 4, 2015
 *  Edited  on: Sep 9, 2015
 *      Author: Fabian Guecker
 *
 *  functions for reading a matrix from a Text or CSV File seperated by whitespaces or tabs
 */

#ifndef MATRIX_IO_H_
#define MATRIX_IO_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

#include "matrix_base.h"
#include "matrix_sparse.h"

using namespace std;

// Functions for the std-matrices

bool MapMatrixFromStream(matrix &rMatrix, unsigned int m, unsigned int n, istream &InputStream)
{
	// initialize the matrix m x n
	rMatrix.allocate(m,n);

	// Jump to begin of stream
	InputStream.seekg (0, InputStream.beg);

	// map the values from stream into the matrix
	for(unsigned int i=0; i<m; i++)
	{
		for(unsigned int j=0; j<n; j++)
		{
			// Read a value from the stream
			InputStream >> rMatrix.element[i][j];

			// if an error occurred while converting failbit was set. Check for this, throw a message and quit
			if((InputStream.rdstate() & ifstream::failbit))
			{
					cerr << "matrix_io : while reading %u. element in row %u from stream \n";
					return false;
			}
		}
	}
	return true;
}

bool LoadMatrixFromFile(matrix &rMatrix, char* FileName)
{
	// Variable initialisation
	char LineBuffer[2048*2048] = {0};
	double ValueBuffer = 0;
	unsigned int m = 0;
	unsigned int n = 0;
	ifstream InputFile;
	stringstream StringBuffer;

	cout << FileName << " \n";
	InputFile.open(FileName, ios::in);

	// try to open file
	if (InputFile.is_open())
	{
		InputFile.seekg (0, InputFile.beg);
		// Get the first row of the file and convert it into a stringbuffer
		InputFile.getline(LineBuffer, 2048*2048);
		StringBuffer << LineBuffer;

		// Determine how many values are in the first row, set n the numbers of columns
		while (StringBuffer >> ValueBuffer)	n++;

		// Jump to begin of file and then loop through it and determine how many rows it has, set m the number of rows
		InputFile.seekg (0, InputFile.beg);
		while(InputFile.getline(LineBuffer, 2048*2048)) m++;

		// 'getline' throws an error after reached the EndOfFile, clear the error flag to allow further operations on the file
		InputFile.clear();
		
		cout << " >> Matrix dimension: " << m << " x " << n << "\n";

		// Finally map the Values from the FileStream into the matrix
		if(!MapMatrixFromStream(rMatrix, m, n, InputFile))
		{
			InputFile.close();
			return false;
		}
	}

	else
	{
		cerr << "matrix_io: File could not be opened. \n" ;
		return false;
	}
	return true;
}

//----------------------------------------------------------------------------------------------------------

// Functions for the index-matrices

bool MapMatrixFromStream(index_matrix &rMatrix, unsigned int m, unsigned int n, istream &InputStream)
{
	// initialize the matrix m x n
	rMatrix.allocate(m, n);

	// Jump to begin of stream
	InputStream.seekg (0, InputStream.beg);

	// map the values from stream into the matrix
	for(unsigned int i=0; i<m; i++)
	{
		for(unsigned int j=0; j<n; j++)
		{
			// Read a value from the stream
			InputStream >> rMatrix.element[i][j];

			// if an error occurred while converting failbit was set. Check for this, throw a message and quit
			if((InputStream.rdstate() & ifstream::failbit))
			{
					cerr << "matrix_io : while reading %u. element in row %u from stream \n";
					return false;
			}
		}
	}
	return true;
}

bool LoadMatrixFromFile(index_matrix &rMatrix, char* FileName)
{
	// Variable initialization
	char LineBuffer[2048*2048] = {0};
	double ValueBuffer = 0;
	unsigned int m = 0;
	unsigned int n = 0;
	ifstream InputFile;
	stringstream StringBuffer;

	cout << FileName << endl;
	InputFile.open(FileName, ios::in);

	// try to open file
	if (InputFile.is_open())
	{
		InputFile.seekg(0, InputFile.beg);
		// Get the first row of the file and convert it into a stringbuffer
		InputFile.getline(LineBuffer, 2048*2048);
		StringBuffer << LineBuffer;

		// Determine how many values are in the first row, set n the numbers of columns
		while (StringBuffer >> ValueBuffer)	n++;

		// Jump to begin of file and then loop through it and determine how many rows it has, set m the number of rows
		InputFile.seekg (0, InputFile.beg);
		while(InputFile.getline(LineBuffer, 2048*2048)) m++;

		// 'getline' throws an error after reached the EndOfFile, clear the error flag to allow further operations on the file
		InputFile.clear();

		cout << " >> Matrix dimension: " << m << " x " << n << "\n";

		// Finally map the Values from the FileStream into the matrix
		if(!MapMatrixFromStream(rMatrix, m, n, InputFile))
		{
			InputFile.close();
			return false;
		}
	}

	else
	{
		cerr << "matrix_io: File could not be opened. \n" ;
		return false;
	}
	return true;
}

//----------------------------------------------------------------------------------------------------------



bool matrix_write_to_file(matrix &rMatrix, char* file_name)
{
	// Variable initialisation
	ofstream file;

	file.open(file_name, ios::out);

	// try to open file
	if (file.is_open())
	{
		file << std::setprecision(8);
		file << std::scientific;

		for(unsigned int i = 0 ; i < rMatrix.M ; i++)
		{
			for(unsigned int  j = 0 ; j < rMatrix.N; j++)
			{
				file << rMatrix.element[i][j] <<" ";
			}
			file << "\n";
		}

		file.close();


		cout << " >> " << file_name << " [ " << rMatrix.M << " x " << rMatrix.N << " ]" << endl;
	}
	else
	{
		cerr << "[matrix_io] File could not be opened. \n" ;
		return false;
	}
	return true;
}

bool matrix_write_to_file(index_matrix &rMatrix, char* file_name)
{
	// Variable initialisation
	ofstream file;

	file.open(file_name, ios::out);

	// try to open file
	if (file.is_open())
	{
		file << std::setprecision(8);
		file << std::scientific;

		for(unsigned int i = 0 ; i < rMatrix.M ; i++)
		{
			for(unsigned int  j = 0 ; j < rMatrix.N; j++)
			{
				file << rMatrix.element[i][j] <<" ";
			}
			file << "\n";
		}
		
		file.close();

		cout << " >> " << file_name << " [ " << rMatrix.M << " x " << rMatrix.N << " ]" << endl;
	}

	else
	{
		cerr << "[matrix_io] File could not be opened. \n" ;
		return false;
	}
	return true;
}
#endif
