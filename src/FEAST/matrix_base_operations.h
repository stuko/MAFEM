/*
 * matrix_base_operations.h
 *
 *  Created on: Oct 12, 2015
 *      Author: fabian
 */

#ifndef MATRIX_BASE_OPERATIONS_H_
#define MATRIX_BASE_OPERATIONS_H_

#include <iostream>
using namespace std;


void matrix_mul_N_N(matrix &dest,matrix &A, matrix &B)
{
	unsigned int l,i,j,k;

	if(A.N == B.M)
	{
		l = A.N;
	}
	else
	{
		cerr << "Dimension Error! Matrix multiply N x N \n";
		return;
	}

	for (i = 0; i < A.M; ++i)
	{
		for (j = 0; j < B.N; ++j)
			{
				double Value = 0;
				for (k = 0; k < l; ++k)
				{
					Value += A.element[i][k] * B.element[k][j];
				}
				dest.element[i][j] = Value;
			}

	 }
}



#endif /* MATRIX_BASE_OPERATIONS_H_ */
