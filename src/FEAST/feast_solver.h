//============================================================================
// Name        : EigenValueSolver.cpp
// Author      : Fabian Gücker, Benedikt Zier
// Version     :
// Copyright   : 
// Description : Eigenvalue calculation with Intel MKL and FEAST
//============================================================================

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include <string.h> // memset

//============================================================================

#ifndef _MKL_H_
#define _MKL_H_

#define _Mkl_Api(rtype,name,arg) extern rtype name    arg;
#define _mkl_api(rtype,name,arg) extern rtype name##_ arg;
#define _MKL_API(rtype,name,arg) extern rtype name##_ arg;

#include "mkl_version.h"
#include "mkl_types.h"
#include "mkl_blas.h"
#include "mkl_trans.h"
//#include "mkl_cblas.h"
#include "mkl_spblas.h"
#include "mkl_lapack.h"
#include "mkl_lapacke.h"
#include "mkl_pardiso.h"
#include "mkl_sparse_handle.h"
#include "mkl_dss.h"
#include "mkl_rci.h"
#include "mkl_vml.h"
#include "mkl_vsl.h"
#include "mkl_df.h"
#include "mkl_service.h"
#include "mkl_dfti.h"
#include "mkl_trig_transforms.h"
#include "mkl_poisson.h"
#include "mkl_solvers_ee.h"
#include "mkl_direct_call.h"

#endif /* _MKL_H_ */

//============================================================================

#include "FEAST/stopwatch.h"
#include "FEAST/matrix_io.h"

using namespace std;

int feast_solve_system(sparse_CRS_matrix &CRS_StiffnessMatrix, 	matrix &EigenValues, matrix &EigenVectors, 	const double emin = 0, const double emax = 1, char uplo = 'U', bool silent = false)
{
	/// 'L' for LowerTri Matrix, 'U' for UpperTri Matrix, 'F' for a Full Matrix
	
	StopWatch LoadTime;
	StopWatch RunTime;

	LoadTime.Start();
	//===================================================================================
	// Prepare Matrix for CRS format
	

	CRS_StiffnessMatrix.sort_quick(); // sort each matrix row
	
	
	
	bool buildHessenberg = false;
	if (uplo == 'A')
	{
		CRS_StiffnessMatrix.MakeAveragedUpperTriangularMatrix('A');
		uplo = 'U';
	}
	else if (uplo == 'G')
	{
		CRS_StiffnessMatrix.MakeAveragedUpperTriangularMatrix('G');
		uplo = 'U';
		cout << CRS_StiffnessMatrix << endl;
	}
	else if (uplo == 'U')
		CRS_StiffnessMatrix.MakeUpperTriangularMatrix();
	else if (uplo == 'L')
		CRS_StiffnessMatrix.MakeLowerTriangularMatrix();
	else if (uplo == 'F')
		buildHessenberg = true;
	
	CRS_StiffnessMatrix.build_CRS(false); // build CRS format
	
	if (buildHessenberg)
	{
		sparse_CRS_matrix hessenbergMatrix;
		hessenbergMatrix.allocate_elements(3, 1);
		double val[9] = {2,0,2,0,2,0,5,0,2};
		int k = 0;
		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 3; ++j)
				hessenbergMatrix.add_matrix(&(val[k++]), 1, i, j);
				
		cout << hessenbergMatrix << endl;
			
		
		int n = CRS_StiffnessMatrix.N;
		int ilo = 1;
		int ihi = n;
		int dlen_ = 9;
//		int desca[dlen_] = {1, BLACS_GRIDINIT, n, n, 1, 1, 0, 0, n};
		int lwork = -1;
		double work;
		double tau;
		int info;
		
		
		
//		pdgehrd(&MatSize, &ilo, &ihi, CRS_StiffnessMatrix.A, CRS_StiffnessMatrix.IA, CRS_StiffnessMatrix.JA, MKL_INT *desca , double *tau , double *work , MKL_INT *lwork , MKL_INT *info );
	}
		

	const unsigned int MatSize = CRS_StiffnessMatrix.N;
	
	if(!silent)
	{
		cout << endl << "=== Eigenvalue Calculation =============================================================" << endl << endl;
		cout << "Problem Size: " << MatSize << endl;
	}
	
	LoadTime.Stop();
    
	//===================================================================================
	// Init
	RunTime.Start();
	
	int fpm[128];
	memset(fpm, '0', sizeof(fpm)); // Set all entries of fpm-array to zero default
	
	if(!silent) fpm[0] = 1; // Print runtime informations;
		else fpm[0] = 0;  
		
	fpm[1] = 8;  // contour points
	fpm[2] = 12; // Error trace double precision stopping criteria
	fpm[3] = 20; // Maximum number of Extended Eigensolver refinement loops allowed
	fpm[4] = 0;  // Use user initial subspace
	fpm[5] = 0;  // Extended Eigensolver stopping test.
	fpm[6] = 5;  // EError trace single precision stopping criteria

	feastinit(fpm);

	//===================================================================================
	// Input

	const int n  = MatSize; // matrix size (number of rows)
	int lda = n; // Use subspace of matrix if lda < n
	int m0 = 10; // Estimated number of eigenvalues in the search interval

	//===================================================================================
	// Output
	double epsout;
	int loop;
	int m = -1; 		// total number of eigenvalues found
	
	EigenValues.allocate_and_clear(m0, 1);			// allocate memory for a list of eigenvalues 
	EigenVectors.allocate_and_clear(m0, MatSize);   // allocate memory for a matrix of eigenvectors, one vector stored in each row, the index corresponds to the eigenvalues
	
	double *res = (double*)malloc(m0*sizeof(double)); // allocate memory for an List of the residuals of each eigenvalue 
	int info;

	//===================================================================================
	// Run the solver

	dfeast_scsrev(&uplo,  &CRS_StiffnessMatrix.N, CRS_StiffnessMatrix.A, CRS_StiffnessMatrix.IA, CRS_StiffnessMatrix.JA, fpm, &epsout, &loop, &emin, &emax, &m0, EigenValues.element[0], EigenVectors.element[0], &m, res, &info);

	RunTime.Stop();
	//===================================================================================
	// Display results
	if(!silent)
	{
		printf("FEAST OUTPUT INFO %d\n",info);
		if (info==0)
		{
			printf("//==== REPORT \n");

			printf("# Search interval [Emin,Emax] %.3e %.3e\n",emin,emax);
			printf("# mode found/subspace %d %d \n",m,m0);
			printf("# iterations %d \n",loop);

			printf("Relative error on the Trace %.3e\n",epsout );
			
			printf("Eigenvalues/Residuals\n");
			cout << EigenValues;
			
			printf("Eigenvectors\n");
			cout << EigenVectors;
		}
		  
		cout  << endl <<  "//================================================================" << endl << endl;
	}

	cout << "[Feast] Preparring Time: " << LoadTime.GetDuration() << " s [Efficency: " << LoadTime.GetEffiency(12) << " % ]" << endl;
	cout << "[Feast] Execution  Time: " << RunTime.GetDuration()  << " s [Efficency: " << RunTime.GetEffiency(12) << " % ]" << endl;
	cout << "[Feast] Found " << m << " eigenvalues in the intervall [" << emin << ", " << emax << "]" << endl;
	
	free(res);
	return m;
}

int ScaLAPACK_solve_system(sparse_CRS_matrix &CRS_StiffnessMatrix, 	matrix &EigenValues, matrix &EigenVectors, 	const double emin = 0, const double emax = 1, char uplo = 'U', bool silent = false)
{
/*
	StopWatch LoadTime;
	StopWatch RunTime;

	LoadTime.Start();
	//===================================================================================
	// Prepare Matrix for CRS format
	

	CRS_StiffnessMatrix.sort_quick(); // sort each matrix row
	
	else if (uplo == 'U')
		CRS_StiffnessMatrix.MakeUpperTriangularMatrix();
	else if (uplo == 'L')
		CRS_StiffnessMatrix.MakeLowerTriangularMatrix();
	
	CRS_StiffnessMatrix.build_CRS(false); // build CRS format
	
	pdsytrd(uplo, CRS_StiffnessMatrix.N, CRS_StiffnessMatrix.A, CRS_StiffnessMatrix.IA, CRS_StiffnessMatrix.JA, desca, d, e, tau, work, lwork, info)
*/
	return 0;
}
