/*
 * matrix_base.h
 *
 *  Created on: Sep 23, 2015
 *      Author: Fabian Guecker
 *
 */

#ifndef MATRIX_BASE_H_
#define MATRIX_BASE_H_

#include <iostream>
#include <stdlib.h>     // malloc, free
using namespace std;


// Type definitions
enum matrix_dimension  {ROWS=0, COLUMNS=1, ELEMENTS=2};
enum allocation_method {COHERENT=true, INDEPENDENT_ROWS=false};

class row_entry
{
	public:
	unsigned int column;
	double value;

	row_entry()
	{
		column = 0;
		value = 0;
	}

	row_entry(int c, double v)
	{
		column = c;
		value = v;
	}

	friend std::ostream& operator<< (std::ostream& stream, row_entry& rhs)
	{
		stream << rhs.column << ' ' << rhs.value << endl;
		return stream;
	}

	bool operator <(const row_entry &rhs) const
	{
		return this->column < rhs.column;
	}

	bool operator >(const row_entry &rhs) const
	{
		return this->column > rhs.column;
	}

	~row_entry()
	{

	}

};

inline void copy_matrix_line (double* dest, double* src, unsigned int len)
{
	for (unsigned int i = 0; i < len; ++i)
		dest[i] = src[i];

}

class matrix
{
public:
	double **element;
	unsigned int M;
	unsigned int N;

private:

public:

	matrix ()
	{
		element = 0;
		M = 0;
		N = 0;
		return;
	}

	matrix (unsigned int m,unsigned int n)
	{
		allocate(m,n);
	}

	~matrix()
	{
		deallocate();
	}


	void allocate(unsigned int m,unsigned int n)
	{
		if(M > 0) deallocate();
		M = m;
		N = n;

		// Allocate a coherent block of memory on the heap
		double *pMemory = (double*)malloc(M*N*sizeof(double));

		// Create the pointer array that will store the addresses of the first element of each row
		element = new double*[M];

		// Check of both allocations were successful. if not throw an error and exit
		if((pMemory == 0) || (element == 0))
		{
			double req_mem_size = (double)(M*N*sizeof(double)) / 1024.0 / 1024.0;
 			cerr << "[matrix-base]: Cannot allocate enough Memory. Requested size [MByte]: " << req_mem_size << endl;
			exit(EXIT_FAILURE);
		}

		// Loop through the pointer array and assign the addresses to the empty pointer
		for(unsigned int i = 0; i < M; ++i)
			element[i] = &pMemory[N*i];

	}

	void allocate_and_clear(unsigned int m,unsigned int n)
	{
		allocate(m, n);
		clear();
	}

	void deallocate(void)
	{
		if(element!=0)
		{
			// the block of memory is coherent, so it is sufficient to free the first element, the operating system knows the size
			free( element[0] );

			// Free the pointer array
			free( element );
		}
		element = 0;
		M = 0;
		N = 0;
	}

	void clear(void)
	{
		for(unsigned int i = 0; i < M; ++i)
			for(unsigned int j = 0; j < N; ++j)
				element[i][j] = 0;
	}

	unsigned int size(matrix_dimension dim)
	{
		if(dim == ROWS)
			return M;
		else if(dim == COLUMNS)
			return N;
		else return M*N;
	}

	friend std::ostream& operator<< (std::ostream& stream, matrix& rmatrix)
	{
		stream << "\n";
		for(unsigned int i = 0 ; i < rmatrix.M ; i++)
		{
			for(unsigned int  j = 0 ; j < rmatrix.N; j++)
			{
				stream << rmatrix.element[i][j] <<" ; ";
			}
			stream << "\n";
		}
	    return stream;
	}

	void add_stima_indices(double *M, unsigned int *I)
	{
		for(unsigned int i = 0; i<8; ++i)
			for(unsigned int j = 0; j<8; ++j)
				this->element[ I[i] -1 ][ I[j] - 1 ] += M[i*8+j];

	}

	void clear_row(unsigned int m_row)
	{
		for(unsigned int i=0; i < N; ++i)
			element[m_row][i] = 0;
	}

	void clear_column(unsigned int m_column)
	{
		for(unsigned int i=0; i < M; ++i)
			element[i][m_column] = 0;
	}

	void dirichlet_boundary(unsigned int node_no)
	{
		clear_row(node_no);
		clear_column(node_no);
		element[node_no][node_no] = 1;
	}

	void insert(matrix &mat, unsigned int pos_x, unsigned int pos_y)
	{
		unsigned int m_rows = mat.size(ROWS);
		unsigned int m_columns = mat.size(COLUMNS);

		for (unsigned int i = 0; i < m_rows; ++i)
		{
			copy_matrix_line(&element[pos_x + i][pos_y], &mat.element[i][0], m_columns);
		}
	}

};

class index_matrix
{
public:
	unsigned int **element;
	unsigned int M;
	unsigned int N ;

private:


public:

	index_matrix ()
	{
		element = 0;
		M = 0;
		N = 0;
		return;
	}

	index_matrix (unsigned int m,unsigned int n)
	{
		allocate(m,n);
	}

	~index_matrix()
	{
		deallocate();
	}


	void allocate(unsigned int m,unsigned int n)
	{
		if(M > 0) deallocate();
		M = m;
		N = n;

		// Allocate a coherent block of memory on the heap
		unsigned int *pMemory = (unsigned int*)malloc(M*N*sizeof(unsigned int));

		// Create the pointer array that will store the addresses of the first element of each row
		element = new unsigned int*[M];

		// Check of both allocations were successful. if not throw an error and exit
		if((pMemory == 0) || (element == 0))
		{
			double req_mem_size = (double)(M*N*sizeof(double)) / 1024.0 / 1024.0;
 			cerr << "[matrix-base]: Cannot allocate enough Memory. Requested size [MByte]: " << req_mem_size << endl;
			exit(EXIT_FAILURE);
		}

		// Loop through the pointer array and assign the addresses to the empty pointer
		for(unsigned int i = 0; i < M; ++i)
			element[i] = &pMemory[N*i];


	}
	
	void clear(void)
	{
		for(unsigned int i = 0; i < M; ++i)
			for(unsigned int j = 0; j < N; ++j)
				element[i][j] = 0;
	}
	
	void allocate_and_clear(unsigned int m,unsigned int n)
	{
		allocate(m, n);
		clear();
	}
	
	void deallocate(void)
	{
		if(element!=0)
		{
			// the block of memory is coherent, so it is sufficient to free the first element, the operating system knows the size
			free( element[0] );

			// Free the pointer array
			free( element );
		}
		element = 0;
		M = 0;
		N = 0;
	}


	unsigned int size(matrix_dimension dim)
	{
		if(dim == ROWS)
			return M;
		else if(dim == COLUMNS)
			return N;
		else return M*N;
	}

	friend std::ostream& operator<< (std::ostream& stream, index_matrix& rmatrix)
	{
		stream << "\n";
		for(unsigned int i = 0 ; i < rmatrix.M ; i++)
		{
			for(unsigned int  j = 0 ; j < rmatrix.N; j++)
			{
				stream << rmatrix.element[i][j] <<"\t\t";
			}
			stream << "\n";
		}
	    return stream;
	}

	void add_stima_indices(double *M, unsigned int *I)
	{
		for(unsigned int i = 0; i<8; ++i)
			for(unsigned int j = 0; j<8; ++j)
				this->element[ I[i] -1 ][ I[j] - 1 ] += M[i*8+j];

	}


};


#endif /* MATRIX_BASE_H_ */
