/*
 * stopwatch.h
 *
 *  Created on: Sep 4, 2015
 *      Author: Fabian Guecker
 */

#ifndef STOP_WATCH_H_
#define STOP_WATCH_H_

#include <cmath>
#include <ostream>
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <iomanip>



double get_cpu_time()
{
    return (double)clock() / CLOCKS_PER_SEC;
}

double get_wall_time()
{
    struct timeval time;
    gettimeofday(&time,NULL);
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}

class StopWatch {

public: 
	double duration_wall;
	double duration_cpu;

	double start_wall;
	double stop_wall;

	double start_cpu;
	double stop_cpu;

public:
	StopWatch()
	{
	   Reset();
	}

	~StopWatch() {}

	double GetActDuration()
	{
		return (double)(get_wall_time() - start_wall) ;
	}

	double GetDuration()
	{
		return duration_wall;
	}

	double GetPercentage(double TotalDurationTime)
	{
		return duration_wall / TotalDurationTime * 100.0;
	}

	double GetEffiency(unsigned int NumOfCores=1)
	{
		double rtv = ((duration_cpu / (double)NumOfCores)) / duration_wall * 100.0;
		if(rtv > 100) return 100;
		else return rtv;
	}


	void Start()
	{
		start_wall  = get_wall_time();
		start_cpu  = get_cpu_time();
	}

	void Stop()
	{
		stop_cpu = get_cpu_time();
		duration_cpu += (double)(stop_cpu - start_cpu) ;

		stop_wall = get_wall_time();
		duration_wall += (double)(stop_wall - start_wall) ;
	}

	void Reset()
	{
		start_wall = 0;
		stop_wall = 0;
		duration_wall = 0;

		start_cpu = 0;
		stop_cpu = 0;
		duration_cpu = 0;
	}

	void Restart()
	{
		Reset();
		Start();
	}
	void PrintWatchInfos(char* Message, unsigned int NumOfCores, double TotalTime, std::ostream &stream=std::cout)
	{
		stream << std::setprecision(3);
		stream << std::fixed;

		unsigned int TimeInSeconds = std::ceil(GetDuration());
		unsigned int Days    = TimeInSeconds / 60 / 60 / 24;
		unsigned int Hours   = (TimeInSeconds / 60 / 60) % 24;
		unsigned int Minutes = (TimeInSeconds / 60) % 60;
		unsigned int Seconds = TimeInSeconds % 60;

		stream << std::setprecision(0);
}
};



#endif 
