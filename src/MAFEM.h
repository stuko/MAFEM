///////////////////////////////////////////////////////////////////////////////
//
// Multiscale Atomistic Finite Element Method (MAFEM) Package.
//
// Copyright Â© 2009, Alexander Stukowski <alex@stukowski.de>
// All rights reserved.
//
// Initial developer: Alexander Stukowski <alex@stukowski.de>
// Modifications contributed by Nirav Prajapati <nirav.prajapati@rub.de>, Bernhard Eidel <bernhard.eidel@googlemail.com>
//
// No redistribution and use in source and binary forms, with or without
// modification, without prior permission by all contributing authors.
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MAFEM_HEADER_H
#define __MAFEM_HEADER_H

/******************************************************************************
* The MAFEM Framework.
******************************************************************************/
#include <Base.h>
#include <linalg/LinAlg.h>
#include <linalg/FloatArray.h>

/******************************************************************************
* OpenMP support.
******************************************************************************/
#include <omp.h>

/******************************************************************************
* Configuration
******************************************************************************/
#define NDOF		3	///< The number of degrees of freedom per node.
#define NEN			4	///< The number of nodes per element.

#endif // __MAFEM_HEADER_H
